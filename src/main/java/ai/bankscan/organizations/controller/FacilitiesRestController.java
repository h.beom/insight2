package ai.bankscan.organizations.controller;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.annotations.ApiPageable;
import ai.bankscan.common.infra.security.common.CurrentUser;
import ai.bankscan.common.infra.web.excel.ExcelXlsxView;
import ai.bankscan.organizations.dto.FacilitiesDto;
import ai.bankscan.organizations.dto.FacilitiesDto.Response;
import ai.bankscan.organizations.service.FacilitiesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Api(tags = {"시설 관리 API"})
@RequiredArgsConstructor
@RestController
@RequestMapping(path = FacilitiesRestController.REQUEST_BASE_PATH, produces = MediaTypes.HAL_JSON_VALUE)
public class FacilitiesRestController {

  static final String REQUEST_BASE_PATH = "/api/facilities";

  private final FacilitiesService facilitiesService;
  private final FacilitiesResourceAssembler facilitiesResourceAssembler;

  @Secured({"ROLE_ADMIN", "ROLE_CENTER_MANAGER"})
  @ApiOperation(value = "시설 관리 등록")
  @PostMapping
  public ResponseEntity<Resource<Response>> createFacilities(
      @RequestBody FacilitiesDto.Create dto
  ) throws URISyntaxException {
    Response savedFacilities = facilitiesService.createFacilities(dto);
    Resource<Response> resource = facilitiesResourceAssembler.toResource(savedFacilities);

    return ResponseEntity
        .created(new URI(resource.getLink(Link.REL_SELF).getHref()))
        .body(resource);
  }

  @Secured({"ROLE_ADMIN", "ROLE_CENTER_MANAGER"})
  @ApiPageable
  @ApiOperation(value = "시설 관리 목록 조회")
  @GetMapping
  public Resources<Resource<Response>> getFacilities(
      FacilitiesDto.Search search,
      @PageableDefault(sort = "month", direction = Direction.DESC) Pageable pageable,
      PagedResourcesAssembler<Response> pagedResourcesAssembler,
      @CurrentUser Account account
  ) {
    Page<Response> page = facilitiesService.getFacilities(search, account, pageable);
    return pagedResourcesAssembler.toResource(page, facilitiesResourceAssembler);
  }

  @Secured({"ROLE_ADMIN", "ROLE_CENTER_MANAGER"})
  @ApiOperation(value = "시설 관리 조회")
  @GetMapping("/{id}")
  public ResponseEntity<Resource<Response>> getFacilities(
      @ApiParam(required = true, example = "1") @PathVariable final Long id
  ) {
    Resource<Response> resource = facilitiesResourceAssembler
        .toResource(facilitiesService.getFacilities(id));
    return ResponseEntity.ok(resource);
  }

  @Secured({"ROLE_ADMIN", "ROLE_CENTER_MANAGER"})
  @ApiOperation(value = "시설 관리 수정")
  @PutMapping("/{id}")
  ResponseEntity<Resource<Response>> updateFacilities(
      @ApiParam(required = true, example = "1") @PathVariable final Long id,
      @RequestBody @Valid FacilitiesDto.Update dto,
      @AuthenticationPrincipal final String username
  ) throws URISyntaxException {
    Response savedTeacher = facilitiesService
        .updateFacilities(id, dto, username);
    Resource<Response> resource = facilitiesResourceAssembler.toResource(savedTeacher);

    return ResponseEntity
        .created(new URI(resource.getLink(Link.REL_SELF).getHref()))
        .body(resource);
  }

  @Secured({"ROLE_ADMIN", "ROLE_CENTER_MANAGER"})
  @ApiOperation(value = "시설 관리 삭제")
  @DeleteMapping("/{id}")
  ResponseEntity<?> deleteFacilities(
      @ApiParam(required = true, example = "1") @PathVariable final Long id,
      @AuthenticationPrincipal final String username
  ) {
    facilitiesService.deleteFacilities(id, username);
    return ResponseEntity
        .noContent()
        .build();
  }

  @Secured("ROLE_ADMIN")
  @ApiOperation(value = "시설 관리 엑셀다운로드")
  @GetMapping("/excel")
  public ModelAndView getFacilitiesToExcel(FacilitiesDto.Search search)
      throws UnsupportedEncodingException {
    Map<String, Object> facilitiesToExcelData = facilitiesService.getFacilitiesToExcel(search);
    return new ModelAndView(new ExcelXlsxView(), facilitiesToExcelData);
  }

}
