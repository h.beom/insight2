package ai.bankscan.consultations.domain;

import ai.bankscan.consultations.domain.consulting.Consultation;
import ai.bankscan.consultations.domain.consulting.Consultation.Type;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ConsultContentRepository extends JpaRepository<ConsultContent, Long>,
    JpaSpecificationExecutor<ConsultContent> {

  List<ConsultContent> findAllByConsultation(Consultation consultation);
  List<ConsultContent> findAllByConsultationAndConsultationType(Consultation consultation, Type type);

}
