package ai.bankscan.finance.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class 국세청_소득금액증명_Dto {


  @Data
  public static class Create {

    private String type;

    private String 귀속연도;
    private String 법인명;
    private String 사업자등록번호;
    private String 소득구분;
    private Long 소득금액;
    private Long 총결정세액;
  }

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class GroupBy {

    private String type;
    private String year;
  }

}
