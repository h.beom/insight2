package ai.bankscan.survey.domain;

import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.survey.domain.금융건강진단표.Status;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface 금융건강진단표Repository extends JpaRepository<금융건강진단표, Long>,
    JpaSpecificationExecutor<금융건강진단표> {

  List<금융건강진단표> findAllByCreatedBy(Member member);
  List<금융건강진단표> findAllByCreatedByAndStatus(Member member, 금융건강진단표.Status status);
  List<금융건강진단표> findAllByCreatedByIdAndStatus(Long createdById, 금융건강진단표.Status status);



  Page<금융건강진단표> findAllByCreatedBy(Member member, Pageable request);

  Optional<금융건강진단표> findFirstByCreatedByIdAndStatusOrderByCreatedAtDesc(Long id, Status active);
}
