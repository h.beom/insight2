package ai.bankscan.organizations.service;

import ai.bankscan.accounts.domain.AccountRepository;
import ai.bankscan.organizations.domain.Center;
import ai.bankscan.organizations.domain.CenterRepository;
import ai.bankscan.organizations.domain.OrganizationRepository;
import ai.bankscan.organizations.dto.CenterDto;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Transactional(readOnly = true)
@Service
public class CenterService extends OrganizationService<Center, CenterDto.Response> {

  @Autowired
  public CenterService(OrganizationRepository organizationRepository,
      CenterRepository centerRepository,
      AccountRepository accountRepository, ModelMapper modelMapper) {
    super(organizationRepository, centerRepository, accountRepository, modelMapper, Center.class,
        CenterDto.Response.class);
  }

}
