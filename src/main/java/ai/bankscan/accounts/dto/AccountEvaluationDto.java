package ai.bankscan.accounts.dto;


import ai.bankscan.accounts.domain.AccountEvaluation;
import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.common.infra.jpa.support.SearchDto;
import ai.bankscan.consultations.dto.ConsultationDto;
import ai.bankscan.organizations.domain.Organization;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AccountEvaluationDto {

    @Setter
    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    @SuperBuilder
    @ToString
    @ApiModel(value = "AccountDto.Create")
    public static class Create {
        @ApiModelProperty(value = "1", notes = "consultationId")
        private long consultationId;
        @ApiModelProperty(value = "1", notes = "accountId")
        private long accountId;
        @ApiModelProperty(value = "3.5", notes = "evaluation")
        private long evaluation;
        @ApiModelProperty(value = "너무 친절하세요", notes = "comment")
        private String comment;
    }

    @Setter
    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    @SuperBuilder
    @ToString
    @ApiModel(value = "AccountDto.Response")
    public static class Response {
        private ConsultationDto.Response consultation;
        private AccountDto.Simple account;
        private long evaluation;
        private String comment;
    }

    @Setter
    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    @SuperBuilder
    @ToString
    @ApiModel(value = "AccountDto.Search")
    public static class Search extends SearchDto<AccountEvaluation> {
        @ApiModelProperty(value = "검색구분")
        private String searchType;

        @ApiModelProperty(value = "검색어")
        private String keyword;

        @ApiModelProperty(value = "기관 검색")
        private Organization.Type type;

        @Override
        protected Restrictions generateRestrictions() {
            final Restrictions restrictions = new Restrictions();

            if (type != null) {
                restrictions.eq("account.organization.type", type);
            }

            if (StringUtils.isNotEmpty(this.keyword)) {
                if (StringUtils.isEmpty(this.searchType)) {
                    final Restrictions restrictionsByKeyword = new Restrictions(Restrictions.Conn.OR);
                    List<String> keywords = Arrays.asList(this.keyword.trim().split("\\s+"));
                    keywords.stream()
                            .forEach(keyword -> {
                                restrictionsByKeyword.like("consultation.center.name", "%" + keyword + "%");
                                restrictionsByKeyword.like("consultation.center.code", "%" + keyword + "%");
                                restrictionsByKeyword.like("account.username", "%" + keyword + "%");
                                restrictionsByKeyword.like("account.name", "%" + keyword + "%");
                            });
                    restrictions.addChild(restrictionsByKeyword);
                }
                if (this.searchType.equals("consultant.id")) {
                    restrictions.eq(searchType, keyword);
                } else {
                    restrictions.like(searchType, "%" + keyword + "%");
                }
            }
            return restrictions;
        }

        protected void addSearchKeyword(final Restrictions restrictions, final String keyword) {
        }
    }
}
