package ai.bankscan.posts.dto;

import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.posts.domain.Notice;
import ai.bankscan.posts.domain.Post;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.springframework.util.ObjectUtils;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class NoticeDto {

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "NoticeDto.Create")
  public static class Create extends PostDto.Create {

    @NotNull
    @ApiModelProperty(value = "카테고리", example = "GROUP_WORKSHOPS", required = true, position = 101)
    private Notice.Category category;

    @ApiModelProperty(value = "상단공지", example = "false", required = true, position = 102)
    private Boolean topNotice;

    @ApiModelProperty(value = "푸쉬발송", example = "true", required = true, position = 103)
    private Boolean sendPush;

    @ApiModelProperty(value = "출력여부", example = "true", required = true, position = 104)
    private Boolean display;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "PostDto.Update")
  public static class Update extends PostDto.Update {

    @NotNull
    @ApiModelProperty(value = "카테고리", example = "GROUP_WORKSHOPS", required = true, position = 101)
    private Notice.Category category;

    @ApiModelProperty(value = "상단공지", example = "false", required = true, position = 102)
    private Boolean topNotice;

    @ApiModelProperty(value = "푸쉬발송", example = "true", required = true, position = 103)
    private Boolean sendPush;

    @ApiModelProperty(value = "출력여부", example = "true", required = true, position = 104)
    private Boolean display;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "PostDto.Response")
  public static class Response extends PostDto.Response {

    @ApiModelProperty(value = "ID", example = "1")
    private Long id;

    @NotNull
    @ApiModelProperty(value = "카테고리", example = "GROUP_WORKSHOPS", required = true, position = 2)
    private Notice.Category category;

    @ApiModelProperty(value = "상단공지", example = "false", required = true, position = 6)
    private Boolean topNotice;

    @ApiModelProperty(value = "푸쉬발송", example = "true", required = true, position = 7)
    private Boolean sendPush;

    @ApiModelProperty(value = "출력여부", example = "true", required = true, position = 8)
    private Boolean display;

    @ApiModelProperty(value = "생성일시", position = 9)
    private LocalDateTime createdAt;

  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString(callSuper = true)
  @ApiModel(value = "PostDto.Search")
  public static class Search extends PostDto.Search<Post> {

    @ApiModelProperty(value = "카테고리", example = "GROUP_WORKSHOPS", required = true, position = 101)
    private Notice.Category category;

    @Default
    @ApiModelProperty(value = "표시여부", position = 102)
    private Boolean display = null;

    @Override
    protected Restrictions generateRestrictions() {
      final Restrictions restrictions = super.generateRestrictions();
      if (!ObjectUtils.isEmpty(this.category)) {
        restrictions.eq("category", this.category);
      }

      if (this.display != null) {
        restrictions.eq("display", this.display);
      }

      return restrictions;
    }

  }

}
