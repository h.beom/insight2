package ai.bankscan.survey.domain;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class 금융건강진단종합 extends AuditableJoinEntity<Account> {

  @Getter
  @RequiredArgsConstructor
  public enum Type {
    현명한금융거래("현명한금융거래"),
    행복한노후준비("행복한노후준비"),
    철저한신용관리("철저한신용관리"),
    내살집마련("내살집마련"),
    내복지찾기("내복지찾기");

    private final String title;
  }

  @ManyToOne
  private 금융건강진단표 금융건강진단표;

  private String title;

  @Lob
  private String result;

  @Enumerated(EnumType.STRING)
  private Type type;


}
