package ai.bankscan.common.infra.jpa.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;

/**
 * The type List attribute converter.
 *
 * @author eomjeongjae
 * @since 2019 -02-27
 */
@Slf4j
@Converter(autoApply = true)
public class JSONObjectAttributeConverter implements
    AttributeConverter<JSONObject, String> {

  @Override
  public String convertToDatabaseColumn(JSONObject attribute) {
    if (attribute != null) {
      return attribute.toString();
    }
    return null;
  }

  @Override
  public JSONObject convertToEntityAttribute(String dbData) {
    if (dbData == null) return null;
    return JSONObject.fromObject(dbData);
  }
}

