package ai.bankscan.bound.controller;

import ai.bankscan.accounts.domain.admin.Admin;
import ai.bankscan.bound.dto.BoundDto.Response;
import ai.bankscan.bound.dto.BoundDto.Search;
import ai.bankscan.bound.service.BoundService;
import ai.bankscan.common.infra.security.common.CurrentUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = {"통화이력 API"})
@RequiredArgsConstructor
@RestController
@RequestMapping(path = BoundRestController.REQUEST_BASE_PATH, produces = MediaTypes.HAL_JSON_VALUE)
public class BoundRestController {

  static final String REQUEST_BASE_PATH = "/api/bound";

  private final BoundService boundService;

  private final ModelMapper modelMapper;


  @ApiOperation(value = "통화이력관리", notes = "통화이력 전체 조회")
  @Secured({"ROLE_ADMIN"})
  @GetMapping
  public ResponseEntity<Page<Response>> getBoundList(
      @CurrentUser Admin admin,
      Search search,
      @PageableDefault(page = 0, size = 10)
      @SortDefault.SortDefaults({
          @SortDefault(sort = "createdAt", direction = Sort.Direction.DESC),
          @SortDefault(sort = "updatedOn", direction = Sort.Direction.DESC)
      }) Pageable pageable) {
    Page<Response> page;

    if(!admin.isAdminRole()) {
      return ResponseEntity.status(403).body(null);
    }
    page = boundService.getBoundList(search, pageable);

    return ResponseEntity.ok(page);
  }

}
