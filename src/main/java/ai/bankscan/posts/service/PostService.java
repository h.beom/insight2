package ai.bankscan.posts.service;

import ai.bankscan.posts.domain.Post;
import ai.bankscan.posts.domain.PostRepository;
import ai.bankscan.posts.dto.PostDto;
import ai.bankscan.posts.dto.PostDto.Create;
import ai.bankscan.posts.dto.PostDto.Update;
import com.google.api.client.util.Lists;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Transactional(readOnly = true)
public abstract class PostService<T extends Post, R extends PostDto.Response> {

  private final PostRepository postRepository;
  private final ModelMapper modelMapper;
  private final Class<T> entityType;
  private final Class<R> responseType;

  public PostService(PostRepository postRepository, ModelMapper modelMapper,
      Class<T> entityType, Class<R> responseType) {
    this.postRepository = postRepository;
    this.modelMapper = modelMapper;
    this.entityType = entityType;
    this.responseType = responseType;
  }

  @Transactional
  public R createPost(Create dto) {
    return createPost(dto, null);
  }

  @Transactional
  protected R createPost(Create dto, Consumer<T> consumer) {
    T newPost = modelMapper.map(dto, entityType);
    if (consumer != null) {
      consumer.accept(newPost);
    }
    return toResponse(postRepository.save(newPost));
  }

  public Page<R> getPosts(PostDto.Search search, Pageable pageable) {
    Page<Post> page = postRepository.findAll(search.toSpecification(entityType), pageable);
    List<R> content = page.getContent().stream()
        .map(this::toResponse)
        .collect(Collectors.toList());
    return new PageImpl<>(content, pageable, page.getTotalElements());
  }

  public R getPost(final Long id) {
    return toResponse(postRepository.findById(id).orElseThrow(IllegalArgumentException::new));
  }

  @Transactional
  // @PostAuthorize("returnObject.createdBy == authentication.principal.username")
  public R updatePost(
      final Long id,
      Update dto
  ) {
    Post existPost = this.validate(id);
    existPost.setAttachFiles(Lists.newArrayList());
    modelMapper.map(dto, existPost);
    return toResponse(postRepository.save(existPost));
  }

  @Transactional
  public void deletePost(
      final Long id
  ) {
    Post existPost = this.validate(id);
    postRepository.delete(existPost);
  }

  protected Post validate(final Long id) {
    Post existingPost = postRepository.findById(id)
        .orElseThrow(IllegalArgumentException::new);
    return existingPost;
  }

  protected R toResponse(Post post) {
    return modelMapper.map(post, responseType);
  }

}
