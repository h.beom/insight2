package ai.bankscan.finance.document;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import ai.bankscan.finance.dto.국세청_소득금액증명_Dto;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@ToString
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Table(indexes = {@Index(columnList = "귀속연도"), @Index(columnList = "소득구분"),
    @Index(columnList = "type")})
@SqlResultSetMapping(name = "국세청_소득금액증명_GroupBy", classes = @ConstructorResult(
    targetClass = 국세청_소득금액증명_Dto.GroupBy.class,
    columns = {
        @ColumnResult(name = "type", type = String.class),
        @ColumnResult(name = "year", type = String.class),
    }))
public class 국세청_소득금액증명 extends AuditableJoinEntity<Account> {

  private String type;

  private String 귀속연도;
  private String 법인명;
  private String 사업자등록번호;
  private String 소득구분;
  private Long 소득금액;
  private Long 총결정세액;

}
