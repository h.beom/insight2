package ai.bankscan.finance.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

public class 은행_수시전계좌조회_Dto {

  @Data
  public static class Create {

    private String bank;
    private String 은행명;
    private String 예금명;
    private String 계좌번호;
    private String 통화코드;
    private Long 잔액;
    private String 신규일자;
    private String 최종입출금일;

  }

  @Data
  @NoArgsConstructor
  public static class GroupBy {

    private String bank;
    private String 계좌번호;
    private String date;
    public GroupBy(String bank, String 계좌번호, String date) {
      this.bank = bank;
      this.계좌번호 = 계좌번호;
      this.date = date;
    }

  }

}
