package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 국세청_소득세액공제조회발급_보험료_Dto {

  private String 상위_종류;
  private String 상위_귀속연도;
  private Integer 상위_인덱스;
  private String 계약자;
  private String 종류;
  private String 상호;
  private String 증권번호;
  private String 납입금액계;
  private String 주피보험자;
  private String 종피보험자;
  private String 종피보험자2;
  private String 종피보험자3;
  private String 구입_납입방법;
  private String 계약기간;

}
