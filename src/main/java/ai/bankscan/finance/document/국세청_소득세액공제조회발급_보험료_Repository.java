package ai.bankscan.finance.document;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface 국세청_소득세액공제조회발급_보험료_Repository extends JpaRepository<국세청_소득세액공제조회발급_보험료, Long>,
    JpaSpecificationExecutor<국세청_소득세액공제조회발급_보험료> {

}
