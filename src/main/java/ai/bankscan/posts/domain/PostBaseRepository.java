package ai.bankscan.posts.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface PostBaseRepository<T extends Post> extends JpaRepository<T, Long>,
    JpaSpecificationExecutor<T> {

}
