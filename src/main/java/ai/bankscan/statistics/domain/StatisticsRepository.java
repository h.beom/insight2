package ai.bankscan.statistics.domain;

import ai.bankscan.consultations.domain.consulting.Consultation;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StatisticsRepository extends JpaRepository<Consultation, Long>,
        JpaSpecificationExecutor<Consultation> {

    @Query(value =
            "select " +
                    " o.name name, " +
                    "o.type,  " +
                    " count(o.id) organizationCount, " +
                    " CONCAT(ROUND(100 * count(o.id)/total.allCount, " +
                    " 2), " +
                    " '%') percentage  " +
                    "    from " +
                    " consultation c, " +
                    " organization o, " +
                    " (select " +
                    "     count(*) allCount  " +
                    " from " +
                    "     consultation) total  " +
                    "    where 1=1 " +
                    " and (o.id = c.center_id  " +
                    " or o.id = c.institution_id )" +
                    " and :code is null or o.code = :code " +
                    " GROUP BY o.id " +
                    " ORDER BY percentage desc ",
            countQuery = "select count(*) from consultation",
            nativeQuery = true)
    Page<Map<String, Object>> findAllByOrganizationCount(@Param("code") String code, Pageable pageable);

    @Query(value =
            "select " +
                    " o.name name, " +
                    "o.type,  " +
                    " count(o.id) organizationCount, " +
                    " CONCAT(ROUND(100 * count(o.id)/total.allCount, " +
                    " 2), " +
                    " '%') percentage  " +
                    "    from " +
                    " consultation c, " +
                    " organization o, " +
                    " (select " +
                    "     count(*) allCount  " +
                    " from " +
                    "     consultation) total  " +
                    "    where 1=1 " +
                    " and (o.id = c.center_id  " +
                    " or o.id = c.institution_id )" +
                    " and :code is null or o.code = :code " +
                    " GROUP BY o.id " +
                    " ORDER BY percentage desc ",
            nativeQuery = true)
    List<Map<String, Object>> findAllByOrganizationCountToExcel(@Param("code") String code);

}
