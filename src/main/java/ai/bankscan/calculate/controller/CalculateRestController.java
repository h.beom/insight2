package ai.bankscan.calculate.controller;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.calculate.dto.CalculateDto;
import ai.bankscan.calculate.dto.CalculateDto.Response;
import ai.bankscan.calculate.service.CalculateService;
import ai.bankscan.common.infra.security.common.CurrentUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Map;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@Api(tags = {"정산관리 api"})
@RequiredArgsConstructor
@RestController
@RequestMapping(path = CalculateRestController.REQUEST_BASE_PATH, produces = MediaTypes.ALPS_JSON_VALUE)
public class CalculateRestController {

  static final String REQUEST_BASE_PATH = "/api/calculate";

  private final CalculateService calculateService;

  @Secured({"ROLE_ADMIN", "ROLE_CENTER_MANAGER"})
  @ApiOperation(value = "정산 update")
  @PutMapping("/update/{id}")
  public ResponseEntity<Map<String, Object>> updateCalculateList(
          @PathVariable long id,
      @RequestBody List<CalculateDto.Simple> updates,
      @CurrentUser Account account) {


    Map<String, Object> lists = calculateService.updateCalculates(id, updates);
    if (lists != null) {
      return ResponseEntity.ok(lists);
    }
    return ResponseEntity.status(400).body(null);
  }

  @Secured({"ROLE_ADMIN", "ROLE_CENTER_MANAGER", "ROLE_CONSULTANT"})
  @ApiOperation(value = "정산 관리 리스트")
  @GetMapping
  public ResponseEntity<List<Map<String, Object>>> getCalculateList(
      CalculateDto.Search search,
      @CurrentUser Account account) {

    List<Map<String, Object>> datas = calculateService.getCalculateList(search, account);

    return ResponseEntity.ok(datas);
  }

  @Secured({"ROLE_ADMIN", "ROLE_CENTER_MANAGER", "ROLE_CONSULTANT"})
  @ApiOperation(value = "정산 관리 리스트")
  @GetMapping("/by-user/{id}")
  public ResponseEntity<Map<String, Object>> getCalculateList(
          @PathVariable long id,
          @CurrentUser Account account) {

    Map<String, Object> data = calculateService.getCalculateListByUser(id);

    return ResponseEntity.ok(data);
  }
}
