package ai.bankscan.posts.domain;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.converters.ListAttributeConverter;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import ai.bankscan.common.infra.utils.Utils;
import com.google.api.client.util.Lists;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;
import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
public abstract class Post extends AuditableJoinEntity<Account> {

  public enum Type {
    NOTICE, QNA;
  }

  @Column(nullable = false, updatable = false, insertable = false)
  @Enumerated(EnumType.STRING)
  private Type type;

  private String title;

  @Lob
  @Column(nullable = false)
  private String contents;

  @Lob
  @Column(nullable = false)
  private String contentsPlainText;

  @Default
  @Setter
  @Convert(converter = ListAttributeConverter.class)
  private List<Long> attachFiles = Lists.newArrayList();

  public void setContents(String contents) {
    this.contents = contents;
    this.contentsPlainText = Utils.toRemoveMarkup(contents);
  }
}
