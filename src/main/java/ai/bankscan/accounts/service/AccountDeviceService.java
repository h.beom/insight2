package ai.bankscan.accounts.service;

import ai.bankscan.accounts.domain.AccountDevice;
import ai.bankscan.accounts.domain.AccountDeviceRepository;
import ai.bankscan.accounts.dto.AccountDeviceDto;
import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.common.infra.jpa.support.Restrictions.Conn;
import ai.bankscan.configs.security.Hasher;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Slf4j
@Transactional(readOnly = true)
@Service
@RequiredArgsConstructor
public class AccountDeviceService {

  @Value("${app.multi-login}")
  private Boolean isMultiLogin;

  private final CacheManager cacheManager;

  private final ModelMapper mapper;
  private final AccountDeviceRepository accountDeviceRepository;
  private final EntityManager entityManager;

  @Cacheable(value = "deviceByToken", key = "#accessToken")
  public AccountDevice findByAccessToken(String accessToken) {
    AccountDevice device = accountDeviceRepository.findByAccessToken(accessToken).orElseThrow(() -> new UsernameNotFoundException(""));
    device.getAccount().getAddress();
    return device;
  }

  public Long countAllByPushTokens() {
    String query = "SELECT COUNT(push_token) as counts " +
            "from account_device " +
            "where push_token is not null ";
    Object resultQuery = entityManager.createNativeQuery(query).getSingleResult();

    return Long.parseLong(resultQuery.toString());
  }

  @Transactional
  @CacheEvict(value = "deviceByToken", key = "#accessToken")
  public void deleteByAccessToken(String accessToken) {
    accountDeviceRepository.deleteByAccessToken(accessToken);
  }

  @Transactional
  public AccountDevice create(AccountDeviceDto.Create dto) {

    Restrictions r = new Restrictions(Conn.OR);
    r.eq("serialNumber", dto.getSerialNumber());
    if (!isMultiLogin) {
      r.eq("account.id", dto.getAccount().getId());
    }

    List<AccountDevice> accountDevices = accountDeviceRepository.findAll(r.output());
    for (AccountDevice accountDevice : accountDevices) {
      cacheManager.getCache("deviceByToken").evict(accountDevice.getAccessToken());
      accountDeviceRepository.delete(accountDevice);
    }

    String nowStr = DateTimeFormatter.ISO_DATE.format(LocalDate.now());

    StringBuilder secret = new StringBuilder();
    secret.append(dto.getDeviceName() + "@");
    secret.append(dto.getSerialNumber() + "@");
    secret.append(nowStr);

    String secretToken = Hasher.parseMD5(secret.toString());
    String accessToken = this.getUniqueAccessToken(secretToken);

    AccountDevice device = AccountDevice.builder()
        .accessToken(accessToken)
        .secretToken(secretToken)
        .account(dto.getAccount())
        .serialNumber(dto.getSerialNumber())
        .pushToken(dto.getPushToken())
        .platform(dto.getPlatform())
        .deviceName(dto.getDeviceName())
        .build();

//    mapper.map(dto, device);
    return accountDeviceRepository.save(device);
  }

  private String getUniqueAccessToken(String secretKey) {
    String accessToken = Hasher.parseMD5(UUID.randomUUID().toString()).substring(0, 22);
    while (true) {
      Restrictions r = new Restrictions();
      r.like("accessToken", accessToken + "%");
      long rowSize = accountDeviceRepository.count(r.output());

      if (rowSize == 0) {
        accessToken = accessToken + secretKey.substring(27) + secretKey.substring(0, 5);
        break;
      } else {
        accessToken = Hasher.parseMD5(UUID.randomUUID().toString()).substring(0, 22);
      }
    }
    return accessToken;
  }

  public String createUniqueUUID() {
    String uid = UUID.randomUUID().toString();
    while (true) {
      Restrictions r = new Restrictions();
      r.like("serialNumber", uid);
      if (accountDeviceRepository.count(r.output()) == 0) {
        break;
      }
      uid = UUID.randomUUID().toString();
    }
    return uid;

  }
}
