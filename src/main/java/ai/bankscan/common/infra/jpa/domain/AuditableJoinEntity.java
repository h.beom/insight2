package ai.bankscan.common.infra.jpa.domain;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

/**
 * The type Auditable entity.
 *
 * @param <T> the type parameter
 * @author eomjeongjae
 * @since 2019 -02-01
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@MappedSuperclass
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
public abstract class AuditableJoinEntity<T extends BaseEntity> extends BaseEntity {

  @CreatedBy
  @ManyToOne(fetch = FetchType.LAZY)
  private T createdBy;

  @LastModifiedBy
  @ManyToOne(fetch = FetchType.LAZY)
  private T lastModifiedBy;

}
