package ai.bankscan.common.infra.exceptions;

import ai.bankscan.common.infra.web.common.HttpStatusMessageException;
import org.springframework.http.HttpStatus;

/**
 * @author eomjeongjae
 * @since 2019/10/14
 */
public class UserDuplicatedException extends HttpStatusMessageException {

  public UserDuplicatedException(String username) {
    super(HttpStatus.BAD_REQUEST, "user.duplicated", username);
  }
}

