package ai.bankscan.finance.document;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Table(indexes = {@Index(columnList = "secure"), @Index(columnList = "계좌번호")})
public class 증권_증권보유계좌조회 extends AuditableJoinEntity<Account> {

  private String secure;

  private String 계좌번호;
  private String 상품코드;
  private String 상품명;

  @Setter
  private String 대용금;

  @Setter
  private String 예수금;

  @Setter
  private Long 평가총액;
}
