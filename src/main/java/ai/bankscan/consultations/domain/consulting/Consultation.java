package ai.bankscan.consultations.domain.consulting;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.Account.Authority;
import ai.bankscan.accounts.domain.consultants.Consultant;
import ai.bankscan.accounts.domain.consultants.ProConsultant;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import ai.bankscan.organizations.domain.Center;
import ai.bankscan.organizations.domain.Institution;
import ai.bankscan.survey.domain.금융건강진단표;
import ai.bankscan.survey.domain.재무달성계획;
import ai.bankscan.survey.domain.희망노후생활비;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class Consultation extends AuditableJoinEntity<Account> {

    @Getter
    @RequiredArgsConstructor
    public enum Type {
        BASIC_CONSULTATION("BASIC_CONSULTATION"), PRO_CONSULTATION("PRO_CONSULTATION");

        private final String title;
    }


    @Getter
    @RequiredArgsConstructor
    public enum ConsultWantTime {
        AM("오전 9:00 ~ 12:00"),
        PM("오후 13:00 ~ 18:00");

        private final String title;
    }

    @Getter
    @RequiredArgsConstructor
    public enum Status {
        APPLY("기본 상담 신청", "상담예약"),
        RECEIPT("상담 지정", "상담예약"),
        COMPANION("기본 상담 반려", "상담예약"),
        ACCEPT("기본 상담 수락", "상담접수"),
        COMPLETE("상담 완료", "상담완료"),
        PRO_APPLY("전문 상담 신청", "전문상담 예약"),

        PRO_RECEIPT("전문 상담 지정", "전문상담 예약"),
        PRO_COMPANION("전문 상담 반려", "전문상담 예약"),
        PRO_ACCEPT("전문 상담 수락", "전문상담 접수"),
        PRO_COMPLETE("전문 상담 완료", "전문상담 완료");

        private final String title;
        private final String memberTitle;


        public static List<Status> getStatuses(Set<Authority> authorities, Status currentStatus) {
            List<Status> responseStatus = new ArrayList<>();

            boolean isAdminOrCenter = authorities.contains(Account.Authority.ADMIN) || authorities.contains(Account.Authority.CENTER_MANAGER);
            boolean isAdminOrRM = authorities.contains(Account.Authority.ADMIN) || authorities.contains(Account.Authority.RELATIONSHIP_MANAGER);
            boolean isBasicConsultant = authorities.contains(Account.Authority.CONSULTANT);
            boolean isProConsultant = authorities.contains(Account.Authority.PRO_CONSULTANT);

            switch (currentStatus) {
                case APPLY:
                    if (isAdminOrCenter) {
                        responseStatus.add(Status.RECEIPT);
                        responseStatus.add(Status.COMPLETE);
                    }
                    break;
                case RECEIPT:
                    if (isAdminOrCenter) {
                        responseStatus.add(Status.RECEIPT);
                    }
                    if (isBasicConsultant) {
                        responseStatus.add(Status.ACCEPT);
                        responseStatus.add(Status.COMPANION);
                    }
                    break;
                case COMPANION:
                    if (isAdminOrCenter) {
                        responseStatus.add(Status.RECEIPT);
                        responseStatus.add(Status.COMPANION);
                    }
                    if (isBasicConsultant) {
                        responseStatus.add(Status.COMPANION);
                    }
                    break;
                case ACCEPT:
                    if (isBasicConsultant) {
                        responseStatus.add(Status.COMPLETE);
                        responseStatus.add(Status.PRO_APPLY);
                    }
                    break;
                case COMPLETE:
                    if (isBasicConsultant) {
                        responseStatus.add(Status.PRO_APPLY);
                    }
                    break;
                case PRO_APPLY:
                    if (isAdminOrRM) {
                        responseStatus.add(Status.PRO_RECEIPT);
                        responseStatus.add(Status.PRO_COMPLETE);
                    }
                    break;
                case PRO_RECEIPT:
                    if (isAdminOrRM) {
                        responseStatus.add(Status.PRO_RECEIPT);
                    }
                    if (isProConsultant) {
                        responseStatus.add(Status.PRO_ACCEPT);
                        responseStatus.add(Status.PRO_COMPANION);
                    }
                    break;

                case PRO_COMPANION:
                    if (isAdminOrRM) {
                        responseStatus.add(Status.PRO_RECEIPT);
                        responseStatus.add(Status.PRO_COMPANION);
                    }
                    if (isProConsultant) {
                        responseStatus.add(Status.PRO_COMPANION);
                    }
                    break;
                case PRO_ACCEPT:
                    if (isProConsultant) {
                        responseStatus.add(Status.PRO_COMPLETE);
                    }
                    break;
                default:
                    break;
            }
            return responseStatus;
        }
    }

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status;

    @NotNull
    private String code;

    @Enumerated(EnumType.STRING)
    private Type type;

    @NotNull
    private LocalDate consultWantDate;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ConsultWantTime consultWantTime;

    @ManyToOne(fetch = FetchType.LAZY)
    private Consultant consultant; // consultant

    @ManyToOne(fetch = FetchType.LAZY)
    private Center center; //기본 상담 기관

    @ManyToOne(fetch = FetchType.LAZY)
    private ProConsultant proConsultant; // consultant

    @ManyToOne(fetch = FetchType.LAZY)
    private Institution institution; //전문 상담 기관

    @ManyToOne(fetch = FetchType.LAZY)
    private 금융건강진단표 금융건강진단표;

    @ManyToOne(fetch = FetchType.LAZY)
    private 재무달성계획 재무달성계획;

    @ManyToOne(fetch = FetchType.LAZY)
    private 희망노후생활비 희망노후생활비;

}
