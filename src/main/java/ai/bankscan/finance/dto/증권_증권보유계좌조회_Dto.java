package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 증권_증권보유계좌조회_Dto {

  private String secure;

  private String 계좌번호;
  private String 상품코드;
  private String 상품명;

  private String 대용금;
  private String 예수금;
  private Long 평가총액;

}
