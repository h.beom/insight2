package ai.bankscan.survey.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface 재무달성계획Repository extends JpaRepository<재무달성계획, Long>,
    JpaSpecificationExecutor<재무달성계획> {

}
