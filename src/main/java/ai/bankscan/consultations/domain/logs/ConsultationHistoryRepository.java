package ai.bankscan.consultations.domain.logs;

import ai.bankscan.consultations.domain.consulting.Consultation;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ConsultationHistoryRepository extends JpaRepository<ConsultationHistory, Long>,
    JpaSpecificationExecutor<ConsultationHistory> {

  List<ConsultationHistory> findByConsultation(
      Consultation consultation);

}
