package ai.bankscan.finance.document;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface 은행_대출계좌조회_Repository extends JpaRepository<은행_대출계좌조회, Long>,
    JpaSpecificationExecutor<은행_대출계좌조회> {

  @Query("SELECT COALESCE(SUM(대출잔액), 0) FROM 은행_대출계좌조회 WHERE createdBy.id = :createdById")
  Long get은행대출(@Param("createdById") Long createdById);

}
