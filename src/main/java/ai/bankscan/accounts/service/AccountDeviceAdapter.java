package ai.bankscan.accounts.service;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.AccountDevice;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

@Getter
public class AccountDeviceAdapter extends User {

  private Account account;
  private AccountDevice accountDevice;

  public AccountDeviceAdapter(Account account, AccountDevice accountDevice) {
    super(
        account.getUsername(),
        account.getPassword(),
        authorities(account.getAuthorities())
    );

    this.accountDevice = accountDevice;
    this.account = account;
  }

  private static Collection<? extends GrantedAuthority> authorities(Set<Account.Authority> roles) {
    return roles.stream()
        .map(r -> new SimpleGrantedAuthority("ROLE_" + r.toString()))
        .collect(Collectors.toSet());
  }


}
