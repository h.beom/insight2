package ai.bankscan.posts.domain;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@DiscriminatorValue("NOTICE")
public class Notice extends Post {

  public enum Category {
    GROUP_WORKSHOPS, FINANCIAL_EDUCATION, CAMPAIGN, NOTICE;
  }

  @Default
  private Type type = Type.NOTICE;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private Category category;

  @Default
  private Boolean topNotice = Boolean.FALSE;

  @Default
  private Boolean sendPush = Boolean.FALSE;

  @Default
  private Boolean display = Boolean.TRUE;

}
