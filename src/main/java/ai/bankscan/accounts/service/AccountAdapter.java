package ai.bankscan.accounts.service;

import ai.bankscan.accounts.domain.Account;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class AccountAdapter extends User {

  private Account account;

  public AccountAdapter(Account account) {
    super(account.getUsername(), account.getPassword(), authorities(account.getAuthorities()));
    this.account = account;
  }

  private static Collection<? extends GrantedAuthority> authorities(Set<Account.Authority> roles) {
    return roles.stream()
        .map(r -> new SimpleGrantedAuthority("ROLE_" + r))
        .collect(Collectors.toSet());
  }

  public Account getAccount() {
    return account;
  }

}
