package ai.bankscan.accounts.domain;

import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class AccountLog extends AuditableJoinEntity<Account> {

  @Getter
  @RequiredArgsConstructor
  public enum LogType {

    LOGIN("로그인"),
    DEVICE("디바이스 등록"),
    DEVICE_CHANGE("디바이스 변경"),
    AGAIN_LOG("2차 로그인"),
    REQUEST_LEAVE("탈퇴 요청"),
    LEAVE("탈퇴");

    private final String title;
  }

  private LogType logType;
  private LocalDateTime logTime;

}
