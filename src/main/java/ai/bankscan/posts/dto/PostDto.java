package ai.bankscan.posts.dto;

import ai.bankscan.accounts.dto.AccountBaseDto;
import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.common.infra.jpa.support.SearchDto;
import ai.bankscan.posts.domain.Post;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PostDto {

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "PostDto.Create")
  public abstract static class Create {

    @NotEmpty
    @ApiModelProperty(value = "제목", example = "제목", required = true, position = 1)
    private String title;

    @NotEmpty
    @ApiModelProperty(value = "내용", example = "내용", required = true, position = 2)
    private String contents;

    @ApiModelProperty(value = "첨부파일", example = "[1, 2]", position = 3)
    private List<Long> attachFiles;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "PostDto.Update")
  public static class Update {

    @NotEmpty
    @ApiModelProperty(value = "제목", example = "제목", required = true, position = 1)
    private String title;

    @NotEmpty
    @ApiModelProperty(value = "내용", example = "내용", required = true, position = 2)
    private String contents;

    @ApiModelProperty(value = "첨부파일", example = "[1, 2]", position = 3)
    private List<Long> attachFiles;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "PostDto.Response")
  public static class Response {

    @ApiModelProperty(value = "ID", example = "1")
    private Long id;

    @NotEmpty
    @ApiModelProperty(value = "제목", example = "제목", required = true, position = 1)
    private String title;

    @NotEmpty
    @ApiModelProperty(value = "내용", example = "내용", required = true, position = 2)
    private String contents;

    @ApiModelProperty(value = "첨부파일", example = "[1, 2]", position = 3)
    private List<Long> attachFiles;

    @ApiModelProperty(value = "생성일시", position = 4)
    private LocalDateTime createdAt;

    @ApiModelProperty(value = "작성자", position = 5)
    private AccountBaseDto.Response createdBy;

  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString(callSuper = true)
  @ApiModel(value = "PostDto.Search")
  public abstract static class Search<T extends Post> extends SearchDto<T> {

    @ApiModelProperty(value = "검색구분", position = 1)
    private String searchType;

    @ApiModelProperty(value = "검색어", position = 2)
    private String keyword;

    @Override
    protected Restrictions generateRestrictions() {
      final Restrictions restrictions = new Restrictions();

      if (StringUtils.isNotEmpty(this.keyword)) {
        if (StringUtils.isEmpty(this.searchType)) {
          final Restrictions restrictionsByKeyword = new Restrictions(Restrictions.Conn.OR);
          List<String> keywords = Arrays.asList(this.keyword.trim().split("\\s+"));
          keywords.stream()
              .forEach(keyword -> {
                restrictionsByKeyword.like("title", "%" + keyword + "%");
                restrictionsByKeyword.like("contentsPlainText", "%" + keyword + "%");
                restrictionsByKeyword.like("createdBy.name", "%" + keyword + "%");
              });
          restrictions.addChild(restrictionsByKeyword);
        } else {
          restrictions.like(searchType, "%" + keyword + "%");
        }
      }

      return restrictions;
    }

  }

}
