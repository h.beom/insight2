package ai.bankscan.finance.document;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface 국세청_소득금액증명_Repository extends JpaRepository<국세청_소득금액증명, Long>,
    JpaSpecificationExecutor<국세청_소득금액증명> {

  @Query("SELECT COALESCE(SUM(소득금액) ,0) FROM 국세청_소득금액증명 WHERE createdBy.id = :createdById AND 소득구분 LIKE '근로소득%' AND 귀속연도 = :year")
  Long get번돈_근로소득(@Param("createdById") long createdById, @Param("year") String year);

  @Query("SELECT COALESCE(SUM(소득금액), 0) FROM 국세청_소득금액증명 WHERE createdBy.id = :createdById AND NOT(소득구분 LIKE '근로소득%') AND 귀속연도 = :year")
  Long get번돈_사업소득(@Param("createdById") long createdById, @Param("year") String year);

}
