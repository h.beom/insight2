package ai.bankscan.configs;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.Account.Type;
import ai.bankscan.accounts.domain.AccountRepository;
import ai.bankscan.accounts.dto.AccountDto;
import ai.bankscan.accounts.service.AccountService;
import ai.bankscan.common.domain.Gender;
import ai.bankscan.configs.properties.AppProperties;
import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Optional;
import java.util.TimeZone;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Profile;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

/**
 * @author eomjeongjae
 * @since 2019-08-26
 */
@Slf4j
@RequiredArgsConstructor
@Component
@Profile("!test")
public class ApplicationInitializer implements ApplicationRunner {

  private final AccountService accountService;
  private final AccountRepository accountRepository;
  private final MessageSourceAccessor messageSourceAccessor;
  private final MessageSource messageSource;
  private final AppProperties appProperties;

  @Override
  public void run(ApplicationArguments args) {
    log.info("Application startup complete.");
    log.info("app-name: {}", appProperties.getName());

    Clock.system(ZoneId.of("Asia/Seoul"));
    TimeZone.setDefault(TimeZone.getTimeZone("Asia/Seoul"));
    AccountDto.Create newAdmin = AccountDto.Create.builder()
        .type(Type.ADMIN)
        .username("admin")
        .password("a123456A")
        .name("최고관리자")
        .email("admin@bankscan.ai")
        .mobilePhone("01012345678")
        .gender(Gender.FEMALE)
        .birthday(LocalDate.of(1987, 11, 30))
        .build();

    Optional<Account> optionalAdmin = accountRepository.findByUsername(newAdmin.getUsername());
    if (!optionalAdmin.isPresent()) {
      accountService.createAccount(newAdmin);
    }

  }
}
