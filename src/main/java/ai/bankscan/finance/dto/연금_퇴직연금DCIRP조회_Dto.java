package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 연금_퇴직연금DCIRP조회_Dto {

  private String 가입회사;
  private String 상품유형;
  private String 상품명;
  private String 가입일;
  private String 연금개시예정일;
  private String 적립금;
  private String 조회기준일;
  private String 계좌번호;
  private String 총납입액;
  private String 중도인출금액;
  private String 납입종료일;
  private String 연납입액;
  private String 연약정납입액;
  private String 예상연금적립액;
  private String 적립금합계금액;

}
