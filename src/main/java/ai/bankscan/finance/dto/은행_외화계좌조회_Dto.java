package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 은행_외화계좌조회_Dto {

  private String bank;

  private String 계좌번호;
  private String 만기일자;
  private String 신규일자;
  private String 예금잔액;
  private String 예금종류;
  private String 통화;

}
