package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 연금_연금정보조회_Dto {

  private String 가입회사;
  private String 상품유형;
  private String 상품명;
  private String 가입일;
  private String 연금개시예정일;
  private String 적립금;
  private String 조회기준일;
  private String 증권번호;
  private String 총납입액;
  private String 중도인출금액;
  private String 적립방식;
  private String 납입상태;
  private String 납입종료일;
  private String 납입방법;
  private String 납입보험료;
  private String 예상연금적립액;
  private String 연금수령종료일;
  private String 연금기수령액;
  private String 미수령연금액;

}
