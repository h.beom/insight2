package ai.bankscan.configs.security;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.service.AccountDeviceAdapter;
import java.util.Optional;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * The type Security auditor aware auto configuration.
 *
 * @author eomjeongjae
 * @since 2019 -07-18
 */
@Configuration
public class SecurityAuditorAwareConfiguration {

  @Bean
  @ConditionalOnMissingBean
  public AuditorAware<Account> auditorProvider() {
    return new SecurityAuditorAccountAware();
  }

  public static class SecurityAuditorUsernameAware implements AuditorAware<String> {
    @Override
    public Optional<String> getCurrentAuditor() {
      return Optional.ofNullable(SecurityContextHolder.getContext())
          .map(SecurityContext::getAuthentication)
          .filter(Authentication::isAuthenticated)
          .filter(authentication -> !AnonymousAuthenticationToken.class
              .isAssignableFrom(authentication.getClass()))
          .map(authentication -> {
            return ((UserDetails) authentication.getPrincipal()).getUsername();
          });
    }
  }


  public static class SecurityAuditorAccountAware implements AuditorAware<Account> {
    @Override
    public Optional<Account> getCurrentAuditor() {
      return Optional.ofNullable(SecurityContextHolder.getContext())
          .map(SecurityContext::getAuthentication)
          .filter(Authentication::isAuthenticated)
          .filter(authentication -> !AnonymousAuthenticationToken.class
              .isAssignableFrom(authentication.getClass()))
          .map(authentication -> {
            return ((AccountDeviceAdapter) authentication.getPrincipal()).getAccount();
          });
    }
  }

}
