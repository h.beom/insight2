package ai.bankscan.accounts.domain.consultants;

import ai.bankscan.accounts.domain.Account;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@SuperBuilder
@Entity
@DiscriminatorValue("PRO_CONSULTANT")
public class ProConsultant extends Account {

}
