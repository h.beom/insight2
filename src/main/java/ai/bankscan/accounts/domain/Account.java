package ai.bankscan.accounts.domain;

import ai.bankscan.common.address.domain.Address;
import ai.bankscan.common.domain.Gender;
import ai.bankscan.common.infra.jpa.converters.MapAttributeConverter;
import ai.bankscan.common.infra.jpa.domain.BaseEntity;
import ai.bankscan.organizations.domain.Organization;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
public abstract class Account extends BaseEntity {

  @Getter
  public enum Authority {
    ADMIN,
    CENTER_MANAGER,
    RELATIONSHIP_MANAGER,
    CONSULTANT,
    PRO_CONSULTANT,
    MEMBER;
  }

  @Getter
  @RequiredArgsConstructor
  public enum Type {
    ADMIN(Sets.newHashSet(Authority.ADMIN)),
    CENTER_MANAGER(Sets.newHashSet(Authority.CENTER_MANAGER, Authority.CONSULTANT)),
    RELATIONSHIP_MANAGER(
        Sets.newHashSet(Authority.RELATIONSHIP_MANAGER, Authority.PRO_CONSULTANT)),
    CONSULTANT(Sets.newHashSet(Authority.CONSULTANT)),
    PRO_CONSULTANT(Sets.newHashSet(Authority.PRO_CONSULTANT)),
    MEMBER(Sets.newHashSet(Authority.MEMBER));

    private final Set<Authority> authorities;

    public boolean isAdmin() {
      return (this == ADMIN);
    }

    public boolean isCenterManager() {
      return (this == CENTER_MANAGER);
    }

    public boolean isRelationshipManager() {
      return (this == RELATIONSHIP_MANAGER);
    }

    public boolean isConsultant() {
      return (this == CONSULTANT);
    }

    public boolean isProConsultant() {
      return (this == PRO_CONSULTANT);
    }

    public boolean isMember() {
      return (this == MEMBER);
    }

  }

  @Getter
  @RequiredArgsConstructor
  public enum WorkerType {
    FULL_TIME("정규직"), PART_TIME("파트타임"), PARTNER("파트너");

    private final String title;
  }

  @Getter
  @RequiredArgsConstructor
  public enum EducationCompletionLevel {
    BASIC("기본수료"), INTERMEDIATE("중급수료"), ADVANCED("고급수료");

    private final String title;
  }

  @Getter
  @RequiredArgsConstructor
  public enum ConsultantLevel {
    A("A레벨"), B("B레벨"), C("C레벨");

    private final String title;
  }

  @Getter
  @RequiredArgsConstructor
  public enum LeaveType {
    THROW_OUT("관리자탈퇴"), LEAVE("회원탈퇴");
    private final String title;
  }

  @Getter
  @RequiredArgsConstructor
  public enum LeaveReasonType {
    REQ_USER("고객요청"),
    FLOW_OUT_USER_INFO("개인정보유출"),
    COPYRIGHT_VIOLATIONS("저작권위반"),
    AD_WALLPAPER("광고성도배"),
    ETC("기타");

    private final String title;
  }

  @Getter
  @RequiredArgsConstructor
  public enum ProfessionalField {
    금융상품("금융상품"),
    재무상담("재무상담"),
    복지상담("복지상담"),
    전문상담("전문상담");

    private final String title;
  }

  @Getter
  @RequiredArgsConstructor
  public enum ProfessionalField2 {
    금융상품_예금("예금", ProfessionalField.금융상품),
    금융상품_대출("대출", ProfessionalField.금융상품),
    금융상품_보험("보험", ProfessionalField.금융상품),
    금융상품_카드("카드", ProfessionalField.금융상품),
    금융상품_펀드("펀드", ProfessionalField.금융상품),
    금융상품_은행관리("은행관리", ProfessionalField.금융상품),
    금융상품_신용등급("신용등급", ProfessionalField.금융상품),

    재무상담_은퇴설계("은퇴설계", ProfessionalField.재무상담),
    재무상담_내집마련("내집마련/주거", ProfessionalField.재무상담),
    재무상담_생애주기("생애주기맞춤상담", ProfessionalField.재무상담),

    복지상담_서민금융("서민금융", ProfessionalField.복지상담),
    복지상담_채무조정("채무조정", ProfessionalField.복지상담),
    복지상담_회생("회생/파산", ProfessionalField.복지상담),
    복지상담_민원("민원", ProfessionalField.복지상담),
    복지상담_복지신청("복지신청", ProfessionalField.복지상담),

    전문상담_세무("세무", ProfessionalField.전문상담),
    전문상담_회계("회계", ProfessionalField.전문상담),
    전문상담_상속("상속/증여", ProfessionalField.전문상담),
    전문상담_정책자금("정책자금", ProfessionalField.전문상담),
    전문상담_부동산("부동산", ProfessionalField.전문상담),
    전문상담_P2P("P2P", ProfessionalField.전문상담),
    전문상담_소상공인("소상공인", ProfessionalField.전문상담),
    전문상담_연말정산("연말정산", ProfessionalField.전문상담),
    전문상담_법률("법률", ProfessionalField.전문상담);

    private final String title;
    private final ProfessionalField professionalField;

    public static HashMap<ProfessionalField2, String> findByType(ProfessionalField professionalField) {
      return Stream.of(ProfessionalField2.values())
          .filter(data -> data.getProfessionalField().equals(professionalField))
          .collect(Collectors.toMap(Function.identity(), ProfessionalField2::getTitle, (e1, e2) -> e2, LinkedHashMap::new));
    }
  }




  public enum Status {
    READY, ACTIVATION, DEACTIVATION, BLOCK, WITHDRAWAL;
  }

  @Getter
  @RequiredArgsConstructor
  public enum SearchPeriodType {
    LEAVE_DATE("leaveDate"),
    LAST_ACCESS_TIME("lastAccessTime");
    private final String title;
  }

  @Default
  @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<AccountDevice> devices = Sets.newHashSet();

  @Setter
  @ManyToOne(fetch = FetchType.LAZY)
  private Organization organization;

  @Column(nullable = false, updatable = false, insertable = false)
  @Enumerated(EnumType.STRING)
  private Type type;

  @Include
  @Column(unique = true, nullable = false)
  private String username;

  @Column(nullable = false)
  private String password;

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private String email;

  @Column(nullable = false)
  private String mobilePhone;

  @Column(nullable = true)
  private String mobilePhoneCorp;

  @Column(nullable = false)
  private LocalDate birthday;

  private String ci;

  private String di;

  @Setter
  @Getter
  @Embedded
  private Address address;

  @Default
  @Column(nullable = true)
  @Enumerated(EnumType.STRING)
  private Gender gender = Gender.MALE;

  @Default
  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private Status status = Status.READY;

  @Default
  private Boolean notificationConsent = Boolean.FALSE;

  private LocalDate effectiveDate;

  @Default
  private long consultationPrice = 0;

  @Enumerated(EnumType.STRING)
  private WorkerType workerType;

  @Enumerated(EnumType.STRING)
  private EducationCompletionLevel educationCompletionLevel;

  @Enumerated(EnumType.STRING)
  private ConsultantLevel consultantLevel;

  @Enumerated(EnumType.STRING)
  private ProfessionalField professionalField;

  @Enumerated(EnumType.STRING)
  private ProfessionalField2 professionalField2;

  @Enumerated(EnumType.STRING)
  private LeaveType leaveType;

  @Setter
  private LocalDateTime lastAccessTime;

  private LocalDateTime leaveDate;

  @Enumerated(EnumType.STRING)
  private LeaveReasonType leaveReasonType;

  private String leaveReason;

  @Default
  private Boolean isLeaved = Boolean.FALSE;

  @Default
  private Boolean isReceiveMail = Boolean.FALSE;

  @Default
  private Boolean isReceivePhone = Boolean.FALSE;

  @Default
  private Boolean isReceivePush = Boolean.TRUE;


  @Lob
  @Builder.Default
  @Convert(converter = MapAttributeConverter.class)
  private Map<String, Object> identityVerificationData = Maps.newHashMap();

  @Convert(converter = AuthorityConverter.class)
  private Set<Authority> authorities = Sets.newHashSet();

  public void create(
      Set<Authority> authorities,
      PasswordEncoder passwordEncoder
  ) {
    this.create(authorities, passwordEncoder, null);
  }

  public void create(
      Set<Authority> authorities,
      PasswordEncoder passwordEncoder,
      AccountDevice device
  ) {
    this.authorities = authorities;
    this.encodePassword(passwordEncoder);

    if (device != null) {
      this.addDevice(device);
    }
  }

  public void updateAuthority(Set<Authority> authorities) {
    this.authorities = authorities;
  }

  public void update(PasswordEncoder passwordEncoder) {
    this.encodePassword(passwordEncoder);
  }

  private void encodePassword(PasswordEncoder passwordEncoder) {
    this.password = passwordEncoder.encode(this.password);
  }

  private void addDevice(AccountDevice device) {
    device.setAccount(this);
  }

  public boolean isAdminRole() {
    return this.hasAnyRole(Authority.ADMIN);
  }

  public boolean isMemberRole() {
    return this.hasAnyRole(Authority.MEMBER);
  }

  public boolean isCenterManageRole() { return this.hasAnyRole(Authority.CENTER_MANAGER); }

  public boolean isRelationshipManagerRole() { return this.hasAnyRole(Authority.RELATIONSHIP_MANAGER); }

  public boolean isConsultantRole() {
    return this.hasAnyRole(Authority.CONSULTANT);
  }

  public boolean isProConsultantRole() {
    return this.hasAnyRole(Authority.PRO_CONSULTANT);
  }

  private boolean hasAnyRole(Authority... authorities) {
    Set<Authority> roleSet = this.getAuthorities();
    return Arrays.asList(authorities).stream()
        .anyMatch(roleSet::contains);
  }



}
