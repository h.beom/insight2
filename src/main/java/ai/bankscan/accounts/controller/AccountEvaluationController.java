package ai.bankscan.accounts.controller;


import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.dto.AccountEvaluationDto;
import ai.bankscan.accounts.service.AccountEvaluationService;
import ai.bankscan.common.infra.security.common.CurrentUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

@Slf4j
@Api(tags = {"상담원 평가 api"})
@RequiredArgsConstructor
@RestController
@RequestMapping(path = AccountEvaluationController.REQUEST_BASE_PATH, produces = MediaTypes.HAL_JSON_VALUE)
public class AccountEvaluationController {
    static final String REQUEST_BASE_PATH = "/api/evaluation";

    private final AccountEvaluationService accountEvaluationService;


    @ApiOperation(value = "상담원 평가 등록 ", notes = "상담원 평가")
    @Secured({"ROLE_MEMBER"})
    @PostMapping
    public ResponseEntity<AccountEvaluationDto.Response> createEvaluation(
            @RequestBody @Valid AccountEvaluationDto.Create dto,
            @CurrentUser Account account
            ) throws URISyntaxException {

        AccountEvaluationDto.Response response = accountEvaluationService.saveUserEvaluation(dto, account);
        return ResponseEntity.created(new URI(Link.REL_SELF))
                .body(response);
    }

    @ApiOperation(value = "상담원 평가 리스트 ", notes = "상담원 평가 리스트")
    @Secured({"ROLE_ADMIN", "ROLE_CENTER_MANAGER", "ROLE_RELATIONSHIP_MANAGER"})
    @GetMapping
    public ResponseEntity<List<Map<String, Object>>> getEvaluationList(AccountEvaluationDto.Search search) {
        List<Map<String, Object>>  list = accountEvaluationService.getUserEvaluationList(search);

        return ResponseEntity.ok(list);
    }


}
