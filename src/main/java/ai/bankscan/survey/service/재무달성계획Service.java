package ai.bankscan.survey.service;

import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.survey.domain.금융건강진단표;
import ai.bankscan.survey.domain.재무달성계획;
import ai.bankscan.survey.domain.재무달성계획Repository;
import ai.bankscan.survey.dto.재무달성계획AnswersDto;
import ai.bankscan.survey.dto.재무달성계획Dto;
import ai.bankscan.survey.dto.재무달성계획Dto.Child;
import ai.bankscan.survey.dto.재무달성계획Dto.Response;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service
public class 재무달성계획Service {

  private final ModelMapper modelMapper;

  private final 재무달성계획Repository 재무달성계획Repository;

  private final EntityManager entityManager;


  public Response getLast(Member member) {
    Restrictions r = new Restrictions();
    r.eq("createdBy.id", member.getId());

    Page<재무달성계획> entities = 재무달성계획Repository.findAll(r.output(), PageRequest.of(0, 1, Sort.by(
        Direction.DESC, "id")));

    if (entities.getTotalElements() > 0) {
      return toResponse(entities.getContent().get(0));
    } else {
      return null;
    }
  }


  @Transactional
  public 재무달성계획Dto.Response updateTarget(JSONArray body, Member member) {
    Restrictions r = new Restrictions();
    r.eq("createdBy.id", member.getId());

    Page<재무달성계획> entities = 재무달성계획Repository.findAll(r.output(), PageRequest.of(0, 1, Sort.by(
        Direction.DESC, "id")));

    if (entities.getTotalElements() > 0) {
      재무달성계획 db = entities.getContent().get(0);
      db.setTarget(body);
      return toResponse(재무달성계획Repository.save(db));
    } else {
      return null;
    }
  }

  @Transactional
  public 재무달성계획Dto.Response toResponseResult(List<재무달성계획AnswersDto.Response> answers) {
    JSONObject jsonObject = new JSONObject();
    JSONObject answer = new JSONObject();

    for (int i = 0; i < answers.size(); i++) {
      List<재무달성계획AnswersDto.AnswersResponse> answersResponses = answers.get(i).getAnswers();
      for (int j = 0; j < answersResponses.size(); j++) {
        if (answersResponses.get(j).getSubAnswers() != null) {
          List<재무달성계획AnswersDto.SubAnswersResponse> subAnswersResponses = answersResponses.get(j)
              .getSubAnswers();
          List<재무달성계획Dto.Child> jsonArray = new ArrayList<>();
          for (int k = 0; k < subAnswersResponses.size(); k++) {
            재무달성계획Dto.Child child = 재무달성계획Dto.Child.builder()
                .answer(subAnswersResponses.get(k).getAnswer())
                .key(subAnswersResponses.get(k).getKey())
                .count(subAnswersResponses.get(k).getCount())
                .build();
            jsonArray.add(child);
          }
          answer.put(answers.get(i).getKey() + "_자녀", jsonArray);
        } else {

          if (answers.get(i).getKey() == 2) {
            if (answer.get(answers.get(i).getKey().toString()) != null) {
              String data = answer.get(answers.get(i).getKey().toString()).toString();
              data += " " + answersResponses.get(j).getAnswer();
              answer.put(answers.get(i).getKey(), data);
            } else {
              answer.put(answers.get(i).getKey(), answersResponses.get(j).getAnswer());
            }
          } else {
            answer.put(answers.get(i).getKey(), answersResponses.get(j).getAnswer());
          }
        }
      }
    }

    String result = "";
    jsonObject.put("result", answer);

    if (answer.get("1").equals("기혼")) { //기혼 여부
      result += "노후자금 ";

    } else {
      result += this.singlePerson(answer.get("0").toString());
    }

    if (answer.get("2_자녀") != null) {
      JSONArray jsonArray = (JSONArray) answer.get("2_자녀");
      answer.get("2_자녀");
      for (int i = 0; i < jsonArray.size(); i++) {
        재무달성계획Dto.Child child = modelMapper.map(jsonArray.get(i), 재무달성계획Dto.Child.class);
        result += childSchoolAge(child.getAnswer());
      }
    }

    result += this.myHome(answer.get("3").toString());

    재무달성계획Dto.Create detailSurvey = 재무달성계획Dto.Create.builder()
        .answerList(answers)
        .result(result)
        .result2(jsonObject.toString())
        .build();

    재무달성계획 재무달성계획1 = 재무달성계획Repository.save(modelMapper.map(detailSurvey, 재무달성계획.class));

    return toResponse(재무달성계획1);
  }

  private String childSchoolAge(String value) {
    if (value.equals("성인") || value.equals("대학교")) {
      return "자녀결혼자금 ";
    } else {
      return "자녀교육비 ";
    }
  }

  private String singlePerson(String value) {
    String returnData = "";
    switch (value.trim()) {
      case "20대이하":
        returnData = "학자금마련 결혼준비자금 ";
        break;
      case "30대":
        returnData = "노후자금 결혼준비자금 ";
        break;
      default:
        returnData = "노후자금 ";
        break;
    }
    return returnData;
  }

  private String myHome(String value) {
    String returnData = "";
    if (value.trim().equals("자가")) {
      returnData = "부채상환자금 주택확장자금";
    } else {
      returnData = "주택마련자금";
    }
    return returnData;
  }

  public 재무달성계획Dto.Response getLatestOne(final long id) {
    String query = "SELECT * "
            + "FROM 재무달성계획 "
            + "WHERE created_by_id = " + id
            + " ORDER BY created_at DESC "
            + "LIMIT 1 ";

    List<재무달성계획> dataList = entityManager.createNativeQuery(query, 재무달성계획.class).getResultList();

    재무달성계획Dto.Response response =  toResponse(dataList.get(0));
    JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(dataList.get(0).getResult2());
    response.setResult2(jsonObject);
    return response;
  }

  private 재무달성계획Dto.Response toResponse(재무달성계획 재무달성계획) {
    return modelMapper.map(재무달성계획, 재무달성계획Dto.Response.class);
  }

}
