package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 은행_수시거래내역조회_Dto {

  private String bank;
  private String parent;

  private String 거래수단1;
  private String 거래수단2;
  private String 거래시각;
  private String 거래일자;
  private String 거래후잔액;
  private String 계좌번호;
  private String 기재사항1;
  private String 기재사항2;
  private String 입금액;
  private String 출금액;
  private String 통화코드;


}
