package ai.bankscan.consultations.service;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.AccountRepository;
import ai.bankscan.common.infra.exceptions.AccountNotFoundException;
import ai.bankscan.common.message.CommonMessage;
import ai.bankscan.consultations.domain.ConsultContent;
import ai.bankscan.consultations.domain.ConsultContentRepository;
import ai.bankscan.consultations.domain.consulting.Consultation;
import ai.bankscan.consultations.domain.consulting.Consultation.Type;
import ai.bankscan.consultations.domain.consulting.ConsultationRepository;
import ai.bankscan.consultations.dto.ConsultContentDto;
import ai.bankscan.consultations.dto.ConsultContentDto.Response;
import ai.bankscan.consultations.dto.ConsultationHistoryDto;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service
public class ConsultContentService {

  private final AccountRepository accountRepository;
  private final ConsultContentRepository consultContentRepository;
  private final ConsultationRepository consultationRepository;

  private final ConsultationHistoryService consultationHistoryService;

  private final ModelMapper modelMapper;

  @Autowired
  private final CommonMessage commonMessage;

  @Transactional
  public ConsultContentDto.Response createConsultContent(final long id,
      ConsultContentDto.Create dto) {

    dto.setConsultationId(id);

    ConsultContent consultContent = modelMapper.map(dto, ConsultContent.class);

    Account consultant = accountRepository.findById(dto.getConsultantId())
        .orElseThrow(() -> new AccountNotFoundException(dto.getConsultantId()));

    Consultation consultation = consultationRepository.findByIdAndType(id, dto.getType());


    consultContent
        .setConsultation(this.getConsultation(dto.getConsultationId(), consultation.getType()));

    consultContent.setConsultant(consultant);
    consultContent.setConsultDate(LocalDate.now());
    consultContent.setConsultTime(LocalTime.now());
    consultContent.setConsultationType(consultation.getType());

    ConsultationHistoryDto.Create historyDto = ConsultationHistoryDto.Create.builder()
        .consultantId(consultant.getId())
        .consultationId(consultation.getId())
        .logDateTime(LocalDateTime.now())
        .status(consultation.getStatus())
        .logComment(commonMessage.getMessage(
            consultation.getType().equals(Type.BASIC_CONSULTATION)
                ? "consultation.history.insert-content"
                : "consultation.history.insert-pro-content" ))
        .build();

    consultationHistoryService
        .createHistory(historyDto, consultation.getStatus(), "insert");

    return toResponse(consultContentRepository.save(consultContent));
  }

  public List<ConsultContentDto.Response> getConsultContentList(final Long consultationId, final Type type) {
    Consultation consultation = consultationRepository.findById(consultationId).orElse(null);
    List<ConsultContent> consultContents = null;
    if(consultation != null) {
      consultContents = consultContentRepository
          .findAllByConsultationAndConsultationType(consultation, type);
      return consultContents.stream()
          .map(this::toResponse)
          .collect(Collectors.toList());
    }
  return null;
  }

  private Response toResponse(ConsultContent consultContent) {
    return modelMapper.map(consultContent, Response.class);
  }

  private Consultation getConsultation(final Long consultationId, Type type) {
    return consultationRepository.findByIdAndType(consultationId, type);
  }

}
