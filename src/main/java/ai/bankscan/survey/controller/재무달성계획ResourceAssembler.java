package ai.bankscan.survey.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import ai.bankscan.survey.dto.재무달성계획Dto;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class 재무달성계획ResourceAssembler implements
    ResourceAssembler<재무달성계획Dto.Response, Resource<재무달성계획Dto.Response>> {

  @Override
  public Resource<재무달성계획Dto.Response> toResource(재무달성계획Dto.Response entity) {
    return new Resource<> (
        entity,
        linkTo(methodOn(SurveyRestController.class)
            .getDetailSurveyAnswer(entity.getAnswerList())).withSelfRel());
  }

}
