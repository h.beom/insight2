package ai.bankscan.organizations.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@DiscriminatorValue("INSTITUTION")
public class Institution extends Organization {

  @Default
  private Type type = Type.INSTITUTION;

  @Override
  public void create() {

  }

  @Override
  public void update() {

  }

  @Override
  public void validate() {

  }
}
