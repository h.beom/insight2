package ai.bankscan.accounts.domain.member;

import ai.bankscan.accounts.domain.AccountBaseRepository;

public interface MemberRepository extends AccountBaseRepository<Member> {

  Member findByCiAndDi(String ci, String di);

}
