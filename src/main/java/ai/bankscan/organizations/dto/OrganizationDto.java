package ai.bankscan.organizations.dto;

import ai.bankscan.accounts.dto.AccountDto;
import ai.bankscan.common.address.dto.AddressDto;
import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.common.infra.jpa.support.SearchDto;
import ai.bankscan.organizations.domain.Organization;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class OrganizationDto {

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "OrganizationDto.Create")
  public abstract static class Create {

    @ApiModelProperty(value = "이름", example = "금융건강지원센터 서울지사", position = 1, required = true)
    private String name;

    @ApiModelProperty(value = "코드", example = "CEN001", position = 2, required = true)
    private String code;

    @ApiModelProperty(value = "사업자등록번호", example = "1234556789", position = 3)
    private String crn;

    @ApiModelProperty(value = "사업자등록증", example = "1", position = 4)
    private Long businessLicense;

    @ApiModelProperty(value = "대표전화번호", example = "0212345678", position = 5)
    private String phoneNumber;

    @JsonUnwrapped
    private AddressDto.Save address;

    @ApiModelProperty(value = "개요", example = "이 오거네이제이션은...", position = 6)
    private String description;

    @ApiModelProperty(value = "로고이미지", example = "1", position = 7)
    private Long logo;

    @ApiModelProperty(value = "url", example = "http://www.naver.com", position = 8)
    private String url;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "OrganizationDto.Update")
  public abstract static class Update {

    @ApiModelProperty(value = "이름", example = "금융건강지원센터 서울지사", position = 1, required = true)
    private String name;

    @ApiModelProperty(value = "코드", example = "CEN001", position = 2, required = true)
    private String code;

    @ApiModelProperty(value = "사업자등록번호", example = "1231212345", position = 3)
    private String crn;

    @ApiModelProperty(value = "사업자등록증", example = "1", position = 4)
    private Long businessLicense;

    @ApiModelProperty(value = "대표전화번호", example = "0212345678", position = 101)
    private String phoneNumber;

    @JsonUnwrapped
    private AddressDto.Save address;

    @ApiModelProperty(value = "개요", example = "이 오거네이제이션은...", position = 101)
    private String description;

    @ApiModelProperty(value = "url", example = "https://naver.com", position = 101)
    private String url;

    @ApiModelProperty(value = "로고이미지", example = "1", position = 4)
    private Long logo;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "OrganizationDto.Response")
  public static class Response {

    @ApiModelProperty(value = "ID", example = "1")
    private Long id;

    @ApiModelProperty(value = "이름", example = "금융건강지원센터 서울지사", position = 1, required = true)
    private String name;

    @ApiModelProperty(value = "코드", example = "CEN001", position = 2, required = true)
    private String code;

    @ApiModelProperty(value = "사업자등록번호", example = "1231212345", position = 3)
    private String crn;

    @ApiModelProperty(value = "사업자등록증", example = "1", position = 4)
    private Long businessLicense;

    @ApiModelProperty(value = "등록자", position = 5)
    private AccountDto.Response createdBy;

    @ApiModelProperty(value = "대표전화번호", example = "0212345678", position = 101)
    private String phoneNumber;

    @ApiModelProperty(value = "url", example = "htttp://www.naver.com", position = 101)
    private String url;

    @JsonUnwrapped
    private AddressDto.Save address;

    @ApiModelProperty(value = "개요", example = "이 오거네이제이션은...", position = 101)
    private String description;

    @ApiModelProperty(value = "로고이미지", example = "1", position = 4)
    private Long logo;

    private LocalDateTime createdAt;

  }


  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "OrganizationDto.Simple")
  public static class Simple {

    @ApiModelProperty(value = "ID", example = "1")
    private Long id;

    @ApiModelProperty(value = "이름", example = "금융건강지원센터 서울지사", position = 1, required = true)
    private String name;

    @ApiModelProperty(value = "코드", example = "CEN001", position = 2, required = true)
    private String code;

    @ApiModelProperty(value = "사업자등록번호", example = "1231212345", position = 3)
    private String crn;

    @ApiModelProperty(value = "사업자등록증", example = "1", position = 4)
    private Long businessLicense;

    @ApiModelProperty(value = "등록자", position = 5)
    private AccountDto.Response manager;

    @ApiModelProperty(value = "대표전화번호", example = "0212345678", position = 101)
    private String phoneNumber;

    @ApiModelProperty(value = "url", example = "htttp://www.naver.com", position = 101)
    private String url;

    @JsonUnwrapped
    private AddressDto.Save address;

    @ApiModelProperty(value = "개요", example = "이 오거네이제이션은...", position = 101)
    private String description;

    @ApiModelProperty(value = "로고이미지", example = "1", position = 4)
    private Long logo;

    private LocalDateTime createdAt;

  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString(callSuper = true)
  @ApiModel(value = "OrganizationDto.Search")
  public abstract static class Search<T extends Organization> extends SearchDto<T> {

    @ApiModelProperty(value = "검색구분", position = 1)
    protected String searchType;

    @ApiModelProperty(value = "검색어", position = 2)
    protected String keyword;

    @Override
    protected Restrictions generateRestrictions() {
      final Restrictions restrictions = new Restrictions();

      if (StringUtils.isNotEmpty(this.keyword)) {
        if (StringUtils.isEmpty(this.searchType)) {
          final Restrictions restrictionsByKeyword = new Restrictions(Restrictions.Conn.OR);
          List<String> keywords = Arrays.asList(this.keyword.trim().split("\\s+"));
          keywords.stream()
              .forEach(keyword -> {
                restrictionsByKeyword.like("name", "%" + keyword + "%");
                restrictionsByKeyword.like("code", "%" + keyword + "%");
                restrictionsByKeyword.like("crn", "%" + keyword + "%");
                this.addSearchKeyword(restrictionsByKeyword, keyword);
              });
          restrictions.addChild(restrictionsByKeyword);
        } else {
          restrictions.like(searchType, "%" + keyword + "%");
        }
      }

      return restrictions;
    }

    protected void addSearchKeyword(final Restrictions restrictions, final String keyword) {}
  }

}
