package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 은행_외화거래내역조회_Dto {

  private String bank;

  private String 계좌번호;
  private String 통화;
  private String 거래일자;
  private String 거래시각;
  private String 입금액;
  private String 출금액;
  private String 잔액;
  private String 적요;
  private String 취급점;
  private String 입금의뢰인명;
  private String 메모;

}
