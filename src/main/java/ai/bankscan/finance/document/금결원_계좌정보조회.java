package ai.bankscan.finance.document;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@ToString
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Table(indexes = {@Index(columnList = "은행명"), @Index(columnList = "계좌번호")})
public class 금결원_계좌정보조회 extends AuditableJoinEntity<Account> {

  @Builder.Default
  private final Boolean 활동성 = Boolean.TRUE;
  private String bank;
  private String 은행명;
  private String 계좌번호;
  private String 지점명;
  private String 상품명;
  private String 개설일;
  private String 최종입출금일;
  private Long 잔고;
  private String 만기일;
  private String 회차;
  private String 부기명;
  private String 비고;
  @Setter
  @Builder.Default
  private Boolean monitor = Boolean.FALSE;
}
