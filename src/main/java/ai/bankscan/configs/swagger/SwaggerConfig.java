package ai.bankscan.configs.swagger;

import static java.util.Collections.singletonList;
import static springfox.documentation.schema.AlternateTypeRules.newRule;

import ai.bankscan.common.infra.security.common.CurrentUser;
import ai.bankscan.common.infra.security.properties.WebSecurityProperties;
import ai.bankscan.configs.properties.AppProperties;
import com.fasterxml.classmate.TypeResolver;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.OAuthBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.GrantType;
import springfox.documentation.service.ResourceOwnerPasswordCredentialsGrant;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author eomjeongjae
 * @since 2019/11/18
 */
// @Profile({"local", "dev"})
@EnableSwagger2
@RequiredArgsConstructor
@Configuration
@ComponentScan(basePackages = "ai.bankscan")
public class SwaggerConfig {

    private final TypeResolver typeResolver;
    private final AppProperties appProperties;
    private final WebSecurityProperties webSecurityProperties;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .host(appProperties.getHost())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.ant("/api/**"))
                .build()
                .directModelSubstitute(LocalDate.class, String.class)
                .genericModelSubstitutes(ResponseEntity.class)
                .alternateTypeRules(
                        newRule(typeResolver.resolve(DeferredResult.class,
                                typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                                typeResolver.resolve(WildcardType.class)
                        ))
                .useDefaultResponseMessages(false)
                .ignoredParameterTypes(
                        Pageable.class,
                        PagedResourcesAssembler.class,
                        AuthenticationPrincipal.class,
                        CurrentUser.class
                )
                .securitySchemes(Arrays.asList(securityScheme(), tokenSecurityScheme()))
                .securityContexts(Arrays.asList(securityContext(), tokenSecurityContext()))
                .apiInfo(getApiInfo());
    }

    @Bean
    public SecurityConfiguration securityConfiguration() {
        return SecurityConfigurationBuilder.builder()
                .clientId(webSecurityProperties.getOauth().getClientId())
                .clientSecret(webSecurityProperties.getOauth().getClientSecret())
                .scopeSeparator("password refresh_token client_credentials")
                .useBasicAuthenticationWithAccessCodeGrant(true)
                .build();
    }

    private ApiInfo getApiInfo() {
        return new ApiInfo(
                "BankScan Application API",
                "",
                "",
                "",
                null,
                "",
                "",
                Collections.emptyList()
        );
    }

    private SecurityScheme securityScheme() {
        GrantType grantType = new ResourceOwnerPasswordCredentialsGrant("/api/auth/token");

        return new OAuthBuilder().name("bankscan_login")
                .grantTypes(singletonList(grantType))
                .build();
    }

    private springfox.documentation.spi.service.contexts.SecurityContext securityContext() {
        return springfox.documentation.spi.service.contexts.SecurityContext.builder()
                .securityReferences(
                        singletonList(new SecurityReference("bankscan_login", scopes()))
                )
                .forPaths(PathSelectors.ant("/api/**"))
                .build();
    }

    private SecurityScheme tokenSecurityScheme() {
        return new ApiKey("bankscan_token", "x-bankscan-token", "header");
    }

    private SecurityContext tokenSecurityContext() {
        return springfox.documentation.spi.service.contexts.SecurityContext.builder()
                .securityReferences(
                        singletonList(new SecurityReference("bankscan_token", scopes()))
                )
                .forPaths(PathSelectors.ant("/api/**"))
                .build();
    }

    private AuthorizationScope[] scopes() {
        return new AuthorizationScope[]{
                new AuthorizationScope("any", "for any operations")};
    }

}
