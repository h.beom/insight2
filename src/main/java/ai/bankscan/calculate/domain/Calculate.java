package ai.bankscan.calculate.domain;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.consultants.Consultant;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import ai.bankscan.consultations.domain.consulting.Consultation;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class Calculate extends AuditableJoinEntity<Account> {

  @ManyToOne
  private Consultation consultation;

  @ManyToOne
  private Consultant consultant;

  @Default
  private boolean isCalculate = false;

}
