package ai.bankscan.survey.domain;

import ai.bankscan.accounts.domain.member.Member;
import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface 희망노후생활비Repository extends JpaRepository<희망노후생활비, Long> ,
    JpaSpecificationExecutor<희망노후생활비> {

  List<희망노후생활비> findAllByCreatedBy(Member member, Sort sort);



}
