package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 은행_펀드거래내역조회_Dto {

  private String bank;
  private String 계좌번호;
  private String 거래일자;
  private String 거래시각;
  private String 거래유형;
  private String 적요;
  private String 종목명;
  private String 수량;
  private Long 거래금액;
  private String 수수료;
  private String 금잔수량;
  private String 금잔금액;
  private String 기준가격;
  private String 기준가격적용일;
  private String 대금결제일;
  private String 기재내용;
  private String 거래점;
}
