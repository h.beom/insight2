package ai.bankscan.organizations.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import ai.bankscan.organizations.dto.InstitutionDto.Response;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
class InstitutionResourceAssembler implements ResourceAssembler<Response, Resource<Response>> {

  @Override
  public Resource<Response> toResource(Response entity) {
    return new Resource<>(
        entity,
        linkTo(methodOn(InstitutionRestController.class).getInstitution(entity.getId())).withSelfRel()
    );

  }
}
