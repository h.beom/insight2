package ai.bankscan.survey.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import net.sf.json.JSONObject;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class 재무달성계획Dto {


  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "재무달성계획Dto.Create")
  public static class Create {

    @ApiModelProperty(value = "답안 리스트",
        example = "[ "
            + "{         "
            + "'key': 0, "
            + "'question': '현재 금융관련 고민을 선택하여주세요. (3가지 선택 가능)', "
            + "'answers' : [ "
            + "{'key': 0, 'answer': '돈 관리 어려움', 'result': '지출', 'result2': null }, "
            + "{'key': 3, 'answer': '신용카드 사용관리', 'result': '지출', 'result2': '신용' }, "
            + "{'key': 8, 'answer': '신용도 하락', 'result': '부채', 'result2': '신용' }"
            + "] "
            + "}, "
            + "{ "
            + "'key': 1, "
            + "'question': '현재 생활에 닥친 어려움을 선택해주세요 (제한 없음)', "
            + "'answers' : [ "
            + "{'key': 8, 'answer': '하우스 푸어', 'result': '부채', 'result2': null }, "
            + "{'key': 9, 'answer': '장기 연체', 'result': '지출', 'result2': null }, "
            + "{'key': 10, 'answer': '고리대출', 'result': '부채', 'result2': null }"
            + "] "
            + "}, "
            + "{ "
            + "'key': 2, "
            + "'question': '현재 필요한 금융 활동을 선택하여 주세요. (3가지 선택 가능)', "
            + "'answers' : [ "
            + "{'key': 0, 'answer': '부채탈출', 'result': '부채', 'result2': null }, "
            + "{'key': 1, 'answer': '금융비용절감', 'result': '지출', 'result2': null }, "
            + "{'key': 2, 'answer': '신용등급관리', 'result': '부채', 'result2': '신용' }"
            + "] "
            + "}"
            + "]",
        position = 1)
    private List<재무달성계획AnswersDto.Response> answerList;

    @ApiModelProperty(value = "결과", example = "소비지출형", position = 2)
    private String result;


    @ApiModelProperty(value = "결과2", example = "[{}]", position = 3)
    private String result2;


  }




  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "DetailSurveyDto.Child")
  public static class Child {

    @ApiModelProperty(value = "key", example = "0", position = 1)
    private long key;

    @ApiModelProperty(value = "학령", example = "초등학생 이하", position = 2)
    private String answer;

    @ApiModelProperty(value = "인원", example = "1", position = 3)
    private long count;

  }


  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "DetailSurveyDto.Response")
  public static class Response {

    @ApiModelProperty(value = "답안 리스트", example = "[]", position = 1)
    private List<재무달성계획AnswersDto.Response> answerList;

    @ApiModelProperty(value = "결과", example = "소비지출형", position = 2)
    private String result;

    @ApiModelProperty(value = "결과", example = "소비지출형", position = 3)
    private JSONObject result2;

    private List<JSONObject> target;

    @ApiModelProperty(value = "생성일시", position = 4)
    private LocalDateTime createdAt;

  }

}
