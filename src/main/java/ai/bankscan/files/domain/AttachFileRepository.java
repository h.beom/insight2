package ai.bankscan.files.domain;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author eomjeongjae
 * @since 2019/10/15
 */
public interface AttachFileRepository extends JpaRepository<AttachFile, Long> {

}
