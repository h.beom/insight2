package ai.bankscan.finance.document;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface 카드_카드목록조회_Repository extends JpaRepository<카드_카드목록조회, Long>,
    JpaSpecificationExecutor<카드_카드목록조회> {

}
