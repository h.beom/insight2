package ai.bankscan.posts.controller;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.annotations.ApiPageable;
import ai.bankscan.common.infra.security.common.CurrentUser;
import ai.bankscan.posts.domain.Notice.Category;
import ai.bankscan.posts.dto.NoticeDto;
import ai.bankscan.posts.dto.NoticeDto.Response;
import ai.bankscan.posts.service.NoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.net.URI;
import java.net.URISyntaxException;
import javax.validation.Valid;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Api(tags = {"공지사항 관리 API"})
@RequiredArgsConstructor
@RestController
@RequestMapping(path = NoticeRestController.REQUEST_BASE_PATH, produces = MediaTypes.HAL_JSON_VALUE)
public class NoticeRestController {

    static final String REQUEST_BASE_PATH = "/api/notices";

    private final NoticeService noticeService;
    private final NoticeResourceAssembler noticeResourceAssembler;

    @ApiOperation(value = "공지사항 등록")
    @Secured("ROLE_ADMIN")
    @PostMapping
    ResponseEntity<Resource<Response>> createPost(
            @RequestBody @Valid NoticeDto.Create dto
    ) throws URISyntaxException {
        Response savedPost = noticeService.createPost(dto);
        Resource<Response> resource = noticeResourceAssembler.toResource(savedPost);

        return ResponseEntity
                .created(new URI(resource.getLink(Link.REL_SELF).getHref()))
                .body(resource);
    }

    @ApiPageable
    @ApiOperation(value = "공지사항 목록 조회")
    @GetMapping
    Resources<Resource<Response>> getPosts(
            @Valid NoticeDto.Search search,
            @PageableDefault(sort = "createdAt", direction = Direction.DESC) Pageable pageable,
            PagedResourcesAssembler<Response> pagedResourcesAssembler,
            @CurrentUser final Account currentUser
    ) {
        search.setDisplay(true);

        if (currentUser != null) {
            if (!currentUser.isAdminRole() ||
                    !currentUser.isCenterManageRole() ||
                    !currentUser.isRelationshipManagerRole() ||
                    !currentUser.isConsultantRole() ||
                    !currentUser.isProConsultantRole()) {
                search.setCategory(Category.NOTICE);
            }
        }
        Page<Response> page = noticeService.getPosts(search, pageable);
        return pagedResourcesAssembler.toResource(page, noticeResourceAssembler);
    }
    @ApiOperation(value = "게시글 조회", notes = "TYPE: NOTICE(공지사항)")
    @GetMapping("/{id}")
    Resource<Response> getPost(
            @ApiParam(required = true, example = "1") @PathVariable final Long id
    ) {
        return noticeResourceAssembler.toResource(noticeService.getPost(id));
    }

    @ApiOperation(value = "공지사항 수정")
    @Secured("ROLE_ADMIN")
    @PutMapping("/{id}")
    ResponseEntity updatePost(
            @ApiParam(required = true, example = "1") @PathVariable final Long id,
            @RequestBody @Valid NoticeDto.Update dto,
            @CurrentUser final Account currentUser
    ) throws URISyntaxException {
        Response savedPost = noticeService.updatePost(id, dto);
        Resource<Response> resource = noticeResourceAssembler.toResource(savedPost);

        return ResponseEntity
                .created(new URI(resource.getLink(Link.REL_SELF).getHref()))
                .body(resource);
    }

    @ApiOperation(value = "공지사항 삭제")
    @Secured("ROLE_ADMIN")
    @DeleteMapping("/{id}")
    ResponseEntity<?> deletePost(
            @ApiParam(required = true, example = "1") @PathVariable final Long id
    ) {
        noticeService.deletePost(id);
        return ResponseEntity
                .noContent()
                .build();
    }

}
