package ai.bankscan.accounts.domain.consultants;

import ai.bankscan.accounts.domain.Account.Type;
import ai.bankscan.accounts.domain.AccountBaseRepository;

public interface ConsultantRepository extends AccountBaseRepository<Consultant> {

  Consultant findByIdAndType(final Long id, Type type);
}
