package ai.bankscan.messages.domain;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.messages.domain.NotificationMessage.Status;
import ai.bankscan.messages.domain.NotificationMessage.Type;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author eomjeongjae
 * @since 2019/12/16
 */
public interface NotificationMessageRepository extends JpaRepository<NotificationMessage, Long>,
    JpaSpecificationExecutor<NotificationMessage> {

  List<NotificationMessage> findAllByStatusAndScheduledDateTimeNotNull(Status status);

  void deleteByTypeAndRelatedIdAndStatusAndReceiver(Type type, Long relatedId, Status status, Account receiver);

}
