package ai.bankscan.files.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

public class ExcelDataDto {
    @Getter
    @RequiredArgsConstructor
    public enum  Consultants {
        상담원ID("username"),
        비밀번호("password"),
        이름("name"),
        생년월일("birthday"),
        휴대폰번호("mobilePhone"),
        이메일("email"),
        주소("address"),
        상세주소("detailedAddress"),
        우편번호("zipCode"),
        센터명("centerName"),
        센터코드("centerCode"),
        근무형태("workerType"),
        교육수료등급("educationCompletionLevel"),
        상담원레벨("consultantLevel"),
        전문분야("professionalField"),
        상담단가("consultationPrice");


        private final String title;

    }
}
