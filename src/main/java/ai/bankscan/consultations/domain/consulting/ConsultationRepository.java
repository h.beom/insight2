package ai.bankscan.consultations.domain.consulting;


import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.consultations.domain.consulting.Consultation.Type;
import ai.bankscan.organizations.domain.Center;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface ConsultationRepository extends JpaRepository<Consultation, Long>,
    JpaSpecificationExecutor<Consultation> {

  Page<Consultation> findAllByConsultant(Account currentUser, Pageable pageable);

  List<Consultation> findAll(Specification<Consultation> toSpecification, Sort createAt);

  List<Consultation> findAllByCreatedBy(Member member);

  Long countByCenter(Center center);

  Consultation findByIdAndType(final Long id, Type type);

  //LEFT JOIN FETCH r.browser LEFT JOIN FETCH r.runner
  @Query("SELECT DISTINCT c FROM Consultation c LEFT JOIN FETCH c.createdBy LEFT OUTER JOIN FETCH c.center LEFT OUTER JOIN FETCH c.institution LEFT OUTER JOIN c.금융건강진단표 LEFT OUTER JOIN c.consultant LEFT OUTER JOIN c.proConsultant WHERE c.id = :id")
  Optional<Consultation> findByIdWithData(@Param("id") Long id);
}
