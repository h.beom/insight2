package ai.bankscan.finance.dto;

import io.swagger.annotations.ApiModel;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PropertyPricesDto {

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "PropertyPricesDto.Create")
  public static class Create {

    private String searchGubun;
    private String sidoName;
    private String gugunName;
    private String dongName;
    private String dongCode;
    private String danjiName;
    private String bunJi;
    private String bonNo;
    private String buNo;
    private String roadNm;

  }

}
