package ai.bankscan.finance.service;

import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.finance.document.국세청_소득금액증명;
import ai.bankscan.finance.document.국세청_소득금액증명_Repository;
import ai.bankscan.finance.document.국세청_소득세액공제조회발급;
import ai.bankscan.finance.document.국세청_소득세액공제조회발급_Repository;
import ai.bankscan.finance.document.국세청_소득세액공제조회발급_보험료;
import ai.bankscan.finance.document.국세청_소득세액공제조회발급_보험료_Repository;
import ai.bankscan.finance.document.금결원_계좌정보조회;
import ai.bankscan.finance.document.금결원_계좌정보조회_Repository;
import ai.bankscan.finance.document.보험_가입특약사항조회;
import ai.bankscan.finance.document.보험_가입특약사항조회_Repository;
import ai.bankscan.finance.document.보험_계약내용조회;
import ai.bankscan.finance.document.보험_계약내용조회_Repository;
import ai.bankscan.finance.document.연금_예상연금액조회;
import ai.bankscan.finance.document.연금_예상연금액조회_Repository;
import ai.bankscan.finance.document.은행_대출거래내역조회;
import ai.bankscan.finance.document.은행_대출거래내역조회_Repository;
import ai.bankscan.finance.document.은행_대출계좌조회;
import ai.bankscan.finance.document.은행_대출계좌조회_Repository;
import ai.bankscan.finance.document.은행_수시거래내역조회;
import ai.bankscan.finance.document.은행_수시거래내역조회_Repository;
import ai.bankscan.finance.document.은행_수시전계좌조회;
import ai.bankscan.finance.document.은행_수시전계좌조회_Repository;
import ai.bankscan.finance.document.은행_예적금거래내역조회;
import ai.bankscan.finance.document.은행_예적금거래내역조회_Repository;
import ai.bankscan.finance.document.은행_예적금계좌조회;
import ai.bankscan.finance.document.은행_예적금계좌조회_Repository;
import ai.bankscan.finance.document.은행_외화거래내역조회;
import ai.bankscan.finance.document.은행_외화거래내역조회_Repository;
import ai.bankscan.finance.document.은행_외화계좌조회;
import ai.bankscan.finance.document.은행_외화계좌조회_Repository;
import ai.bankscan.finance.document.은행_펀드거래내역조회;
import ai.bankscan.finance.document.은행_펀드거래내역조회_Repository;
import ai.bankscan.finance.document.은행_펀드계좌조회;
import ai.bankscan.finance.document.은행_펀드계좌조회_Repository;
import ai.bankscan.finance.document.증권_증권보유계좌조회;
import ai.bankscan.finance.document.증권_증권보유계좌조회_Repository;
import ai.bankscan.finance.document.카드_장기카드대출_카드론거래내역;
import ai.bankscan.finance.document.카드_장기카드대출_카드론거래내역_Repository;
import ai.bankscan.finance.document.카드_장기카드대출_카드론이용내역;
import ai.bankscan.finance.document.카드_장기카드대출_카드론이용내역_Repository;
import ai.bankscan.finance.document.카드_카드목록조회;
import ai.bankscan.finance.document.카드_카드목록조회_Repository;
import ai.bankscan.finance.document.카드_카드승인내역;
import ai.bankscan.finance.document.카드_카드승인내역_Repository;
import ai.bankscan.finance.document.카드_카드청구내역;
import ai.bankscan.finance.document.카드_카드청구내역_Repository;
import ai.bankscan.finance.document.카드_포인트내역;
import ai.bankscan.finance.document.카드_포인트내역_Repository;
import ai.bankscan.finance.domain.CooconLog;
import ai.bankscan.finance.domain.CooconLogRepository;
import ai.bankscan.finance.domain.House;
import ai.bankscan.finance.domain.HouseRepository;
import ai.bankscan.finance.dto.HouseDto;
import ai.bankscan.finance.dto.국세청_소득금액증명_Dto;
import ai.bankscan.finance.dto.국세청_소득세액공제조회발급_Dto;
import ai.bankscan.finance.dto.국세청_소득세액공제조회발급_보험료_Dto;
import ai.bankscan.finance.dto.금결원_계좌정보조회_Dto;
import ai.bankscan.finance.dto.보험_가입특약사항조회_Dto;
import ai.bankscan.finance.dto.보험_계약내용조회_Dto;
import ai.bankscan.finance.dto.연금_예상연금액조회_Dto;
import ai.bankscan.finance.dto.은행_대출거래내역조회_Dto;
import ai.bankscan.finance.dto.은행_대출계좌조회_Dto;
import ai.bankscan.finance.dto.은행_수시거래내역조회_Dto;
import ai.bankscan.finance.dto.은행_수시전계좌조회_Dto;
import ai.bankscan.finance.dto.은행_예적금거래내역조회_Dto;
import ai.bankscan.finance.dto.은행_예적금계좌조회_Dto;
import ai.bankscan.finance.dto.은행_외화거래내역조회_Dto;
import ai.bankscan.finance.dto.은행_외화계좌조회_Dto;
import ai.bankscan.finance.dto.은행_펀드거래내역조회_Dto;
import ai.bankscan.finance.dto.은행_펀드계좌조회_Dto;
import ai.bankscan.finance.dto.증권_증권보유계좌조회_Dto;
import ai.bankscan.finance.dto.카드_장기카드대출_카드론거래내역_Dto;
import ai.bankscan.finance.dto.카드_장기카드대출_카드론이용내역_Dto;
import ai.bankscan.finance.dto.카드_카드목록조회_Dto;
import ai.bankscan.finance.dto.카드_카드승인내역_Dto;
import ai.bankscan.finance.dto.카드_카드청구내역_Dto;
import ai.bankscan.finance.dto.카드_포인트내역_Dto;
import com.google.common.collect.Lists;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class CooconService {

  private final ModelMapper mapper;
  private final EntityManager entityManager;
  private final CooconLogRepository cooconLogRepository;
  private final HouseRepository houseRepository;

  private final 금결원_계좌정보조회_Repository _금결원_계좌정보조회_Repository;

  private final 국세청_소득금액증명_Repository _국세청_소득금액증명_Repository;
  private final 국세청_소득세액공제조회발급_Repository _국세청_소득세액공제조회발급_Repository;
  private final 국세청_소득세액공제조회발급_보험료_Repository _국세청_소득세액공제조회발급_보험료_Repository;

  private final 은행_예적금계좌조회_Repository _은행_예적금계좌조회_Repository;
  private final 은행_예적금거래내역조회_Repository _은행_예적금거래내역조회_Repository;
  private final 은행_수시전계좌조회_Repository _은행_수시전계좌조회_Repository;
  private final 은행_수시거래내역조회_Repository _은행_수시거래내역조회_Repository;
  private final 은행_펀드계좌조회_Repository _은행_펀드계좌조회_Repository;
  private final 은행_펀드거래내역조회_Repository _은행_펀드거래내역조회_Repository;
  private final 은행_대출계좌조회_Repository _은행_대출계좌조회_Repository;
  private final 은행_대출거래내역조회_Repository _은행_대출거래내역조회_Repository;
  private final 은행_외화계좌조회_Repository _은행_외화계좌조회_Repository;
  private final 은행_외화거래내역조회_Repository _은행_외화거래내역조회_Repository;

  private final 카드_카드목록조회_Repository _카드_카드목록조회_Repository;
  private final 카드_카드승인내역_Repository _카드_카드승인내역_Repository;
  private final 카드_카드청구내역_Repository _카드_카드청구내역_Repository;
  private final 카드_포인트내역_Repository _카드_포인트내역_Repository;
  private final 카드_장기카드대출_카드론거래내역_Repository _카드_장기카드대출_카드론거래내역_Repository;
  private final 카드_장기카드대출_카드론이용내역_Repository _카드_장기카드대출_카드론이용내역_Repository;

  private final 보험_계약내용조회_Repository _보험_계약내용조회_Repository;
  private final 보험_가입특약사항조회_Repository _보험_가입특약사항조회_Repository;

  private final 증권_증권보유계좌조회_Repository _증권_증권보유계좌조회_Repository;

  private final 연금_예상연금액조회_Repository _연금_예상연금액조회_repository;

  private final List<String> IGNORE_CODES = Lists.newArrayList(
      "",
      "42110000",
      "80002E10", // 업무가능한 시간대가 아닙니다. 업무시간대에 다시 거래하십시오.
      "80002F30", // 해당 기관의 점검 또는 서버 장애입니다. 잠시 후 다시 이용하십시오.
      "8000D502", // 가입한 보험이 없는 보험사입니다. 확인 후 등록하시기 바랍니다.
      "8000D504", // 보험사 회원가입이나 인증서 등록 후 사용하시기 바랍니다.
      "80002E23", // 약정 정보가 없습니다. 해당 기관 홈페이지에 접속하셔서 확인 후 거래해 주십시오
      "80004016", // 삼성카드 회원에 한해 이용 가능합니다. 삼성카드가 없는 경우 아이디/비밀번호 로그인을 이용해 주시기 바랍니다.
      "80003116", // 사용(조회)가능등급 아닙니다.(본인 명의로 이동전화를 등록한 회원 이상 사용 가능). 확인 후 다시 거래하십시오.
      "80004107", // (타기관)인증서가 등록되지 않았습니다. 해당 사이트 방문하시어 (타기관)인증서 등록 후 이용해 주시기 바랍니다.
      "8000d502", // 가입한 보험이 없는 보험사입니다. 확인 후 등록하시기 바랍니다.
      ""
  );

  public JSONObject getDateRange(Member member) {

    LocalDate maxDate = LocalDate.now();
    LocalDate minDate = LocalDate.of(maxDate.getYear() - 3, 1, 1);

    JSONArray 기본 = new JSONArray();
    기본.add(minDate.format(DateTimeFormatter.ofPattern("yyyyMMdd")));
    기본.add(maxDate.format(DateTimeFormatter.ofPattern("yyyyMMdd")));

    JSONObject output = new JSONObject();
    output.put("기본", 기본);

    Integer days = Integer.parseInt(maxDate.format(DateTimeFormatter.ofPattern("MMdd")), 10);
    output.put("banks", _금결원_계좌정보조회_Repository.getBanks(member.getId()));

    {
      // 소득금액 관련
      JSONObject 국세청_소득금액 = new JSONObject();

      List<국세청_소득금액증명_Dto.GroupBy> items = entityManager
          .createNamedQuery("국세청_소득금액증명.GroupBy_Query")
          .setParameter("created_by_id", member.getId())
          .getResultList();

      Map<String, String> mindts = new HashMap<>();
      items.stream().forEach(v -> mindts.put(v.getType(), v.getYear()));

      String data1min = String.valueOf(maxDate.getYear() - 4);
      String data1max = String.valueOf(maxDate.getYear() - (days > 201 ? 1 : 2));
      국세청_소득금액.put("1", Arrays.asList(mindts.getOrDefault("1", data1min), data1max));

      String data2min = String.valueOf(maxDate.getYear() - 4);
      String data2max = String.valueOf(maxDate.getYear() - (days > 715 ? 1 : 2));

      국세청_소득금액.put("2", Arrays.asList(mindts.getOrDefault("2", data2min), data2max));
      국세청_소득금액.put("3", Arrays.asList(mindts.getOrDefault("3", data2min), data2max));

      output.put("국세청_소득금액", 국세청_소득금액);
    }

    {
      String data1min = String.valueOf(maxDate.getYear() - 4);
      String data1max = String.valueOf(maxDate.getYear() - (days > 201 ? 1 : 2));

      String user1min = _국세청_소득세액공제조회발급_Repository.getMaxYear(member.getId());

      output.put("국세청_소득세액", Arrays.asList(user1min != null ? user1min : data1min, data1max));
    }

    {
      // 은행_수시전계좌조회 최종일자 가져오기
      Map<String, Map<String, String>> result = new HashMap();
      List<은행_수시전계좌조회_Dto.GroupBy> items = entityManager
          .createNamedQuery("은행_수시전계좌조회.GroupBy_Query")
          .setParameter("created_by_id", member.getId())
          .getResultList();

      items.stream().forEach(v -> {
        if (!result.containsKey(v.getBank())) {
          result.put(v.getBank(), new HashMap());
        }
        result.get(v.getBank()).put(v.get계좌번호(), v.getDate());
      });
      output.put("은행_수시전계좌조회", result);
    }

    {
      // 은행_예금적금계좌조회 최종일자 가져오기
      Map<String, Map<String, String>> result = new HashMap();
      List<은행_예적금계좌조회_Dto.GroupBy> items = entityManager
          .createNamedQuery("은행_예적금계좌조회.GroupBy_Query")
          .setParameter("created_by_id", member.getId())
          .getResultList();

      items.stream().forEach(v -> {
        if (!result.containsKey(v.getBank())) {
          result.put(v.getBank(), new HashMap());
        }
        result.get(v.getBank()).put(v.get계좌번호(), v.getDate());
      });
      output.put("은행_예적금계좌조회", result);
    }

    {
      // 은행_펀드계좌조회 최종일자 가져오기
      Map<String, Map<String, String>> result = new HashMap();
      List<은행_펀드계좌조회_Dto.GroupBy> items = entityManager
          .createNamedQuery("은행_펀드계좌조회.GroupBy_Query")
          .setParameter("created_by_id", member.getId())
          .getResultList();

      items.stream().forEach(v -> {
        if (!result.containsKey(v.getBank())) {
          result.put(v.getBank(), new HashMap());
        }
        result.get(v.getBank()).put(v.get계좌번호(), v.getDate());
      });
      output.put("은행_펀드계좌조회", result);
    }

    {
      // 은행_대출계좌조회 최종일자 가져오기
      Map<String, Map<String, String>> result = new HashMap();
      List<은행_대출계좌조회_Dto.GroupBy> items = entityManager
          .createNamedQuery("은행_대출계좌조회.GroupBy_Query")
          .setParameter("created_by_id", member.getId())
          .getResultList();

      items.stream().forEach(v -> {
        if (!result.containsKey(v.getBank())) {
          result.put(v.getBank(), new HashMap());
        }
        result.get(v.getBank()).put(v.get계좌번호(), v.getDate());
      });
      output.put("은행_대출계좌조회", result);
    }

    {
      String data1min = minDate.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
      String data1max = maxDate.format(DateTimeFormatter.ofPattern("yyyyMMdd"));

      String user1min = _카드_카드승인내역_Repository.getMaxDate(member.getId());
      output.put("카드_카드승인내역", Arrays.asList(user1min != null ? user1min : data1min, data1max));
    }

    {
      String data1min = minDate.format(DateTimeFormatter.ofPattern("yyyyMM"));
      String data1max = maxDate.format(DateTimeFormatter.ofPattern("yyyyMM"));

      String user1min = _카드_카드청구내역_Repository.getMaxDate(member.getId());
      output.put("카드_카드청구내역", Arrays.asList(user1min != null ? user1min : data1min, data1max));
    }

    return output;
  }

  private boolean isError(Map<String, Object> output) {
    return (!output.getOrDefault("ErrorCode", "00000000").equals("00000000"));
  }

  @Transactional
  public void mergeObject(JSONObject o, Member member) throws Exception {
    String mode = o.get("Class").toString() + "*" + o.get("Job");

    JSONObject input = o.getJSONObject("Input");
    JSONObject output = o.getJSONObject("Output");

    if (isError(output) && !IGNORE_CODES.contains(output.getOrDefault("ErrorCode", "00000000"))) {
      cooconLogRepository.save(new CooconLog(o));
    }

    if (output == null || isError(output)) {
      return;
    }

    JSONObject result = output.getJSONObject("Result");
    if (mode.equals("PC조회발급서비스*소득금액증명")) {
      JSONArray 소득금액상세들 = result.getJSONArray("소득금액상세");

      for (Object row : 소득금액상세들) {
        국세청_소득금액증명_Dto.Create col = mapper.map(row, 국세청_소득금액증명_Dto.Create.class);
        col.setType(String.valueOf(input.get("증명구분")));
        System.out.println(col.getType());

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("type", col.getType());
        r.eq("귀속연도", col.get귀속연도());
        r.eq("소득구분", col.get소득구분());
        r.eq("사업자등록번호", col.get사업자등록번호());

        국세청_소득금액증명 x = _국세청_소득금액증명_Repository
            .findOne(r.output()).orElse(new 국세청_소득금액증명());
        mapper.map(col, x);

        System.out.println(x);

        _국세청_소득금액증명_Repository.save(x);
      }
    } else if (mode.startsWith("PC조회발급서비스*소득세액공제조회발급_")) {
      국세청_소득세액공제조회발급_Dto col = mapper.map(result, 국세청_소득세액공제조회발급_Dto.class);
      col.set귀속연도(input.get("귀속년도").toString());
      col.setExtra(JSONObject.fromObject(result).toString());

      Restrictions r = new Restrictions();
      r.eq("createdBy", member);
      r.eq("귀속연도", col.get귀속연도());

      if (mode.equals("PC조회발급서비스*소득세액공제조회발급_건강보험")) {
        col.set종류("건강보험");
        r.eq("종류", col.get종류());

        국세청_소득세액공제조회발급 x = _국세청_소득세액공제조회발급_Repository
            .findOne(r.output()).orElse(new 국세청_소득세액공제조회발급());
        mapper.map(col, x);
        _국세청_소득세액공제조회발급_Repository.save(x);
      } else if (mode.equals("PC조회발급서비스*소득세액공제조회발급_국민연금")) {
        col.set종류("국민연금");
        r.eq("종류", col.get종류());

        국세청_소득세액공제조회발급 x = _국세청_소득세액공제조회발급_Repository
            .findOne(r.output()).orElse(new 국세청_소득세액공제조회발급());
        mapper.map(col, x);
        _국세청_소득세액공제조회발급_Repository.save(x);
      } else if (mode.startsWith("PC조회발급서비스*소득세액공제조회발급_기부금")) {
        col.set종류("기부금");
        r.eq("종류", col.get종류());

        국세청_소득세액공제조회발급 x = _국세청_소득세액공제조회발급_Repository
            .findOne(r.output()).orElse(new 국세청_소득세액공제조회발급());
        mapper.map(col, x);
        _국세청_소득세액공제조회발급_Repository.save(x);
      } else if (mode.startsWith("PC조회발급서비스*소득세액공제조회발급_주택자금")) {
        col.set종류("주택자금");
        r.eq("종류", col.get종류());

        국세청_소득세액공제조회발급 x = _국세청_소득세액공제조회발급_Repository
            .findOne(r.output()).orElse(new 국세청_소득세액공제조회발급());
        mapper.map(col, x);
        _국세청_소득세액공제조회발급_Repository.save(x);
      } else if (mode.startsWith("PC조회발급서비스*소득세액공제조회발급_주택저축")) {
        col.set종류("주택저축");
        r.eq("종류", col.get종류());

        국세청_소득세액공제조회발급 x = _국세청_소득세액공제조회발급_Repository
            .findOne(r.output()).orElse(new 국세청_소득세액공제조회발급());
        mapper.map(col, x);
        _국세청_소득세액공제조회발급_Repository.save(x);
      } else if (mode.startsWith("PC조회발급서비스*소득세액공제조회발급_교육비")) {
        col.set종류("교육비");
        r.eq("종류", col.get종류());

        국세청_소득세액공제조회발급 x = _국세청_소득세액공제조회발급_Repository
            .findOne(r.output()).orElse(new 국세청_소득세액공제조회발급());
        mapper.map(col, x);
        _국세청_소득세액공제조회발급_Repository.save(x);
      } else if (mode.startsWith("PC조회발급서비스*소득세액공제조회발급_의료비")) {
        col.set종류("의료비");
        r.eq("종류", col.get종류());

        국세청_소득세액공제조회발급 x = _국세청_소득세액공제조회발급_Repository
            .findOne(r.output()).orElse(new 국세청_소득세액공제조회발급());
        mapper.map(col, x);
        _국세청_소득세액공제조회발급_Repository.save(x);
      } else if (mode.startsWith("PC조회발급서비스*소득세액공제조회발급_보험료")) {
        col.set종류("보험료");
        r.eq("종류", col.get종류());

        국세청_소득세액공제조회발급 x = _국세청_소득세액공제조회발급_Repository
            .findOne(r.output()).orElse(new 국세청_소득세액공제조회발급());
        mapper.map(col, x);
        _국세청_소득세액공제조회발급_Repository.save(x);

        for (Object s1 : result.getJSONArray("보험료내역")) {
          JSONArray s1j = ((JSONObject) s1).getJSONArray("인별상세내역");

          for (int n = 0; n < s1j.size(); n++) {
            JSONObject s2 = s1j.getJSONObject(n);

            국세청_소득세액공제조회발급_보험료_Dto sdto = mapper.map(s2, 국세청_소득세액공제조회발급_보험료_Dto.class);
            sdto.set상위_귀속연도(col.get귀속연도());
            sdto.set상위_종류(col.get종류());
            sdto.set상위_인덱스(n);

            Restrictions r2 = new Restrictions();
            r2.eq("createdBy", member);
            r2.eq("상위_귀속연도", sdto.get상위_귀속연도());
            r2.eq("상위_종류", sdto.get상위_종류());
            r2.eq("상위_인덱스", sdto.get상위_인덱스());

            국세청_소득세액공제조회발급_보험료 x2 = _국세청_소득세액공제조회발급_보험료_Repository
                .findOne(r2.output())
                .orElse(new 국세청_소득세액공제조회발급_보험료());
            mapper.map(sdto, x2);
            _국세청_소득세액공제조회발급_보험료_Repository.save(x2);
          }
        }
      }
    } else if (mode.equals("개인뱅킹*수시전계좌조회")) {
      List<Object> rows = result.getJSONArray("수시전계좌조회");
      for (Object row : rows) {
        은행_수시전계좌조회_Dto.Create col = mapper.map(row, 은행_수시전계좌조회_Dto.Create.class);
        col.setBank(o.get("Module").toString());

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("bank", col.getBank());
        r.eq("계좌번호", col.get계좌번호());

        은행_수시전계좌조회 x = _은행_수시전계좌조회_Repository
            .findOne(r.output()).orElse(new 은행_수시전계좌조회());
        mapper.map(col, x);
        _은행_수시전계좌조회_Repository.save(x);

        List<금결원_계좌정보조회> accounts = _금결원_계좌정보조회_Repository
            .findByBankAndCreatedById(col.getBank(), member.getId());
        for (금결원_계좌정보조회 account : accounts) {
          Pattern pattern = Pattern.compile("^(" + account.get계좌번호().replaceAll("\\*", ".") + ")$");
          Matcher matcher = pattern.matcher(col.get계좌번호());
          if (matcher.find()) {
            account.setMonitor(true);
            _금결원_계좌정보조회_Repository.save(account);
          }
        }

      }
    } else if (mode.equals("개인뱅킹*수시거래내역조회")) {
      List<Object> rows = result.getJSONArray("수시거래내역조회");
      for (Object row : rows) {
        은행_수시거래내역조회_Dto col = mapper.map(row, 은행_수시거래내역조회_Dto.class);
        col.setBank(o.get("Module").toString());

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("bank", col.getBank());
        r.eq("계좌번호", col.get계좌번호());
        r.eq("거래일자", col.get거래일자());
        r.eq("거래시각", col.get거래시각());

        은행_수시거래내역조회 x = _은행_수시거래내역조회_Repository
            .findOne(r.output())
            .orElse(new 은행_수시거래내역조회());
        mapper.map(col, x);
        _은행_수시거래내역조회_Repository.save(x);
      }
    } else if (mode.equals("개인뱅킹*예적금계좌조회")) {
      List<Object> rows = result.getJSONArray("예적금계좌조회");
      for (Object row : rows) {
        은행_예적금계좌조회_Dto.Create col = mapper.map(row, 은행_예적금계좌조회_Dto.Create.class);
        col.setBank(o.get("Module").toString());

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("bank", col.getBank());
        r.eq("계좌번호", col.get계좌번호());

        은행_예적금계좌조회 x = _은행_예적금계좌조회_Repository
            .findOne(r.output())
            .orElse(new 은행_예적금계좌조회());
        mapper.map(col, x);
        _은행_예적금계좌조회_Repository.save(x);

        List<금결원_계좌정보조회> accounts = _금결원_계좌정보조회_Repository
            .findByBankAndCreatedById(col.getBank(), member.getId());
        for (금결원_계좌정보조회 account : accounts) {
          Pattern pattern = Pattern.compile("^(" + account.get계좌번호().replaceAll("\\*", ".") + ")$");
          Matcher matcher = pattern.matcher(col.get계좌번호());
          if (matcher.find()) {
            account.setMonitor(true);
            _금결원_계좌정보조회_Repository.save(account);
          }
        }
      }
    } else if (mode.equals("개인뱅킹*예적금거래내역조회")) {
      List<Object> rows = result.getJSONArray("예적금거래내역조회");
      for (Object row : rows) {
        은행_예적금거래내역조회_Dto col = mapper.map(row, 은행_예적금거래내역조회_Dto.class);
        col.setBank(o.get("Module").toString());

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("bank", col.getBank());
        r.eq("계좌번호", col.get계좌번호());
        r.eq("거래회차", col.get거래회차());

        은행_예적금거래내역조회 x = _은행_예적금거래내역조회_Repository
            .findOne(r.output())
            .orElse(new 은행_예적금거래내역조회());
        mapper.map(col, x);
        _은행_예적금거래내역조회_Repository.save(x);
      }
    } else if (mode.equals("개인뱅킹*펀드계좌조회")) {
      List<Object> rows = result.getJSONArray("펀드계좌조회");
      for (Object row : rows) {
        은행_펀드계좌조회_Dto.Create col = mapper.map(row, 은행_펀드계좌조회_Dto.Create.class);
        col.setBank(o.get("Module").toString());

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("bank", col.getBank());
        r.eq("계좌번호", col.get계좌번호());

        은행_펀드계좌조회 x = _은행_펀드계좌조회_Repository
            .findOne(r.output()).orElse(new 은행_펀드계좌조회());
        mapper.map(col, x);
        _은행_펀드계좌조회_Repository.save(x);

        List<금결원_계좌정보조회> accounts = _금결원_계좌정보조회_Repository
            .findByBankAndCreatedById(col.getBank(), member.getId());
        for (금결원_계좌정보조회 account : accounts) {
          Pattern pattern = Pattern.compile("^(" + account.get계좌번호().replaceAll("\\*", ".") + ")$");
          Matcher matcher = pattern.matcher(col.get계좌번호());
          if (matcher.find()) {
            account.setMonitor(true);
            _금결원_계좌정보조회_Repository.save(account);
          }
        }
      }
    } else if (mode.equals("개인뱅킹*펀드거래내역조회")) {
      List<Object> rows = result.getJSONArray("예적금거래내역조회");
      for (Object row : rows) {
        은행_펀드거래내역조회_Dto col = mapper.map(row, 은행_펀드거래내역조회_Dto.class);
        col.setBank(o.get("Module").toString());

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("bank", col.getBank());
        r.eq("계좌번호", col.get계좌번호());

        은행_펀드거래내역조회 x = _은행_펀드거래내역조회_Repository
            .findOne(r.output()).orElse(new 은행_펀드거래내역조회());
        mapper.map(col, x);
        _은행_펀드거래내역조회_Repository.save(x);
      }
    } else if (mode.equals("개인뱅킹*대출계좌조회")) {
      List<Object> rows = result.getJSONArray("대출계좌조회");
      for (Object row : rows) {
        은행_대출계좌조회_Dto.Create col = mapper.map(row, 은행_대출계좌조회_Dto.Create.class);
        col.setBank(o.get("Module").toString());

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("bank", col.getBank());
        r.eq("계좌번호", col.get계좌번호());

        은행_대출계좌조회 x = _은행_대출계좌조회_Repository
            .findOne(r.output()).orElse(new 은행_대출계좌조회());
        mapper.map(col, x);
        _은행_대출계좌조회_Repository.save(x);

        List<금결원_계좌정보조회> accounts = _금결원_계좌정보조회_Repository
            .findByBankAndCreatedById(col.getBank(), member.getId());
        for (금결원_계좌정보조회 account : accounts) {
          Pattern pattern = Pattern.compile("^(" + account.get계좌번호().replaceAll("\\*", ".") + ")$");
          Matcher matcher = pattern.matcher(col.get계좌번호());
          if (matcher.find()) {
            account.setMonitor(true);
            _금결원_계좌정보조회_Repository.save(account);
          }
        }
      }
    } else if (mode.equals("개인뱅킹*대출거래내역조회")) {
      List<Object> rows = result.getJSONArray("대출거래내역조회");
      for (Object row : rows) {
        은행_대출거래내역조회_Dto col = mapper.map(row, 은행_대출거래내역조회_Dto.class);
        col.setBank(o.get("Module").toString());

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("bank", col.getBank());
        r.eq("계좌번호", col.get계좌번호());
        r.eq("거래일자", col.get거래일자());
        r.eq("대출잔액", col.get대출잔액());

        은행_대출거래내역조회 x = _은행_대출거래내역조회_Repository
            .findOne(r.output()).orElse(new 은행_대출거래내역조회());
        mapper.map(col, x);
        _은행_대출거래내역조회_Repository.save(x);
      }
    } else if (mode.equals("개인뱅킹*외화계좌조회")) {
      List<Object> rows = result.getJSONArray("외화계좌조회");
      for (Object row : rows) {
        은행_외화계좌조회_Dto col = mapper.map(row, 은행_외화계좌조회_Dto.class);
        col.setBank(o.get("Module").toString());

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("bank", col.getBank());
        r.eq("계좌번호", col.get계좌번호());

        은행_외화계좌조회 x = _은행_외화계좌조회_Repository
            .findOne(r.output()).orElse(new 은행_외화계좌조회());
        mapper.map(col, x);
        _은행_외화계좌조회_Repository.save(x);
      }

    } else if (mode.equals("개인뱅킹*외화거래내역조회")) {
      List<Object> rows = result.getJSONArray("외화거래내역조회");
      for (Object row : rows) {
        은행_외화거래내역조회_Dto col = mapper.map(row, 은행_외화거래내역조회_Dto.class);
        col.setBank(o.get("Module").toString());

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("bank", col.getBank());
        r.eq("계좌번호", col.get계좌번호());
        r.eq("거래일자", col.get거래일자());
        r.eq("거래시각", col.get거래시각());

        은행_외화거래내역조회 x = _은행_외화거래내역조회_Repository
            .findOne(r.output()).orElse(new 은행_외화거래내역조회());
        mapper.map(col, x);
        _은행_외화거래내역조회_Repository.save(x);
      }

    } else if (mode.equals("개인카드*카드목록조회")) {
      List<Object> rows = result.getJSONArray("보유카드");
      for (Object row : rows) {
        카드_카드목록조회_Dto col = mapper.map(row, 카드_카드목록조회_Dto.class);
        col.setCard(o.get("Module").toString());

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("card", col.getCard());
        r.eq("카드번호", col.get카드번호());

        카드_카드목록조회 x = _카드_카드목록조회_Repository
            .findOne(r.output()).orElse(new 카드_카드목록조회());
        mapper.map(col, x);
        _카드_카드목록조회_Repository.save(x);
      }
    } else if (mode.equals("개인카드*승인내역")) {
      List<Object> rows = result.getJSONArray("승인내역조회");
      for (Object row : rows) {
        카드_카드승인내역_Dto col = mapper.map(row, 카드_카드승인내역_Dto.class);
        col.setCard(o.get("Module").toString());

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("card", col.getCard());
        r.eq("카드번호", col.get카드번호());
        r.eq("승인번호", col.get승인번호());

        카드_카드승인내역 x = _카드_카드승인내역_Repository
            .findOne(r.output()).orElse(new 카드_카드승인내역());
        mapper.map(col, x);
        _카드_카드승인내역_Repository.save(x);
      }
    } else if (mode.equals("개인카드*청구내역")) {
      List<Object> rows = result.getJSONArray("청구내역조회");
      for (int i = 0; i < rows.size(); i++) {
        카드_카드청구내역_Dto.Create col = mapper.map(rows.get(i), 카드_카드청구내역_Dto.Create.class);
        col.setCard(o.get("Module").toString());
        col.setMonth(input.getString("결제일"));
        col.setIdx(i);
        col.set결제계좌은행(result.getString("결제계좌은행"));
        col.set결제계좌번호(result.getString("결제계좌번호"));

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("card", col.getCard());
        r.eq("카드번호", col.get카드번호());
        r.eq("month", col.getMonth());
        r.eq("idx", col.getIdx());

        카드_카드청구내역 x = _카드_카드청구내역_Repository
            .findOne(r.output())
            .orElse(new 카드_카드청구내역());
        mapper.map(col, x);
        _카드_카드청구내역_Repository.save(x);
      }
    } else if (mode.equals("개인카드*포인트조회")) {
      List<Object> rows = result.getJSONArray("포인트내역");
      for (Object row : rows) {
        카드_포인트내역_Dto dto = mapper.map(row, 카드_포인트내역_Dto.class);
        dto.setCard(o.get("Module").toString());

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("card", dto.getCard());
        r.eq("포인트구분", dto.get포인트구분());

        카드_포인트내역 x = _카드_포인트내역_Repository
            .findOne(r.output()).orElse(new 카드_포인트내역());
        mapper.map(dto, x);
        _카드_포인트내역_Repository.save(x);
      }
    } else if (mode.equals("개인카드*장기카드대출_카드론거래내역") && result.has("카드론거래내역")) {
      List<Object> rows = result.getJSONArray("카드론거래내역");
      for (Object row : rows) {
        카드_장기카드대출_카드론거래내역_Dto dto = mapper.map(row, 카드_장기카드대출_카드론거래내역_Dto.class);
        dto.setCard(o.get("Module").toString());

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("card", dto.getCard());
        r.eq("거래번호", dto.get거래번호());
        r.eq("거래회차", dto.get거래회차());

        카드_장기카드대출_카드론거래내역 x = _카드_장기카드대출_카드론거래내역_Repository
            .findOne(r.output()).orElse(new 카드_장기카드대출_카드론거래내역());
        mapper.map(dto, x);
        _카드_장기카드대출_카드론거래내역_Repository.save(x);
      }
    } else if (mode.equals("개인카드*장기카드대출_카드론이용내역")) {
      List<Object> rows = result.getJSONArray("카드론이용내역");
      for (Object row : rows) {
        카드_장기카드대출_카드론이용내역_Dto dto = mapper.map(row, 카드_장기카드대출_카드론이용내역_Dto.class);
        dto.setCard(o.get("Module").toString());

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("card", dto.getCard());
        r.eq("대출번호", dto.get대출번호());

        카드_장기카드대출_카드론이용내역 x = _카드_장기카드대출_카드론이용내역_Repository
            .findOne(r.output()).orElse(new 카드_장기카드대출_카드론이용내역());
        mapper.map(dto, x);
        _카드_장기카드대출_카드론이용내역_Repository.save(x);
      }
      //_보험_가입특약사항조회_Repository
    } else if (mode.equals("개인보험*계약내용조회")) {
      List<Object> rows = result.getJSONArray("보험계약내용조회");
      for (Object row : rows) {
        보험_계약내용조회_Dto dto = mapper.map(row, 보험_계약내용조회_Dto.class);
        dto.setInsure(o.get("Module").toString());

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("insure", dto.getInsure());
        r.eq("증권번호", dto.get증권번호());

        보험_계약내용조회 x = _보험_계약내용조회_Repository
            .findOne(r.output()).orElse(new 보험_계약내용조회());
        mapper.map(dto, x);
        _보험_계약내용조회_Repository.save(x);
      }


    } else if (mode.equals("개인보험*가입특약사항조회")) {
      List<Object> rows = result.getJSONArray("가입특약사항");
      Integer i = 0;
      for (Object row : rows) {
        보험_가입특약사항조회_Dto dto = mapper.map(row, 보험_가입특약사항조회_Dto.class);
        dto.setInsure(o.get("Module").toString());
        dto.set증권번호(String.valueOf(input.get("증권번호")));
        dto.setIdx(i);

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("insure", dto.getInsure());
        r.eq("증권번호", dto.get증권번호());
        r.eq("idx", dto.getIdx());

        보험_가입특약사항조회 x = _보험_가입특약사항조회_Repository
            .findOne(r.output()).orElse(new 보험_가입특약사항조회());
        mapper.map(dto, x);
        _보험_가입특약사항조회_Repository.save(x);
        i++;
      }

    } else if (mode.equals("증권서비스*증권보유계좌조회")) {
      List<Object> rows = result.getJSONArray("증권보유계좌조회");
      for (Object row : rows) {
        증권_증권보유계좌조회_Dto dto = mapper.map(row, 증권_증권보유계좌조회_Dto.class);
        dto.setSecure(o.get("Module").toString());

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("secure", dto.getSecure());
        r.eq("계좌번호", dto.get계좌번호());

        증권_증권보유계좌조회 x = _증권_증권보유계좌조회_Repository
            .findOne(r.output()).orElse(new 증권_증권보유계좌조회());
        mapper.map(dto, x);

        _증권_증권보유계좌조회_Repository.save(x);
      }
    } else if (mode.equals("증권서비스*펀드잔고조회")) {
      증권_증권보유계좌조회_Dto dto = mapper.map(result, 증권_증권보유계좌조회_Dto.class);

      Restrictions r = new Restrictions();
      r.eq("createdBy", member);
      r.eq("계좌번호", result.getString("계좌번호"));

      증권_증권보유계좌조회 x = _증권_증권보유계좌조회_Repository
          .findOne(r.output()).orElse(null);

      if (x != null) {
        x.set예수금(dto.get예수금());
        x.set대용금(dto.get대용금());
        x.set평가총액(dto.get평가총액());
        _증권_증권보유계좌조회_Repository.save(x);
      }

    } else if (mode.equals("연금정보조회*예상연금액조회")) {
      List<Object> rows = result.getJSONArray("예상연금액조회");
      for (Object row : rows) {
        연금_예상연금액조회_Dto dto = mapper.map(row, 연금_예상연금액조회_Dto.class);

        Restrictions r = new Restrictions();
        r.eq("createdBy", member);
        r.eq("지급기관", dto.get지급기관());
        r.eq("상품명", dto.get상품명());

        연금_예상연금액조회 x = _연금_예상연금액조회_repository
            .findOne(r.output()).orElse(new 연금_예상연금액조회());
        mapper.map(dto, x);
        _연금_예상연금액조회_repository.save(x);
      }

    } else if (mode.equals("계좌통합조회*계좌통합조회")) {

      for (Object row : result.getJSONArray("계좌내역")) {
        JSONObject bank = (JSONObject) row;
        for (Object 구분_활동성계좌 : bank.getJSONArray("구분_활동성계좌")) {
          JSONArray 활동상세조회들 = ((JSONObject) 구분_활동성계좌).getJSONArray("활동상세조회");
          for (Object 활동상세조회 : 활동상세조회들) {
            금결원_계좌정보조회_Dto dto = mapper.map(활동상세조회, 금결원_계좌정보조회_Dto.class);
            dto.set활동성(true);

            System.out.println(dto);

            Restrictions r = new Restrictions();
            r.eq("createdBy", member);
            r.eq("은행명", dto.get은행명());
            r.eq("계좌번호", dto.get계좌번호());

            금결원_계좌정보조회 x = _금결원_계좌정보조회_Repository
                .findOne(r.output()).orElse(new 금결원_계좌정보조회());
            mapper.map(dto, x);

            _금결원_계좌정보조회_Repository.save(x);

          }
        }

        for (Map<String, Object> 구분_비활동성계좌 : (List<Map<String, Object>>) bank.get("구분_비활동성계좌")) {
          List<Object> 비활동상세조회들 = (List<Object>) 구분_비활동성계좌.get("비활동상세조회");
          for (Object 비활동상세조회 : 비활동상세조회들) {
            금결원_계좌정보조회_Dto dto = mapper.map(비활동상세조회, 금결원_계좌정보조회_Dto.class);
            dto.set활동성(false);

            Restrictions r = new Restrictions();
            r.eq("createdBy", member);
            r.eq("은행명", dto.get은행명());
            r.eq("계좌번호", dto.get계좌번호());

            금결원_계좌정보조회 x = _금결원_계좌정보조회_Repository
                .findOne(r.output()).orElse(new 금결원_계좌정보조회());
            mapper.map(dto, x);

            _금결원_계좌정보조회_Repository.save(x);
          }
        }

      }
    }


  }

  @Transactional
  public void mergeHouse(List<HouseDto> items, Member member) {
    houseRepository.deleteByCreatedBy(member);
    for (HouseDto dto : items) {
      House house = mapper.map(dto, House.class);
      houseRepository.save(house);
    }
  }

  public List<House> getHouses(Member member) {
    Restrictions r = new Restrictions();
    r.eq("createdBy", member);

    return houseRepository.findAll(r.output());
  }

}
