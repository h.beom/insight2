package ai.bankscan.accounts.domain;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.*;

public interface AccountDeviceRepository extends JpaRepository<AccountDevice, Long>,
    JpaSpecificationExecutor<AccountDevice> {


  @EntityGraph(attributePaths = "account")
  Optional<AccountDevice> findByAccessToken(String token);

  AccountDevice findByAccount(Account account);

  List<AccountDevice> findAllByPushTokenNotNull();

  @Modifying
  void deleteByAccessToken(String accessToken);
}
