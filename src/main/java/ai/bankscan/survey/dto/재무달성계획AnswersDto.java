package ai.bankscan.survey.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class 재무달성계획AnswersDto {

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "재무달성계획AnswersDto.AnswersResponse")
  public static class AnswersResponse {
    @ApiModelProperty(value = "key", example = "1", position = 1)
    private Long key;

    @ApiModelProperty(value = "답변", example = "저축마", position = 2)
    private String answer;

    @ApiModelProperty(value = "결과", example = "신용", position = 3)
    private String result;

    @Default
    @ApiModelProperty(value = "subAnswers", example = "[{}]", position = 4)
    private List<SubAnswersResponse> subAnswers = null;

  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "재무달성계획AnswersDto.SubAnswersResponse")
  public static class SubAnswersResponse {
    @ApiModelProperty(value = "key", example = "1", position = 1)
    private Long key;

    @ApiModelProperty(value = "답변", example = "저축마", position = 2)
    private String answer;

    @ApiModelProperty(value = "결과", example = "신용", position = 3)
    private String result;

    @Default
    @ApiModelProperty(value = "결과", example = "0", position = 4)
    private long count = 0;
  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "재무달성계획AnswersDto.Response")
  public static class Response {
    @ApiModelProperty(value = "key", example = "1", position = 1)
    private Long key;

    @ApiModelProperty(value = "질문", example = "~~~~다.", position = 1)
    private String question;

    @ApiModelProperty(value = "답변 리스트", example = "1., 2., 3.", position = 2)
    private List<AnswersResponse> answers;
  }

}
