package ai.bankscan.finance.document;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import ai.bankscan.finance.dto.카드_카드청구내역_Dto;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Table(indexes = {@Index(columnList = "card"), @Index(columnList = "카드번호"),
    @Index(columnList = "month"), @Index(columnList = "idx")})
@SqlResultSetMapping(name = "카드_카드청구내역_GroupBy", classes = @ConstructorResult(
    targetClass = 카드_카드청구내역_Dto.GroupBy.class,
    columns = {
        @ColumnResult(name = "원금", type = Long.class),
        @ColumnResult(name = "이자", type = Long.class),
    }))
public class 카드_카드청구내역 extends AuditableJoinEntity<Account> {

  private String card;
  private String month;
  private Integer idx;

  private String 결제계좌은행;
  private String 결제계좌번호;

  private String 카드번호;
  private String 카드종류;
  private String 결제일;
  private String 출금예정일;
  private String 이용일자;
  private String 가맹점명;
  private String 할부개월;
  private String 입금회차;
  private Long 이용대금;
  private Long 청구금액;
  private Long 수수료;
  private Long 결제후잔액;
  private String 가맹점사업자번호;
  private String 가맹점업종;
  private String 가맹점주소;
  private String 가맹점전화번호;
  private String 가맹점대표자명;
  private String 회원사;
  private String 청구내역포인트;
  private String 구분;

}
