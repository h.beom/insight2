package ai.bankscan.finance.document;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Getter
@ToString
@Table(indexes = {@Index(columnList = "귀속연도"), @Index(columnList = "종류")})
public class 국세청_소득세액공제조회발급 extends AuditableJoinEntity<Account> {

  private String 종류;
  private String 귀속연도;
  private String 가입자;
  private Long 고지금액합계;
  private Long 납부금액합계;
  private Long 총합계;

  @Lob
  private String extra;

}
