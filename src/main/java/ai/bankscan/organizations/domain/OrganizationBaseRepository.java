package ai.bankscan.organizations.domain;

import ai.bankscan.organizations.domain.Organization.Type;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface OrganizationBaseRepository<T extends Organization> extends JpaRepository<T, Long>,
    JpaSpecificationExecutor<T> {

  Optional<Organization> findByTypeAndCode(Type type, String code);
  Optional<Organization> findByCodeAndName(String code, String name);
}
