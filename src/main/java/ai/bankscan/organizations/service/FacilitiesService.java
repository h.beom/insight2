package ai.bankscan.organizations.service;


import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.cm.CenterManager;
import ai.bankscan.common.infra.web.common.HttpStatusMessageException;
import ai.bankscan.common.infra.web.excel.constants.Excel;
import ai.bankscan.organizations.domain.Center;
import ai.bankscan.organizations.domain.CenterRepository;
import ai.bankscan.organizations.domain.Facilities;
import ai.bankscan.organizations.domain.FacilitiesRepository;
import ai.bankscan.organizations.dto.FacilitiesDto;
import ai.bankscan.organizations.dto.FacilitiesDto.Create;
import ai.bankscan.organizations.dto.FacilitiesDto.Response;
import ai.bankscan.organizations.dto.FacilitiesDto.Search;
import ai.bankscan.organizations.dto.FacilitiesDto.Update;
import com.google.common.collect.Maps;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service
public class FacilitiesService {

  private final FacilitiesRepository facilitiesRepository;
  private final CenterRepository centerRepository;
  private final ModelMapper modelMapper;

  @Transactional
  public Response createFacilities(Create dto) {



    DateTimeFormatter formatter = new DateTimeFormatterBuilder()
        .appendPattern("yyyy-MM")
        .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
        .toFormatter();

    LocalDate days = LocalDate.parse(dto.getDays(), formatter);

    Center center = centerRepository.findById(dto.getCenterId())
        .orElseThrow(IllegalArgumentException::new);

    Optional<Facilities> optionalFacilities = facilitiesRepository
        .findByDaysAndCenter(dto.getDays(), center);

    if (optionalFacilities.isPresent()) {
      throw new HttpStatusMessageException(HttpStatus.BAD_REQUEST, "facilities.duplicated",
          days.getYear()+"", days.getMonth().getValue());
    }

    Facilities newFacilities = modelMapper.map(dto, Facilities.class);

    newFacilities.setMonth(days.getMonth().getValue());
    newFacilities.setYear(days.getYear());

    newFacilities.setCenter(center);
    return toResponse(facilitiesRepository.save(newFacilities));
  }

  public Page<Response> getFacilities(FacilitiesDto.Search search, Account account, Pageable pageable) {
    if(account.isCenterManageRole()) {
      search.setCenterId(account.getOrganization().getId());
    }
    Page<Facilities> page = facilitiesRepository.findAll(search.toSpecification(), pageable);
    List<Response> content = page.getContent().stream()
        .map(this::toResponse)
        .collect(Collectors.toList());
    return new PageImpl<>(content, pageable, page.getTotalElements());
  }

  public List<Response> getFacilities(FacilitiesDto.Search search) {
    return facilitiesRepository.findAll(search.toSpecification())
        .stream()
        .map(this::toResponse)
        .collect(Collectors.toList());
  }

  public Response getFacilities(final Long id) {
    return toResponse(facilitiesRepository.findById(id).orElseThrow(IllegalArgumentException::new));
  }

  @Transactional
  public Response updateFacilities(
      final Long id,
      Update dto,
      final String username
  ) {
    Facilities existFacilities = this.validate(id, username);
    modelMapper.map(dto, existFacilities);
    return toResponse(facilitiesRepository.save(existFacilities));
  }

  @Transactional
  public void deleteFacilities(
      final Long id,
      final String username
  ) {
    Facilities existFacilities = this.validate(id, username);
    facilitiesRepository.delete(existFacilities);
  }

  public Map<String, BigDecimal> getFacilitiesSum(FacilitiesDto.Search search) {
    return facilitiesRepository.findSumByYear(search.getYear());
  }

  public Map<String, Object> getFacilitiesToExcel(FacilitiesDto.Search search) {
    List<Facilities> facilitiesList = facilitiesRepository
        .findAll(search.toSpecification(), Sort.by(Direction.DESC, "year", "month"));

    List<List<String>> body = facilitiesList.stream()
        .map(facilities -> {
          final Center center = facilities.getCenter();
          Optional<Account> optionalCenterManager = Optional.ofNullable(center.getManager());

          return Arrays.asList(
              center.getCode(),
              String.join(".", facilities.getYear().toString(), facilities.getMonth().toString()),
              center.getName(),
              center.getCrn(),
              optionalCenterManager.orElse(CenterManager.builder().build()).getName(),
              Objects.toString(facilities.getRent()),
              Objects.toString(facilities.getMaintenanceFee()),
              Objects.toString(facilities.getElectricityFee()),
              Objects.toString(facilities.getWaterFee()),
              Objects.toString(facilities.getCityGasFee()),
              Objects.toString(facilities.getPhoneFee()),
              Objects.toString(facilities.getInternetFee()),
              Objects.toString(facilities.getOfficeSuppliesCost()),
              Objects.toString(facilities.getOtherCost()),
              Objects.toString(facilities.getTotalAmount())
          );
        })
        .collect(Collectors.toList());

    Map<String, Object> data = Maps.newHashMap();
    data.put(Excel.FILE_NAME.getName(), "시설관리");
    data.put(Excel.HEAD.getName(),
        Arrays.asList(
            "센터ID",
            "날짜",
            "센터명",
            "사업자등록번호",
            "센터장이름",
            "임대료",
            "관리비",
            "전기세",
            "수도세",
            "도시가스비",
            "유선전화비",
            "인터넷비",
            "사무용품비",
            "기타",
            "총합계"
        ));
    data.put(Excel.BODY.getName(), body);
    return data;
  }

  public Map<String, Object> getFacilitiesToExcel(final Long centerId, final Integer year) {
    Search search = Search.builder()
        .centerId(centerId)
        .year(year)
        .build();

    List<Facilities> facilitiesList = facilitiesRepository
        .findAll(search.toSpecification(), Sort.by(Direction.DESC, "year", "month"));

    List<List<String>> body = facilitiesList.stream()
        .map(facilities -> Arrays.asList(
            String.join(".", facilities.getYear().toString(), facilities.getMonth().toString()),
            Objects.toString(facilities.getRent()),
            Objects.toString(facilities.getMaintenanceFee()),
            Objects.toString(facilities.getElectricityFee()),
            Objects.toString(facilities.getWaterFee()),
            Objects.toString(facilities.getCityGasFee()),
            Objects.toString(facilities.getPhoneFee()),
            Objects.toString(facilities.getInternetFee()),
            Objects.toString(facilities.getOfficeSuppliesCost()),
            Objects.toString(facilities.getOtherCost()),
            Objects.toString(facilities.getTotalAmount())
        ))
        .collect(Collectors.toList());

    body.add(
        Arrays.asList(
            year + "년 소계",
            Objects.toString(this.getSum(facilitiesList, Facilities::getRent)),
            Objects.toString(this.getSum(facilitiesList, Facilities::getMaintenanceFee)),
            Objects.toString(this.getSum(facilitiesList, Facilities::getElectricityFee)),
            Objects.toString(this.getSum(facilitiesList, Facilities::getWaterFee)),
            Objects.toString(this.getSum(facilitiesList, Facilities::getCityGasFee)),
            Objects.toString(this.getSum(facilitiesList, Facilities::getPhoneFee)),
            Objects.toString(this.getSum(facilitiesList, Facilities::getInternetFee)),
            Objects.toString(this.getSum(facilitiesList, Facilities::getOfficeSuppliesCost)),
            Objects.toString(this.getSum(facilitiesList, Facilities::getOtherCost)),
            Objects.toString(this.getSum(facilitiesList, Facilities::getTotalAmount))
        )
    );

    Map<String, Object> data = Maps.newHashMap();
    data.put(Excel.FILE_NAME.getName(), "시설관리");
    data.put(Excel.HEAD.getName(),
        Arrays.asList(
            "날짜",
            "임대료",
            "관리비",
            "전기세",
            "수도세",
            "도시가스비",
            "유선전화비",
            "인터넷비",
            "사무용품비",
            "기타",
            "총합계"
        ));
    data.put(Excel.BODY.getName(), body);
    return data;
  }

  private BigDecimal getSum(
      List<Facilities> facilitiesList,
      Function<Facilities, BigDecimal> mapper
  ) {
    return facilitiesList.stream()
        .map(Facilities::getRent)
        .reduce(BigDecimal.ZERO, BigDecimal::add);
  }

  private Facilities validate(final Long id, final String username) {
    Facilities existingFacilities = facilitiesRepository.findById(id)
        .orElseThrow(IllegalArgumentException::new);

    /*if (!existingFacilities.getCenter().get.equals(username)) {
      throw new AccessDeniedException("Access is denied");
    }*/
    return existingFacilities;
  }

  private Response toResponse(Facilities facilities) {
    return modelMapper.map(facilities, Response.class);
  }
}
