package ai.bankscan.organizations.domain;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.address.domain.Address;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
public abstract class Organization extends AuditableJoinEntity<Account> {

  @Getter
  @RequiredArgsConstructor
  public enum Type {
    CENTER("센터"), INSTITUTION("기관단체");
    private final String title;
  }

  @Setter
  @ManyToOne(fetch = FetchType.LAZY)
  private Account manager;

  @Column(nullable = false, updatable = false, insertable = false)
  @Enumerated(EnumType.STRING)
  private Type type;

  @Column(nullable = false)
  private String name;

  @Column(length = 6, nullable = false)
  private String code;

  private String crn;

  private Long businessLicense;

  private String phoneNumber;

  private String url;

  private Long logo;

  @Setter
  @Embedded
  protected Address address;

  @Lob
  private String description;

  public abstract void create();

  public abstract void update();

  public abstract void validate();

}
