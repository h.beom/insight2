package ai.bankscan.finance.domain;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import ai.bankscan.finance.dto.AssetTableDto;
import java.time.LocalDateTime;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.SqlResultSetMapping;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@SqlResultSetMapping(name = "AssetTable_Simple", classes = @ConstructorResult(
    targetClass = AssetTableDto.Simple.class,
    columns = {
        @ColumnResult(name = "createdAt", type = LocalDateTime.class),
        @ColumnResult(name = "price", type = Long.class),
    }))
public class AssetTable extends AuditableJoinEntity<Account> {

  private Long total;

  private Long assetsTotal;

  @Lob
  private String assetsJson;

  private Long liabilitiesTotal;

  @Lob
  private String liabilitiesJson;

}
