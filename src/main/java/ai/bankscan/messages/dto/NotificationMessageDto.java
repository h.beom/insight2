package ai.bankscan.messages.dto;

import ai.bankscan.accounts.dto.AccountDto;
import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.common.infra.jpa.support.SearchDto;
import ai.bankscan.messages.domain.NotificationMessage;
import ai.bankscan.messages.domain.NotificationMessage.SendType;
import ai.bankscan.messages.domain.NotificationMessage.Status;
import ai.bankscan.messages.domain.NotificationMessage.Type;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * @author eomjeongjae
 * @since 2019/12/16
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class NotificationMessageDto {

  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  @ToString
  public static class Send {

    @NotNull
    private Type type;

    @NotNull
    private SendType sendType;

    @NotNull
    private Long receiverId;

    @NotEmpty
    private String title;

    @NotEmpty
    private String body;

    private String link;

    @DateTimeFormat(iso = ISO.DATE_TIME)
    private LocalDateTime scheduledDateTime;

  }

  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  @ToString
  public static class SendToAll {

    @NotNull
    private Type type;

    @NotNull
    private SendType sendType;

    @NotEmpty
    private String title;

    @NotEmpty
    private String body;

    private String link;

    @NotNull
    private Long relatedId;

    @DateTimeFormat(iso = ISO.DATE_TIME)
    private LocalDateTime scheduledDateTime;
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  @ToString
  @ApiModel(value = "NotificationMessageDto.Response")
  public static class Response {

    @ApiModelProperty(value = "ID", example = "1")
    private Long id;

    @ApiModelProperty(value = "타입", example = "APPLICATION_FOR_LESSON", position = 1)
    private Type type;

    @ApiModelProperty(value = "제목", example = "제목", position = 2)
    private String title;

    @ApiModelProperty(value = "내용", example = "내용", position = 3)
    private String body;

    @ApiModelProperty(value = "상태", example = "SENT", position = 4)
    private Status status;

    @ApiModelProperty(value = "전송일시", position = 5)
    private LocalDateTime sentDateTime;

    @ApiModelProperty(value = "생성일시", position = 6)
    private LocalDateTime createdAt;

    @ApiModelProperty(value = "수신자", position = 8)
    private AccountDto.Response receiver;

  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString(callSuper = true)
  @ApiModel(value = "NotificationMessageDto.Search")
  public static class Search extends SearchDto<NotificationMessage> {

    @ApiModelProperty(value = "유저네임", hidden = true)
    private String username;

    @Default
    @ApiModelProperty(value = "전송유무", hidden = true)
    private Boolean isSendDateTime = Boolean.TRUE;

    @Override
    protected Restrictions generateRestrictions() {
      final Restrictions restrictions = new Restrictions();

      if (StringUtils.isNotEmpty(this.username)) {
        restrictions.eq("receiver.email", this.username);
      }

      if (Boolean.TRUE.equals(this.isSendDateTime)) {
        restrictions.isNotNull("sentDateTime");
      }

      return restrictions;
    }
  }

}