package ai.bankscan.certification.controller;

import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.accounts.dto.MemberDto;
import ai.bankscan.certification.service.CertificationService;
import com.icert.comm.secu.IcertSecuManager;
import io.swagger.annotations.Api;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import net.sf.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.MediaTypes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Api(tags = {"사용자 인증 api"})
@RequiredArgsConstructor
@Controller
@RequestMapping(path = CertificationController.REQUEST_BASE_PATH, produces = MediaTypes.HAL_JSON_VALUE)
public class CertificationController {

  @Value("${app.url}")
  private String APP_URL;

  @Value("${app.certcode}")
  private String URL_CODE;

  static final String REQUEST_BASE_PATH = "/certification";

  static final String CP_ID = "BKSM1001";
  static final String CERT_MET = "M";
  static final String TR_URL = "/certification/result";

  private final CertificationService certificationService;

  private final ModelMapper mapper;

  @GetMapping("/{type}")
  public String typeCheck(@PathVariable String type, HttpSession session)
      throws IOException, ServletException {
    session.setAttribute("sysType", type);
    return "redirect:/certification";
  }


  @GetMapping
  public String userCertification(Model model, HttpSession session, HttpServletResponse response) {

    if (session.getAttribute("sysType") == null) {
      response.setStatus(404);
      return null;
    }

    Calendar today = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    String day = sdf.format(today.getTime());

    IcertSecuManager seed = new IcertSecuManager();

    String cpId = CP_ID;
    String urlCode = URL_CODE;
    String certMet = CERT_MET;
    String tr_url = APP_URL + TR_URL;

    String enc_tr_cert = "";
    String tr_cert = "";
    String hmacMsg = "";
    String extendVar = "0000000000000000";                  // 확장변수

    Random ran = new Random();

    int numLength = 6;
    StringBuilder randomStr = new StringBuilder();

    for (int i = 0; i < numLength; i++) {
      randomStr.append(ran.nextInt(10));
    }

    String reqNum = day + randomStr;

    tr_cert = cpId
        + "/"
        + urlCode
        + "/"
        + reqNum
        + "/"
        + day
        + "/"
        + certMet
        + "/" + "/" + "/" + "/" + "/" + "/" + "/" + "/"
        + extendVar;

    enc_tr_cert = seed.getEnc(tr_cert, "");
    hmacMsg = seed.getMsg(enc_tr_cert);

    tr_cert = seed.getEnc(enc_tr_cert + "/" + hmacMsg + "/" + extendVar, "");

    model.addAttribute("day", day);
    model.addAttribute("reqNum", reqNum);
    model.addAttribute("cpId", cpId);
    model.addAttribute("urlCode", urlCode);
    model.addAttribute("certMet", certMet);
    model.addAttribute("tr_url", tr_url);
    model.addAttribute("tr_add", "Y");
    model.addAttribute("tr_cert", tr_cert);
    model.addAttribute("hmacMsg", hmacMsg);
    model.addAttribute("enc_tr_cert", enc_tr_cert);

    return "cert/certMain";
  }

  @GetMapping("/result")
  public ModelAndView resultData(@RequestParam Map<String, Object> data, ModelAndView mav,
      HttpSession session) {
    JSONObject resultMap = new JSONObject();
    Map<String, Object> dataList = this
        .certificationDeciphering(data.get("rec_cert").toString(), data.get("certNum").toString());
    Member member = this.certificationService
        .userCiDiValidation(dataList.get("CI").toString(), dataList.get("DI").toString());

    if (session.getAttribute("sysType").equals("find")) {
      resultMap.put("member", mapper.map(member, MemberDto.Response.class));
    } else {
      resultMap.put("isMember", member != null);
    }

    resultMap.put("rec_cert", data.get("rec_cert"));
    resultMap.put("certNum", data.get("certNum"));
    resultMap.put("data", dataList);
    mav.setViewName("cert/certResult2");
    mav.addObject("resultData", resultMap.toString());

    return mav;
  }

  private Map<String, Object> certificationDeciphering(String recCert, String certNum) {

    String rec_cert = "";           // 결과수신DATA

    String k_certNum = "";          // 파라미터로 수신한 요청번호
    String date = "";      // 요청일시
    String CI = "";      // 연계정보(CI)
    String DI = "";      // 중복가입확인정보(DI)
    String phoneNo = "";      // 휴대폰번호
    String phoneCorp = "";      // 이동통신사
    String birthDay = "";      // 생년월일
    String gender = "";      // 성별
    String nation = "";      // 내국인
    String name = "";      // 성명
    String M_name = "";      // 미성년자 성명
    String M_birthDay = "";      // 미성년자 생년월일
    String M_Gender = "";      // 미성년자 성별
    String M_nation = "";      // 미성년자 내외국인
    String result = "";      // 결과값

    String certMet = "";      // 인증방법
    String ip = "";      // ip주소
    String plusInfo = "";

    String encPara = "";
    String encMsg1 = "";
    String encMsg2 = "";
    String msgChk = "";

    try {
      rec_cert = recCert;
      k_certNum = certNum;

      /**
       * 01. 암호화 모듈 (jar) Loading
       * */
      IcertSecuManager seed = new IcertSecuManager();

      /**
       * 02. 1차 파싱
       * */
      rec_cert = seed.getDec(rec_cert, k_certNum);

      /**
       * 03. 1차 파싱
       * */
      int inf1 = rec_cert.indexOf("/", 0);
      int inf2 = rec_cert.indexOf("/", inf1 + 1);

      encPara = rec_cert.substring(0, inf1); // 암호화 된 통합 parameter
      encMsg1 = rec_cert.substring(inf1 + 1, inf2); // 암호화 된 통합 parameter 의 hash value

      /**
       * 04. 위변조 검증
       */
      encMsg2 = seed.getMsg(encPara);

      if (encMsg2.equals(encMsg1)) {
        msgChk = "Y";
      }

      if (msgChk.equals("N")) {
        throw new Exception();
      }

      /**
       * 05. 2차 복호화
       * */
      rec_cert = seed.getDec(encPara, k_certNum);

      /**
       * 06. 2차 파싱
       * */
      int info1 = rec_cert.indexOf("/", 0);
      int info2 = rec_cert.indexOf("/", info1 + 1);
      int info3 = rec_cert.indexOf("/", info2 + 1);
      int info4 = rec_cert.indexOf("/", info3 + 1);
      int info5 = rec_cert.indexOf("/", info4 + 1);
      int info6 = rec_cert.indexOf("/", info5 + 1);
      int info7 = rec_cert.indexOf("/", info6 + 1);
      int info8 = rec_cert.indexOf("/", info7 + 1);
      int info9 = rec_cert.indexOf("/", info8 + 1);
      int info10 = rec_cert.indexOf("/", info9 + 1);
      int info11 = rec_cert.indexOf("/", info10 + 1);
      int info12 = rec_cert.indexOf("/", info11 + 1);
      int info13 = rec_cert.indexOf("/", info12 + 1);
      int info14 = rec_cert.indexOf("/", info13 + 1);
      int info15 = rec_cert.indexOf("/", info14 + 1);
      int info16 = rec_cert.indexOf("/", info15 + 1);
      int info17 = rec_cert.indexOf("/", info16 + 1);
      int info18 = rec_cert.indexOf("/", info17 + 1);

      name = rec_cert.substring(info8 + 1, info9);
      CI = rec_cert.substring(info2 + 1, info3);
      phoneNo = rec_cert.substring(info3 + 1, info4);
      DI = rec_cert.substring(info17 + 1, info18);
      certNum = rec_cert.substring(0, info1);
      ip = rec_cert.substring(info11 + 1, info12);

      // 이름 전화번호 생년월일 ci di

      certNum = rec_cert.substring(0, info1);
      date = rec_cert.substring(info1 + 1, info2);
      phoneCorp = rec_cert.substring(info4 + 1, info5);
      birthDay = rec_cert.substring(info5 + 1, info6);
      gender = rec_cert.substring(info6 + 1, info7);
      nation = rec_cert.substring(info7 + 1, info8);
      result = rec_cert.substring(info9 + 1, info10);
      certMet = rec_cert.substring(info10 + 1, info11);
      M_name = rec_cert.substring(info12 + 1, info13);
      M_birthDay = rec_cert.substring(info13 + 1, info14);
      M_Gender = rec_cert.substring(info14 + 1, info15);
      M_nation = rec_cert.substring(info15 + 1, info16);
      plusInfo = rec_cert.substring(info16 + 1, info17);

      /** ci ,di 복호화 */
      CI = seed.getDec(CI, k_certNum);
      DI = seed.getDec(DI, k_certNum);

      Map<String, Object> data = new HashMap<>();
      Map<String, Object> allData = new HashMap<>();

      allData.put("certNum", certNum);
      allData.put("date", date);
      allData.put("phoneCorp", phoneCorp);
      allData.put("birthDay", birthDay);
      allData.put("gender", gender);
      allData.put("nation", nation);
      allData.put("result", result);
      allData.put("certMet", certMet);
      allData.put("M_name", M_name);
      allData.put("M_birthDay", M_birthDay);
      allData.put("M_Gender", M_Gender);
      allData.put("M_nation", M_nation);
      allData.put("plusInfo", plusInfo);
      allData.put("CI", CI);
      allData.put("DI", DI);
      allData.put("name", name);
      allData.put("phoneNo", phoneNo);
      allData.put("ip", ip);

      data.put("CI", CI);
      data.put("DI", DI);
      data.put("name", name);
      data.put("phoneNo", phoneNo);
      data.put("ip", ip);
      data.put("resultDatas", allData);

      return data;

    } catch (Exception e) {
      System.out.println("[KSCIS] Receive Error - " + e.getMessage());
    }
    return null;
  }

}

