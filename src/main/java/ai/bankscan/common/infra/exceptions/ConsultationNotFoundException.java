package ai.bankscan.common.infra.exceptions;

import ai.bankscan.common.infra.web.common.HttpStatusException;
import org.springframework.http.HttpStatus;

/**
 * @author eomjeongjae
 * @since 2019-08-23
 */
public class ConsultationNotFoundException extends HttpStatusException {

  public ConsultationNotFoundException(Long id) {
    super(HttpStatus.NOT_FOUND, "Could not find consultation " + id, "consultation.notFound");
  }

  public ConsultationNotFoundException(String email) {
    super(HttpStatus.NOT_FOUND, "Could not find consultation " + email, "consultation.notFound");
  }
}
