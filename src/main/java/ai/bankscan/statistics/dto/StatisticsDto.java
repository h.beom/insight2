package ai.bankscan.statistics.dto;

import ai.bankscan.organizations.domain.Organization;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StatisticsDto {

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "StatisticsDto.Response")
  public static class Response {

    @ApiModelProperty(value = "기관 명", example = "금융건강지원센터 서울지사", position = 1)
    private String name;

    @ApiModelProperty(value = "상담횟수", example = "1", position = 2)
    private Long organizationCount;

    @ApiModelProperty(value = "상담비율", example = "10.33%", position = 3)
    private String percentage;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  @ToString
  @ApiModel(value ="StatisticsDto.Search", description = "검색")
  public static class Search {

    @ApiModelProperty(value = "검색어, name, code, all", position = 1)
    private String keyword;

    @ApiModelProperty(value ="name, code", position = 2)
    private String value;

    @ApiModelProperty(value = "type", position = 3)
    private Organization.Type type;

    @ApiModelProperty(value ="start date", position = 4)
    private LocalDate startDate;

    @ApiModelProperty(value ="end date", position = 5)
    private LocalDate endDate;

    @ApiModelProperty(value = "1", position = 6)
    private Long organizationId;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  @ToString
  @ApiModel(value ="StatisticsDto.Simple1", description = "consultationLog 통계")
  public static class Simple1 {
    private String monthDate;
    private Long consultationCount;

  }



}
