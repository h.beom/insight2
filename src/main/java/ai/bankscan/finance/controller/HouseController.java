package ai.bankscan.finance.controller;

import ai.bankscan.finance.dto.PropertyPricesDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.time.Month;
import lombok.RequiredArgsConstructor;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = {"부동산 시세 조회 api"})
@RequiredArgsConstructor
@RestController
@RequestMapping(path = HouseController.REQUEST_BASE_PATH, produces = MediaTypes.HAL_JSON_VALUE)
public class HouseController {

  static final String REQUEST_BASE_PATH = "/api/property-prices";

  @Value("${app.coocon-url}")
  private String requestUrl;

  @Value("${app.coocon-id}")

  private String apiId;
  @Value("${app.coocon-key}")
  private String apiKey;


  @ApiOperation(value = "부동산 시세 조회")
  @PostMapping
  public ResponseEntity<String> getPropertyPrices(@RequestBody PropertyPricesDto.Create dto) {
    String response = "";
    try {
      URL url = new URL(requestUrl);
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setRequestMethod("POST");
      conn.setReadTimeout(15000);
      conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
      conn.setConnectTimeout(15000);
      conn.setDoOutput(true);
      conn.setDoInput(true);
      conn.connect();

      OutputStream os = conn.getOutputStream();
      BufferedWriter bw = new BufferedWriter(
          new OutputStreamWriter(os, "UTF-8")
      );

      JSONObject jsonObject = new JSONObject();
      jsonObject.put("API_KEY", apiKey);
      jsonObject.put("API_ID", apiId);
      jsonObject.put("SEARCH_GUBUN", dto.getSearchGubun());
      jsonObject.put("SEARCH_YEAR", "2020");
      jsonObject.put("TRADE_TYPE", "0");
      jsonObject.put("SIDO_NAME", dto.getSidoName());
      jsonObject.put("GUGN_NAME", dto.getGugunName());
      jsonObject.put("DONG_NAME", dto.getDongName());
      jsonObject.put("DONG_CODE", dto.getDongCode());
      jsonObject.put("SEARCH_SEASON", this.getSeasonForCurrentMonth());
      jsonObject.put("DANJI_NAME", null);
      jsonObject.put("BUN_JI", dto.getBunJi());
      jsonObject.put("BON_NO", null);
      jsonObject.put("BU_NO", null);
      jsonObject.put("ROAD_NM", dto.getRoadNm());
      jsonObject.put("SPAGE", "1");
      jsonObject.put("EPAGE", "100");

      bw.write("JSONData=" + encodeURIComponent(encodeURIComponent(jsonObject.toString())));

      bw.flush();
      bw.close();
      os.close();

      int responseCode = conn.getResponseCode();

      if (responseCode == HttpURLConnection.HTTP_OK) {
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        while ((line = br.readLine()) != null) {
          response += line;

        }
      } else {
        response = "";
      }

      return ResponseEntity.ok(response);
    } catch (Exception e) {
      e.printStackTrace();
    }

    return ResponseEntity.status(404).body(null);

  }


  public String encodeURIComponent(String s) {
    String result = null;

    try {
      result = URLEncoder.encode(s, "UTF-8")
          .replaceAll("\\+", "%20")
          .replaceAll("\\%21", "!")
          .replaceAll("\\%27", "'")
          .replaceAll("\\%28", "(")
          .replaceAll("\\%29", ")")
          .replaceAll("\\%7E", "~");
    }

    // This exception should never occur.
    catch (UnknownError | UnsupportedEncodingException e) {
      result = s;
    }

    return result;
  }

  private String getSeasonForCurrentMonth() {
    LocalDate currentDate = LocalDate.now();
    Month month = currentDate.getMonth();
    int quarter = (int) Math.ceil(month.getValue() / 3.0);
    return quarter + "";
  }

}
