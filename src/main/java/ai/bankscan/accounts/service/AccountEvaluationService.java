package ai.bankscan.accounts.service;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.AccountEvaluation;
import ai.bankscan.accounts.domain.AccountEvaluationRepository;
import ai.bankscan.accounts.domain.AccountRepository;
import ai.bankscan.accounts.dto.AccountEvaluationDto;
import ai.bankscan.consultations.domain.consulting.Consultation;
import ai.bankscan.consultations.domain.consulting.ConsultationRepository;
import ai.bankscan.consultations.dto.ConsultationDto;
import ai.bankscan.organizations.domain.Organization;
import ai.bankscan.organizations.domain.OrganizationRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Transactional(readOnly = true)
@Service
@RequiredArgsConstructor
public class AccountEvaluationService {

    private final AccountEvaluationRepository accountEvaluationRepository;
    private final ConsultationRepository consultationRepository;
    private final AccountRepository accountRepository;
    private final ModelMapper modelMapper;
    private final EntityManager entityManager;

    @Transactional
    public AccountEvaluationDto.Response saveUserEvaluation(AccountEvaluationDto.Create dto, Account currentUser) {
        Consultation consultation = consultationRepository.findById(dto.getConsultationId()).orElseThrow();
        Account account = accountRepository.findById(dto.getAccountId()).orElseThrow();

        AccountEvaluation accountEvaluation = AccountEvaluation.builder()
                .evaluation(dto.getEvaluation())
                .consultation(consultation)
                .account(account)
                .comment(dto.getComment())
                .build();

        return toResponse(accountEvaluationRepository.save(accountEvaluation));
    }

    public List<Map<String, Object>> getUserEvaluationList(AccountEvaluationDto.Search search) {
        ConsultationDto.Search search1 = new ConsultationDto.Search();
        search1.setOrganizationType(search.getType());
        search1.setKeyword(search.getKeyword());
        search1.setSearchType(search.getSearchType());
        search1.setFromDate(search.getFromDate());
        search1.setToDate(search.getToDate());
        List<Consultation> consultations = consultationRepository.findAll(search1.toSpecification());
        String where = "";
        String where2 = "";
        if (search.getType() != null) {
            if (search.getType().equals(Organization.Type.CENTER)) {
                where += "AND a.type = 'CENTER' ";
            } else {
                where += "AND a.type = 'INSTITUTION' ";
            }
        }

        if (search.getKeyword() != null) {
            if (search.getSearchType().equals("center.code") ||
                    search.getSearchType().equals("institution.code")) {
                where += "AND a.centerCode like '%" + search.getKeyword() + "%' ";
            } else if (search.getSearchType().equals("center.name") ||
                    search.getSearchType().equals("institution.name")) {
                where += "AND a.centerName like '%" + search.getKeyword() + "%' ";
            }
        }

        if (search.getToDate() != null && search.getFromDate() != null) {
            where2 += "AND a.created_at between date('"+search.getFromDate()+"') and date('"+search.getToDate()+"')+1 ";
        }


        String sql = "SELECT  " +
                "b.id, "+
                "a.type, " +
                "a.centerName, " +
                "a.username, " +
                "a.name, " +
                "COUNT(a.id) as allConsultations, " +
                "AVG(b.evaluation) avgEvaluation, " +
                "COUNT(a.id) / " + consultations.size() + " * 100 as avgRate " +
                "FROM  " +
                "(  " +
                "SELECT  " +
                "a.id, " +
                "o.id centerId, " +
                "acc.id userId, " +
                "o.type, " +
                "o.name centerName, " +
                "o.code centerCode, " +
                "acc.username, " +
                "acc.name " +
                "FROM consultation a, " +
                "organization o, " +
                "account acc " +
                "WHERE " +
                "(acc.id = a.consultant_id or acc.id = a.pro_consultant_id) AND " +
                "(o.id = a.center_id or o.id = a.institution_id) AND " +
                "acc.organization_id = o.id " +
                where2 +
                ") a " +
                "LEFT JOIN account_evaluation b on a.id = b.consultation_id " +
                "WHERE 1=1 " +
                where +
                "GROUP BY a.userId " +
                "ORDER BY avgRate desc";

        String[] header = {
                "id",
                "type",
                "centerName",
                "userId",
                "userName",
                "allConsultations",
                "avgEvaluation",
                "avgRate"
        };

        List<Map<String, Object>> result = new ArrayList<>();
        List<Object[]> dataList = entityManager.createNativeQuery(sql).getResultList();
        for (Object[] objects : dataList) {
            Map<String, Object> data = new HashMap<>();
            data.put(header[0], objects[0]);
            data.put(header[1], objects[1]);
            data.put(header[2], objects[2]);
            data.put(header[3], objects[3]);
            data.put(header[4], objects[4]);
            data.put(header[5], objects[5]);
            data.put(header[6], objects[6]);
            data.put(header[7], objects[7]);
            result.add(data);
        }

        return result;

    }

    private AccountEvaluationDto.Response toResponse(AccountEvaluation accountEvaluation) {
        return modelMapper.map(accountEvaluation, AccountEvaluationDto.Response.class);
    }
}
