package ai.bankscan.finance.domain;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CooconLogRepository extends JpaRepository<CooconLog, Long> {

}
