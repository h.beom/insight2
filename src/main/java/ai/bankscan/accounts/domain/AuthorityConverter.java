package ai.bankscan.accounts.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class AuthorityConverter implements AttributeConverter<Set<Account.Authority>, String> {

    private final static ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(Set<Account.Authority> attribute) {
        try {
            return objectMapper.writeValueAsString(attribute);
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public Set<Account.Authority> convertToEntityAttribute(String dbData) {
        try {
            return (Set<Account.Authority>) objectMapper.readValue(dbData, Set.class)
                    .stream()
                    .map(v -> Account.Authority.valueOf(v.toString()))
                    .collect(Collectors.toSet());
        } catch (Exception e) {
        }
        return null;
    }

}


