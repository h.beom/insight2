package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 연금_예상연금액조회_Dto {

  private String 관계구분;
  private String 지급기관;
  private String 상품명;
  private String 연령별예시연금액;

}
