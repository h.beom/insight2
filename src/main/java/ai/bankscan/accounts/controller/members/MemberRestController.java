package ai.bankscan.accounts.controller.members;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.accounts.dto.AccountDeviceDto;
import ai.bankscan.accounts.dto.MemberDto;
import ai.bankscan.accounts.dto.MemberDto.Response;
import ai.bankscan.accounts.service.AccountDeviceAdapter;
import ai.bankscan.accounts.service.member.MemberService;
import ai.bankscan.common.infra.annotations.ApiPageable;
import ai.bankscan.common.infra.security.common.CurrentUser;
import ai.bankscan.common.infra.web.excel.ExcelXlsView;
import ai.bankscan.common.infra.web.excel.constants.Excel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Api(tags = {"회원 API"})
@RequiredArgsConstructor
@RestController
@RequestMapping(path = MemberRestController.REQUEST_BASE_PATH, produces = MediaTypes.HAL_JSON_VALUE)
public class MemberRestController {

  static final String REQUEST_BASE_PATH = "/api/members";

  private final MemberService memberService;
  private final MemberResourceAssembler memberResourceAssembler;

  @ApiOperation(value = "회원 가입")
  @PostMapping
  public ResponseEntity<Resource<Response>> createMember(
      @RequestBody @Valid MemberDto.Create dto
  ) throws URISyntaxException {
    Response savedMember = memberService.createMember(dto);
    Resource<Response> resource = memberResourceAssembler.toResource(savedMember);

    return ResponseEntity
        .created(new URI(resource.getLink(Link.REL_SELF).getHref()))
        .body(resource);
  }

  @ApiOperation(value = "회원 조회")
  @GetMapping("/{id}")
  @PreAuthorize("isAuthenticated() and (( #id == #currentUser.id ) or hasRole('ROLE_ADMIN'))")
  public ResponseEntity<Resource<Response>> getMember(
      @ApiParam(required = true, example = "1") @PathVariable final Long id,
      @CurrentUser Account currentUser
  ) {
    Resource<Response> resource = memberResourceAssembler
        .toResource(memberService.getMember(id, currentUser.getUsername()));
    return ResponseEntity.ok(resource);
  }

  @ApiPageable
  @ApiOperation(value = "회원 리스트 조회")
  @GetMapping
  @Secured("ROLE_ADMIN")
  public Resources<Resource<Response>> getMembers(
      MemberDto.Search search,
      @PageableDefault(sort = "createdAt", direction = Direction.DESC) Pageable pageable,
      PagedResourcesAssembler<Response> pagedResourcesAssembler
  ) {
    Page<Response> page = memberService.getMembers(search, pageable);
    return pagedResourcesAssembler.toResource(page, memberResourceAssembler);
  }

  @ApiOperation(value = "회원 excel 리스트 조회")
  @GetMapping("/excel-download/{listType}")
  @Secured("ROLE_ADMIN")
  public ModelAndView getMembersToExcel(
      MemberDto.Search search,
      @PathVariable String listType
  ) {
    Map<String, Object> resource = memberService.getMembersToExcel(search, listType);
    return new ModelAndView(new ExcelXlsView(), resource);
  }

  @ApiOperation(value = "회원 정보 수정")
  @PutMapping("/{id}")
  @PreAuthorize("isAuthenticated() and (( #id == #currentUser.id ) or hasRole('ROLE_ADMIN'))")
  public ResponseEntity<Resource<Response>> updateMember(
      @ApiParam(required = true, example = "1") @PathVariable final Long id,
      @RequestBody @Valid MemberDto.Update dto,
      @CurrentUser Account currentUser
  ) throws URISyntaxException {
    Response savedMember = memberService.updateMember(id, dto, currentUser.getUsername());
    Resource<Response> resource = memberResourceAssembler.toResource(savedMember);

    return ResponseEntity
        .created(new URI(resource.getLink(Link.REL_SELF).getHref()))
        .body(resource);
  }

  @ApiOperation(value = "인증된 사용자 정보 조회", notes = "인증이 되어있지 않을 경우 Unauthorized(401)")
  @GetMapping("/authentication")
  @PreAuthorize("isAuthenticated()")
  public ResponseEntity<MemberDto.Response> getAuthentication(
      @CurrentUser Member currentUser
  ) {
    MemberDto.Response response = memberService
        .getMember(currentUser.getId(), currentUser.getUsername());
    response.setNow(System.currentTimeMillis());
    return ResponseEntity.ok(response);
  }

  @ApiOperation(value = "id, pw 찾기", notes = "id pw를 잃어버렸을 때")
  @PostMapping("/find")
  public ResponseEntity<MemberDto.Response> findByUserInformation(
      @RequestBody Map<String, String> data) {
    String ci = data.get("CI").toString();
    String di = data.get("DI").toString();
    MemberDto.Response response = memberService.findMember(ci, di);
    if(response != null) {
      return ResponseEntity.ok(response);
    }
    return ResponseEntity.status(400).body(null);
  }

  @ApiOperation(value = "pw 변경", notes = "pw 변경 될 때")
  @PostMapping("/update-password/{id}")
  public ResponseEntity<Resource<MemberDto.Response>> changedMyPassword(
      @ApiParam(required = true, example = "1") @PathVariable final Long id,
      @RequestBody MemberDto.Update update) throws URISyntaxException {

    MemberDto.Response member = memberService.findMember(update.getCi(), update.getDi());
    Response savedMember = memberService.updateMember(id, update, member.getUsername());
    Resource<Response> resource = memberResourceAssembler.toResource(savedMember);

    return ResponseEntity
        .created(new URI(resource.getLink(Link.REL_SELF).getHref()))
        .body(resource);
  }

  @ApiOperation(value = "사용자 푸시 여부 리스트", notes = "사용자 푸시 여부 리스트 get")
  @GetMapping("/my-push-list")
  @PreAuthorize("isAuthenticated()")
  public ResponseEntity<AccountDeviceDto.Response> getMyPushList(@CurrentUser Member member) {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (!authentication.isAuthenticated()) {
      return ResponseEntity.status(403).body(null);
    }
    AccountDeviceAdapter adapter = (AccountDeviceAdapter) authentication.getPrincipal();
    String token = adapter.getAccountDevice().getAccessToken();
    return ResponseEntity.ok(memberService.myList(token));
  }

  @ApiOperation(value = "사용자 푸시 여부 리스트 update", notes = "사용자 푸시 여부 리스트 update")
  @PutMapping("/my-push-list")
  @PreAuthorize("isAuthenticated()")
  public ResponseEntity<AccountDeviceDto.Response> putMyPushList(
      @CurrentUser Member member,
      @RequestBody @Valid AccountDeviceDto.Update update) {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (!authentication.isAuthenticated()) {
      return ResponseEntity.status(403).body(null);
    }
    AccountDeviceAdapter adapter = (AccountDeviceAdapter) authentication.getPrincipal();
    String token = adapter.getAccountDevice().getAccessToken();
    return ResponseEntity.ok(memberService.myPushListUpdate(token, update));
  }
}
