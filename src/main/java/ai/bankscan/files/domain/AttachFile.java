package ai.bankscan.files.domain;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import java.io.File;
import javax.persistence.Column;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;

/**
 * @author eomjeongjae
 * @since 2019/10/15
 */
@Slf4j
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class AttachFile extends AuditableJoinEntity<Account> {

  @Column(nullable = false)
  private String serverPath;

  @Column(nullable = false)
  private String filename;

  @Column(length = 20)
  private String extension;

  @Column(length = 30, nullable = false)
  private String contentType;

  @Builder.Default
  private Long size = 0L;

  public void setFilename(String filename) {
    this.filename = filename;
    this.extension = AttachFile.extractExtension(filename);
  }

  public static class AttachFileBuilder {

    public AttachFileBuilder filename(String filename) {
      this.filename = filename;
      this.extension = AttachFile.extractExtension(filename);
      return this;
    }
  }

  public boolean isImage() {
    return getMediaType().isCompatibleWith(MediaType.IMAGE_JPEG);
  }

  public boolean isGif() {
    return getMediaType().equals(MediaType.IMAGE_GIF);
  }

  public boolean isVideo() {
    return getMediaType().getType().startsWith("video");
  }

  public MediaType getMediaType() {
    MediaType mediaType = null;
    try {
      mediaType = MediaType.parseMediaType(this.contentType);
    } catch (Exception e) {
      log.error("Fail parse media type");
    }

    if (mediaType == null) {
      mediaType = MediaType.APPLICATION_OCTET_STREAM;
    }
    return mediaType;
  }

  public String getServerFilePath() {
    return this.serverPath
        + File.separator
        + this.getId()
        + File.separator
        + this.getServerFilename();
  }

  public String getServerFilename() {
    String serverFileName = "source";
    if (StringUtils.isNotEmpty(this.extension)) {
      serverFileName += "." + this.extension;
    }
    return serverFileName;
  }

  private static String extractExtension(String filename) {
    if (filename.contains(".")) {
      return filename.substring(filename.lastIndexOf(".") + 1);
    }
    return StringUtils.EMPTY;
  }

}
