package ai.bankscan.bound.domain;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import ai.bankscan.consultations.domain.consulting.Consultation;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class Bound extends AuditableJoinEntity<Account> {

  @Getter
  @RequiredArgsConstructor
  public enum CallType {
    INBOUND("Inbound"),
    OUTBOUND("outbound");
    private final String title;
  }

  @Column(name = "call_type")
  @Enumerated(EnumType.STRING)
  private CallType callType;

  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
  @JsonSerialize(using = ToStringSerializer.class)
  private LocalDateTime sCall;

  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
  @JsonSerialize(using = ToStringSerializer.class)
  private LocalDateTime eCall;

  private String receivePhone;

  private String senderPhone;

  @ManyToOne
  private Consultation consultation;

  @ManyToOne
  private Account consultant;

  @ManyToOne
  private Member member;

}
