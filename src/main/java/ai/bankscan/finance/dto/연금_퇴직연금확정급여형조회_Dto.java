package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 연금_퇴직연금확정급여형조회_Dto {

  private String 상품유형;
  private String 상품명;
  private String 입사연도;
  private String 퇴직예정연도;
  private String 예시연금액;
  private String 가입회사;

}
