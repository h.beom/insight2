package ai.bankscan.configs.web;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ViewServerConfig {

  private static final int DEFAULT_TIME_OUT  = 1000;
  private static String userAgent = "";
  private static String encode = "UTF-8";
  private static String path = "http://localhost:3022";

  public static void setEncode(String code) {
    ViewServerConfig.encode = encode;
  }

  public static String getPath() { return path; }

  public JSONObject getUserData(String addUrl) {
    String pathUrl = path + addUrl;
    String result = "";
    String line;
    BufferedReader br;

    try {
      URL url = new URL(pathUrl);
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      con.setRequestMethod("GET");
      con.setDoInput(true);
      con.setRequestProperty("Content-Type", "application/json; utf-8");
      br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
      while ((line = br.readLine()) != null)
        result += line;
      br.close();

      JSONParser jsonParser = new JSONParser();
      JSONObject jsonObject = (JSONObject) jsonParser.parse(result);
      JSONArray dataArray = (JSONArray) jsonObject.get("data");

      con.disconnect();

      return jsonObject;

    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public String sendData(Map<String, Object> dataList) {
    URL url;
    HttpURLConnection con = null;
    BufferedReader br;
    String line;
    String result = "";
    StringBuffer outResult = new StringBuffer();
    String charSet = "UTF-8";

    try {
      JSONObject data = new JSONObject();
      data.put("data", dataList);

      if(con != null) con.disconnect();
      url = new URL(path +"/test");
      con = (HttpURLConnection) url.openConnection();
      con.setRequestMethod("POST");
      con.setDoInput(true);
      con.setDoOutput(true);
      con.setRequestProperty("Content-Type", "application/json");
      con.setRequestProperty("Accept-Charset", encode);

      con.disconnect();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return result;
  }

}
