package ai.bankscan.organizations.domain;

import ai.bankscan.common.infra.web.common.HttpStatusMessageException;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.AccessLevel;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.util.ObjectUtils;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@DiscriminatorValue("CENTER")
public class Center extends Organization {

  public enum AddressType {
    OFFICE, VIRTUAL_OFFICE;

    public boolean isOffice() {
      return (this == OFFICE);
    }

    public boolean isVirtualOffice() {
      return (this == VIRTUAL_OFFICE);
    }
  }

  @Default
  @Enumerated(EnumType.STRING)
  private Type type = Type.CENTER;

  @Default
  @Enumerated(EnumType.STRING)
  private AddressType addressType = AddressType.OFFICE;


  @Default
  private Boolean display = Boolean.TRUE;

  @Override
  public void create() {
    this.validate();
  }

  @Override
  public void update() {
    this.validate();
  }

  @Override
  public void validate() {
    if (this.addressType.isOffice() && ObjectUtils.isEmpty(this.address)) {
      throw new HttpStatusMessageException(HttpStatus.BAD_REQUEST, "center.address.required");
    }
  }

}
