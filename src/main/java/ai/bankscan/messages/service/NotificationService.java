package ai.bankscan.messages.service;

import ai.bankscan.messages.domain.Notification;
import ai.bankscan.messages.domain.NotificationMessage.SendType;
import ai.bankscan.messages.domain.NotificationRepository;
import ai.bankscan.messages.dto.NotificationDto;
import ai.bankscan.messages.dto.NotificationDto.Response;
import ai.bankscan.messages.dto.NotificationMessageDto.SendToAll;
import com.google.firebase.messaging.BatchResponse;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service
public class NotificationService {

  private final NotificationRepository notificationRepository;
  private final NotificationMessageService notificationMessageService;
  private final ModelMapper modelMapper;

  @Transactional
  public NotificationDto.Response createNotification(NotificationDto.Create dto) {
    Notification newNotification = modelMapper.map(dto, Notification.class);
    Notification savedNotification = notificationRepository.save(newNotification);

    CompletableFuture<BatchResponse> messageResponse = notificationMessageService
        .sendToAll(SendToAll.builder()
            .type(dto.getType())
            .sendType(SendType.DIRECT)
            .title(savedNotification.getTitle())
            .body(savedNotification.getBody())
            .link(savedNotification.getLink())
            .relatedId(savedNotification.getId())
            .build());

    messageResponse.thenAccept((batchResponse) -> {
      savedNotification
          .completeToSend(batchResponse.getSuccessCount(), batchResponse.getFailureCount());
      notificationRepository.save(savedNotification);
    }).exceptionally(throwable -> {
      log.error("Send message error:", throwable);
      savedNotification.failToSend(throwable.getMessage());
      notificationRepository.save(savedNotification);
      return null;
    });

    return this.toResponse(savedNotification);
  }

  public Page<Response> getNotification(
      NotificationDto.Search search,
      Pageable pageable
  ) {
    Page<Notification> page = notificationRepository.findAll(search.toSpecification(), pageable);
    List<Response> content = page.getContent().stream()
        .map(this::toResponse)
        .collect(Collectors.toList());
    return new PageImpl<>(content, pageable, page.getTotalElements());
  }

  public NotificationDto.Response getNotification(final Long id) {
    Notification foundNotification = notificationRepository.findById(id).orElseThrow();
    return this.toResponse(foundNotification);
  }

  private NotificationDto.Response toResponse(Notification notification) {
    return modelMapper.map(notification, NotificationDto.Response.class);
  }

}
