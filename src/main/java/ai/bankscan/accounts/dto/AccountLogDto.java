package ai.bankscan.accounts.dto;

import ai.bankscan.accounts.domain.AccountLog.LogType;
import io.swagger.annotations.ApiModel;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

public class AccountLogDto {

  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  @ToString
  @ApiModel(value = "AccountLogDto.Create")
  public static class Create {

    private MemberDto.Response member;

    private LogType logType;

    @Default
    private LocalDateTime logTime = LocalDateTime.now();

  }

  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  @ToString
  @ApiModel(value = "AccountLogDto.Response")
  public static class Response {

    private Long id;

    private LogType logType;

    private LocalDateTime logTime;

    private MemberDto.Response member;

  }

}
