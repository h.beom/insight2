package ai.bankscan.finance.document;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface 은행_외화거래내역조회_Repository extends JpaRepository<은행_외화거래내역조회, Long>,
    JpaSpecificationExecutor<은행_외화거래내역조회> {

}
