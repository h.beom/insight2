package ai.bankscan.statistics.service;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.Account.Type;
import ai.bankscan.common.infra.web.excel.constants.Excel;
import ai.bankscan.consultations.domain.consulting.Consultation.Status;
import ai.bankscan.organizations.domain.Facilities;
import ai.bankscan.organizations.domain.Organization;
import ai.bankscan.statistics.domain.StatisticsRepository;
import ai.bankscan.statistics.dto.StatisticsDto;
import java.time.LocalDate;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.google.common.collect.Maps;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service
public class StatisticsService {

  private final StatisticsRepository statisticsRepository;
  private final ModelMapper modelMapper;

  @PersistenceContext
  EntityManager entityManager;


  public Map<String, Object> getMainAdmin(Account admin) {

    Map<String, Object> dataList = new HashMap<>();

    Map<String, Integer> consultations = getConsultationOfCount(admin.getType(), admin);
    Map<String, Integer> members = getMemberAllCount(admin.getType());
    Map<String, Integer> organizations = getOrganizationOfCount(admin.getType(), admin);
    Map<String, Integer> accounts = getConsultantOfCount(admin.getType(), admin);
    Map<String, Integer> inOutBonds = getInOutBoundAllCount(admin.getType());
    Map<String, Integer> inOutBoundTypeConsultant = getInOutBoundTypeOfConsultant(admin.getType());
    Map<String, Integer> facilitiesMap = getFacilitiesAllCount(admin);
    Map<String, Integer> consultationStatus = getConsultationStatusStatistics(Type.CONSULTANT);
    Map<String, Integer> consultationStatus2 = getConsultationStatusStatistics(Type.PRO_CONSULTANT);
    Map<String, Integer> liveUser = getMemberLive();
    Map<String, Integer> leavedUser = getMemberLeave();
    dataList.put("상담통계", consultations);
    dataList.put("회원통계", members);
    dataList.put("기관통계", organizations);
    dataList.put("상담원통계", accounts);
    dataList.put("통화이력", inOutBonds);
    dataList.put("상담건별_통화이력", inOutBoundTypeConsultant);
    dataList.put("시설관리", facilitiesMap);
    dataList.put("누적기본상담신청건수", consultationStatus);
    dataList.put("누적전문상담신청건수", consultationStatus2);
    dataList.put("누적정상회원추이", liveUser);
    dataList.put("누적탈퇴회원추이", leavedUser);

    return dataList;
  }

  public Map<String, Object> getMainCenterManager(Account account) {
    Map<String, Object> dataList = new HashMap<>();
    Map<String, Integer> consultations = getConsultationOfCount(account.getType(), account);
    Map<String, Integer> accounts = getConsultantOfCount(account.getType(), account);
    Map<String, Integer> facilitiesMap = getFacilitiesAllCount(account);

    dataList.put("상담통계", consultations);
    dataList.put("상담원통계", accounts);
    dataList.put("시설관리", facilitiesMap);

    return dataList;
  }


  public Page<StatisticsDto.Response> getStatisticsFromConsultationCount(
      Pageable pageable,
      StatisticsDto.Search search) {

    Page<Map<String, Object>> page = statisticsRepository
        .findAllByOrganizationCount(search.getValue(), pageable);

    List<StatisticsDto.Response> content = page.getContent().stream()
        .map(this::toResponse)
        .collect(Collectors.toList());

    return new PageImpl<>(content, pageable, page.getTotalElements());

  }
  public Map<String, Object> getStatisticsFromConsultationCountToExcel(
          StatisticsDto.Search search) {
    List<List<String>> body = new ArrayList<>();
    List<Map<String, Object>> lists = statisticsRepository
            .findAllByOrganizationCountToExcel(search.getValue());

    for(int i =0; i< lists.size(); i++) {
      List<String> excel;
      excel = Arrays.asList(
              lists.get(i).get("name").toString(),
              lists.get(i).get("type").toString().equals(Organization.Type.CENTER) ? "센터" : "기관",
              lists.get(i).get("organizationCount").toString(),
              lists.get(i).get("percentage").toString()

      );
      body.add(excel);
    }

    Map<String, Object> data = Maps.newHashMap();
    data.put(Excel.FILE_NAME.getName(), "통계.xls");
    data.put(Excel.HEAD.getName(), Arrays.asList(
            "센터/기관 명",
            "센터/ 기관 구분",
            "센터 상담 건수",
            "센터 상담 비율"
    ));
    data.put(Excel.BODY.getName(), body);

    return data;
  }

  private StatisticsDto.Response toResponse(Map<String, Object> statistics) {
    return modelMapper.map(statistics, StatisticsDto.Response.class);
  }


  private Page<Map<String, Object>> getSearchSpecification(Pageable pageable,
      StatisticsDto.Search search, final long allCount) {

    String whereQuery = "";

    if (search.getKeyword().equals("code")) {
      whereQuery += " and o.code like '%" + search.getValue() + "%'";
    }

    if (search.getKeyword().equals("name")) {
      whereQuery += " and o.name like '%" + search.getValue() + "%'";
    }

    if (search.getKeyword().equals("all")) {
      whereQuery +=
          " and (o.code like '%" + search.getValue() + "%' or o.name like '%" + search.getValue()
              + "%')";
    }

    if (!ObjectUtils.isEmpty(search.getType())) {
      whereQuery += " and c.type = '" + search.getType().name() + "'";
    }

    if (search.getOrganizationId() != null) {
      whereQuery += " and c.organization_id = " + search.getOrganizationId();
    }

    if (search.getStartDate() != null
        && search.getEndDate() != null
        && search.getKeyword() != null) {

      whereQuery +=
          " and c.created_at between" + search.getStartDate() + " and " + search.getEndDate();

      if (search.getKeyword().equals("all")) {
        whereQuery +=
            " and (o.name' %" + search.getValue() + "%' or o.code '%" + search.getValue() + "%')";
      } else {
        whereQuery += " and o." + search.getKeyword() + " '%" + search.getValue() + "%'";
      }
    }

    TypedQuery<Entry> query = entityManager.createQuery(
            String.format("SELECT o.name name, count(organization_id) organizationCount, ROUND(100 * count(organization_id)/total.allCount, 2) percentage from consultation c,organization o, (select count(*) allCount from consultation) total where o.id = c.organization_id %s GROUP BY organization_id ORDER BY percentage desc", whereQuery),
        Entry.class
    );

    query.setMaxResults(pageable.getPageSize());
    query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());

    return (Page<Map<String, Object>>) query;
  }

  public Map<String, Integer> getConsultantOfCount(final Type type, final Account account) {
    String query = "SELECT type, COUNT(*) count FROM account where ";
    String whereQuery = "is_leaved = false ";
    String endQuery = " GROUP BY type";

    switch (type) {
      case CENTER_MANAGER:
        whereQuery += "and type = 'CONSULTANT' and organization_id = "+ account.getOrganization().getId();
        break;
      case RELATIONSHIP_MANAGER:
        whereQuery += "and type = 'PRO_CONSULTANT' and organization_id = "+ account.getOrganization().getId();
        break;
      case MEMBER:
        whereQuery += "and type = 'MEMBER'";
        break;
      default:
        whereQuery += "and 1 = 1";
        break;
    }

    List<Object[]> resultQuery = entityManager.createNativeQuery(query + whereQuery + endQuery)
        .getResultList();

    Map<String, Integer> values = new HashMap<>();

    for (Object[] objects : resultQuery) {
      values.put(objects[0].toString(), Integer.parseInt(objects[1].toString()));
    }
    return values;
  }

  // 상담의 경우 당일 기준 이전 날들의 상담 신청을 모두 카운팅함.
  public Map<String, Integer> getConsultationOfCount(final Type type, final Account account) {
    String whereQuery = "1 = 1 ";

    if (type.equals(Type.CENTER_MANAGER)) {
      whereQuery = "(status = 'APPLY' or status = 'COMPANION') AND center_id = "+account.getOrganization().getId();
    } else if (type.equals(Type.RELATIONSHIP_MANAGER)) {
      whereQuery = "(status = 'PRO_APPLY' or status = 'PRO_COMPANION') AND  institution_id = "+account.getOrganization().getId();
    }

    String query = "SELECT status, COUNT(status) status_count "
        + "FROM consultation "
        + "WHERE " + whereQuery
        + " AND DATE_FORMAT(updated_on,'YYYY-MM-DD') >= DATE_FORMAT(now(),'YYYY-MM-DD') "
        + "GROUP BY status";

    List<Object[]> resultQuery = entityManager.createNativeQuery(query)
        .getResultList();

    Map<String, Integer> values = new HashMap<>();

    for (Object[] objects : resultQuery) {
      values.put(objects[0].toString(), Integer.parseInt(objects[1].toString()));
    }
    return values;
  }


  // 상담의 경우 당일 기준 이전 날들의 상담 신청을 모두 카운팅함.
  public Map<String, Integer> getOrganizationOfCount(final Type type, final Account account) {
    String whereQuery = "1 = 1 ";

    Type type1 = this.validationUserCheck(account);

    if (type.equals(Type.CENTER_MANAGER)) {
      whereQuery = "type = 'CENTER'";
    } else if (type.equals(Type.RELATIONSHIP_MANAGER)) {
      whereQuery = "type = 'INSTITUTION'";
    }

    String query = "SELECT type, COUNT(*) organizationCount "
        + "FROM organization "
        + "WHERE " + whereQuery
        + "GROUP BY type";

    List<Object[]> resultQuery = entityManager.createNativeQuery(query).getResultList();

    Map<String, Integer> values = new HashMap<>();

    for (Object[] objects : resultQuery) {
      values.put(objects[0].toString(), Integer.parseInt(objects[1].toString()));
    }
    return values;
  }


  public Map<String, Integer> getMemberAllCount(Type type) {
    String whereQuery = "1 = 1 ";
    if (type.equals(Type.ADMIN)) {
      whereQuery = "type = 'MEMBER'";
    } else if (type.equals(Type.CENTER_MANAGER)) {
      whereQuery = "type = 'CONSULTANT'";
    } else if (type.equals(Type.PRO_CONSULTANT)) {
      whereQuery = "type = 'PRO_CONSULTANT'";
    }
    String query = "SELECT "
        + "'live' as live, "
        + "count(if(is_leaved = 0, is_leaved, null)) live_count, "
        + "'leaved' as leaved, "
        + "count(if(is_leaved = 1, is_leaved, null)) leaved_count "
        + "FROM account "
        + "WHERE " + whereQuery;

    List<Object[]> resultQuery = entityManager.createNativeQuery(query)
        .getResultList();

    Map<String, Integer> values = new HashMap<>();

    values
        .put(resultQuery.get(0)[0].toString(), Integer.parseInt(resultQuery.get(0)[1].toString()));
    values
        .put(resultQuery.get(0)[2].toString(), Integer.parseInt(resultQuery.get(0)[3].toString()));

    return values;
  }

  public Map<String, Integer> getInOutBoundAllCount(Type type) {
    String whereQuery = "1 = 1 ";
    if (type.equals(Type.ADMIN)) {
    }
    String query = "SELECT "
        + "'inbound' as inbound, "
        + "count(if(call_type = 'INBOUND', call_type, null)) inbound_count, "
        + "'outbound' as outbound, "
        + "count(if(call_type = 'OUTBOUND', call_type, null)) outbound_count "
        + "FROM bound "
        + "WHERE " + whereQuery;

    List<Object[]> resultQuery = entityManager.createNativeQuery(query)
        .getResultList();

    Map<String, Integer> values = new HashMap<>();

    values
        .put(resultQuery.get(0)[0].toString(), Integer.parseInt(resultQuery.get(0)[1].toString()));
    values
        .put(resultQuery.get(0)[2].toString(), Integer.parseInt(resultQuery.get(0)[3].toString()));

    return values;
  }

  public Map<String, Integer> getInOutBoundTypeOfConsultant(Type type) {
    String whereQuery = "b.consultant_id = a.id ";
    String endQuery = " GROUP BY a.type";
    if (type.equals(Type.ADMIN)) {

    }
    String query = "SELECT "
        + "a.type,"
        + "count(a.type) type_count "
        + "FROM bound b, account a "
        + "WHERE " + whereQuery + endQuery;

    List<Object[]> resultQuery = entityManager.createNativeQuery(query)
        .getResultList();

    Map<String, Integer> values = new HashMap<>();

    for (Object[] objects : resultQuery) {
      values.put(objects[0].toString(), Integer.parseInt(objects[1].toString()));
    }
    return values;
  }

  private Type validationUserCheck(Account account) {

    if (account.isAdminRole()) {
      return Type.ADMIN;
    } else if (account.isCenterManageRole()) {
      return Type.CENTER_MANAGER;
    } else if (account.isRelationshipManagerRole()) {
      return Type.RELATIONSHIP_MANAGER;
    }
    return Type.MEMBER;
  }

  public Map<String, Integer> getConsultationStatusStatistics(Type type) {
    String whereQuery = "";

    if(type.equals(Type.CONSULTANT)) {
      whereQuery = " status = '"+ Status.APPLY+"' ";

    }
    if(type.equals(Type.PRO_CONSULTANT)) {
      whereQuery = " status = '"+ Status.PRO_APPLY+"' ";
    }

    String query = "SELECT MONTH(created_at) as month_date, "
        + "count(*) as consultation_count "
        + "FROM consultation_log "
        + "WHERE" + whereQuery
        + "AND created_at >= date_add(now(), interval -6 month) "
        + "GROUP BY month_date";

    List<Object[]> resultQuery = entityManager.createNativeQuery(query).getResultList();

    HashMap<String, Integer> result = new HashMap<>();

    for (int i = 0; i < resultQuery.size(); i++ ) {
      result.put(resultQuery.get(i)[0].toString(), Integer.parseInt(resultQuery.get(i)[1].toString()));
    }

    return result;
  }

  public Map<String, Integer> getMemberLive() {
    String query = "SELECT MONTH(created_at) as month_date, "
        + "count(*) as account_count "
        + "FROM account "
        + "WHERE type = 'MEMBER'"
        + "AND created_at >= date_add(now(), interval -6 month) "
        + "GROUP BY month_date";

    List<Object[]> resultQuery = entityManager.createNativeQuery(query).getResultList();

    HashMap<String, Integer> result = new HashMap<>();

    for (int i = 0; i < resultQuery.size(); i++ ) {
      result.put(resultQuery.get(i)[0].toString(), Integer.parseInt(resultQuery.get(i)[1].toString()));
    }

    return result;
  }

  public Map<String, Integer> getMemberLeave() {
    String query = "SELECT MONTH(leave_date) as month_date, "
        + "count(*) as account_count "
        + "FROM account "
        + "WHERE type = 'MEMBER'"
        + "AND leave_date >= date_add(now(), interval -6 month) "
        + "GROUP BY month_date";

    List<Object[]> resultQuery = entityManager.createNativeQuery(query).getResultList();

    HashMap<String, Integer> result = new HashMap<>();

    for (int i = 0; i < resultQuery.size(); i++ ) {
      result.put(resultQuery.get(i)[0].toString(), Integer.parseInt(resultQuery.get(i)[1].toString()));
    }

    return result;
  }


  public Map<String, Integer> getFacilitiesAllCount(Account account) {
    String whereQuery = "";

    LocalDate localDate = LocalDate.now();
    int currentMonth = localDate.getMonth().getValue();
    int preMonth = localDate.getMonthValue() - 1;
    int currentYear = localDate.getYear();
    int preYear = localDate.getYear() - 1;
    boolean isPre = false;

    if (localDate.getMonth().getValue() == 1) {
      isPre = true;
      preMonth = 12;
    }

    if (isPre) {
      whereQuery = "(year = " + preYear + " and month = " + preMonth + ") or"
          + "(year = " + currentYear + " and month = " + currentMonth + ")";
    } else {
      whereQuery = "( month = " + preMonth + " or month = " + currentMonth + ") "
          + " and year = " + currentYear;
    }
    if (account.getType().isCenterManager() ||
        account.getType().isRelationshipManager()) {
      whereQuery += " and center_id = " + account.getOrganization().getId();
    }

    String query = "SELECT * "
        + "FROM facilities "
        + "WHERE " + whereQuery;

    List<Facilities> resultQuery = entityManager.createNativeQuery(query, Facilities.class)
        .getResultList();

    int preDateMoney = 0;
    int nowDateMoney = 0;

    for (int i = 0; i < resultQuery.size(); i++) {
      if (resultQuery.get(i).getMonth() == currentMonth) {
        nowDateMoney = +resultQuery.get(i).getTotalAmount().intValue();
      } else {
        preDateMoney = +resultQuery.get(i).getTotalAmount().intValue();
      }
    }

    Map<String, Integer> values = new HashMap<>();
    values.put(currentMonth + "", nowDateMoney);
    values.put(preMonth + "", preDateMoney);

    return values;

  }

}
