package ai.bankscan.finance.document;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Getter
@ToString
@Table(indexes = {@Index(columnList = "상위_귀속연도"), @Index(columnList = "상위_인덱스")})
public class 국세청_소득세액공제조회발급_보험료 extends AuditableJoinEntity<Account> {

  private String 상위_종류;
  private String 상위_귀속연도;
  private Integer 상위_인덱스;
  private String 계약자;
  private String 종류;
  private String 상호;
  private String 증권번호;
  private String 납입금액계;
  private String 주피보험자;
  private String 종피보험자;
  private String 종피보험자2;
  private String 종피보험자3;
  private String 구입_납입방법;
  private String 계약기간;

}
