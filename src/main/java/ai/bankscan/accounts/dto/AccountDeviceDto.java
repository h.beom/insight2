package ai.bankscan.accounts.dto;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.AccountDevice.Platform;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author eomjeongjae
 * @since 2019/11/27
 */
public class AccountDeviceDto {

  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  @ToString
  @ApiModel(value = "AccountDeviceDto.Create")
  public static class Create {

    @Setter
    private Account account;

    private String serialNumber;

    private String pushToken;

    private Platform platform;

    private String deviceName;

  }

  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  @ToString
  @ApiModel(value = "AccountDeviceDto.Update")
  public static class Update {

    private boolean pushConsulting; // 상담 진행 여부

    private boolean pushDiagnosisEnd; // 진단 결과 완료부 여부

    private boolean pushNotice; //공지사항 알림 여부

  }

  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  @ToString
  @ApiModel(value = "AccountDeviceDto.Response")
  public static class Response {

    @ApiModelProperty(value = "회원", position = 1, required = true)
    private AccountBaseDto.Response account;

    @ApiModelProperty(value = "엑세스토큰", example = "DFXX-EWFWF", position = 1, required = true)
    private String accessToken;

    @ApiModelProperty(value = "장비 고유키", example = "DFXX-EWFWF", position = 1, required = true)
    private String serialNumber;

    @ApiModelProperty(value = "장비타입", example = "IOS", position = 1, required = true)
    private Platform platform;

    @ApiModelProperty(value = "장비명", example = "iPhone 11 Pro", position = 1, required = true)
    private String deviceName;

    @ApiModelProperty(value = "상담 진행 여부", position = 1, required = true)
    private boolean pushConsulting; // 상담 진행 여부

    @ApiModelProperty(value = "진단 결과 완료부 여부", position = 1, required = true)
    private boolean pushDiagnosisEnd; // 진단 결과 완료부 여부

    @ApiModelProperty(value = "공지사항 알림 여부", position = 1, required = true)
    private boolean pushNotice; //공지사항 알림 여부

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonSerialize(using = ToStringSerializer.class)
    private LocalDateTime createdAt;

  }

}
