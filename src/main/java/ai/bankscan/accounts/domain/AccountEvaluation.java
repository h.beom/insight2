package ai.bankscan.accounts.domain;

import ai.bankscan.common.infra.jpa.domain.AuditableEntity;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import ai.bankscan.consultations.domain.consulting.Consultation;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class AccountEvaluation extends AuditableJoinEntity<Account> {
    @ManyToOne
    private Consultation consultation;

    @ManyToOne
    private Account account;

    @Builder.Default
    private long evaluation = 0;

    @Lob
    private String comment;
}
