package ai.bankscan.consultations.dto;

import ai.bankscan.accounts.dto.AccountDto;
import ai.bankscan.consultations.domain.consulting.Consultation.Status;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConsultationHistoryDto {

  @Setter
  @Getter
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "ConsultationHistoryDto.Create")
  public static class Create{
    @ApiModelProperty(value = "상담 정보", example = "1", position = 1)
    private Long consultationId;

    @Default
    @ApiModelProperty(value = "상담 상태", example = "1", position = 1)
    private Status status = Status.APPLY;

    @ApiModelProperty(value = "상담사", example = "consultant", position = 2)
    private Long consultantId;

    @Default
    @ApiModelProperty(value = "상태 일자", example = "2020-01-01 12:00", position = 3)
    private LocalDateTime logDateTime = LocalDateTime.now();

    @ApiModelProperty(value = "상태 메세지", example = "기본 정보 조회", position = 5)
    private String logComment;
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "ConsultationHistoryDto.Response")
  public static class Response{
    private Long id;

    @ApiModelProperty(value = "상담 정보", example = "1", position = 1)
    private ConsultationDto.Response consultation;

    @ApiModelProperty(value = "상담 상태", example = "1", position = 2)
    private Status status;

    @ApiModelProperty(value = "상담사", example = "consultant", position = 3)
    private AccountDto.Response consultant;

    @ApiModelProperty(value = "상태 일자", example = "2020-01-01 12:00", position = 4)
    private LocalDateTime logDateTime;

    @ApiModelProperty(value = "상태 메세지", example = "기본 정보 조회", position = 5)
    private String logComment;

  }


}
