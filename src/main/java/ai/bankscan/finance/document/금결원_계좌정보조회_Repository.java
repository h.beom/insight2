package ai.bankscan.finance.document;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface 금결원_계좌정보조회_Repository extends JpaRepository<금결원_계좌정보조회, Long>,
    JpaSpecificationExecutor<금결원_계좌정보조회> {

  List<금결원_계좌정보조회> findByBankAndCreatedById(String bank, Long createdById);

  @Query("SELECT bank FROM 금결원_계좌정보조회 WHERE createdBy.id = :createdById AND NOT(bank like '@%') GROUP BY bank")
  List<String> getBanks(@Param("createdById") Long createdById);

  @Query("SELECT COALESCE(SUM(잔고), 0) FROM 금결원_계좌정보조회 WHERE createdBy.id = :createdById AND 활동성 = '1' AND monitor = 0")
  Long getTotalByBank(@Param("createdById") Long createdById);
}
