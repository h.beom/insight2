package ai.bankscan.organizations.dto;

import ai.bankscan.common.address.dto.AddressDto;
import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.organizations.domain.Center;
import ai.bankscan.organizations.domain.Center.AddressType;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CenterDto {

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "CenterDto.Create")
  public static class Create extends OrganizationDto.Create {

    @ApiModelProperty(value = "공개여부", example = "true", position = 102, required = true)
    private Boolean display;

    @NotNull
    @ApiModelProperty(value = "주소타입", example = "OFFICE", position = 103, required = true)
    private AddressType addressType;


  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "CenterDto.Update")
  public static class Update extends OrganizationDto.Update {

    @ApiModelProperty(value = "공개여부", example = "true", position = 102, required = true)
    private Boolean display;

    @NotNull
    @ApiModelProperty(value = "주소타입", example = "OFFICE", position = 103, required = true)
    private AddressType addressType;


  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString(callSuper = true)
  @ApiModel(value = "CenterDto.Response")
  public static class Response extends OrganizationDto.Response {

    @ApiModelProperty(value = "공개여부", example = "true", position = 102, required = true)
    private Boolean display;

    @ApiModelProperty(value = "주소타입", example = "OFFICE", position = 103, required = true)
    private AddressType addressType;

  }


  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString(callSuper = true)
  @ApiModel(value = "CenterDto.Simple")
  public static class Simple extends OrganizationDto.Simple {

    @ApiModelProperty(value = "공개여부", example = "true", position = 102, required = true)
    private Boolean display;

    @ApiModelProperty(value = "주소타입", example = "OFFICE", position = 103, required = true)
    private AddressType addressType;

  }

  @Getter
  @Setter
  @NoArgsConstructor
  @SuperBuilder
  @ToString(callSuper = true)
  @ApiModel(value = "CenterDto.Search")
  public static class Search extends OrganizationDto.Search<Center> {

    @Default
    @ApiModelProperty(value = "검색어", position = 101)
    protected Boolean display = null;

    @Override
    protected Restrictions generateRestrictions() {
      final Restrictions restrictions = super.generateRestrictions();

      if (this.display != null) {
        restrictions.eq("display", this.display);
      }

      return restrictions;
    }
  }

}
