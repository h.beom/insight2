package ai.bankscan.messages.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import ai.bankscan.messages.dto.NotificationDto.Response;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
class NotificationResourceAssembler implements
    ResourceAssembler<Response, Resource<Response>> {

  @Override
  public Resource<Response> toResource(Response entity) {
    return new Resource<>(
        entity,
        linkTo(methodOn(NotificationRestController.class).getNotification(entity.getId()))
            .withSelfRel()
    );
  }
}