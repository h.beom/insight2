package ai.bankscan.organizations.domain;

import ai.bankscan.common.infra.jpa.domain.BaseEntity;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Table(uniqueConstraints = {
    @UniqueConstraint(columnNames = {"year", "month", "center_id"})
})
public class Facilities extends BaseEntity {

  @Setter
  @ManyToOne(optional = false)
  private Center center;

  @Setter
  @Column(nullable = false)
  private Integer year;

  @Setter
  @Column(nullable = false)
  private Integer month;

  @Column(nullable = false)
  private String days;

  @Default
  private BigDecimal rent = BigDecimal.ZERO;

  @Default
  private BigDecimal maintenanceFee = BigDecimal.ZERO;

  @Default
  private BigDecimal electricityFee = BigDecimal.ZERO;

  @Default
  private BigDecimal waterFee = BigDecimal.ZERO;

  @Default
  private BigDecimal cityGasFee = BigDecimal.ZERO;

  @Default
  private BigDecimal phoneFee = BigDecimal.ZERO;

  @Default
  private BigDecimal internetFee = BigDecimal.ZERO;

  @Default
  private BigDecimal officeSuppliesCost = BigDecimal.ZERO;

  @Default
  private BigDecimal otherCost = BigDecimal.ZERO;

  public BigDecimal getTotalAmount() {
    return this.rent
        .add(this.maintenanceFee)
        .add(this.electricityFee)
        .add(this.waterFee)
        .add(this.cityGasFee)
        .add(this.phoneFee)
        .add(this.internetFee)
        .add(this.officeSuppliesCost)
        .add(this.otherCost);
  }

}
