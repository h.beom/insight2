package ai.bankscan.files.controller;

import ai.bankscan.finance.domain.CashFlow;
import ai.bankscan.finance.service.FinanceService;
import ai.bankscan.survey.dto.금융건강진단표Dto;
import ai.bankscan.survey.dto.재무달성계획Dto;
import ai.bankscan.survey.service.금융건강진단표Service;
import ai.bankscan.survey.service.재무달성계획Service;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import net.sf.json.JSONObject;
import org.springframework.hateoas.MediaTypes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ai.bankscan.survey.service.금융건강진단표Service;
import org.springframework.web.servlet.ModelAndView;

import java.time.format.DateTimeFormatter;

@Api (tags = {"pdf 생성 api"})
@RequiredArgsConstructor
@Controller
@RequestMapping(path = PdfController.REQUEST_BASE_PATH, produces = MediaTypes.HAL_JSON_VALUE)
public class PdfController {
  static final String REQUEST_BASE_PATH = "/pdf";
  private final 금융건강진단표Service 금융건강진단표Service;
  private final FinanceService financeService;
  private final 재무달성계획Service 재무달성계획Service;

  @GetMapping("/create/{id}")
  public ModelAndView createPDF(@PathVariable final long id, ModelAndView mav) {
    금융건강진단표Dto.Response dto = 금융건강진단표Service.responseResult(id);
    CashFlow cashFlow = financeService.findLastCashFlow(id);
    JSONObject cashFlowResult = new JSONObject();
    if (cashFlow != null) {
      cashFlowResult
              .put("createdAt", cashFlow.getCreatedAt().format(DateTimeFormatter.ISO_DATE_TIME));
      cashFlowResult.put("data", JSONObject.fromObject(cashFlow.getJson()));
      cashFlowResult.put("year", cashFlow.getYear());
    }

    재무달성계획Dto.Response dto1 = 재무달성계획Service.getLatestOne(id);

    mav.setViewName("pdf/intro");

    mav.addObject("id", dto.getId());
    mav.addObject("answerList", dto.getAnswerList());
    mav.addObject("createdAt", dto.getCreatedAt());
    mav.addObject("financeResult", dto.getFinanceResult2());
    mav.addObject("firstType", dto.getFirstType());
    mav.addObject("lastType", dto.getLastType());
    mav.addObject("result", dto.getResult());
    mav.addObject("allData", dto.getFinanceResult());
    mav.addObject("cashFlow", cashFlowResult);
    mav.addObject("lifeCycle", dto1);
    return mav;
  }

}
