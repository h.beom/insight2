package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 카드_카드목록조회_Dto {

  private String card;

  private String 결제예정액;
  private String 결제일;
  private String 구분;
  private String 카드명;
  private String 카드번호;

}
