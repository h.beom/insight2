package ai.bankscan.finance.domain;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.member.Member;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface HouseRepository extends JpaRepository<House, Long>,
    JpaSpecificationExecutor<House> {

  void deleteByCreatedBy(Member member);

  List<House> findByCreatedBy(Account member);
}
