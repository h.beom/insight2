package ai.bankscan.survey.controller;

import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.common.infra.security.common.CurrentUser;
import ai.bankscan.survey.dto.금융건강진단표AnswersDto;
import ai.bankscan.survey.dto.금융건강진단표Dto;
import ai.bankscan.survey.dto.재무달성계획AnswersDto;
import ai.bankscan.survey.dto.재무달성계획Dto;
import ai.bankscan.survey.dto.희망노후생활비Dto;
import ai.bankscan.survey.service.금융건강진단표Service;
import ai.bankscan.survey.service.재무달성계획Service;
import ai.bankscan.survey.service.희망노후생활비Service;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.JsonObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import lombok.RequiredArgsConstructor;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = {"설문지 api"})
@RequiredArgsConstructor
@RestController
@RequestMapping(path = SurveyRestController.REQUEST_BASE_PATH, produces = MediaTypes.HAL_JSON_VALUE)
public class SurveyRestController {

    static final String REQUEST_BASE_PATH = "/api/survey";

    private final 금융건강진단표Service 금융건강진단표Service;
    private final 재무달성계획Service 재무달성계획Service;
    private final 희망노후생활비Service 희망노후생활비Service;

    private final 금융건강진단표ResourceAssembler 금융건강진단표ResourceAssembler;
    private final 재무달성계획ResourceAssembler 재무달성계획ResourceAssembler;


    @ApiOperation(value = "금귱건강진단표 질의 응답한 결과", notes = "금귱건강진단표 질의 응답한 결과")
    @PostMapping("/bank-health")
    @Secured("ROLE_MEMBER")
    public ResponseEntity<Resource<금융건강진단표Dto.Response>> getSurveyAnswer(
            @RequestBody List<금융건강진단표AnswersDto.Response> dataList) {
        Resource<금융건강진단표Dto.Response> resource = toDefaultSurveyResource(금융건강진단표Service
                .toResponseResult(dataList));
        return ResponseEntity.ok(resource);
    }

    @ApiOperation(value = "금융건강진단표 질의 응랍 리스트", notes = "금융 건강 진단표 질의 응답한 리스트")
    @GetMapping("/bank-health")
    @Secured("ROLE_MEMBER")
    public ResponseEntity<List<금융건강진단표Dto.Response>> getMySurveyList(
            @CurrentUser Member member,
            @RequestParam(value = "limit", required = false) Long limit
    ) {

        List<금융건강진단표Dto.Response> resource = 금융건강진단표Service
                .getMyResult(member, limit);
        return ResponseEntity.ok(resource);
    }

    @ApiOperation(value = "재무달성계획 질의 응답한 결과", notes = "재무달성계획 질의 응답한 결과")
    @PostMapping("/finances-goal")
    @Secured("ROLE_MEMBER")
    public ResponseEntity<Resource<재무달성계획Dto.Response>> getDetailSurveyAnswer(
            @RequestBody List<재무달성계획AnswersDto.Response> dataList) {
        Resource<재무달성계획Dto.Response> resource = toDetailSurveyResource(재무달성계획Service
                .toResponseResult(dataList));
        return ResponseEntity.ok(resource);
    }

    @ApiOperation(value = "재무달성계획 질의 응답한 결과", notes = "재무달성계획 질의 응답한 결과")
    @GetMapping("/finances-goal")
    @Secured("ROLE_MEMBER")
    public ResponseEntity<Resource<재무달성계획Dto.Response>> getLast(@CurrentUser Member member) {
        재무달성계획Dto.Response response = 재무달성계획Service.getLast(member);
        if (response == null) {
            return ResponseEntity.status(404).build();
        } else {
            Resource<재무달성계획Dto.Response> resource = toDetailSurveyResource(response);
            return ResponseEntity.ok(resource);
        }
    }


    @ApiOperation(value = "금융건강진단 결과 지표", notes = "금융건강진단 결과 지표")
    @GetMapping("/bank-health/summary")
    @Secured("ROLE_MEMBER")
    public ResponseEntity<JSONObject> responseSummary(@CurrentUser Member member) {
        금융건강진단표Dto.Response response = 금융건강진단표Service.responseResult(member.getId());
        if (response != null) {
            System.out.println(">>> NO FOUND ");
            if (response.getFinanceResult2() != null) {
                System.out.println(">>> NO DATA ");
                JSONObject jsonObject = JSONObject.fromObject(response.getFinanceResult2());
                return ResponseEntity.ok(jsonObject);
            }
        }
        return ResponseEntity.status(400).body(null);

        // TODO: summary 값 변경 필요.
//    JSONObject data = new JSONObject();
//    data.put("건강도", 2);
//    data.put("createdAt", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
//
//    JSONObject 우수항목 = new JSONObject();
//    우수항목.put("title", "저축율");
//    우수항목.put("value", 5);
//    우수항목.put("suffix", "%");
//    우수항목.put("than", 4);
//    data.put("우수항목", 우수항목);
//
//    JSONObject 금융위험도 = new JSONObject();
//
//    JSONArray 조치항목 = new JSONArray();
//    조치항목.add("소득 증가율");
//    조치항목.add("한계저축성향 지표");
//    조치항목.add("과소비 지표");
//    조치항목.add("저축률");
//    조치항목.add("금융상품 평균 수익률 외 3건");
//    조치항목.add("노후준비 저축률이 5%로 권장치인 10%에 부족");
//
//    금융위험도.put("조치항목", 조치항목);
//
//    data.put("금융위험도", 금융위험도);
//
//    data.put("재무달성도", 88);
//    data.put("현재자산", 99);
//
//    JSONArray 진단이력 = new JSONArray();
//    data.put("진단이력", 진단이력);
//    return ResponseEntity.ok(data);
    }

    @ApiOperation(value = "금융건강진단 결과 지표", notes = "금융건강진단 결과 지표")
    @GetMapping("/bank-health/result")
    @Secured("ROLE_MEMBER")
    public ResponseEntity<금융건강진단표Dto.Response> responseResult(@CurrentUser Member member) {
        금융건강진단표Dto.Response response = 금융건강진단표Service.responseResult(member.getId());
        if (response == null) {
            return ResponseEntity.status(400).body(null);
        }
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "금융건강진단 내역", notes = "금융건강진단 결과 내역")
    @GetMapping("/bank-health/history")
    @Secured("ROLE_MEMBER")
    public ResponseEntity<List<금융건강진단표Dto.Response>> responseHistory(@CurrentUser Member member) {
        List<금융건강진단표Dto.Response> response = 금융건강진단표Service.responseHistory(member);
        if (response == null) {
            return ResponseEntity.status(400).body(null);
        }
        return ResponseEntity.ok(response);
    }


    @ApiOperation(value = "재무달성계획 질의 응답한 결과", notes = "재무달성계획 질의 응답한 결과")
    @PutMapping("/finances-goal")
    @Secured("ROLE_MEMBER")
    public ResponseEntity<Resource<재무달성계획Dto.Response>> getDetailSurveyAnswer(
            @CurrentUser Member member,
            @RequestBody String body) {
        Resource<재무달성계획Dto.Response> resource = toDetailSurveyResource(
                재무달성계획Service.updateTarget(JSONArray.fromObject(body), member));
        return ResponseEntity.ok(resource);
    }

    @ApiOperation(value = "희망노후생활비 질의 결과", notes = "희망노후생활비 질의 결과 ")
    @PostMapping("/aged-life")
    @Secured("ROLE_MEMBER")
    public ResponseEntity<희망노후생활비Dto.Response> saveAgedLife(
            @RequestBody 희망노후생활비Dto.Create dto,
            @CurrentUser Member member) {

        if (!member.isMemberRole()) {
            return ResponseEntity.status(403).body(null);
        }

        희망노후생활비Dto.Response response = 희망노후생활비Service.toSaveAnswer(dto);
        if (response != null) {
            return ResponseEntity.ok(response);
        }
        return ResponseEntity.status(400).body(null);
    }

    @ApiOperation(value = "희망노후생활비 질의 결과", notes = "희망노후생활비 질의 결과 ")
    @GetMapping("/aged-life")
    @Secured("ROLE_MEMBER")
    public ResponseEntity<희망노후생활비Dto.Response> getMyAnswers(
            @CurrentUser Member member) {

        if (!member.isMemberRole()) {
            return ResponseEntity.status(403).body(null);
        }

        희망노후생활비Dto.Response response = 희망노후생활비Service.getData(member);
        if (response != null) {
            return ResponseEntity.ok(response);
        }
        return ResponseEntity.status(400).body(null);
    }

    private Resource<재무달성계획Dto.Response> toDetailSurveyResource(재무달성계획Dto.Response response) {
        return 재무달성계획ResourceAssembler.toResource(response);
    }


    private Resource<금융건강진단표Dto.Response> toDefaultSurveyResource(금융건강진단표Dto.Response response) {
        return 금융건강진단표ResourceAssembler.toResource(response);
    }


}
