package ai.bankscan.consultations.service;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.AccountRepository;
import ai.bankscan.accounts.domain.consultants.Consultant;
import ai.bankscan.accounts.domain.consultants.ConsultantRepository;
import ai.bankscan.accounts.domain.consultants.ProConsultant;
import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.calculate.domain.Calculate;
import ai.bankscan.calculate.domain.CalculateRepository;
import ai.bankscan.calculate.dto.CalculateDto;
import ai.bankscan.common.infra.web.excel.constants.Excel;
import ai.bankscan.consultations.domain.ConsultContent;
import ai.bankscan.consultations.domain.ConsultContentRepository;
import ai.bankscan.consultations.domain.consulting.Consultation;
import ai.bankscan.consultations.domain.consulting.Consultation.Status;
import ai.bankscan.consultations.domain.consulting.Consultation.Type;
import ai.bankscan.consultations.domain.consulting.ConsultationRepository;
import ai.bankscan.consultations.domain.logs.ConsultationHistory;
import ai.bankscan.consultations.domain.logs.ConsultationHistoryRepository;
import ai.bankscan.consultations.domain.logs.ConsultationLog;
import ai.bankscan.consultations.domain.logs.ConsultationLogRepository;
import ai.bankscan.consultations.dto.ConsultContentDto;
import ai.bankscan.consultations.dto.ConsultationDto;
import ai.bankscan.consultations.dto.ConsultationDto.Response;
import ai.bankscan.consultations.dto.ConsultationHistoryDto;
import ai.bankscan.consultations.dto.ConsultationLogDto;
import ai.bankscan.consultations.dto.ConsultationMapper;
import ai.bankscan.finance.domain.House;
import ai.bankscan.finance.domain.HouseRepository;
import ai.bankscan.finance.dto.HouseDto;
import ai.bankscan.organizations.domain.Center;
import ai.bankscan.organizations.domain.Institution;
import ai.bankscan.organizations.domain.InstitutionRepository;
import ai.bankscan.organizations.domain.OrganizationRepository;
import ai.bankscan.survey.domain.금융건강진단표Repository;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import ai.bankscan.survey.dto.희망노후생활비Dto;
import com.google.common.collect.Maps;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Slf4j
@RequiredArgsConstructor
@Transactional
@Service
public class ConsultationService {

  private final ConsultationRepository consultationRepository;
  private final ConsultationHistoryRepository consultationHistoryRepository;
  private final ConsultationLogRepository consultationLogRepository;
  private final OrganizationRepository organizationRepository;
  private final InstitutionRepository institutionRepository;
  private final AccountRepository accountRepository;
  private final CalculateRepository calculateRepository;
  private final ModelMapper modelMapper;
  private final 금융건강진단표Repository 금융건강진단표_Repository;
  private final HouseRepository houseRepository;
  private final ConsultContentRepository consultContentRepository;
  private final ConsultantRepository consultantRepository;

  private final EntityManager entityManager;


  public Page<ConsultationDto.Response> getConsultationList(
      Account currentUser,
      Pageable pageable,
      ConsultationDto.Search search
  ) {
    if (!currentUser.isAdminRole() && !currentUser.isMemberRole()) {
      if (currentUser.isConsultantRole() || currentUser.isCenterManageRole()) {
        search.setType(Type.BASIC_CONSULTATION);
      } else {
        search.setType(checkMyRole(currentUser));
      }
    }

    if (currentUser.isCenterManageRole() || currentUser.isRelationshipManagerRole()) {
      search.setOrganizationId(currentUser.getOrganization().getId());
    } else if (currentUser.isConsultantRole()) {
      search.setConsultantId(currentUser.getId());
    } else if (currentUser.isProConsultantRole()) {
      search.setProConsultantId(currentUser.getId());
    }

    Page<Consultation> consultations = consultationRepository
        .findAll(search.toSpecification(), pageable);

    List<ConsultationDto.Response> content = consultations.getContent().stream()
        .map(this::toResponse)
        .collect(Collectors.toList());

    return new PageImpl<>(content, pageable, consultations.getTotalElements());
  }



  public Map<String, Object> getConsultationAllCountFromStatus(
          Account currentUser,
          ConsultationDto.Search search
  ) {
    if (!currentUser.isAdminRole() && !currentUser.isMemberRole()) {
      if (currentUser.isConsultantRole() || currentUser.isCenterManageRole()) {
        search.setType(Type.BASIC_CONSULTATION);
      } else {
        search.setType(checkMyRole(currentUser));
      }
    }

    String where = "and created_at < now() ";

    if(!currentUser.isAdminRole()) {
      if(currentUser.isConsultantRole() || currentUser.isCenterManageRole()) {
        where += "and type = '"+ Type.BASIC_CONSULTATION +"' ";
        where += "and center_id = " + currentUser.getOrganization().getId() +" ";
        if(currentUser.getType().equals(Account.Type.CONSULTANT)) {
          where += "and consultant_id = "+currentUser.getId() +" ";
        }
      } else {
        if (currentUser.isProConsultantRole() || currentUser.isRelationshipManagerRole()) {
          where += "and type = '"+ Type.PRO_CONSULTATION +"' ";
          where += "and institution_id = " + currentUser.getOrganization().getId() + " ";
          if (currentUser.getType().equals(Account.Type.PRO_CONSULTANT)) {
            where += "and pro_consultant_id = " + currentUser.getId() + " ";
          }
        }
      }

    }
    String query = "SELECT status, count(status) as counts " +
            "FROM consultation " +
            "WHERE 1=1 " +
            where +
            " GROUP BY status";

    List<Object[]> resultQuery = entityManager.createNativeQuery(query).getResultList();

    Map<String, Object> values = new HashMap<>();

    for(Object[] objects : resultQuery) {
      values.put(objects[0].toString(), Integer.parseInt(objects[1].toString()));
    }
    return values;
  }
  public List<Response> getMyConsultationListRoleUser(
      Member member
  ) {

    List<Consultation> consultations = consultationRepository.findAllByCreatedBy(member);

    List<ConsultationDto.Response> content = consultations.stream().map(this::toResponse)
        .collect(Collectors.toList());
    return content;
  }

  @Transactional
  public ConsultationDto.Response getConsultation(Account currentUser, final Long consultationId) {

    Consultation consultation = getMyData(consultationId);

    if (!currentUser.isMemberRole()) {
      ConsultationHistoryDto.Create dto = ConsultationHistoryDto.Create.builder()
          .consultantId(currentUser.getId())
          .consultationId(consultationId)
          .logComment("상담 읽기")
          .status(consultation.getStatus())
          .build();

      consultationHistoryRepository.save(modelMapper.map(dto, ConsultationHistory.class));
    }

    ConsultationDto.Response response = ConsultationMapper.INSTANCE.consultationToDto(consultation);
//    modelMapper.map(consultation, ConsultationDto.Response.class);

    List<House> house = houseRepository.findByCreatedBy(consultation.getCreatedBy());
    response.setHouse(house.stream().map(this::toHouseDto).collect(Collectors.toList()));

    try {
      희망노후생활비Dto.Response response1 = 희망노후생활비Dto.Response.builder()
          .id(consultation.getId())
              .answer(this.isJSONValid(consultation.get희망노후생활비().getAnswer()))
              .result(this.isJSONValid(consultation.get희망노후생활비().getResult()))
          .build();
      response.set희망노후생활비(response1);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }

    return response;
  }

  private JSONObject isJSONValid(String test) {
    try {
      if(test != null) {
        return (JSONObject) JSONSerializer.toJSON(test);
      } else {
        return new JSONObject();
      }
    } catch (JSONException ex) {
      return new JSONObject();
    }
  }


  public ConsultationDto.BasicSimple getConsultationBasic(Account currentUser,
      final Long consultationId) {

    Consultation consultation = getMyData(consultationId);

    if (!currentUser.isMemberRole()) {
      ConsultationHistoryDto.Create dto = ConsultationHistoryDto.Create.builder()
          .consultantId(currentUser.getId())
          .consultationId(consultationId)
          .logComment("상담 읽기")
          .status(consultation.getStatus())
          .build();

      consultationHistoryRepository.save(modelMapper.map(dto, ConsultationHistory.class));

    }

    ConsultationDto.BasicSimple response = modelMapper
        .map(consultation, ConsultationDto.BasicSimple.class);
    List<ConsultContent> consultContents = consultContentRepository
        .findAllByConsultationAndConsultationType(consultation, Type.BASIC_CONSULTATION);

    response.setConsultContent(consultContents.stream().map(this::toConsultContent).collect(
        Collectors.toList()));

    return response;

  }

  public ConsultationDto.ProSimple getConsultationPro(Account currentUser,
      final Long consultationId) {

    Consultation consultation = consultationRepository.findById(consultationId).orElseThrow();
    if (!currentUser.isMemberRole()) {
      ConsultationHistoryDto.Create dto = ConsultationHistoryDto.Create.builder()
          .consultantId(currentUser.getId())
          .consultationId(consultationId)
          .logComment("상담 읽기")
          .status(consultation.getStatus())
          .build();

      consultationHistoryRepository.save(modelMapper.map(dto, ConsultationHistory.class));

    }

    ConsultationDto.ProSimple response = modelMapper
        .map(consultation, ConsultationDto.ProSimple.class);
    List<ConsultContent> consultContents = consultContentRepository
        .findAllByConsultationAndConsultationType(consultation, Type.PRO_CONSULTATION);

    response.setConsultContent(consultContents.stream().map(this::toConsultContent).collect(
        Collectors.toList()));

    return response;

  }

  private HouseDto.Response toHouseDto(House house) {
    return modelMapper.map(house, HouseDto.Response.class);
  }

  private ConsultContentDto.Response toConsultContent(ConsultContent consultContent) {
    return modelMapper.map(consultContent, ConsultContentDto.Response.class);
  }

  public ConsultationDto.Response createBasicConsultation(ConsultationDto.Create dto,
      Member member) {

    Center choiceCenter = (Center) organizationRepository.findById(dto.getCenterId()).get();
    Consultation consultation = modelMapper.map(dto, Consultation.class);
    consultation.setCenter(choiceCenter);
    consultation.setCode(this.makeConsultCode(choiceCenter, LocalDate.now()));
    consultation.set금융건강진단표(금융건강진단표_Repository.findById(dto.getSurveyId()).orElse(null));
    consultationRepository.save(consultation);

    ConsultationLogDto.Create logDto = ConsultationLogDto.Create.builder()
        .consultationId(consultation.getId())
        .status(consultation.getStatus())
        .build();

    ConsultationHistoryDto.Create historyDto = ConsultationHistoryDto.Create.builder()
        .consultantId(consultation.getCenter().getCreatedBy().getId())
        .consultationId(consultation.getId())
        .logComment("상담 생성")
        .status(consultation.getStatus())
        .build();

    ConsultationLog consultationLog = modelMapper.map(logDto, ConsultationLog.class);
    consultationLog.setConsultation(consultation);
    consultationLogRepository.save(consultationLog);
    ConsultationHistory consultationHistory = modelMapper
        .map(historyDto, ConsultationHistory.class);
    consultationHistory.setConsultation(consultation);
    consultationHistoryRepository.save(consultationHistory);
    return toResponse(consultation);
  }

  public ConsultationDto.Response updateConsultation(ConsultationDto.Update dto, final Long id) {
    Consultation existingConsultation = consultationRepository.findById(id).orElseThrow();
    if (existingConsultation != null) {
      modelMapper.map(dto, existingConsultation);
      if (dto.getConsultantId() != null) {
        Optional<Account> account = accountRepository.findById(dto.getConsultantId());

        existingConsultation.setConsultant(modelMapper.map(account.get(), Consultant.class));
      }
      if (dto.getInstitutionId() != null) {
        Optional<Institution> institution = institutionRepository.findById(dto.getInstitutionId());
        existingConsultation.setInstitution(institution.get());
      }
      if (dto.getProConsultantId() != null) {
        Optional<Account> account = accountRepository.findById(dto.getProConsultantId());
        existingConsultation.setProConsultant((ProConsultant) account.get());
      }

      consultationRepository.save(existingConsultation);

      ConsultationLogDto.Create logDto = ConsultationLogDto.Create.builder()
          .consultationId(existingConsultation.getId())
          .status(dto.getStatus())
          .build();

      ConsultationHistoryDto.Create historyDto = ConsultationHistoryDto.Create.builder()
          .consultantId(existingConsultation.getCenter().getCreatedBy().getId())
          .consultationId(existingConsultation.getId())
          .logComment("상담 정보 변경")
          .status(dto.getStatus())
          .build();

      if (dto.getStatus().equals(Status.PRO_APPLY) || dto.getStatus().equals(Status.COMPLETE)) {
        Calculate exCalculate = calculateRepository.findByConsultation(existingConsultation);
        if (exCalculate == null) {
          CalculateDto.Create calculateDto = CalculateDto.Create.builder()
              .consultationId(existingConsultation.getId())
              .consultant(existingConsultation.getConsultant().getId())
              .build();
          Calculate calculate = modelMapper.map(calculateDto, Calculate.class);
          calculate.setConsultation(existingConsultation);
          calculate.setConsultant(existingConsultation.getConsultant());

          calculateRepository.save(calculate);

        }
      }

      ConsultationLog consultationLog = modelMapper.map(logDto, ConsultationLog.class);
      consultationLog.setConsultation(existingConsultation);
      consultationLogRepository.save(consultationLog);
      ConsultationHistory consultationHistory = modelMapper
          .map(historyDto, ConsultationHistory.class);
      consultationHistory.setConsultation(existingConsultation);
      consultationHistoryRepository.save(consultationHistory);

    }
    return toResponse(existingConsultation);
  }


  public ConsultationDto.Response updateConsultation2(ConsultationDto.Update2 dto, final Long id) {
    Consultation existingConsultation = consultationRepository.findById(id).orElseThrow();
    if (existingConsultation != null) {
      modelMapper.map(dto, existingConsultation);
      if (dto.getConsultantId() != null) {
        Optional<Account> account = accountRepository.findById(dto.getConsultantId());

        existingConsultation.setConsultant(modelMapper.map(account.get(), Consultant.class));
      }
      if (dto.getInstitutionId() != null) {
        Optional<Institution> institution = institutionRepository.findById(dto.getInstitutionId());
        existingConsultation.setInstitution(institution.get());
      }
      if (dto.getProConsultantId() != null) {
        Optional<Account> account = accountRepository.findById(dto.getProConsultantId());
        existingConsultation.setProConsultant((ProConsultant) account.get());
      }

      consultationRepository.save(existingConsultation);

      ConsultationLogDto.Create logDto = ConsultationLogDto.Create.builder()
              .consultationId(existingConsultation.getId())
              .status(dto.getStatus())
              .build();
      ConsultationHistoryDto.Create historyDto = null;
      if(existingConsultation.getCenter() == null) {
        historyDto = ConsultationHistoryDto.Create.builder()
                .consultantId(existingConsultation.getInstitution().getCreatedBy().getId())
                .consultationId(existingConsultation.getId())
                .logComment("상담 정보 변경")
                .status(dto.getStatus())
                .build();
      } else {
        historyDto = ConsultationHistoryDto.Create.builder()
                .consultantId(existingConsultation.getCenter().getCreatedBy().getId())
                .consultationId(existingConsultation.getId())
                .logComment("상담 정보 변경")
                .status(dto.getStatus())
                .build();

      }

      if (dto.getStatus().equals(Status.PRO_APPLY) || dto.getStatus().equals(Status.COMPLETE)) {
        Calculate exCalculate = calculateRepository.findByConsultation(existingConsultation);
        if (exCalculate == null) {
          CalculateDto.Create calculateDto = CalculateDto.Create.builder()
                  .consultationId(existingConsultation.getId())
                  .consultant(existingConsultation.getConsultant().getId())
                  .build();
          Calculate calculate = modelMapper.map(calculateDto, Calculate.class);
          calculate.setConsultation(existingConsultation);
          calculate.setConsultant(existingConsultation.getConsultant());

          calculateRepository.save(calculate);

        }
      }

      ConsultationLog consultationLog = modelMapper.map(logDto, ConsultationLog.class);
      consultationLog.setConsultation(existingConsultation);
      consultationLogRepository.save(consultationLog);
      ConsultationHistory consultationHistory = modelMapper
              .map(historyDto, ConsultationHistory.class);
      consultationHistory.setConsultation(existingConsultation);
      consultationHistoryRepository.save(consultationHistory);

    }
    return toResponse(existingConsultation);
  }


  private String makeConsultCode(Center center, LocalDate localDate) {
    Long centerCount = consultationRepository.countByCenter(center);
    String returnCode = null;
    String numberCode = null;
    numberCode = String.format("%04d", centerCount + 1);

    returnCode = center.getCode().substring(0, 3) + localDate
        .format(DateTimeFormatter.ofPattern("yyyyMMdd")) + numberCode;

    return returnCode;
  }

  private ConsultationDto.Response toResponse(Consultation consultation) {
    return modelMapper.map(consultation, ConsultationDto.Response.class);
  }

  public Map<String, Object> getConsultationListToExcelDownload(
          Account currentUser,
          ConsultationDto.Search search
  ) {

    search.setType(checkMyRole(currentUser));
    if (currentUser.isCenterManageRole() || currentUser.isRelationshipManagerRole()) {
      search.setOrganizationId(currentUser.getOrganization().getId());
    } else if (currentUser.isConsultantRole()) {
      search.setConsultantId(currentUser.getId());
    } else if (currentUser.isProConsultantRole()) {
      search.setProConsultantId(currentUser.getId());
    }

    List<Consultation> consultations = consultationRepository
            .findAll(search.toSpecification());

    List<List<String>> body = consultations.stream()
            .map(consultation -> {
              List<String> a = new ArrayList();
              a.add(consultation.getCode());
              a.add(consultation.getCreatedBy().getUsername());
              a.add(consultation.getCreatedBy().getName());
              a.add(consultation.getCreatedBy().getMobilePhone());
              a.add(consultation.getCreatedBy().getEmail());
              if(consultation.getCenter() != null && consultation.getCenter().getName() != null) {
                a.add(consultation.getCenter().getName());
              } else {
                a.add(" ");
              }

              a.add(consultation.getConsultWantDate().format(DateTimeFormatter.ISO_DATE));
              a.add(consultation.getConsultWantTime().getTitle());
              a.add(consultation.getStatus().getTitle());
              if(consultation.getCenter() != null && consultation.getCenter().getName() != null) {
                a.add(consultation.getCenter().getName());
              } else {
                a.add(" ");
              }
              if(consultation.getCenter() != null && consultation.getCenter().getCode() != null) {
                a.add(consultation.getCenter().getCode());
              } else {
                a.add(" ");
              }

              if (consultation.getConsultant() != null) {
                a.add(consultation.getConsultant().getName());
                a.add(consultation.getConsultant().getMobilePhone());
                a.add(consultation.getConsultant().getEmail());
              } else {
                if(consultation.getCenter() != null && consultation.getCenter().getManager() != null) {
                  a.add(consultation.getCenter().getManager().getName());
                  a.add(consultation.getCenter().getManager().getMobilePhone());
                  a.add(consultation.getCenter().getManager().getEmail());
                } else {
                  a.add(" ");
                  a.add(" ");
                  a.add(" ");
                }
              }


              if(consultation.getInstitution() != null) {
                a.add(consultation.getInstitution().getName());
                a.add(consultation.getInstitution().getCode());
              } else {
                a.add("");
                a.add("");
              }

              if(consultation.getProConsultant() != null) {
                a.add(consultation.getProConsultant().getName());
                a.add(consultation.getProConsultant().getMobilePhone());
                a.add(consultation.getProConsultant().getEmail());
              } else {
                if(consultation.getInstitution() != null && consultation.getInstitution().getManager() != null ) {
                  a.add(consultation.getInstitution().getManager().getName());
                  a.add(consultation.getInstitution().getManager().getMobilePhone());
                  a.add(consultation.getInstitution().getManager().getEmail());
                } else {
                  a.add("");
                  a.add("");
                  a.add("");
                }
              }
              a.add(consultation.getCreatedAt().format(DateTimeFormatter.ISO_DATE));
              a.add(consultation.getUpdatedOn().format(DateTimeFormatter.ISO_DATE));
              return a;
            }).collect(Collectors.toList());

    Map<String, Object> data = Maps.newHashMap();
    data.put(Excel.FILE_NAME.getName(), "상담 정보");
    data.put(Excel.HEAD.getName(),
            Arrays.asList(
                    "상담번호",
                    "고객ID",
                    "이름",
                    "휴대폰번호",
                    "이메일",
                    "희망상담센터",
                    "희망상담날짜",
                    "희망상담시간",
                    "상담진행상태",
                    "센터명",
                    "센터코드",
                    "기본상담원명",
                    "기본상담원 연락처",
                    "기본상담원 이메일",
                    "기관명",
                    "기관코드",
                    "전문상담원명",
                    "전문상담원 연락처",
                    "전문상담원 이메일",
                    "작성일",
                    "수정일"
            ));
    data.put(Excel.BODY.getName(), body);
    return  data;

  }

  private Type checkMyRole (Account currentUser) {
    if (!currentUser.isAdminRole() && !currentUser.isMemberRole()) {
      if (currentUser.isConsultantRole() || currentUser.isCenterManageRole()) {
        return Type.BASIC_CONSULTATION;
      } else {
        return Type.PRO_CONSULTATION;
      }
    }
    return null;
  }

  private Consultation getMyData(final long consultationId) {
    return consultationRepository.findByIdWithData(consultationId).orElseThrow();
  }
}
