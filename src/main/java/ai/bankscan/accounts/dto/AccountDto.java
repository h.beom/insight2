package ai.bankscan.accounts.dto;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.Account.ConsultantLevel;
import ai.bankscan.accounts.domain.Account.EducationCompletionLevel;
import ai.bankscan.accounts.domain.Account.ProfessionalField;
import ai.bankscan.accounts.domain.Account.ProfessionalField2;
import ai.bankscan.accounts.domain.Account.Type;
import ai.bankscan.accounts.domain.Account.WorkerType;
import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.consultations.domain.consulting.Consultation;
import ai.bankscan.organizations.dto.OrganizationDto;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.util.CollectionUtils;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AccountDto {


  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "AccountDto.Create")
  public static class Create extends AccountBaseDto.Create {

    @NotNull
    @ApiModelProperty(value = "타입", example = "CENTER_MANAGER", position = 1, required = true)
    private Account.Type type;

    @DateTimeFormat(iso = ISO.DATE)
    @JsonSerialize(using = ToStringSerializer.class)
    private LocalDate effectiveDate;

    @JsonUnwrapped
    private long consultationPrice;

    private WorkerType workerType;

    private EducationCompletionLevel educationCompletionLevel;

    private ConsultantLevel consultantLevel;

    private ProfessionalField professionalField;

    @NotNull
    private Long organizationId;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "AccountDto.Update")
  public static class Update extends AccountBaseDto.Update {

    private OrganizationDto.Response organization;

    @DateTimeFormat(iso = ISO.DATE)
    @JsonSerialize(using = ToStringSerializer.class)
    private LocalDate effectiveDate;

    @JsonUnwrapped
    @ApiModelProperty(value = "상담 단가", example = "30,000", position = 12, required = true)
    private long consultationPrice;

    @ApiModelProperty(value = "근무 형태", example = "정규직", position = 13, required = true)
    private WorkerType workerType;

    @ApiModelProperty(value = "교육수료 등급", example = "기본수료", position = 14, required = true)
    private EducationCompletionLevel educationCompletionLevel;

    @ApiModelProperty(value = "상담원 레벨", example = "A레벨", position = 15, required = true)
    private ConsultantLevel consultantLevel;

    @ApiModelProperty(value = "전문분야", example = "금융상담", position = 16, required = true)
    private ProfessionalField professionalField;

    @ApiModelProperty(value = "전문분야_상", example = "금융상담 > 대출", position = 17, required = true)
    private ProfessionalField2 professionalField2;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "AccountDto.Response")
  public static class Response extends AccountBaseDto.Response {

    @ApiModelProperty(value = "타입", example = "CENTER_MANAGER", position = 20, required = true)
    private Account.Type type;

    @ApiModelProperty(value = "센터정보", example = "{type: center, name: dd, code: ESD1111}", position = 21, required = true)
    private OrganizationDto.Response organization;

    @DateTimeFormat(iso = ISO.DATE)
    @JsonSerialize(using = ToStringSerializer.class)
    private LocalDate effectiveDate;

    @JsonUnwrapped
    @ApiModelProperty(value = "상담 단가", example = "30,000", position = 22, required = true)
    private long consultationPrice;

    @ApiModelProperty(value = "근무 형태", example = "정규직", position = 23, required = true)
    private WorkerType workerType;

    @ApiModelProperty(value = "교육수료 등급", example = "기본수료", position = 24, required = true)
    private EducationCompletionLevel educationCompletionLevel;

    @ApiModelProperty(value = "상담원 레벨", example = "A레벨", position = 25, required = true)
    private ConsultantLevel consultantLevel;

    @ApiModelProperty(value = "전문분야", example = "대출", position = 26, required = true)
    private ProfessionalField professionalField;

    @ApiModelProperty(value = "전문분야2", example = "대출", position = 26, required = true)
    private ProfessionalField2 professionalField2;

    @Default
    @ApiModelProperty(value = "로그인 시각", position = 27)
    private long now = System.currentTimeMillis();

  }


  @Setter
  @Getter
  @NoArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "AccountDto.Simple")
  public static class Simple extends AccountBaseDto.Simple {

    @ApiModelProperty(value = "타입", example = "CENTER_MANAGER", position = 20, required = true)
    private Account.Type type;

    @DateTimeFormat(iso = ISO.DATE)
    @JsonSerialize(using = ToStringSerializer.class)
    private LocalDate effectiveDate;

    @ApiModelProperty(value = "상담 단가", example = "30,000", position = 22, required = true)
    private long consultationPrice;

    @ApiModelProperty(value = "근무 형태", example = "정규직", position = 23, required = true)
    private WorkerType workerType;

    @ApiModelProperty(value = "교육수료 등급", example = "기본수료", position = 24, required = true)
    private EducationCompletionLevel educationCompletionLevel;

    @ApiModelProperty(value = "상담원 레벨", example = "A레벨", position = 25, required = true)
    private ConsultantLevel consultantLevel;

    @ApiModelProperty(value = "전문분야", example = "대출", position = 26, required = true)
    private ProfessionalField professionalField;

    @ApiModelProperty(value = "전문분야2", example = "대출", position = 26, required = true)
    private ProfessionalField2 professionalField2;

    @Default
    @ApiModelProperty(value = "로그인 시각", position = 27)
    private long now = System.currentTimeMillis();

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "AccountDto.ResponseOrganization")
  public static class ResponseOrganization extends AccountBaseDto.Response {

    @ApiModelProperty(value = "타입", example = "CENTER_MANAGER", position = 10, required = true)
    private Account.Type type;

    private OrganizationDto.Response organization;

    @ApiModelProperty(value = "상담 단가", example = "30,000", position = 12, required = true)
    private long consultationPrice;

    @ApiModelProperty(value = "근무 형태", example = "정규직", position = 13, required = true)
    private WorkerType workerType;

    @ApiModelProperty(value = "교육수료 등급", example = "기본수료", position = 14, required = true)
    private EducationCompletionLevel educationCompletionLevel;

    @ApiModelProperty(value = "상담원 레벨", example = "A레벨", position = 15, required = true)
    private ConsultantLevel consultantLevel;

    @ApiModelProperty(value = "전문분야", example = "대출", position = 16, required = true)
    private ProfessionalField professionalField;

    @ApiModelProperty(value = "전문분야상세", example = "대출", position = 16, required = true)
    private ProfessionalField2 professionalField2;

  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString(callSuper = true)
  public static class Search extends AccountBaseDto.Search<Account> {

    @Default
    @ApiModelProperty(value = "계정유형", position = 101)
    private List<Type> types = Lists.newArrayList();

    private Consultation.Type currentConsultationType;


    private Type type;

    private WorkerType workerType;

    private EducationCompletionLevel educationCompletionLevel;

    private ConsultantLevel consultantLevel;

    private ProfessionalField2 professionalField2;

    @Default
    private String centerOrderBy = "desc";

    @Default
    private long center = 0;

    @Default
    private long institution = 0;


    @Override
    protected Restrictions generateRestrictions() {
      final Restrictions restrictions = super.generateRestrictions();

      if(center > 0) {
        restrictions.eq("organization.id", this.center);
      }

      if(institution > 0 ) {
        restrictions.eq("organization.id", this.institution);
      }

      if (currentConsultationType != null && currentConsultationType
          .equals(Consultation.Type.BASIC_CONSULTATION)) { // 기본 상담 검색
        if (type == null && CollectionUtils.isEmpty(this.types)) {
          this.types.add(Type.CONSULTANT);
          this.types.add(Type.CENTER_MANAGER);
        }

      } else if (currentConsultationType != null && currentConsultationType
          .equals(Consultation.Type.PRO_CONSULTATION)) { //전문 상담 검색
        if (type == null && CollectionUtils.isEmpty(this.types)) {
          this.types.add(Type.PRO_CONSULTANT);
          this.types.add(Type.RELATIONSHIP_MANAGER);
        }
      } else { //anything
        if (type == null) {
          this.types.add(Type.CONSULTANT);
          this.types.add(Type.PRO_CONSULTANT);
          this.types.add(Type.RELATIONSHIP_MANAGER);
          this.types.add(Type.CENTER_MANAGER);
        }
      }

      if (!CollectionUtils.isEmpty(this.types)) {
        restrictions.in("type", this.types);
      }

      if (this.type != null) {
        restrictions.eq("type", this.type);
      }

      if (!this.centerOrderBy.equals("desc")) {
        restrictions.addOrder("organization", Direction.ASC);
      } else {
        restrictions.addOrder("organization", Direction.DESC);
      }

      if (this.workerType != null) {
        restrictions.eq("workerType", this.workerType);
      }

      if (this.educationCompletionLevel != null && !this.educationCompletionLevel.equals("all")) {
        restrictions.eq("educationCompletionLevel", this.educationCompletionLevel);
      }

      if (this.consultantLevel != null && !this.consultantLevel.equals("all")) {
        restrictions.eq("consultantLevel", this.consultantLevel);
      }

      if (this.professionalField2 != null && !this.professionalField2.equals("all")) {
        restrictions.eq("professionalField2", this.professionalField2);
      }

      return restrictions;
    }

  }

  @Data
  public static class Find {

    @ApiModelProperty(value = "로그인 아이디", position = 101)
    private String username;

    @ApiModelProperty(value = "이메일 주소", position = 101)
    private String email;

  }
}
