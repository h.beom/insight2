package ai.bankscan.finance.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class 은행_대출계좌조회_Dto {

  @Data
  public static class Create {

    private String bank;

    private String 계좌번호;
    private String 대출금리;
    private Long 대출잔액;
    private Long 대출한도액;
    private String 만기일자;
    private String 신규일자;
    private String 예금종류;
    private String 이자납입일;
    private String 최종이수일자;
  }

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class GroupBy {

    private String bank;
    private String date;
    private String 계좌번호;
  }

}
