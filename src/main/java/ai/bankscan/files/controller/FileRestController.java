package ai.bankscan.files.controller;

import ai.bankscan.files.dto.FileDto;
import ai.bankscan.files.dto.FileDto.Download;
import ai.bankscan.files.properties.FileProperties;
import ai.bankscan.files.service.FileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.io.IOException;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author eomjeongjae
 * @since 2019-02-20
 */
@Api(tags = {"파일 관리 API"})
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/api/files", produces = MediaTypes.HAL_JSON_VALUE)
public class FileRestController {

  private final FileProperties fileProperties;
  private final FileService fileService;

  @ApiOperation(value = "파일 업로드", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  public FileDto.Response uploadFile(
      @ApiParam(value = "Select the file to Upload", required = true) MultipartFile file
  ) {
    return fileService.storeFile(file);
  }

  @ApiOperation(value = "상담사 등록 파일 업로드", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  @PostMapping(value = "/consultant/{type}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  public String uploadConsultant(
          @PathVariable String type,
      @ApiParam(value = "Select the consultant list file to Upload", required = true) MultipartFile file
  ) throws IOException {
    return fileService.saveConsultant(file, type);
  }

  @ApiOperation(value = "다중 파일 업로드", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  @PostMapping(path = "/multiple", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  public List<FileDto.Response> uploadFile(
      @ApiParam(value = "Select the files to Upload", required = true) List<MultipartFile> files
  ) {
    return fileService.storeFile(files);
  }

  @ApiOperation(value = "다중 파일 정보 가져오기")
  @PostMapping(path = "/info")
  public List<FileDto.Response> uploadFile(@RequestBody FileDto.Info body) {
    return fileService.getFiles(body.getIds());
  }

  @ApiOperation(value = "파일 다운로드")
  @GetMapping("/{id}")
  public ResponseEntity<Resource> downloadFile(
      @PathVariable("id") Long id,
      @RequestParam(required = false) boolean isAttachFile
  ) {
    Download download = fileService.loadFile(id);
    Resource resource = download.getResource();

    FileDto.Response attachFile = download.getAttachFile();
    String contentDisposition = attachFile.isImage() ? "inline" : "attachment";

    log.info("attachFile: {}", attachFile);

    if (isAttachFile) {
      contentDisposition = "attachment";
    }

    HttpHeaders headers = new HttpHeaders();
    if (StringUtils.isNotEmpty(fileProperties.getXSendFile())) {
      headers.add(fileProperties.getXSendFile(),
          "/attach/" + attachFile.getServerFilePath());
    } else {
      headers.add(HttpHeaders.CONTENT_DISPOSITION,
          contentDisposition + "; filename=\"" + attachFile.getFilename() + "\"");
    }

    return ResponseEntity.ok()
        .contentType(attachFile.getMediaType())
        .headers(headers)
        .body(resource);
  }

  @ApiOperation(value = "썸네일 다운로드")
  @GetMapping("/{id}/thumb")
  public ResponseEntity<Resource> downloadThumbnail(
      @PathVariable("id") Long id,
      @ApiParam(example = "200") @RequestParam(value = "w", required = false) long width,
      @ApiParam(example = "200") @RequestParam(value = "h", required = false) long height,
      @ApiParam(example = "crop") @RequestParam(value = "t", required = false, defaultValue = "crop") String type
  ) {
    Download download = fileService.loadFile(id, width, height, type);
    Resource resource = download.getResource();
    FileDto.Response attachFile = download.getAttachFile();

    log.info("attachFile: {}", attachFile);
    return ResponseEntity.ok()
        .contentType(attachFile.getMediaType())
        .header(HttpHeaders.CONTENT_DISPOSITION,
            "inline; filename=\"" + attachFile.getFilename() + "\"")
        .body(resource);
  }


}
