package ai.bankscan.finance.document;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface 보험_계약내용조회_Repository extends JpaRepository<보험_계약내용조회, Long>,
    JpaSpecificationExecutor<보험_계약내용조회> {

  @Query("SELECT COALESCE(SUM(기대출금액), 0) FROM 보험_계약내용조회 WHERE createdBy.id = :createdById")
  Long get기대출금액(@Param("createdById") Long createdById);

  @Query("SELECT COALESCE(SUM(해지환급금), 0) FROM 보험_계약내용조회 WHERE createdBy.id = :createdById")
  Object get해지환급금(@Param("createdById") Long createdById);
}
