$(function(){

  var thisTop = new Array();
  var thisIdx = new Array();
  var thisId = new Array();

  var $selector = $('.target');
  var $naviItem = $('.navigation-item');
  var $naviIcon = $naviItem.find('.icon-area');
  var headerH = $('header').innerHeight();
  console.log(headerH);

  for( i = 0; i < $selector.length; i++ ){

    var $thisTop = $selector.eq(i).offset().top;
    var $thisIdx = $selector.eq(i).index();
    var $thisId = $selector.eq(i).attr('id');
    thisTop.push($thisTop);
    thisIdx.push($thisIdx);
    thisId.push($thisId);
  };

  //header stiky
  $(window).scroll(function(){

    var $scr= $(this).scrollTop(); //현재 스크롤 위치
    console.log($scr);
    console.log(thisTop);

    if( $scr >= thisTop[0] - headerH && $scr <= thisTop[1] - 50  ){
      console.log("0번째 섹션");
      $naviItem.removeClass('active').eq(0).addClass('active');
    }
    else if( $scr >= thisTop[1] && $scr <= thisTop[2] - 50  ){
      $naviItem.removeClass('active').eq(1).addClass('active');
    }
    else if( $scr >= thisTop[2] && $scr <= thisTop[3] - 50  ){
      $naviItem.removeClass('active').eq(2).addClass('active');
    }
    else if( $scr >= thisTop[3] && $scr <= thisTop[4] - 50  ){
      $naviItem.removeClass('active').eq(3).addClass('active');
    }
    else if( $scr >= thisTop[4] && $scr <= thisTop[5] - 50  ){
      $naviItem.removeClass('active').eq(4).addClass('active');
    }
    else if( $scr >= thisTop[5] && $scr <= thisTop[6] - 50  ){
      $naviItem.removeClass('active').eq(5).addClass('active');
    }
    else if( $scr >= thisTop[6] && $scr <= thisTop[7] - 50  ){
      $naviItem.removeClass('active').eq(6).addClass('active');
    }
    else if( $scr >= thisTop[7] - headerH && $scr <= thisTop[8] - 50  ){
      $naviItem.removeClass('active').eq(7).addClass('active');
    }
    else if( $scr >= thisTop[8] - headerH && $scr <= thisTop[9] - 50  ){
      $naviItem.removeClass('active').eq(8).addClass('active');
    }
    else if( $scr >= thisTop[9] - headerH ){
      $naviItem.removeClass('active').eq(9).addClass('active');
    }

    // $('.navigation-wrap').addClass('active'); //네비게이션 보이기


    if( $scr ){
      $('header').addClass('active');
    }else{
      $('header').removeClass('active');
    }

    if( $scr > 700 ){
      $('.navigation-wrap').addClass('active');
    }else{
      $('.navigation-wrap').removeClass('active');
    }

  });

  /* 네비게이션 버튼 클릭 이벤트 */
  $('.navigation-item').on('click',function(){
    var $this = $(this);
    var crrIdx = $this.index();
    var $thisIcon = $this.find('.icon-area');
    var posY = thisTop[crrIdx];

    console.log($this);
    console.log($thisIcon);
    console.log(posY);

    $this.addClass('active').siblings().removeClass('active');
//    $thisIcon.addClass('active').parent('li').siblings().children('.icon-area').animate({
//      width : '30px'
//    },300).removeClass('active');
    $('html,body').animate({
      scrollTop : posY - headerH + 'px'
    },500);

  });

  //top - btn 클릭 이벤트
  $('.btn-scrtop').on("click",function(){
    $('html,body').stop().animate({
      scrollTop : 0
    },500);
  });




});