package ai.bankscan.accounts.domain;

import ai.bankscan.common.infra.jpa.domain.BaseEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author eomjeongjae
 * @since 2019/11/27
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@ToString
@Table(indexes = {
    @Index(columnList = "accessToken", unique = true),
    @Index(columnList = "serialNumber")
})
public class AccountDevice extends BaseEntity {

  public enum Platform {IOS, ANDROID, WEB}

  @Include
  @ManyToOne(optional = false, fetch = FetchType.EAGER)
  private Account account;

  @Include
  @Column(nullable = false)
  private String accessToken;

  @Include
  @Column(nullable = false)
  private String secretToken;

  @Include
  @Column(nullable = false)
  private String serialNumber;

  @Column(nullable = true)
  private String pushToken;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private Platform platform;

  @Column(nullable = false)
  private String deviceName;

  @Default
  @Setter
  private boolean pushConsulting = true; // 상담 진행 여부

  @Default
  @Setter
  private boolean pushDiagnosisEnd = true; // 진단 결과 완료부 여부

  @Default
  @Setter
  private boolean pushNotice = true; //공지사항 알림 여부

  public void setAccount(Account account) {
    if (this.account != null) {
      this.account.getDevices().remove(this);
    }
    this.account = account;
    this.account.getDevices().add(this);
  }

}
