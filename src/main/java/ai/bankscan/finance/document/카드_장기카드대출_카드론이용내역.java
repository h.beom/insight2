package ai.bankscan.finance.document;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Table(indexes = {@Index(columnList = "card"), @Index(columnList = "대출번호")})
public class 카드_장기카드대출_카드론이용내역 extends AuditableJoinEntity<Account> {

  private String card;

  private String 대출번호;
  private String 대출종류;
  private String 신규일자;
  private String 만기일자;
  private String 완납일자;
  private Long 대출금액;
  private Long 대출잔액;
  private String 대출금리;
  private String 대출개월;

}
