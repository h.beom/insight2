package ai.bankscan.common.infra.security.utils;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.service.AccountAdapter;
import com.google.common.collect.Sets;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author eomjeongjae
 * @since 2019/12/17
 */
public class SecurityUtils {

  public static boolean anyGranted(String... roles) {
    return anyGranted(Sets.newHashSet(roles));
  }

  public static boolean anyGranted(Set<String> roles) {
    List<String> grantedRoles = getGrantedRoles();
    for (String role : roles) {
      if (grantedRoles.contains(role) || grantedRoles.contains("ROLE_" + role)) {
        return true;
      }
    }
    return false;
  }

  public static boolean notGranted(String... roles) {
    return notGranted(Sets.newHashSet(roles));
  }

  public static boolean notGranted(Set<String> roles) {
    List<String> grantedRoles = getGrantedRoles();
    for (String role : roles) {
      if (grantedRoles.contains(role)) {
        return false;
      }
    }
    return true;
  }

  public static AccountAdapter getPrincipal() {
    return Optional.ofNullable(SecurityContextHolder.getContext())
        .map(SecurityContext::getAuthentication)
        .filter(Authentication::isAuthenticated)
        .map(Authentication::getPrincipal)
        .filter(o -> AccountAdapter.class.isAssignableFrom(o.getClass()))
        .map(AccountAdapter.class::cast)
        .orElse(null);
  }

  public static Account getCurrentUser() {
    return Optional.ofNullable(getPrincipal())
        .map(AccountAdapter::getAccount)
        .orElse(null);
  }

  private static List<String> getGrantedRoles() {
    return getAuthorities().stream()
        .map(GrantedAuthority::getAuthority)
        .collect(Collectors.toList());
  }

  public static List<? extends GrantedAuthority> getAuthorities() {
    return (List<? extends GrantedAuthority>) SecurityContextHolder.getContext()
        .getAuthentication().getAuthorities();
  }

}
