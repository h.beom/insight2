package ai.bankscan.bound.dto;

import ai.bankscan.accounts.domain.Account.Type;
import ai.bankscan.bound.domain.Bound;
import ai.bankscan.bound.domain.Bound.CallType;
import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.common.infra.jpa.support.SearchDto;
import ai.bankscan.consultations.dto.ConsultationDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BoundDto {


  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "BoundDto.Response")
  public static class Response {
    private Long id;
    private CallType callType;
    private LocalDateTime sCall;
    private LocalDateTime eCall;
    private String receivePhone;
    private String senderPhone;
    private ConsultationDto.Response consultation;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "BoundDto.Search", description = "serach")
  public static class Search extends SearchDto<Bound> {

    @ApiModelProperty(value = "검색어")
    private String keyword;

    @ApiModelProperty(value = "검색 구분",
        example = "고객명: memberName, "
            + "상담원명: accountName, "
            + "센터명/기관명: organizationName, "
            + "센터코드/기관코드: organizationCode")
    private String searchType;

    @ApiModelProperty(value = "상담유형")
    private CallType callType;



    @ApiModelProperty(value = "계정유형", position = 100)
    private Type type;

    @Override
    protected Restrictions generateRestrictions() {
      final Restrictions restrictions = new Restrictions();


      if(this.callType != null) {
        restrictions.eq("callType", this.callType);
      }

      if(this.type != null) {
        restrictions.eq("type", this.type);
      }

      if(StringUtils.isNotEmpty(this.keyword)) {
        if(StringUtils.isEmpty(this.searchType)) {
          final Restrictions restrictionsByKeyword = new Restrictions(Restrictions.Conn.OR);
          List<String> keywords = Arrays.asList(this.keyword.trim().split("\\s+"));
          keywords.stream()
              .forEach(keyword -> {
                restrictionsByKeyword.like("member.username", "%" + keyword + "%");
                restrictionsByKeyword.like("consultant.username", "%" + keyword + "%");
                restrictionsByKeyword.like("member.name", "%" + keyword + "%");
                restrictionsByKeyword.like("consultant.name", "%" + keyword + "%");

              });
          restrictions.addChild(restrictionsByKeyword);
        } else {
          restrictions.like(searchType, "%" + keyword + "%");
        }
      }

      return restrictions;

    }
    protected void addSearchKeyword(final Restrictions restrictions, final String keyword) {}

  }

}
