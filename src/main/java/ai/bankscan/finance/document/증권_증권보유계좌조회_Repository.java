package ai.bankscan.finance.document;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface 증권_증권보유계좌조회_Repository extends JpaRepository<증권_증권보유계좌조회, Long>,
    JpaSpecificationExecutor<증권_증권보유계좌조회> {

  @Query("SELECT COALESCE(SUM(평가총액), 0) FROM 증권_증권보유계좌조회 WHERE createdBy.id = :createdById")
  Long get전체잔고(@Param("createdById") Long createdById);
}
