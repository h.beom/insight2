package ai.bankscan.configs.properties;

import javax.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@ToString
@Configuration
@ConfigurationProperties("app")
public class AppProperties {

  @NotEmpty
  private String name;

  @NotEmpty
  private String host;

  @NotEmpty
  private String url;

}
