package ai.bankscan.common.address.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Embeddable
public class Address {

  @Column(length = 5)
  private String zipCode;

  private String address;

  private String detailedAddress;

}
