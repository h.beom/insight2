package ai.bankscan.survey.dto;

import ai.bankscan.accounts.dto.MemberDto;
import ai.bankscan.survey.domain.금융건강진단종합.Type;
import ai.bankscan.survey.dto.금융건강진단표AnswersDto.Result;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class 금융건강진단종합Dto {

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "금융건강진단종합Dto.Create")
  public static class Create {
    @ApiModelProperty(value = "종류", example = "현명한금융거래", position = 1)
    private Type type;
    @ApiModelProperty(value = "관리 타이틀", example = "소득관리", position = 2)
    private String title;
    @ApiModelProperty(value = "소득관리 object", example = "{}", position = 3)
    private String values;
    @ApiModelProperty(value = "금융건강진단표 결과", example = "금융건강진단표 결과", position = 4)
    private 금융건강진단표Dto.Response 금융건강진단표;

  }


  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ApiModel(value = "금융건강진단종합Dto.Response")
  public static class Response {

    @ApiModelProperty(value = "결과", example = "소비지출형", position = 2)
    private Long id;
    @ApiModelProperty(value = "종류", example = "현명한금융거래", position = 1)
    private Type type;
    @ApiModelProperty(value = "관리 타이틀", example = "소득관리", position = 2)
    private String title;
    @ApiModelProperty(value = "소득관리 object", example = "{}", position = 3)
    private String values;
    @ApiModelProperty(value = "금융건강진단표 결과", example = "금융건강진단표 결과", position = 4)
    private 금융건강진단표Dto.Response 금융건강진단표;
  }

}
