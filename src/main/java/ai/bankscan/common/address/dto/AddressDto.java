package ai.bankscan.common.address.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

public class AddressDto {

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  @ToString
  @ApiModel(value = "AddressDto.Save")
  public static class Save {

    @ApiModelProperty(value = "우편번호", example = "12345", required = true, position = 1001)
    private String zipCode;

    @ApiModelProperty(value = "주소", example = "서울 영등포구 신풍로 77", required = true, position = 1002)
    private String address;

    @ApiModelProperty(value = "상세주소", example = "레미안에스티움 102동 902호", required = true, position = 1003)
    private String detailedAddress;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  @ToString
  @ApiModel(value = "AddressDto.Response")
  public static class Response {

    @ApiModelProperty(value = "우편번호", example = "12345", required = true, position = 1001)
    private String zipCode;

    @ApiModelProperty(value = "주소", example = "서울 영등포구 신풍로 77", required = true, position = 1002)
    private String address;

    @ApiModelProperty(value = "상세주소", example = "레미안에스티움 102동 902호", required = true, position = 1003)
    private String detailedAddress;

  }

}
