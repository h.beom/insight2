package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 은행_대출거래내역조회_Dto {

  private String bank;

  private String 계좌번호;
  private String 거래일자;
  private String 거래구분;
  private String 거래금액;
  private String 거래원금;
  private String 대출잔액;
  private String 대출이율;
  private String 이자계산시작일;
  private String 이자계산종료일;
  private String 이자계산기간;
  private String 이자금액;
  private String 이자상세1;
  private String 이자상세2;
  private String 이자상세3;
  private String 이자상세4;
  private String 이자상세5;
  private String 비고1;
  private String 비고2;
  private String 계좌번호확장1;

}
