package ai.bankscan.accounts.controller;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.AccountDevice;
import ai.bankscan.accounts.dto.AccountDeviceDto;
import ai.bankscan.accounts.dto.MemberDto;
import ai.bankscan.accounts.service.AccountDeviceService;
import ai.bankscan.accounts.service.AccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.Arrays;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import net.sf.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = {"인증 API"})
@RequiredArgsConstructor
@RestController
@RequestMapping(path = AuthRestController.REQUEST_BASE_PATH)
public class AuthRestController {

  static final String REQUEST_BASE_PATH = "/api/auth";

  private final AccountService accountService;
  private final AccountDeviceService accountDeviceService;
  private final ModelMapper mapper;

  @RequestMapping("/expired")
  public ResponseEntity expired() {
    return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
  }

  @ApiOperation(value = "match password")
  @PreAuthorize("isAuthenticated()")
  @PostMapping("/match-password")
  public ResponseEntity<Boolean> matchedPassword(@RequestBody Map<String, String> data) {
    String username = data.get("username");
    String password = data.get("password");
   Account account = accountService.checkByAuthorize(username, password);
   if(account != null) {
     return ResponseEntity.ok(true);
   }
   return ResponseEntity.status(400).body(false);
  }

  @ApiOperation(value = "회원 로그인")
  @PostMapping("/login")
  public ResponseEntity<AccountDeviceDto.Response> loginMember(
      HttpServletRequest request,
      HttpServletResponse response,
      MemberDto.Login dto
  ) {

    try {
      Account account = accountService.findByAuthorize(dto.getUsername(), dto.getPassword());

      setUUID(request, response, dto);

      AccountDeviceDto.Create createDto = mapper.map(dto, AccountDeviceDto.Create.class);
      createDto.setAccount(account);

      AccountDevice accountDevice = accountDeviceService.create(createDto);
      return ResponseEntity.ok(mapper.map(accountDevice, AccountDeviceDto.Response.class));
    } catch (AuthenticationException authEx) {
      return ResponseEntity.status(400).build();
    } catch (Exception ex) {
      ex.printStackTrace();
      return ResponseEntity.status(500).build();
    }

  }

  @ApiOperation(value = "회원 로그인 (oauth2 기반)")
  @PostMapping("/token")
  public ResponseEntity token(
      HttpServletRequest request,
      HttpServletResponse response,
      MemberDto.Login dto
  ) {

    try {
      Account account = accountService.findByAuthorize(dto.getUsername(), dto.getPassword());

      setUUID(request, response, dto);

      AccountDeviceDto.Create createDto = mapper.map(dto, AccountDeviceDto.Create.class);
      createDto.setAccount(account);

      AccountDevice accountDevice = accountDeviceService.create(createDto);

      JSONObject object = new JSONObject();
      object.put("access_token", accountDevice.getAccessToken());
      object.put("refresh_token", accountDevice.getSecretToken());
      object.put("token_type", "bs_token");

      return ResponseEntity.ok(object);

    } catch (AuthenticationException authEx) {
      return ResponseEntity.status(400).build();
    } catch (Exception ex) {
      ex.printStackTrace();
      return ResponseEntity.status(500).build();
    }
  }

  private void setUUID(HttpServletRequest request, HttpServletResponse response,
      MemberDto.Login dto) {
    if (dto.getPlatform() == null || dto.getPlatform() == AccountDevice.Platform.WEB) {
      Cookie cookie = request.getCookies() == null ? null : Arrays.stream(request.getCookies())
          .filter(v -> v.getName().equalsIgnoreCase("_uid"))
          .findFirst()
          .orElse(null);

      if (cookie == null) {
        String uuid = accountDeviceService.createUniqueUUID();
        cookie = new Cookie("_uid", uuid);
        cookie.setMaxAge(60 * 60 * 24 * 365);
        response.addCookie(cookie);
      }
      dto.setPlatform(AccountDevice.Platform.WEB);
      dto.setSerialNumber(cookie.getValue());
      dto.setDeviceName(request.getHeader("User-Agent"));
    }
  }

}
