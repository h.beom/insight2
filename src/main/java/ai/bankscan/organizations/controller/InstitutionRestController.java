package ai.bankscan.organizations.controller;

import ai.bankscan.common.infra.annotations.ApiPageable;
import ai.bankscan.organizations.domain.Organization;
import ai.bankscan.organizations.domain.Organization.Type;
import ai.bankscan.organizations.domain.OrganizationRepository;
import ai.bankscan.organizations.dto.InstitutionDto;
import ai.bankscan.organizations.dto.InstitutionDto.Response;
import ai.bankscan.organizations.service.InstitutionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = {"기관 단체 관리 API"})
@RequiredArgsConstructor
@RestController
@RequestMapping(path = InstitutionRestController.REQUEST_BASE_PATH, produces = MediaTypes.HAL_JSON_VALUE)
public class InstitutionRestController {

  static final String REQUEST_BASE_PATH = "/api/institutions";

  private final InstitutionService institutionService;
  private final OrganizationRepository organizationRepository;
  private final InstitutionResourceAssembler institutionResourceAssembler;

  @Secured("ROLE_ADMIN")
  @ApiOperation(value = "기관 단체 코드 중복 검사", notes = "기관 단체가 중복되지 않으면 성공(200)")
  @GetMapping("/{code}/check")
  public ResponseEntity<?> checkInstitutionCode(
      @ApiParam(required = true, example = "code") @PathVariable final String code
  ) {
    Optional<Organization> optionalOrganization = organizationRepository
        .findByTypeAndCode(Type.INSTITUTION, code);

    if (optionalOrganization.isPresent()) {
      return ResponseEntity.badRequest().build();
    }
    return ResponseEntity.ok().build();
  }

  @Secured("ROLE_ADMIN")
  @ApiOperation(value = "기관 단체 등록")
  @PostMapping
  public ResponseEntity<Resource<Response>> createInstitution(
      @RequestBody @Valid InstitutionDto.Create dto
  ) throws URISyntaxException {
    Response savedInstitution = institutionService
        .createOrganization(dto);
    Resource<Response> resource = institutionResourceAssembler.toResource(savedInstitution);

    return ResponseEntity
        .created(new URI(resource.getLink(Link.REL_SELF).getHref()))
        .body(resource);
  }

  @Secured("ROLE_ADMIN")
  @ApiPageable
  @ApiOperation(value = "기관 단체 목록 조회")
  @GetMapping
  public Resources<Resource<Response>> getInstitutions(
      InstitutionDto.Search search,
      @PageableDefault(sort = "createdAt", direction = Direction.DESC) Pageable pageable,
      PagedResourcesAssembler<Response> pagedResourcesAssembler
  ) {
    Page<Response> page = institutionService.getOrganizations(search, pageable);
    return pagedResourcesAssembler.toResource(page, institutionResourceAssembler);
  }

  @Secured("ROLE_ADMIN")
  @ApiOperation(value = "기관 단체 조회")
  @GetMapping("/{id}")
  public ResponseEntity<Resource<Response>> getInstitution(
      @ApiParam(required = true, example = "1") @PathVariable final Long id
  ) {
    Resource<Response> resource = institutionResourceAssembler
        .toResource(institutionService.getOrganization(id));
    return ResponseEntity.ok(resource);
  }

  @Secured("ROLE_ADMIN")
  @ApiOperation(value = "기관 단체 수정")
  @PutMapping("/{id}")
  ResponseEntity<Resource<Response>> updateInstitution(
      @ApiParam(required = true, example = "1") @PathVariable final Long id,
      @RequestBody @Valid InstitutionDto.Update dto,
      @AuthenticationPrincipal final String username
  ) throws URISyntaxException {
    Response savedTeacher = institutionService
        .updateOrganization(id, dto, username);
    Resource<Response> resource = institutionResourceAssembler.toResource(savedTeacher);

    return ResponseEntity
        .created(new URI(resource.getLink(Link.REL_SELF).getHref()))
        .body(resource);
  }

  @Secured("ROLE_ADMIN")
  @ApiOperation(value = "기관 단체 삭제")
  @DeleteMapping("/{id}")
  ResponseEntity<?> deleteInstitution(
      @ApiParam(required = true, example = "1") @PathVariable final Long id,
      @AuthenticationPrincipal final String username
  ) {
    institutionService.deleteOrganization(id);
    return ResponseEntity
        .noContent()
        .build();
  }

}
