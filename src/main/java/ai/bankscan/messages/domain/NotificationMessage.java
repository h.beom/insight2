package ai.bankscan.messages.domain;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.AccountDevice;
import ai.bankscan.common.infra.jpa.domain.BaseEntity;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * @author eomjeongjae
 * @since 2019/12/16
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class NotificationMessage extends BaseEntity {

  @Getter
  @RequiredArgsConstructor
  public enum Type {
    MANUAL,
    NOTICE,
    BASIC_CONSULTATION_RECEPTION
  }

  @RequiredArgsConstructor
  @Getter
  public enum SendType {
    DIRECT,
    SCHEDULED;

    public boolean isDirect() {
      return (this == DIRECT);
    }

  }

  @RequiredArgsConstructor
  @Getter
  public enum Status {
    IN_PREPARATION, SUCCESS, CANCELLATION, FAILURE;
  }

  @Setter
  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  private AccountDevice receiver;

  @Default
  @Setter
  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private SendType sendType = SendType.DIRECT;

  @Setter
  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private Type type;

  @Column(nullable = false)
  private String title;

  @Lob
  @Column(nullable = false)
  private String body;

  @Column(length = 500)
  private String link;

  @Setter
  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  @Default
  private Status status = Status.IN_PREPARATION;

  @Setter
  private LocalDateTime sentDateTime;

  private LocalDateTime scheduledDateTime;

  private Long relatedId;

  @Setter
  private int successCount;

  @Setter
  private int failureCount;

  @Lob
  private String errorMsg;

  public void create(AccountDevice receiver) {
    this.receiver = receiver;
    if (this.sendType.isDirect()) {
      this.sentDateTime = LocalDateTime.now();
      this.status = Status.SUCCESS;
    }
  }

  public void completeToSend() {
    this.sentDateTime = LocalDateTime.now();
    this.status = Status.SUCCESS;
  }

  public void completeToSend(
      int successCount,
      int failureCount
  ) {
    this.successCount = successCount;
    this.failureCount = failureCount;
    this.status = Status.SUCCESS;
    this.sentDateTime = LocalDateTime.now();
  }

  public void failToSend(String errorMsg) {
    this.status = Status.FAILURE;
    this.errorMsg = errorMsg;
    this.sentDateTime = LocalDateTime.now();
  }

}
