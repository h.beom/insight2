package ai.bankscan.accounts.domain.member;

import ai.bankscan.accounts.domain.Account;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@DiscriminatorValue("MEMBER")
public class Member extends Account {
  @Default
  private Type type = Type.MEMBER;

  @Getter
  @RequiredArgsConstructor
  public enum MemberLevel {
    DEFAULT("일반"), BASIC_CONSULTANT("기본상담원");
    private final String title;
  }

  @Default
  private MemberLevel memberLevel = MemberLevel.DEFAULT;



}
