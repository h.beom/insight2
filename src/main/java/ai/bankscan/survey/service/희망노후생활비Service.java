package ai.bankscan.survey.service;

import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.survey.domain.희망노후생활비;
import ai.bankscan.survey.domain.희망노후생활비Repository;
import ai.bankscan.survey.dto.희망노후생활비Dto;

import java.util.List;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service
public class 희망노후생활비Service {
    private final ModelMapper modelMapper;
    private final 희망노후생활비Repository 희망노후생활비Repository;


    @Transactional
    public 희망노후생활비Dto.Response toSaveAnswer(희망노후생활비Dto.Create dto) {
        희망노후생활비 ex = modelMapper.map(dto, 희망노후생활비.class);

        ex.setAnswer(dto.getAnswer().toString());
        ex.setResult(dto.getResult().toString());

        희망노후생활비Repository.save(ex);
        return modelMapper.map(ex, 희망노후생활비Dto.Response.class);
    }

    public 희망노후생활비Dto.Response getData(Member member) {
        List<희망노후생활비> dataList = 희망노후생활비Repository.findAllByCreatedBy(member, Sort.by(Direction.DESC, "createdAt"));

        if (dataList.size() > 0) {
            List<희망노후생활비Dto.Response> data = dataList.stream()
                    .map(v -> {
                        희망노후생활비Dto.Response res =
                                희망노후생활비Dto.Response.builder()
                                        .id(v.getId())
                                        .answer(isJSONValid(v.getAnswer()))
                                        .result(isJSONValid(v.getAnswer()))
                                        .build();

                        return res;
                    })
                    .collect(Collectors.toList());


            return data.get(0);
        }
        return null;
    }

    private 희망노후생활비Dto.Response toResponse(희망노후생활비 data) {
        return modelMapper.map(data, 희망노후생활비Dto.Response.class);
    }

    private JSONObject isJSONValid(String test) {
        try {
            if(test != null) {
                return (JSONObject) JSONSerializer.toJSON(test);
            } else {
                return new JSONObject();
            }
        } catch (JSONException ex) {
            return new JSONObject();
        }
    }
}
