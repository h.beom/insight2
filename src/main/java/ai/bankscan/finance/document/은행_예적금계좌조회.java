package ai.bankscan.finance.document;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import ai.bankscan.finance.dto.은행_예적금계좌조회_Dto;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Table(indexes = {@Index(columnList = "bank"), @Index(columnList = "계좌번호")})
@SqlResultSetMapping(name = "은행_예적금계좌조회_GroupBy", classes = @ConstructorResult(
    targetClass = 은행_예적금계좌조회_Dto.GroupBy.class,
    columns = {
        @ColumnResult(name = "bank", type = String.class),
        @ColumnResult(name = "계좌번호", type = String.class),
        @ColumnResult(name = "date", type = String.class),
    }))
public class 은행_예적금계좌조회 extends AuditableJoinEntity<Account> {

  private String bank;

  private String 계좌번호;

  private String 계좌번호확장;

  private String 만기일자;

  private String 신규일자;

  private String 예금종류;

  private String 이자율;

  private Long 현재잔액;

  private String 월부금;

}
