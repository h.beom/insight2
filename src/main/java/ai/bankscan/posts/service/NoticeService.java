package ai.bankscan.posts.service;

import ai.bankscan.messages.domain.NotificationMessage.Type;
import ai.bankscan.messages.dto.NotificationDto;
import ai.bankscan.messages.service.NotificationService;
import ai.bankscan.posts.domain.Notice;
import ai.bankscan.posts.domain.PostRepository;
import ai.bankscan.posts.dto.NoticeDto;
import ai.bankscan.posts.dto.NoticeDto.Response;
import ai.bankscan.posts.dto.PostDto.Create;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Transactional(readOnly = true)
@Service
public class NoticeService extends PostService<Notice, NoticeDto.Response> {

  private final NotificationService notificationService;

  @Autowired
  public NoticeService(PostRepository postRepository, ModelMapper modelMapper,
      NotificationService notificationService) {
    super(postRepository, modelMapper, Notice.class, NoticeDto.Response.class);
    this.notificationService = notificationService;
  }

  @Override
  public Response createPost(Create dto) {
    Response notice = super.createPost(dto);
    if (notice.getSendPush().booleanValue()) {
      try {
        NotificationDto.Create newNotification = NotificationDto.Create.builder()
            .type(Type.NOTICE)
            .title(notice.getTitle())
            .body(notice.getTitle())
            .link("bankscan:://notice")
            .build();
        notificationService.createNotification(newNotification);
      } catch (Exception e) {
        // nothing
        log.error("Notice notification error", e);
      }
    }
    return notice;
  }
}
