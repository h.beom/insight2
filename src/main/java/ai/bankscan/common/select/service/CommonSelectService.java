package ai.bankscan.common.select.service;

import static java.util.stream.Collectors.toMap;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.Account.EducationCompletionLevel;
import ai.bankscan.accounts.domain.Account.LeaveType;
import ai.bankscan.accounts.domain.Account.ProfessionalField;
import ai.bankscan.accounts.domain.Account.ProfessionalField2;
import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.consultations.domain.consulting.Consultation;
import io.swagger.annotations.ApiOperation;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service
public class CommonSelectService {

  @ApiOperation(value = "탈퇴 타입", notes = "탈퇴 타입: 관리자 탈퇴, 회원 탈")
  public Map<Account.LeaveType, String> getLeaveTypes() {
   return Arrays.stream(LeaveType.values())
        .collect(toMap(Function.identity(), LeaveType::getTitle, (e1, e2) -> e2,
            LinkedHashMap::new));
  }

  public Map<Account.ProfessionalField, String> getProfessionalTypes() {
    return Arrays.stream(ProfessionalField.values())
        .collect(toMap(Function.identity(), ProfessionalField::getTitle, (e1, e2) -> e2,
            LinkedHashMap::new));
  }

  public Map<Account.ProfessionalField2, String> getProfessionalTypesSub(
      ProfessionalField professionalField) {
    return ProfessionalField2.findByType(professionalField);
  }

  public Map<Account.EducationCompletionLevel, String> getEducationCompletionLevel() {
    return Arrays.stream(EducationCompletionLevel.values())
        .collect(toMap(Function.identity(), EducationCompletionLevel::getTitle,
            (e1, e2) -> e2, LinkedHashMap::new));
  }

  public Map<Account.LeaveReasonType, String> getLeaveReasonType() {
    return Arrays.stream(Account.LeaveReasonType.values())
        .collect(toMap(Function.identity(), Account.LeaveReasonType::getTitle,
            (e1, e2) -> e2, LinkedHashMap::new));
  }

  public Map<Account.ConsultantLevel, String> getConsultantLevel() {
    return Arrays.stream(Account.ConsultantLevel.values())
        .collect(toMap(Function.identity(), Account.ConsultantLevel::getTitle,
            (e1, e2) -> e2, LinkedHashMap::new));
  }


  public Map<Account.WorkerType, String> getWorkerType() {
    return Arrays.stream(Account.WorkerType.values())
        .collect(toMap(Function.identity(), Account.WorkerType::getTitle,
            (e1, e2) -> e2, LinkedHashMap::new));
  }

  public Map<Member.MemberLevel, String> getMemberLevel() {
    return Arrays.stream(Member.MemberLevel.values())
        .collect(toMap(Function.identity(), Member.MemberLevel::getTitle,
            (e1, e2) -> e2, LinkedHashMap::new));
  }

  public Map<Consultation.Status, String> getConsultationStatus() {
    return Arrays.stream(Consultation.Status.values())
        .collect(toMap(Function.identity(), Consultation.Status::getTitle,
            (e1, e2) -> e2, LinkedHashMap::new));
  }

}
