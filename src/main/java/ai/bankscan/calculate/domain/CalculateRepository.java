package ai.bankscan.calculate.domain;

import ai.bankscan.consultations.domain.consulting.Consultation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface CalculateRepository extends JpaRepository<Calculate, Long>,
    JpaSpecificationExecutor<Calculate> {

  Page<Calculate> findAll(Specification<Calculate> toSpecification, Pageable pageable);

  Calculate findByConsultation(Consultation consultation);


}
