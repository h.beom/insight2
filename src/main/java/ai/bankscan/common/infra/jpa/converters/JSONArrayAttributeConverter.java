package ai.bankscan.common.infra.jpa.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;

/**
 * The type List attribute converter.
 *
 * @author eomjeongjae
 * @since 2019 -02-27
 */
@Slf4j
@Converter(autoApply = false)
public class JSONArrayAttributeConverter implements
    AttributeConverter<JSONArray, String> {

  @Override
  public String convertToDatabaseColumn(JSONArray attribute) {
    if (attribute != null) {
      return attribute.toString();
    }
    return null;
  }

  @Override
  public JSONArray convertToEntityAttribute(String dbData) {
    if (dbData == null) return null;
    return JSONArray.fromObject(dbData);
  }
}

