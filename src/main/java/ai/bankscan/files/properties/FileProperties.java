package ai.bankscan.files.properties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author eomjeongjae
 * @since 04/10/2019
 */
@Getter
@Setter
@ToString
@Component
@ConfigurationProperties("file")
public class FileProperties {

  private String fileRedirect;
  private String xSendFile;
  private String extensionHeic;
  private String extensionConvert;
  private String extensionComposite;
}
