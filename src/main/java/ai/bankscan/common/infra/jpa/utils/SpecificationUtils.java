package ai.bankscan.common.infra.jpa.utils;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

public class SpecificationUtils {

  public static <Y> Path<Y> getPath(Root root, String key) {
    Path<Y> x = null;
    if (key.contains(".")) {
      for (String k : key.split("\\.")) {
        x = (x == null ? root.get(k) : x.get(k));
      }
    } else {
      x = root.<Y>get(key);
    }
    return x;
  }

}
