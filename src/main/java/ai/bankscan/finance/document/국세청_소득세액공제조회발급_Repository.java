package ai.bankscan.finance.document;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface 국세청_소득세액공제조회발급_Repository extends JpaRepository<국세청_소득세액공제조회발급, Long>,
    JpaSpecificationExecutor<국세청_소득세액공제조회발급> {

  @Query("SELECT COALESCE(SUM(총합계),0) FROM 국세청_소득세액공제조회발급 WHERE createdBy.id = :createdById AND 종류 LIKE '기부%' AND 귀속연도 = :year")
  Long get나눈돈_본인기부금(@Param("createdById") Long createdById, @Param("year") String year);

  @Query("SELECT COALESCE(SUM(총합계),0) FROM 국세청_소득세액공제조회발급 WHERE createdBy.id = :createdById AND 종류 = '교육비' AND 귀속연도 = :year")
  Long get쓴돈_교육비(@Param("createdById") Long createdById, @Param("year") String year);

  @Query("SELECT COALESCE(SUM(총합계),0) FROM 국세청_소득세액공제조회발급 WHERE createdBy.id = :createdById AND 종류 = '의료비' AND 귀속연도 = :year")
  Long get쓴돈_의료비(@Param("createdById") Long createdById, @Param("year") String year);

  @Query("SELECT COALESCE(SUM(총합계),0) FROM 국세청_소득세액공제조회발급 WHERE createdBy.id = :createdById AND 종류 = '국민연금' AND 귀속연도 = :year")
  Long get모은돈_국민연금(@Param("createdById") Long createdById, @Param("year") String year);

  @Query("SELECT COALESCE(SUM(총합계),0) FROM 국세청_소득세액공제조회발급 WHERE createdBy.id = :createdById AND 종류 = '건강보험' AND 귀속연도 = :year")
  Long get모은돈_건강보험(@Param("createdById") Long createdById, @Param("year") String year);

  @Query("SELECT MAX(귀속연도) FROM 국세청_소득세액공제조회발급 WHERE createdBy.id = :createdById")
  String getMaxYear(@Param("createdById") Long createdById);
}
