package ai.bankscan.finance.document;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Table(indexes = {@Index(columnList = "bank"), @Index(columnList = "계좌번호"),
    @Index(columnList = "거래시각"), @Index(columnList = "거래일자")})
public class 은행_펀드거래내역조회 extends AuditableJoinEntity<Account> {

  private String bank;
  private String 계좌번호;
  private String 거래일자;
  private String 거래시각;
  private String 거래유형;
  private String 적요;
  private String 종목명;
  private String 수량;
  private Long 거래금액;
  private String 수수료;
  private String 금잔수량;
  private String 금잔금액;
  private String 기준가격;
  private String 기준가격적용일;
  private String 대금결제일;
  private String 기재내용;
  private String 거래점;

}
