package ai.bankscan.consultations.domain;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.Account.ProfessionalField;
import ai.bankscan.accounts.domain.Account.ProfessionalField2;
import ai.bankscan.common.infra.jpa.domain.BaseEntity;
import ai.bankscan.consultations.domain.consulting.Consultation;
import ai.bankscan.consultations.domain.consulting.Consultation.Type;
import java.time.LocalDate;
import java.time.LocalTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class ConsultContent extends BaseEntity {

  @Getter
  @RequiredArgsConstructor
  public enum ConsultType {
    CALL("유선상담"),
    SNS("카카오톡상담"),
    EMAIL("이메일 상담"),
    MEET_INSIDE("대면(상담센터방문)상담"),
    MEET_OUTSIDE("대면(고객방문)상담"),
    ETC("기타");

    private final String title;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  private Consultation consultation;

  @ManyToOne(fetch = FetchType.LAZY)
  private Account consultant;

  @Lob
  private String content;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private ProfessionalField category;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private ProfessionalField2 subCategory;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private ConsultType consultType;

  private LocalDate consultDate;

  private LocalTime consultTime;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private Type consultationType;


}
