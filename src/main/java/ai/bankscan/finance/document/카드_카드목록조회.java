package ai.bankscan.finance.document;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Table(indexes = {@Index(columnList = "card"), @Index(columnList = "카드번호")})
public class 카드_카드목록조회 extends AuditableJoinEntity<Account> {

  private String card;

  private String 결제예정액;
  private String 결제일;
  private String 구분;
  private String 카드명;
  private String 카드번호;


  private String 결제설정일;
  private String 결제은행;
  private String 결제계좌;

}
