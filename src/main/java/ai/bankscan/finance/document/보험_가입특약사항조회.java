package ai.bankscan.finance.document;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Table(indexes = {@Index(columnList = "insure"), @Index(columnList = "증권번호"), @Index(columnList = "idx")})
public class 보험_가입특약사항조회 extends AuditableJoinEntity<Account> {

  private String insure;

  private Integer idx;
  private String 증권번호;

  private String 주보험및특약;
  private String 가입금액;
  private String 보험료;
  private String 보험기간;
  private String 납입기간;

}
