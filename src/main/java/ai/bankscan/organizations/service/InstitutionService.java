package ai.bankscan.organizations.service;

import ai.bankscan.accounts.domain.AccountRepository;
import ai.bankscan.organizations.domain.Institution;
import ai.bankscan.organizations.domain.InstitutionRepository;
import ai.bankscan.organizations.domain.OrganizationRepository;
import ai.bankscan.organizations.dto.InstitutionDto;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Transactional(readOnly = true)
@Service
public class InstitutionService extends OrganizationService<Institution, InstitutionDto.Response> {

  @Autowired
  public InstitutionService(
      OrganizationRepository organizationRepository,
      InstitutionRepository institutionRepository,
      AccountRepository accountRepository, ModelMapper modelMapper) {
    super(organizationRepository, institutionRepository, accountRepository, modelMapper, Institution.class,
        InstitutionDto.Response.class);
  }

}
