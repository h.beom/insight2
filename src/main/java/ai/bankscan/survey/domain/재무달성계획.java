package ai.bankscan.survey.domain;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.converters.JSONArrayAttributeConverter;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import ai.bankscan.survey.dto.재무달성계획AnswersDto;
import java.util.List;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import net.sf.json.JSONArray;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class 재무달성계획 extends AuditableJoinEntity<Account> {

  @Lob
  @NotNull
  private String result;

  @Lob
  @NotNull
  private String result2;


  @Default
  private String surveyType = "재무달성계획";

  @Lob
  private List<재무달성계획AnswersDto.Response> answerList;

  @Lob
  @Default
  @Convert(converter = JSONArrayAttributeConverter.class)
  private JSONArray target = null;

  // 목표금액
  private Long targetMoney;

  // 목표개월
  private Long targetMonth;


}
