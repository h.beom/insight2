package ai.bankscan.accounts.dto;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.AccountDevice;
import ai.bankscan.accounts.domain.member.Member;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.AccessLevel;
import lombok.Builder.Default;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MemberDto {

  @Data
  @ApiModel(value = "MemberDto.Login")
  public static class Login {

    @ApiModelProperty(value = "아이디", position = 10, required = true, example = "member1")
    private String username;

    @ApiModelProperty(value = "비밀번호", position = 11, required = true, example = "a123456A")
    private String password;

    @ApiModelProperty(value = "시리얼번호", position = 12, required = false)
    private String serialNumber;

    @ApiModelProperty(value = "FCM Token", position = 13, required = false)
    private String pushToken;

    @ApiModelProperty(value = "Platform", position = 14, required = false)
    private AccountDevice.Platform platform;

    @ApiModelProperty(value = "장비명", position = 15, required = false)
    private String deviceName;
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "MemberDto.Check")
  public static class Check {

    @ApiModelProperty(value = "아이디", position = 10, required = true, example = "member1")
    private String username;

    @ApiModelProperty(value = "비밀번호", position = 11, required = true, example = "a123456A")
    private String password;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "MemberDto.Create")
  public static class Create extends AccountBaseDto.Create {

    @Default
    @ApiModelProperty(value = "이메일 알림 여부", example = "false", position = 10, required = true)
    private Boolean isReceiveMail = Boolean.FALSE;

    @Default
    @ApiModelProperty(value = "핸드폰 알림 여부", example = "false", position = 11, required = true)
    private Boolean isReceivePhone = Boolean.FALSE;

    @Default
    @ApiModelProperty(value = "푸시 알림 여부", example = "false", position = 11, required = true)
    private Boolean isReceivePush = Boolean.FALSE;


  }

  @Getter
  @Setter
  @NoArgsConstructor
  @SuperBuilder
  @ToString(callSuper = true)
  @ApiModel(value = "MemberDto.Search")
  public static class Search extends AccountBaseDto.Search<Member> {

    @ApiModelProperty(value = "탈퇴일")
    private LocalDate leaveDate;

    @ApiModelProperty(value = "탈퇴유형")
    private String leaveType;

    @ApiModelProperty(value = "가입일")
    private LocalDate createdAt;

    @ApiModelProperty(value = "최근 접속일")
    private LocalDateTime lastAccessTime;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "MemberDto.Simple")
  public static class Simple extends AccountBaseDto.Simple {

    @Default
    @ApiModelProperty(value = "이메일 알림 여부", example = "false", position = 14, required = true)
    private Boolean isReceiveMail = Boolean.FALSE;

    @Default
    @ApiModelProperty(value = "핸드폰 알림 여부", example = "false", position = 15, required = true)
    private Boolean isReceivePhone = Boolean.FALSE;

    @Default
    @ApiModelProperty(value = "푸시 알림 여부", example = "false", position = 15, required = true)
    private Boolean isReceivePush = Boolean.FALSE;


    @ApiModelProperty(value = "탈퇴 여부 ", example = "false", position = 16, required = true)
    private Boolean isLeaved;

    @Default
    @ApiModelProperty(value = "현재 response 사용 된 시각", position = 20)
    private long now = System.currentTimeMillis();

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "MemberDto.Response")
  public static class Response extends AccountBaseDto.Response {

    @Default
    @ApiModelProperty(value = "이메일 알림 여부", example = "false", position = 14, required = true)
    private Boolean isReceiveMail = Boolean.FALSE;

    @Default
    @ApiModelProperty(value = "핸드폰 알림 여부", example = "false", position = 15, required = true)
    private Boolean isReceivePhone = Boolean.FALSE;

    @Default
    @ApiModelProperty(value = "핸드폰 알림 여부", example = "false", position = 15, required = true)
    private Boolean isReceivePush = Boolean.FALSE;


    @ApiModelProperty(value = "탈퇴 여부 ", example = "false", position = 16, required = true)
    private Boolean isLeaved;

    @Enumerated(EnumType.STRING)
    @ApiModelProperty(value = "탈퇴 유형", example = "THROW_OUT", position = 17)
    private Account.LeaveType leaveType;

    @ApiModelProperty(value = "탈퇴 일자", example = "2020-03-22-22:11", position = 18)
    private LocalDate leaveDate;

    @ApiModelProperty(value = "탈퇴 사유", example = "탈퇴 합니다.", position = 19)
    private String leaveReason;

    @Default
    @ApiModelProperty(value = "현재 response 사용 된 시각", position = 20)
    private long now = System.currentTimeMillis();

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "MemberDto.Update")
  public static class Update extends AccountBaseDto.Update {

    @Default
    @ApiModelProperty(value = "이메일 알림 여부", example = "false", position = 11)
    private Boolean isReceiveMail = Boolean.FALSE;

    @Default
    @ApiModelProperty(value = "핸드폰 알림 여부", example = "false", position = 12)
    private Boolean isReceivePhone = Boolean.FALSE;

    @Default
    @ApiModelProperty(value = "푸시 알림 여부", example = "false", position = 12)
    private Boolean isReceivePush = Boolean.FALSE;


    @ApiModelProperty(value = "Connecting Information", example = "ci", position = 13)
    private String ci;

    @ApiModelProperty(value = "Duplication information", example = "di", position = 14)
    private String di;


  }

}
