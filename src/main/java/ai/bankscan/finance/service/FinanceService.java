package ai.bankscan.finance.service;

import static ai.bankscan.survey.domain.금융건강진단종합.Type.내복지찾기;
import static ai.bankscan.survey.domain.금융건강진단종합.Type.내살집마련;
import static ai.bankscan.survey.domain.금융건강진단종합.Type.철저한신용관리;
import static ai.bankscan.survey.domain.금융건강진단종합.Type.행복한노후준비;
import static ai.bankscan.survey.domain.금융건강진단종합.Type.현명한금융거래;

import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.accounts.domain.member.MemberRepository;
import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.finance.document.국세청_소득금액증명_Repository;
import ai.bankscan.finance.document.국세청_소득세액공제조회발급_Repository;
import ai.bankscan.finance.document.금결원_계좌정보조회_Repository;
import ai.bankscan.finance.document.보험_계약내용조회_Repository;
import ai.bankscan.finance.document.연금_예상연금액조회;
import ai.bankscan.finance.document.연금_예상연금액조회_Repository;
import ai.bankscan.finance.document.은행_대출거래내역조회_Repository;
import ai.bankscan.finance.document.은행_대출계좌조회_Repository;
import ai.bankscan.finance.document.은행_수시거래내역조회_Repository;
import ai.bankscan.finance.document.은행_수시전계좌조회_Repository;
import ai.bankscan.finance.document.은행_예적금계좌조회_Repository;
import ai.bankscan.finance.document.은행_외화계좌조회_Repository;
import ai.bankscan.finance.document.은행_펀드계좌조회_Repository;
import ai.bankscan.finance.document.증권_증권보유계좌조회_Repository;
import ai.bankscan.finance.document.카드_장기카드대출_카드론거래내역_Repository;
import ai.bankscan.finance.document.카드_장기카드대출_카드론이용내역_Repository;
import ai.bankscan.finance.document.카드_카드목록조회_Repository;
import ai.bankscan.finance.document.카드_카드승인내역_Repository;
import ai.bankscan.finance.document.카드_포인트내역_Repository;
import ai.bankscan.finance.domain.AssetTable;
import ai.bankscan.finance.domain.AssetTableRepository;
import ai.bankscan.finance.domain.CashFlow;
import ai.bankscan.finance.domain.CashFlowRepository;
import ai.bankscan.finance.domain.House;
import ai.bankscan.finance.domain.House.HouseMode;
import ai.bankscan.finance.domain.HouseRepository;
import ai.bankscan.finance.dto.AssetTableDto;
import ai.bankscan.finance.dto.카드_카드청구내역_Dto;
import ai.bankscan.survey.domain.금융건강진단종합;
import ai.bankscan.survey.domain.금융건강진단종합Repository;
import ai.bankscan.survey.domain.금융건강진단표;
import ai.bankscan.survey.domain.금융건강진단표Repository;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class FinanceService {

  private final ModelMapper mapper;
  private final EntityManager entityManager;

  private final HouseRepository houseRepository;
  private final CashFlowRepository cashFlowRepository;
  private final AssetTableRepository assetTableRepository;

  private final MemberRepository memberRepository;

  private final 금결원_계좌정보조회_Repository _금결원_계좌정보조회_Repository;

  private final 국세청_소득금액증명_Repository _국세청_소득금액증명Repository;
  private final 국세청_소득세액공제조회발급_Repository _국세청_소득세액공제조회발급_Repository;

  private final 은행_예적금계좌조회_Repository _은행_예적금계좌조회_Repository;
  private final 은행_수시전계좌조회_Repository _은행_수시전계좌조회_Repository;
  private final 은행_수시거래내역조회_Repository _은행_수시거래내역조회_Repository;
  private final 은행_펀드계좌조회_Repository _은행_펀드계좌조회_Repository;
  private final 은행_대출계좌조회_Repository _은행_대출계좌조회_Repository;
  private final 은행_대출거래내역조회_Repository _은행_대출거래내역조회_Repository;
  private final 은행_외화계좌조회_Repository _은행_외화계좌조회_Repository;

  private final 카드_카드목록조회_Repository _카드_카드목록조회_Repository;
  private final 카드_카드승인내역_Repository _카드_카드승인내역_Repository;
  private final 카드_포인트내역_Repository _카드_포인트내역_Repository;
  private final 카드_장기카드대출_카드론거래내역_Repository _카드_장기카드대출_카드론거래내역_Repository;
  private final 카드_장기카드대출_카드론이용내역_Repository _카드_장기카드대출_카드론이용내역_Repository;

  private final 보험_계약내용조회_Repository _보험_계약내용조회_Repository;

  private final 증권_증권보유계좌조회_Repository _증권_증권보유계좌조회_Repository;
  private final 연금_예상연금액조회_Repository _연금_예상연금액조회_repository;

  private final 금융건강진단표Repository _금융건강진단표_Repository;
  private final 금융건강진단종합Repository _금융건강진단종합_Repository;

  @Transactional
  public CashFlow createCashFlow(long memberId, String year) {
    return createCashFlow(memberId, year, null);
  }

  @Transactional
  public CashFlow createCashFlow(long memberId, String year, CashFlow before) {

    JSONObject 번돈 = new JSONObject(); // 6
    번돈.put("근로소득", _국세청_소득금액증명Repository.get번돈_근로소득(memberId, year));
    if (before != null) {
      JSONObject o = JSONObject.fromObject(before.getJson());
      번돈.put("사업소득", o.getJSONObject("수입").getJSONObject("번돈@총소득").getLong("사업소득"));
    } else {
      번돈.put("사업소득", _국세청_소득금액증명Repository.get번돈_사업소득(memberId, year));
    }
    번돈.put("연금소득", 0); // N/A
    번돈.put("임대소득", 0); // N/A
    번돈.put("금융소득", 0); // N/A
    번돈.put("보험보상금", 0); // N/A

    JSONObject 빌린돈 = new JSONObject(); // 4

    Long 현금흐름표_빌린돈_총액 = parseLong(entityManager.createNamedQuery("현금흐름표_빌린돈_총액")
        .setParameter("created_by", memberId)
        .setParameter("year", year)
        .getSingleResult());

    Long 현금흐름표_빌린돈_카드대출 = parseLong(
        entityManager.createNamedQuery("현금흐름표_빌린돈_카드대출")
            .setParameter("created_by", memberId)
            .setParameter("year", year)
            .getSingleResult()
    );
    빌린돈.put("카드대출", 현금흐름표_빌린돈_카드대출);

    Long 현금흐름표_빌린돈_보험대출 = parseLong(
        entityManager.createNamedQuery("현금흐름표_빌린돈_보험대출")
            .setParameter("created_by", memberId)
            .setParameter("year", year)
            .getSingleResult()
    );
    빌린돈.put("보험대출", 현금흐름표_빌린돈_보험대출);
    빌린돈.put("은행대출", 현금흐름표_빌린돈_총액 - 현금흐름표_빌린돈_카드대출 - 현금흐름표_빌린돈_보험대출);

    빌린돈.put("제2금융대출", 0); // N/A
    빌린돈.put("임대보증금", 0); // N/A

    JSONObject 찾은돈 = new JSONObject(); // 8

    Long 현금흐름표_찾은돈_은행저축인출액 = parseLong(
        entityManager.createNamedQuery("현금흐름표_찾은돈_은행저축인출액")
            .setParameter("created_by", memberId)
            .setParameter("year", year)
            .getSingleResult()
    );
    찾은돈.put("은행저축인출액", 현금흐름표_찾은돈_은행저축인출액);

//    Long 현금흐름표_찾은돈_펀드투자인출액 = parseLong((
//        entityManager.createNamedQuery("현금흐름표_찾은돈_펀드투자인출액")
//            .setParameter("created_by", memberId)
//            .setParameter("year", year)
//            .getSingleResult()
//    ));
    // TODO ::
    찾은돈.put("펀드투자인출액", 0);

    {

      Long 현금흐름표_보험_받은돈 = parseLong(
          entityManager.createNamedQuery("현금흐름표_보험_받은돈")
              .setParameter("created_by", memberId)
              .setParameter("year", year)
              .getSingleResult()
      );

      찾은돈.put("보험저축인출액", 현금흐름표_보험_받은돈 - 현금흐름표_빌린돈_보험대출);
    }

    찾은돈.put("증권저축인출액", 0); // N/A
    찾은돈.put("제2금융저축인출액", 0); // N/A

    JSONObject 갚은돈 = new JSONObject(); // 10

    Long 현금흐름표_갚은돈_상환총액 = parseLong(
        entityManager.createNamedQuery("현금흐름표_갚은돈_상환총액")
            .setParameter("created_by", memberId)
            .setParameter("year", year)
            .getSingleResult()
    );

    Long 현금흐름표_갚은돈_이자총액 = parseLong(
        entityManager.createNamedQuery("현금흐름표_갚은돈_이자총액")
            .setParameter("created_by", memberId)
            .setParameter("year", year)
            .getSingleResult()
    );

    {
      카드_카드청구내역_Dto.GroupBy 현금흐름표_갚은돈_카드대출 =
          entityManager.createNamedQuery("현금흐름표_갚은돈_카드대출", 카드_카드청구내역_Dto.GroupBy.class)
              .setParameter("created_by", memberId)
              .setParameter("year", year)
              .getSingleResult();

      갚은돈.put("카드대출상환액", 현금흐름표_갚은돈_카드대출.get원금());
      갚은돈.put("카드대출이자", 현금흐름표_갚은돈_카드대출.get이자());
    }

    Long 현금흐름표_갚은돈_보험대출상환액 = parseLong(
        entityManager.createNamedQuery("현금흐름표_갚은돈_보험대출상환액")
            .setParameter("created_by", memberId)
            .setParameter("year", year)
            .getSingleResult()
    );

    갚은돈.put("보험대출상환액", 현금흐름표_갚은돈_보험대출상환액);
    갚은돈.put("보험대출이자", 0); // N/A

    갚은돈.put("은행대출상환액", 현금흐름표_갚은돈_상환총액 - 현금흐름표_갚은돈_보험대출상환액);
    갚은돈.put("은행대출이자", 현금흐름표_갚은돈_이자총액);

    갚은돈.put("2금융_대출상환액", 0); // N/A
    갚은돈.put("제2금융대출이자", 0); // N/A

    JSONObject 모은돈 = new JSONObject(); // 11
    {
      Long 현금흐름표_모은돈_은행저축증가액 = parseLong((
          entityManager.createNamedQuery("현금흐름표_모은돈_은행저축증가액")
              .setParameter("created_by", memberId)
              .setParameter("year", year)
              .getSingleResult()
      ));

      모은돈.put("은행저축증가액", 현금흐름표_모은돈_은행저축증가액);
    }

    {
      // TODO :: 2.2.10	펀드거래내역조회
      모은돈.put("펀드투자증가액", 0);
    }

    {
      Long 현금흐름표_모은돈_보험보낸돈 = parseLong((
          entityManager.createNamedQuery("현금흐름표_모은돈_보험보낸돈")
              .setParameter("created_by", memberId)
              .setParameter("year", year)
              .getSingleResult()
      ));

      모은돈.put("보험저축증가액", 현금흐름표_모은돈_보험보낸돈 - 현금흐름표_갚은돈_보험대출상환액);
    }

    모은돈.put("국민연금납부액", _국세청_소득세액공제조회발급_Repository.get모은돈_국민연금(memberId, year));

    JSONObject 쓴돈 = new JSONObject(); // 12
    쓴돈.put("교육비", _국세청_소득세액공제조회발급_Repository.get쓴돈_교육비(memberId, year));
    쓴돈.put("의료비", _국세청_소득세액공제조회발급_Repository.get쓴돈_의료비(memberId, year));
    쓴돈.put("건강보험납부액", _국세청_소득세액공제조회발급_Repository.get모은돈_건강보험(memberId, year));

    {
      Long 현금흐름표_쓴돈_보장성보험 = parseLong((
          entityManager.createNamedQuery("현금흐름표_쓴돈_보장성보험")
              .setParameter("created_by", memberId)
              .setParameter("year", year)
              .getSingleResult()
      ));
      쓴돈.put("보장성보험", 현금흐름표_쓴돈_보장성보험);
    }

    JSONObject 나눈돈 = new JSONObject(); // 13
    나눈돈.put("본인_기부금", _국세청_소득세액공제조회발급_Repository.get나눈돈_본인기부금(memberId, year));

    Long 수입합계 = 0L;
    for (Object key : 번돈.keySet()) {
      수입합계 += 번돈.getLong(String.valueOf(key));
    }
    for (Object key : 빌린돈.keySet()) {
      수입합계 += 빌린돈.getLong(String.valueOf(key));
    }
    for (Object key : 찾은돈.keySet()) {
      수입합계 += 찾은돈.getLong(String.valueOf(key));
    }

    Long 지출합계 = 0L;
    for (Object key : 갚은돈.keySet()) {
      지출합계 += 갚은돈.getLong(String.valueOf(key));
    }
    for (Object key : 모은돈.keySet()) {
      지출합계 += 모은돈.getLong(String.valueOf(key));
    }
    for (Object key : 쓴돈.keySet()) {
      지출합계 += 쓴돈.getLong(String.valueOf(key));
    }
    for (Object key : 나눈돈.keySet()) {
      지출합계 += 나눈돈.getLong(String.valueOf(key));
    }

    JSONObject 수입 = new JSONObject();
    if (수입합계 - 지출합계 > 0) {
      쓴돈.put("기타생활비", 수입합계 - 지출합계); // TODO :: 나머지들
    } else {
      JSONObject 현금인출액 = new JSONObject();
      현금인출액.put("현금인출액", Math.abs(수입합계 - 지출합계));
      수입.put("현금인출액", 현금인출액);
    }

    수입.put("번돈@총소득", 번돈);
    수입.put("빌린돈@대출", 빌린돈);
    수입.put("찾은돈@저축/투자", 찾은돈);

    JSONObject 지출 = new JSONObject();
    지출.put("갚은돈@금융비용", 갚은돈);
    지출.put("모은돈@저축/투자", 모은돈);
    지출.put("쓴돈@소비", 쓴돈);
    지출.put("나눈돈@기부액", 나눈돈);

    JSONObject json = new JSONObject();
    json.put("수입", 수입);
    json.put("지출", 지출);

    CashFlow cashFlow = cashFlowRepository.findByCreatedByIdAndYear(memberId, year)
        .orElse(new CashFlow());
    cashFlow.setJson(json.toString());
    cashFlow.setYear(year);

    return cashFlowRepository.save(cashFlow);
  }

  public CashFlow findLastCashFlow(Long createdById) {
    String year = cashFlowRepository.getLastYear(createdById);
    return cashFlowRepository.findByCreatedByIdAndYear(createdById, year).orElse(null);
  }

  public boolean hasCashflow(Long createdById) {
    Long size = cashFlowRepository.countByCreatedById(createdById);
    if (size == null) {
      size = 0L;
    }
    return size > 0;
  }


  @Transactional
  public AssetTable createAssetTable(Long memberId) {

    Restrictions r = new Restrictions();
    r.eq("createdBy.id", memberId);

    List<House> houses = houseRepository.findAll(r.output());

    JSONObject 금융자산 = new JSONObject();
    {
      Long 은행저축 = 0L;
      은행저축 += _금결원_계좌정보조회_Repository.getTotalByBank(memberId);
      은행저축 += _은행_수시전계좌조회_Repository.get전체잔고(memberId);
      은행저축 += _은행_예적금계좌조회_Repository.get전체잔고(memberId);
      금융자산.put("은행저축", 은행저축);
    }

    금융자산.put("펀드투자", _은행_펀드계좌조회_Repository.get전체잔고(memberId));

    {
      Long 외화계정 = 0L;
      외화계정 += _은행_외화계좌조회_Repository.get전체잔고(memberId);
      금융자산.put("외화계정", 외화계정);
    }

    금융자산.put("보험저축", _보험_계약내용조회_Repository.get해지환급금(memberId));
    금융자산.put("증권저축", _증권_증권보유계좌조회_Repository.get전체잔고(memberId));
    금융자산.put("2금융저축", 0);
    금융자산.put("기타", 0);

    Long 금융자산_총액 = 0L;
    List<Long> 금융자산_금액들 = (List<Long>) 금융자산.keySet().stream().map(o -> 금융자산.getLong(o.toString()))
        .collect(Collectors.toList());
    for (Long 금융자산_금액 : 금융자산_금액들) {
      금융자산_총액 += 금융자산_금액;
    }

    JSONObject 부동산 = new JSONObject();
    {
      for (House house : houses) {
        if (house.getMode() == HouseMode.LEASE) {
          부동산.put("임대보증금", house.getPrice());
        } else if (house.getMode() == HouseMode.TRADING) {
          부동산.put(house.getCategory() + "@" + house.getName(), house.getPrice());
        }
      }
    }

    Long 부동산_총액 = 0L;
    List<Long> 부동산_금액들 = (List<Long>) 부동산.keySet().stream().map(o -> 부동산.getLong(o.toString()))
        .collect(Collectors.toList());
    for (Long 부동산_금액 : 부동산_금액들) {
      부동산_총액 += 부동산_금액;
    }

//    JSONObject 유동자산 = new JSONObject();
//    유동자산.put("기타", 0);
//
//    Long 유동자산_총액 = 0L;
//    List<Long> 유동자산_금액들 = (List<Long>) 유동자산.keySet().stream().map(o -> 유동자산.getLong(o.toString()))
//        .collect(Collectors.toList());
//    for (Long 유동자산_금액 : 유동자산_금액들) {
//      유동자산_총액 += 유동자산_금액;
//    }

    JSONObject 총부채 = new JSONObject();
    총부채.put("은행대출", _은행_대출계좌조회_Repository.get은행대출(memberId));
    총부채.put("카드대출", _카드_장기카드대출_카드론이용내역_Repository.get대출잔액(memberId));
    총부채.put("보험대출", _보험_계약내용조회_Repository.get기대출금액(memberId));
    총부채.put("2금융대출", 0);
    {
      Long 임차보증금 = 0L;
      List<Long> prices = houses
          .stream()
          .filter(v -> v.getMode() == HouseMode.HIRE)
          .map(v -> v.getPrice())
          .collect(Collectors.toList());

      for (Long price : prices) {
        임차보증금 += price;
      }
      총부채.put("임차보증금", 임차보증금);
    }
    총부채.put("기타", 0);

    Long 총부채_총액 = 0L;
    List<Long> 총부채_금액들 = (List<Long>) 총부채.keySet().stream().map(o -> 총부채.getLong(o.toString()))
        .collect(Collectors.toList());
    for (Long 총부채_금액 : 총부채_금액들) {
      총부채_총액 += 총부채_금액;
    }

    JSONObject 자산 = new JSONObject();
    자산.put("금융자산", 금융자산);
    자산.put("부동산", 부동산);
//    자산.put("유동자산", 유동자산);

    JSONObject 부채 = new JSONObject();
    부채.put("총부채", 총부채);

    AssetTable assetTable = new AssetTable();
    assetTable.setAssetsJson(자산.toString());
    assetTable.setAssetsTotal(금융자산_총액 + 부동산_총액);
    assetTable.setLiabilitiesJson(부채.toString());
    assetTable.setLiabilitiesTotal(총부채_총액);
    assetTable.setTotal(assetTable.getAssetsTotal() - assetTable.getLiabilitiesTotal());

    return assetTableRepository.save(assetTable);
  }

  public AssetTableDto.Output findLastAssetTable(Long createdById) {

    Restrictions r = new Restrictions();
    r.eq("createdBy.id", createdById);

    Page<AssetTable> page = assetTableRepository
        .findAll(r.output(), PageRequest.of(0, 1, Sort.Direction.DESC, "createdAt"));
    if (page.getTotalElements() == 0) {
      return null;
    }

    AssetTable nowAssetTable = page.getContent().get(0);
    AssetTableDto.Response response = mapper.map(nowAssetTable, AssetTableDto.Response.class);
    response.setAssetsJson(JSONObject.fromObject(nowAssetTable.getAssetsJson()));
    response.setLiabilitiesJson(JSONObject.fromObject(nowAssetTable.getLiabilitiesJson()));

    AssetTableDto.Output output = new AssetTableDto.Output();
    output.setData(response);
    output.setOlders(
        entityManager.createNamedQuery("AssetTable.Simple_Query", AssetTableDto.Simple.class)
            .setParameter("created_by_id", createdById)
            .getResultList());

    return output;
  }

  public boolean hasAssetTable(Long createdById) {
    Long size = assetTableRepository.countByCreatedById(createdById);
    if (size == null) {
      size = 0L;
    }
    return size > 0;
  }


  @Transactional
  public void calcReport(long id) {

    Member member = memberRepository.findById(id).orElseThrow();

    Map<String, Integer> 배점 = new HashMap<>();
    배점.put("소득증가율", 10);
    배점.put("소득분위등급변동율", 10);
    배점.put("한계저축성향지표", 30);
    배점.put("가계수지지표", 50);
    배점.put("주택복지이용도", 20);
    배점.put("서민금융이용도", 20);
    배점.put("신용회복이용도", 10);
    배점.put("과소비지표", 20);

    CashFlow cashFlow2Year = cashFlowRepository.findByCreatedByIdAndYear(id, "2018").orElse(null);
    JSONObject cashflowJSON2 = JSONObject.fromObject(cashFlow2Year.getJson());

    CashFlow cashFlow1Year = cashFlowRepository.findByCreatedByIdAndYear(id, "2019").orElse(null);
    JSONObject cashflowJSON1 = JSONObject.fromObject(cashFlow1Year.getJson());

    List<금융건강진단표> parents = _금융건강진단표_Repository
        .findAllByCreatedByIdAndStatus(id, 금융건강진단표.Status.READY);
    for (금융건강진단표 parent : parents) {

      JSONObject 현명한금융거래_소득관리 = readJson("현명한금융거래_소득관리");

      JSONObject 소득증가율 = new JSONObject();
      소득증가율.put("title", "소득증가율");
      소득증가율.put("value", 0);
      소득증가율.put("suffix", "%");

      JSONArray 소득증가율_children = new JSONArray();

      Double 전전년_총소득Total = 0.0;
      {
        JSONObject 전전년_총소득 = new JSONObject();
        전전년_총소득.put("title", "전전년(" + cashFlow2Year.getYear() + ") 총소득");

        JSONArray 전전년_총소득_children = new JSONArray();
        JSONObject 총소득 = cashflowJSON2.getJSONObject("수입").getJSONObject("번돈@총소득");
        for (Object key : 총소득.keySet()) {
          JSONObject item = new JSONObject();
          item.put("title", key);
          item.put("value", 총소득.getLong(key.toString()));
          전전년_총소득_children.add(item);
          전전년_총소득Total += 총소득.getLong(key.toString());
        }

        전전년_총소득.put("value", 전전년_총소득Total);
        전전년_총소득.put("children", 전전년_총소득_children);
        소득증가율_children.add(전전년_총소득);
      }

      Double 전년_총소득total = 0.0;
      {
        JSONObject 전년_총소득 = new JSONObject();
        전년_총소득.put("title", "전년(" + cashFlow1Year.getYear() + ") 총소득");

        JSONArray 전년_총소득_children = new JSONArray();

        JSONObject 총소득 = cashflowJSON1.getJSONObject("수입").getJSONObject("번돈@총소득");
        for (Object key : 총소득.keySet()) {
          JSONObject item = new JSONObject();
          item.put("title", key);
          item.put("value", 총소득.getLong(key.toString()));
          전년_총소득_children.add(item);
          전년_총소득total += 총소득.getLong(key.toString());
        }

        전년_총소득.put("value", 전년_총소득total);
        전년_총소득.put("children", 전년_총소득_children);
        소득증가율_children.add(전년_총소득);
      }

      소득증가율.put("children", 소득증가율_children);

      Double 소득증가율Value = ((전년_총소득total - 전전년_총소득Total) / (전년_총소득total)) * 100.0;
      소득증가율Value = 소득증가율Value < 0 ? 0 : 소득증가율Value;
      소득증가율.put("value", Math.round(소득증가율Value));
      소득증가율.put("point", getPoint(소득증가율Value * 10.0) * 배점.get("소득증가율"));

      현명한금융거래_소득관리.getJSONArray("value").add(0, 소득증가율);

      Double 전년도_소득분위 = getMoneyLevel(cashFlow1Year.getYear(), 전년_총소득total.longValue()) * 1.0;
      Double 전전년도_소득분위 = getMoneyLevel(cashFlow2Year.getYear(), 전전년_총소득Total.longValue()) * 1.0;

      Double 소득분위변동 = ((전년도_소득분위 - 전전년도_소득분위) / 전전년도_소득분위) * 100.0;

      JSONObject 소득분위등급변동율 = new JSONObject();
      소득분위등급변동율.put("title", "소득분위등급변동율");
      소득분위등급변동율.put("value", Math.floor(소득분위변동));
      소득분위등급변동율.put("point", 소득분위변동 < 0 ? 0 : getPoint(소득분위변동) * 배점.get("소득분위등급변동율"));
      소득분위등급변동율.put("suffix", "%");
      {
        JSONArray 소득분위등급변동율_children = new JSONArray();

        JSONObject 최근년도소득분위등급 = new JSONObject();
        최근년도소득분위등급.put("title", "최근년도소득분위등급");
        최근년도소득분위등급.put("value", 전년도_소득분위);
        최근년도소득분위등급.put("suffix", "분위");
        소득분위등급변동율_children.add(최근년도소득분위등급);

        소득분위등급변동율.put("children", 소득분위등급변동율_children);
      }
      현명한금융거래_소득관리.getJSONArray("value").set(1, 소득분위등급변동율);

      JSONObject 한계저축성향지표 = new JSONObject();
      한계저축성향지표.put("title", "한계저축성향지표");
      한계저축성향지표.put("suffix", "%");
      {
        JSONArray 한계저축성향지표_children = new JSONArray();

        Double 전전년_모은돈 = 0.0;
        {
          JSONObject o = cashflowJSON2.getJSONObject("지출").getJSONObject("모은돈@저축/투자");
          전전년_모은돈 += o.getLong("은행저축증가액");
          전전년_모은돈 += o.getLong("펀드투자증가액");
          전전년_모은돈 += o.getLong("보험저축증가액");
        }

        Double 전년_모은돈 = 0.0;
        {
          JSONObject o = cashflowJSON1.getJSONObject("지출").getJSONObject("모은돈@저축/투자");
          전년_모은돈 += o.getLong("은행저축증가액");
          전년_모은돈 += o.getLong("펀드투자증가액");
          전년_모은돈 += o.getLong("보험저축증가액");
        }

        JSONObject 저축증가액 = new JSONObject();
        저축증가액.put("title", "저축증가액");
        저축증가액.put("value", 전년_모은돈 - 전전년_모은돈);
        한계저축성향지표_children.add(저축증가액);

        JSONObject 소득증가액 = new JSONObject();
        소득증가액.put("title", "소득증가액");
        소득증가액.put("value", 전년_총소득total - 전전년_총소득Total);
        한계저축성향지표_children.add(소득증가액);

        한계저축성향지표.put("children", 한계저축성향지표_children);

        Double 한계저축성향지표Value = (전년_모은돈 - 전전년_모은돈) / (전년_총소득total - 전전년_총소득Total) * 100.0;
        한계저축성향지표.put("value", Math.floor(한계저축성향지표Value));
        if ((전년_모은돈 - 전전년_모은돈) == (전년_총소득total - 전전년_총소득Total)
            && (전년_총소득total - 전전년_총소득Total) == 0) {
          한계저축성향지표.put("point", 배점.get("한계저축성향지표"));
        } else if ((전년_총소득total - 전전년_총소득Total) == 0) {
          한계저축성향지표.put("point", 1.3 * 배점.get("한계저축성향지표"));
        } else {
          한계저축성향지표.put("point", getPoint(한계저축성향지표Value) * 배점.get("한계저축성향지표"));
        }
      }
      현명한금융거래_소득관리.getJSONArray("value").set(2, 한계저축성향지표);

      AtomicReference<Double> 소득관리point = new AtomicReference<>(0.0);
      현명한금융거래_소득관리.getJSONArray("value").forEach(v -> {
        소득관리point.updateAndGet(v1 -> v1 + ((JSONObject) v).getDouble("point"));
      });

      Integer 소득관리_배점합 = 배점.get("한계저축성향지표") + 배점.get("소득분위등급변동율") + 배점.get("소득분위등급변동율");
      현명한금융거래_소득관리.put("point", 소득관리point.get());
      현명한금융거래_소득관리.put("등급", getLevel100(소득관리point.get() / 소득관리_배점합));

      금융건강진단종합 현명한금융거래_소득관리_entity = new 금융건강진단종합();
      현명한금융거래_소득관리_entity.set금융건강진단표(parent);
      현명한금융거래_소득관리_entity.setType(현명한금융거래);
      현명한금융거래_소득관리_entity.setTitle("소득관리");
      현명한금융거래_소득관리_entity.setResult(현명한금융거래_소득관리.toString());
      _금융건강진단종합_Repository.save(현명한금융거래_소득관리_entity);

      /*** ***/

      JSONObject 현명한금융거래_지출관리 = readJson("현명한금융거래_지출관리");

      JSONObject 가계수지지표 = new JSONObject();
      가계수지지표.put("title", "가계수지지표");

      JSONArray 가계수지지표_children = new JSONArray();

      Double 전년_총지출_Total = 0.0;
      {
        JSONObject 전년_총지출 = new JSONObject();
        전년_총지출.put("title", "전년(" + cashFlow1Year.getYear() + ") 총지출");

        JSONArray 전년_총지출_children = new JSONArray();

        Long 갚은돈Total = 0L;
        JSONObject 갚은돈 = cashflowJSON1.getJSONObject("지출").getJSONObject("갚은돈@금융비용");
        for (Object key : 갚은돈.keySet()) {
          갚은돈Total += 갚은돈.getLong(key.toString());
        }

        Long 쓴돈Total = 0L;
        JSONObject 쓴돈 = cashflowJSON1.getJSONObject("지출").getJSONObject("쓴돈@소비");
        for (Object key : 쓴돈.keySet()) {
          쓴돈Total += 쓴돈.getLong(key.toString());
        }

        JSONObject 쓴돈데이터 = new JSONObject();
        쓴돈데이터.put("title", "소비성지출");
        쓴돈데이터.put("value", 쓴돈Total);
        전년_총지출_children.add(쓴돈데이터);

        JSONObject 갚은돈데이터 = new JSONObject();
        갚은돈데이터.put("title", "비용성지출");
        갚은돈데이터.put("value", 갚은돈Total);
        전년_총지출_children.add(갚은돈데이터);

        전년_총지출_Total = 갚은돈Total + 쓴돈Total + 0.0;
        전년_총지출.put("value", 전년_총지출_Total);
        전년_총지출.put("children", 전년_총지출_children);
        가계수지지표_children.add(전년_총지출);
      }

      Double 전년_총소득_Total = 0.0;
      {
        JSONObject 전년_총소득 = new JSONObject();
        전년_총소득.put("title", "전년(" + cashFlow1Year.getYear() + ") 총소득");

        JSONArray 전년_총소득_children = new JSONArray();

        JSONObject 총소득 = cashflowJSON1.getJSONObject("수입").getJSONObject("번돈@총소득");
        for (Object key : 총소득.keySet()) {
          JSONObject item = new JSONObject();
          item.put("title", key);
          item.put("value", 총소득.getLong(key.toString()));
          전년_총소득_children.add(item);
          전년_총소득_Total += 총소득.getLong(key.toString());
        }

        전년_총소득.put("value", 전년_총소득_Total);
        전년_총소득.put("children", 전년_총소득_children);
        가계수지지표_children.add(전년_총소득);
      }

      가계수지지표.put("children", 가계수지지표_children);

      Double 가계수지지표_Value = (전년_총지출_Total / 전년_총소득_Total);
      Double 가계수지지표_달성율 = ((1.0 - 가계수지지표_Value) / (1.0 - getAgePage(member.getBirthday()))) * 100;
      Double 가계수지지표_Point = getPoint(가계수지지표_달성율) * 배점.get("가계수지지표");
      가계수지지표.put("value", Math.floor(가계수지지표_달성율));
      가계수지지표.put("point", 가계수지지표_Point);
      가계수지지표.put("suffix", "%");

      현명한금융거래_지출관리.getJSONArray("value").set(0, 가계수지지표);

      JSONObject 과소비지표 = new JSONObject();
      과소비지표.put("title", "과소비지표");

      Long 연간_총저축_Total = 0L;
      Double 연간_총소득_Total = 0.0;
      JSONArray 과소비지표_children = new JSONArray();
      {
        JSONObject 전년_총소득 = new JSONObject();
        전년_총소득.put("title", "연간총소득");

        JSONArray 전년_총소득_children = new JSONArray();

        JSONObject 총소득 = cashflowJSON1.getJSONObject("수입").getJSONObject("번돈@총소득");
        for (Object key : 총소득.keySet()) {
          JSONObject item = new JSONObject();
          item.put("title", key);
          item.put("value", 총소득.getLong(key.toString()));
          전년_총소득_children.add(item);
          연간_총소득_Total += 총소득.getLong(key.toString());
        }

        전년_총소득.put("value", 연간_총소득_Total);
        전년_총소득.put("children", 전년_총소득_children);
        과소비지표_children.add(전년_총소득);

        JSONObject 전년_총저축 = new JSONObject();
        전년_총저축.put("title", "연간총저축");

        JSONArray 전년_총저축_children = new JSONArray();
        {
          JSONObject o = cashflowJSON1.getJSONObject("지출").getJSONObject("모은돈@저축/투자");

          JSONObject object1 = new JSONObject();
          object1.put("title", "예금");
          object1.put("value", o.getLong("은행저축증가액") + o.getLong("펀드투자증가액") + o.getLong("보험저축증가액"));
          연간_총저축_Total += o.getLong("은행저축증가액") + o.getLong("펀드투자증가액") + o.getLong("보험저축증가액");
          전년_총저축_children.add(object1);

          JSONObject object2 = new JSONObject();
          object2.put("title", "연금보험");
          object2.put("value", o.getLong("국민연금납부액"));
          연간_총저축_Total += o.getLong("국민연금납부액");
          전년_총저축_children.add(object2);
        }

        전년_총저축.put("value", 연간_총저축_Total);
        전년_총저축.put("children", 전년_총저축_children);
        과소비지표_children.add(전년_총저축);
      }

      Double 과소비지표_준거기준 = 80.0 / 100.0;
      Double 과소비지표_Value = (연간_총소득_Total - 연간_총저축_Total) / 연간_총소득_Total;
      Double 과소비지표_달성율 = (1 - 과소비지표_Value) / (1 - 과소비지표_준거기준) * 100.0;
      Double 과소비지표_Point = getPoint(과소비지표_달성율) * 배점.get("과소비지표");

      과소비지표.put("children", 과소비지표_children);
      과소비지표.put("value", Math.floor(과소비지표_Value * 100));
      과소비지표.put("point", 과소비지표_Point);
      과소비지표.put("suffix", "%");

      현명한금융거래_지출관리.getJSONArray("value").set(1, 과소비지표);

      AtomicReference<Double> 지출관리point = new AtomicReference<>(0.0);
      현명한금융거래_소득관리.getJSONArray("value").forEach(v -> {
        지출관리point.updateAndGet(v1 -> v1 + ((JSONObject) v).getDouble("point"));
      });

      Integer 지출관리_배점합 = 배점.get("과소비지표") + 배점.get("가계수지지표");
      현명한금융거래_지출관리.put("point", 지출관리point.get());
      현명한금융거래_지출관리.put("등급", getLevel(지출관리point.get() / 지출관리_배점합));

      금융건강진단종합 현명한금융거래_지출관리_entity = new 금융건강진단종합();
      현명한금융거래_지출관리_entity.set금융건강진단표(parent);
      현명한금융거래_지출관리_entity.setType(현명한금융거래);
      현명한금융거래_지출관리_entity.setTitle("지출관리");
      현명한금융거래_지출관리_entity.setResult(현명한금융거래_지출관리.toString());
      _금융건강진단종합_Repository.save(현명한금융거래_지출관리_entity);

      /*** ***/

      JSONObject 현명한금융거래_부채관리 = readJson("현명한금융거래_부채관리");

      금융건강진단종합 현명한금융거래_부채관리_entity = new 금융건강진단종합();
      현명한금융거래_부채관리_entity.set금융건강진단표(parent);
      현명한금융거래_부채관리_entity.setType(현명한금융거래);
      현명한금융거래_부채관리_entity.setTitle("부채관리");
      현명한금융거래_부채관리_entity.setResult(현명한금융거래_부채관리.toString());
      _금융건강진단종합_Repository.save(현명한금융거래_부채관리_entity);

      /*** ***/

      JSONObject 현명한금융거래_저축관리 = readJson("현명한금융거래_저축관리");

      금융건강진단종합 현명한금융거래_저축관리_entity = new 금융건강진단종합();
      현명한금융거래_저축관리_entity.set금융건강진단표(parent);
      현명한금융거래_저축관리_entity.setType(현명한금융거래);
      현명한금융거래_저축관리_entity.setTitle("저축관리");
      현명한금융거래_저축관리_entity.setResult(현명한금융거래_저축관리.toString());
      _금융건강진단종합_Repository.save(현명한금융거래_저축관리_entity);

      /*** ***/

      JSONObject 현명한금융거래_투자관리 = readJson("현명한금융거래_투자관리");

      금융건강진단종합 현명한금융거래_투자관리_entity = new 금융건강진단종합();
      현명한금융거래_투자관리_entity.set금융건강진단표(parent);
      현명한금융거래_투자관리_entity.setType(현명한금융거래);
      현명한금융거래_투자관리_entity.setTitle("투자관리");
      현명한금융거래_투자관리_entity.setResult(현명한금융거래_투자관리.toString());
      _금융건강진단종합_Repository.save(현명한금융거래_투자관리_entity);

      /*** ***/

      JSONObject 현명한금융거래_보험관리 = readJson("현명한금융거래_보험관리");

      금융건강진단종합 현명한금융거래_보험관리_entity = new 금융건강진단종합();
      현명한금융거래_보험관리_entity.set금융건강진단표(parent);
      현명한금융거래_보험관리_entity.setType(현명한금융거래);
      현명한금융거래_보험관리_entity.setTitle("보험관리");
      현명한금융거래_보험관리_entity.setResult(현명한금융거래_보험관리.toString());
      _금융건강진단종합_Repository.save(현명한금융거래_보험관리_entity);

      /*** ***/

      JSONObject 현명한금융거래_자산관리 = readJson("현명한금융거래_자산관리");

      금융건강진단종합 현명한금융거래_자산관리_entity = new 금융건강진단종합();
      현명한금융거래_자산관리_entity.set금융건강진단표(parent);
      현명한금융거래_자산관리_entity.setType(현명한금융거래);
      현명한금융거래_자산관리_entity.setTitle("자산관리");
      현명한금융거래_자산관리_entity.setResult(현명한금융거래_자산관리.toString());
      _금융건강진단종합_Repository.save(현명한금융거래_자산관리_entity);

      /*** ***/

      JSONObject 현명한금융거래_금융거래관리 = readJson("현명한금융거래_금융거래관리");

      금융건강진단종합 현명한금융거래_금융거래관리_entity = new 금융건강진단종합();
      현명한금융거래_금융거래관리_entity.set금융건강진단표(parent);
      현명한금융거래_금융거래관리_entity.setType(현명한금융거래);
      현명한금융거래_금융거래관리_entity.setTitle("금융거래관리");
      현명한금융거래_금융거래관리_entity.setResult(현명한금융거래_금융거래관리.toString());
      _금융건강진단종합_Repository.save(현명한금융거래_금융거래관리_entity);

      /*** ***/

      JSONObject 행복한노후준비_노후관리 = readJson("행복한노후준비_노후관리");

      금융건강진단종합 행복한노후준비_노후관리_entity = new 금융건강진단종합();
      행복한노후준비_노후관리_entity.set금융건강진단표(parent);
      행복한노후준비_노후관리_entity.setType(행복한노후준비);
      행복한노후준비_노후관리_entity.setTitle("노후관리");
      행복한노후준비_노후관리_entity.setResult(행복한노후준비_노후관리.toString());
      _금융건강진단종합_Repository.save(행복한노후준비_노후관리_entity);

      /*** ***/

      JSONObject 철저한신용관리_신용관리 = readJson("철저한신용관리_신용관리");

      금융건강진단종합 철저한신용관리_신용관리_entity = new 금융건강진단종합();
      철저한신용관리_신용관리_entity.set금융건강진단표(parent);
      철저한신용관리_신용관리_entity.setType(철저한신용관리);
      철저한신용관리_신용관리_entity.setTitle("신용관리");
      철저한신용관리_신용관리_entity.setResult(철저한신용관리_신용관리.toString());
      _금융건강진단종합_Repository.save(철저한신용관리_신용관리_entity);

      /*** ***/

      JSONObject 내살집마련_주택관리 = readJson("내살집마련_주택관리");

      JSONObject 주택마련월저축액 = 내살집마련_주택관리.getJSONArray("value").getJSONObject(1);

      JSONArray 주택마련월저축액_children = new JSONArray();

      JSONObject 주택청약종합저축 = new JSONObject();
      주택청약종합저축.put("title", "주택청약종합저축");
      주택청약종합저축.put("value", _은행_예적금계좌조회_Repository.get전체잔고(id));
      주택마련월저축액_children.add(주택청약종합저축);

      주택마련월저축액.put("value", 0);
      주택마련월저축액.put("children", 주택마련월저축액_children);

      내살집마련_주택관리.getJSONArray("value").set(1, 주택마련월저축액);






      금융건강진단종합 내살집마련_주택관리_entity = new 금융건강진단종합();
      내살집마련_주택관리_entity.set금융건강진단표(parent);
      내살집마련_주택관리_entity.setType(내살집마련);
      내살집마련_주택관리_entity.setTitle("주택관리");
      내살집마련_주택관리_entity.setResult(내살집마련_주택관리.toString());
      _금융건강진단종합_Repository.save(내살집마련_주택관리_entity);

      /*** ***/

      Long 서민금융대출이용액Total = 0L;
      Long 주택복지이용건수Total = 0L;
      JSONObject 내복지찾기_복지관리 = readJson("내복지찾기_복지관리");

      JSONArray 주택복지이용도_children = new JSONArray();

      JSONObject 전세대출 = new JSONObject();
      전세대출.put("title", "전세대출");
      전세대출.put("value", 5000); // TODO :: DUMMY
      전세대출.put("suffix", "건");
      주택복지이용도_children.add(전세대출);
      주택복지이용건수Total += 전세대출.getLong("value");

      List<String> dummies = Lists
          .newArrayList("행복주택입주", "임대주택입주", "청년주택입주");
      for (String dummy : dummies) {
        JSONObject dummyJSON = new JSONObject();
        dummyJSON.put("title", dummy);
        dummyJSON.put("value", 0);
        dummyJSON.put("suffix", "건");
        주택복지이용도_children.add(dummyJSON);
        주택복지이용건수Total += dummyJSON.getLong("value");
      }

      JSONObject 주택복지이용도 = 내복지찾기_복지관리.getJSONArray("value").getJSONObject(0);
      JSONObject 주택복지이용건수 = 주택복지이용도.getJSONArray("children").getJSONObject(0);

      주택복지이용건수.put("children", 주택복지이용도_children);
      주택복지이용건수.put("value", 주택복지이용건수Total);

      주택복지이용도.put("value", 주택복지이용건수Total > 0 ? 100 : 0);
      주택복지이용도.put("point", getPoint(주택복지이용건수Total > 0 ? 100.0 : 0.0) * 배점.get("주택복지이용도"));

      내복지찾기_복지관리.getJSONArray("value").set(0, 주택복지이용도);

      JSONArray 서민금융대출이용액_children = new JSONArray();

      JSONObject 미소금융 = new JSONObject();
      미소금융.put("title", "미소금융");
      미소금융.put("value", 300); // TODO :: DUMMY
      서민금융대출이용액_children.add(미소금융);
      서민금융대출이용액Total += 미소금융.getLong("value");

      JSONObject 햇살론 = new JSONObject();
      햇살론.put("title", "햇살론");
      햇살론.put("value", 0); // TODO :: DUMMY
      서민금융대출이용액_children.add(햇살론);
      서민금융대출이용액Total += 햇살론.getLong("value");

      JSONObject 바꿔드림론 = new JSONObject();
      바꿔드림론.put("title", "바꿔드림론");
      바꿔드림론.put("value", 0); // TODO :: DUMMY
      서민금융대출이용액_children.add(바꿔드림론);
      서민금융대출이용액Total += 바꿔드림론.getLong("value");

      JSONObject 새희망홀씨 = new JSONObject();
      새희망홀씨.put("title", "새희망홀씨");
      새희망홀씨.put("value", 0); // TODO :: DUMMY
      서민금융대출이용액_children.add(새희망홀씨);
      서민금융대출이용액Total += 새희망홀씨.getLong("value");

      JSONObject 징검다리론 = new JSONObject();
      징검다리론.put("title", "징검다리론");
      징검다리론.put("value", 0); // TODO :: DUMMY
      서민금융대출이용액_children.add(징검다리론);
      서민금융대출이용액Total += 징검다리론.getLong("value");

      List<String> dummies1 = Lists
          .newArrayList("취업성공대출", "교육지원비대출", "취약계층자립자금", "생활안정자금", "임차보증금대출");
      for (String dummy : dummies1) {
        JSONObject dummyJSON = new JSONObject();
        dummyJSON.put("title", dummy);
        dummyJSON.put("value", 0);
        서민금융대출이용액_children.add(dummyJSON);
        서민금융대출이용액Total += dummyJSON.getLong("value");
      }

      JSONObject 서민금융이용도 = 내복지찾기_복지관리.getJSONArray("value").getJSONObject(1);
      JSONObject 서민금융대출이용액 = 서민금융이용도.getJSONArray("children").getJSONObject(0);

      서민금융대출이용액.put("children", 서민금융대출이용액_children);
      서민금융대출이용액.put("value", 서민금융대출이용액Total);

      서민금융이용도.put("value", 서민금융대출이용액Total > 0 ? 100 : 0);
      서민금융이용도.put("point", getPoint(서민금융대출이용액Total > 0 ? 100.0 : 0.0) * 배점.get("서민금융이용도"));

      서민금융이용도.getJSONArray("children").set(0, 서민금융대출이용액);
      내복지찾기_복지관리.getJSONArray("value").set(1, 서민금융이용도);

      JSONObject 신용회복이용도 = 내복지찾기_복지관리.getJSONArray("value").getJSONObject(2);
      신용회복이용도.put("point", getPoint(신용회복이용도.getDouble("value")) * 배점.get("신용회복이용도"));

      내복지찾기_복지관리.getJSONArray("value").set(2, 신용회복이용도);

      AtomicReference<Double> 내복지찾기point = new AtomicReference<>(0.0);
      내복지찾기_복지관리.getJSONArray("value").forEach(v -> {
        내복지찾기point.updateAndGet(v1 -> v1 + ((JSONObject) v).getDouble("point"));
      });

      Double 내복지찾기_배점 = Double.valueOf(배점.get("주택복지이용도") + 배점.get("서민금융이용도") + 배점.get("신용회복이용도"));

      내복지찾기_복지관리.put("point", 내복지찾기point.get());
      내복지찾기_복지관리.put("등급", getLevel100(내복지찾기point.get() / 내복지찾기_배점));

      parent.set내복지찾기(String.valueOf(getLevel100(내복지찾기point.get() / 내복지찾기_배점)));

      금융건강진단종합 내복지찾기_복지관리_entity = new 금융건강진단종합();
      내복지찾기_복지관리_entity.set금융건강진단표(parent);
      내복지찾기_복지관리_entity.setType(내복지찾기);
      내복지찾기_복지관리_entity.setTitle("복지관리");
      내복지찾기_복지관리_entity.setResult(내복지찾기_복지관리.toString());
      _금융건강진단종합_Repository.save(내복지찾기_복지관리_entity);

      /*** ***/

      JSONObject 우수항목 = new JSONObject();
      우수항목.put("title", "저축률");
      우수항목.put("value", 60);
      우수항목.put("suffix", "%");
      우수항목.put("than", "20");

      JSONObject 금융위험도 = new JSONObject();

      JSONArray 조치항목 = new JSONArray();
      조치항목.add("소득 증가율");
      조치항목.add("한계저축성향 지표");
      조치항목.add("과소비 지표");
      조치항목.add("저축률");
      조치항목.add("금융상품 평균 수익률 외 3건");
      조치항목.add("노후준비 저축률이 5%로 권장치인 10%에 부족");

      금융위험도.put("조치항목", 조치항목);

      JSONObject result2 = new JSONObject();
      result2.put("건강도", 2);
      result2.put("createdAt", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE));
      result2.put("우수항목", 우수항목);
      result2.put("금융위험도", 금융위험도);

      result2.put("재무달성도", 88);
      result2.put("현재자산", 99);

      parent.setFinanceResult2(result2.toString());
      parent.setStatus(금융건강진단표.Status.ACTIVE);
      _금융건강진단표_Repository.save(parent);

    }


  }

  List<Long> prices2018 = Lists
      .newArrayList(9620L, 5714L, 4365L, 3548L, 2967L, 2507L, 2140L, 1801L, 1416L, 656L);
  List<Long> prices2019 = Lists
      .newArrayList(9931L, 5893L, 4528L, 3701L, 3105L, 2639L, 2290L, 1988L, 1562L, 689L);

  private Integer getMoneyLevel(String year, Long uprice) {

    Map<String, List> moneyTables = new HashMap<>();
    moneyTables.put("2018", prices2018);
    moneyTables.put("2019", prices2019);

    List<Long> prices = moneyTables.get(year);
    for (int i = 0; i < prices.size(); i++) {
      if ((uprice / 12) > prices.get(i) * 1000) {
        return prices.size() - i;
      }
    }

    return prices.size() - 1;
  }

  private double getAgePage(LocalDate date) {

    int age = LocalDate.now().getYear() - date.getYear();
    double percent = 0.95;
    if (age < 30) {
      percent = 0.50;
    } else if (age < 40) {
      percent = 0.70;
    } else if (age < 50) {
      percent = 0.60;
    } else if (age < 60) {
      percent = 0.90;
    }

    return percent;
  }

  private Double getPoint(Double value) {
    if (value < 50.0) {
      value = 50.0;
    }
    if (value > 150.0) {
      value = 150.0;
    }
    return (value - 20.0) / 100.0;
  }

  private Integer getLevel(Double value) {
    if (value >= 1.21) {
      return 1;
    }
    if (value >= 1.11) {
      return 2;
    }
    if (value >= 1.01) {
      return 3;
    }
    if (value >= 0.91) {
      return 4;
    }
    if (value >= 0.81) {
      return 5;
    }
    if (value >= 0.71) {
      return 6;
    }
    if (value >= 0.61) {
      return 7;
    }
    if (value >= 0.51) {
      return 8;
    }
    if (value >= 0.41) {
      return 9;
    }
    return 10;
  }

  private Integer getLevel100(Double value) {
    if (value >= 0.91) {
      return 1;
    }
    if (value >= 0.81) {
      return 2;
    }
    if (value >= 0.71) {
      return 3;
    }
    if (value >= 0.61) {
      return 4;
    }
    if (value >= 0.51) {
      return 5;
    }
    if (value >= 0.41) {
      return 6;
    }
    if (value >= 0.31) {
      return 7;
    }
    if (value >= 0.21) {
      return 8;
    }
    if (value >= 0.11) {
      return 9;
    }
    return 10;
  }

  private JSONObject readJson(String name) {
    ClassPathResource resource = new ClassPathResource("json/" + name + ".json");
    try {
      Path path = Paths.get(resource.getURI());
      List<String> content = Files.readAllLines(path);
      return JSONObject.fromObject(StringUtils.join(content, ""));
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }

  }


  @Transactional
  public AssetTable calcData(final long id) {

    Integer days = Integer
        .parseInt((LocalDate.now()).format(DateTimeFormatter.ofPattern("MMdd")), 10);
    Integer sYear = LocalDate.now().getYear() - (days > 201 ? 1 : 2);

    CashFlow before = createCashFlow(id, String.valueOf(sYear - 1)); // BeforeYear
    CashFlow cashFlow = createCashFlow(id, String.valueOf(sYear), (days > 715 ? null : before));

    System.out.println(cashFlow);

    return createAssetTable(id);

  }

  private Long parseLong(Object data) {
    if (data == null) {
      return 0L;
    }

    if (data.getClass() == Double.class) {
      return new BigDecimal((Double) data).longValue();
    } else {
      String str = String.valueOf(data);
      if (str.contains(".")) {
        str = str.substring(0, str.indexOf("."));
      }
      str = str.replaceAll("[^0-9]*", "");
      if (str.isEmpty()) {
        return 0L;
      }
      return Long.parseLong(str);
    }
  }

  public Long getPension(Long id) {

    Restrictions r = new Restrictions();
    r.eq("createdBy.id", id);

    Long totalPrice = 0L;
    List<연금_예상연금액조회> items = _연금_예상연금액조회_repository.findAll(r.output());
    for (연금_예상연금액조회 item : items) {
      try {
        JSONArray json = JSONArray.fromObject(item.get연령별예시연금액());
        for (int i = 0; i < json.size(); i++) {
          if (json.getJSONObject(i).getString("연령").startsWith("65")) {
            totalPrice += json.getJSONObject(i).getLong("예상연금액");
          }
        }
      } catch (Exception ex) {

      }
    }

    return totalPrice > 0L ? ((long) Math.floor(totalPrice / 12)) : totalPrice;
  }
}
