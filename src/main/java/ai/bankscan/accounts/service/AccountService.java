package ai.bankscan.accounts.service;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.Account.Type;
import ai.bankscan.accounts.domain.AccountRepository;
import ai.bankscan.accounts.domain.admin.Admin;
import ai.bankscan.accounts.domain.cm.CenterManager;
import ai.bankscan.accounts.domain.consultants.Consultant;
import ai.bankscan.accounts.domain.consultants.ProConsultant;
import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.accounts.domain.rm.RelationshipManager;
import ai.bankscan.accounts.dto.AccountDto;
import ai.bankscan.accounts.dto.AccountDto.Response;
import ai.bankscan.common.email.service.EmailService;
import ai.bankscan.common.infra.exceptions.AccountNotFoundException;
import ai.bankscan.common.infra.mail.messages.PebbleMailMessage;
import ai.bankscan.common.infra.web.excel.constants.Excel;
import ai.bankscan.configs.security.Hasher;
import ai.bankscan.organizations.domain.Organization;
import ai.bankscan.organizations.domain.OrganizationRepository;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.Opt;
import org.modelmapper.ModelMapper;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service
public class AccountService {

    private final AccountRepository accountRepository;
    private final OrganizationRepository organizationRepository;
    private final PasswordEncoder passwordEncoder;
    private final ModelMapper modelMapper;
    private final EmailService emailService;
    private final EntityManager entityManager;

    @Transactional
    public Account findByAuthorize(String username, String password) throws AuthenticationException {
        Account account = accountRepository.findByUsername(username).orElse(null);
        if (account == null) {
            throw new UsernameNotFoundException("Not found User");
        } else if (!passwordEncoder.matches(password, account.getPassword())) {
            throw new BadCredentialsException("No matched Password");
        } else {
            account.setLastAccessTime(LocalDateTime.now());
            return accountRepository.save(account);
        }
    }

    public Account checkByAuthorize(String username, String password) throws AuthenticationException {
        Account account = accountRepository.findByUsername(username).orElse(null);
        if (account == null) {
            return null;
        } else if (!passwordEncoder.matches(password, account.getPassword())) {
            return null;
        }
        return account;
    }

    @Transactional
    public AccountDto.Response createAccount(AccountDto.Create dto) {
        Account account = this.createAccount(dto, this.getEntityClass(dto.getType()));

        if (account.getType().isCenterManager() ||
                account.getType().isRelationshipManager()) {
            Organization organization = account.getOrganization();
            organization.setManager(account);
            organizationRepository.save(organization);
        }


        return this.toResponse(account);
    }

    private <T extends Account> T createAccount(
            AccountDto.Create dto,
            Class<T> entityType
    ) {
        Organization organization = null;
        if (dto.getOrganizationId() != null) {
            organization = organizationRepository.findById(dto.getOrganizationId())
                    .orElseThrow(IllegalArgumentException::new);
        }

        T account = modelMapper.map(dto, entityType);

        account.create(account.getType().getAuthorities(), passwordEncoder);

        if (dto.getOrganizationId() != null) {
            account.setOrganization(organization);
        }

        if (dto.getType().equals(Type.RELATIONSHIP_MANAGER) || dto.getType().equals(Type.CENTER_MANAGER)) {
            if (organization.getManager() != null) {
                Set<Account.Authority> data = Sets.newHashSet();
                int result = 0;
                long preAccount = organization.getManager().getId();
                if(dto.getType().equals(Type.CENTER_MANAGER)) {
                    data.add(Account.Authority.CONSULTANT);
                    String query = "UPDATE account SET type = '"+Type.CONSULTANT+"' where id = ";
                    result = entityManager.createNativeQuery( query + preAccount).executeUpdate();
                } else {
                    data.add(Account.Authority.PRO_CONSULTANT);
                  result = entityManager.createNativeQuery("UPDATE account SET type = 'PRO_CONSULTANT' where id = " + preAccount).executeUpdate();
                }
                Optional<Account> test = accountRepository.findById(preAccount);
                test.get().updateAuthority(data);
                if(result > 0) {
                    accountRepository.save(test.get());
                }

            }
            organization.setManager(account);
            organizationRepository.save(organization);
        }

        return accountRepository.save(account);
    }

    public AccountDto.Response getAccount(final Long id) {
        Account account = accountRepository.findById(id)
                .orElseThrow(() -> new AccountNotFoundException(id));
        return toResponse(account);
    }

    public Map<String, Object> getAccountsListToExcel(
            AccountDto.Search search, Type type, String type2
    ) {

        List<List<String>> body = new ArrayList<>();
        List<Account> lists = accountRepository.findAll(search.toSpecification());

        for (int i = 0; i < lists.size(); i++) {
            List<String> excel;
            excel = Arrays.asList(
                    lists.get(i).getUsername() == null ? "" : lists.get(i).getUsername(),
                    lists.get(i).getName() == null ? "" : lists.get(i).getName(),
                    lists.get(i).getBirthday() == null ? "" : lists.get(i).getBirthday().toString(),
                    lists.get(i).getMobilePhone() == null ? "" : lists.get(i).getMobilePhone(),
                    lists.get(i).getEmail() == null ? "" : lists.get(i).getEmail(),
                    lists.get(i).getAddress() == null ? "" : lists.get(i).getAddress().getAddress(),
                    lists.get(i).getAddress() == null ? "" : lists.get(i).getAddress().getDetailedAddress(),
                    lists.get(i).getAddress() == null ? "" : lists.get(i).getAddress().getZipCode(),
                    lists.get(i).getOrganization() == null ? "" : lists.get(i).getOrganization().getName(),
                    lists.get(i).getOrganization() == null ? "" : lists.get(i).getOrganization().getCode(),
                    lists.get(i).getWorkerType() == null ? "" : lists.get(i).getWorkerType().getTitle(),
                    lists.get(i).getEducationCompletionLevel() == null ? "" : lists.get(i).getEducationCompletionLevel().getTitle(),
                    lists.get(i).getConsultantLevel() == null ? "" : lists.get(i).getConsultantLevel().getTitle(),
                    lists.get(i).getProfessionalField() == null ? "" : lists.get(i).getProfessionalField().getTitle(),
                    lists.get(i).getConsultationPrice() <= 0 ? "0" : String.valueOf(lists.get(i).getConsultationPrice())
            );
            body.add(excel);
        }


        Map<String, Object> data = Maps.newHashMap();
        data.put(Excel.FILE_NAME.getName(), "상담원 조회.xls");
        data.put(Excel.HEAD.getName(), this.getConsultantHeadersInData(type, type2));
        data.put(Excel.BODY.getName(), body);
        return data;
    }


    public Page<Response> getAccounts(
            AccountDto.Search search,
            Pageable pageable
    ) {
        Page<Account> page = accountRepository.findAll(search.toSpecification(), pageable);
        List<Response> content = page.getContent().stream()
                .map(this::toResponse)
                .collect(Collectors.toList());

        return new PageImpl<>(content, pageable, page.getTotalElements());
    }

    public AccountDto.Response findByUsername(String username) {
        Account account = accountRepository.findByUsername(username)
                .orElseThrow(() -> new AccountNotFoundException(username));
        return toResponse(account);
    }

    @Transactional
    public AccountDto.Response updateAccount(final Long id, AccountDto.Update dto,
                                             final String username) {
        Account account = this.validate(id, username);

        Organization organization = null;
        if (dto.getOrganization() != null) {
            organization = organizationRepository.findById(dto.getOrganization().getId())
                    .orElseThrow(IllegalArgumentException::new);
        }

        if (dto.getOrganization() != null) {
            account.setOrganization(organization);
        }

        modelMapper.map(dto, account);
        account.setAuthorities(dto.getAuthorities());

        if (dto.getConsultantLevel() != account.getConsultantLevel()) {
            account.setConsultantLevel(dto.getConsultantLevel());
        }
        if (dto.getEducationCompletionLevel() != account.getEducationCompletionLevel()) {
            account.setEducationCompletionLevel(dto.getEducationCompletionLevel());
        }
        if (dto.getProfessionalField() != account.getProfessionalField()) {
            account.setProfessionalField(dto.getProfessionalField());
        }

        if (dto.getProfessionalField2() != account.getProfessionalField2()) {
            account.setProfessionalField2(dto.getProfessionalField2());
        }

        if(dto.getPassword() != null) {
            account.setPassword(passwordEncoder.encode(dto.getPassword()));
        }

        if(account.getStatus().equals(Account.Status.READY)) {
            account.setStatus(Account.Status.ACTIVATION);
        }


        // 탈퇴.
        if (dto.getIsLeaved()) {
            dto.setStatus(Account.Status.DEACTIVATION);
            dto.setAddress(null);
            dto.setEmail(null);
            dto.setMobilePhone(null);
            dto.setNotificationConsent(false);
            dto.setMobilePhoneCorp(null);
            dto.setLeaveDate(LocalDateTime.now());
        }

        //관리자가 탈퇴시킨 경우
        if (dto.getLeaveReasonType() != null && dto.getLeaveType().equals(Account.LeaveType.THROW_OUT)) {
            if (!dto.getLeaveReasonType().equals(Account.LeaveReasonType.REQ_USER)) {
                dto.setStatus(Account.Status.BLOCK);
            } else {
                dto.setStatus(Account.Status.DEACTIVATION);
            }
            dto.setAddress(null);
            dto.setEmail(null);
            dto.setMobilePhone(null);
            dto.setNotificationConsent(false);
            dto.setMobilePhoneCorp(null);
            dto.setLeaveDate(LocalDateTime.now());
        }

        account.updateAuthority(dto.getAuthorities());

        Account resultData = accountRepository.save(account);


        if ((account.getType().isCenterManager() ||
                account.getType().isRelationshipManager()) && !organization.getManager().getUsername()
                .equals(username)) {
            Organization updateOrganization = account.getOrganization();
            updateOrganization.setManager(resultData);
            organizationRepository.save(updateOrganization);
        }
        return toResponse(resultData, o -> modelMapper.map(o, AccountDto.Response.class));
    }

    private <T extends Account, R extends AccountDto.Response> R toResponse(T t,
                                                                            Function<T, R> responseMapper) {
        return responseMapper.apply(t);
    }


    private AccountDto.Response toResponse(Account account) {
        return modelMapper.map(account, AccountDto.Response.class);
    }

    private Account validate(final Long id, final String username) {

        Account account = accountRepository.findById(id)
                .orElseThrow(() -> new AccountNotFoundException(id));

        Account authUser = accountRepository.findByUsername(username)
                .orElseThrow(() -> new AccountNotFoundException(username));

        if (authUser.isAdminRole()) {
            return account;
        }

        if ((authUser.isCenterManageRole() || authUser.isRelationshipManagerRole())
                && authUser.getOrganization().getId() == account.getOrganization().getId()) {
            return account;
        }

        if (!account.getUsername().equals(username)) {
            throw new AccessDeniedException("Access is denied ");
        }

        return account;
    }


    private Class<? extends Account> getEntityClass(Type type) {
        if (type.isAdmin()) {
            return Admin.class;
        } else if (type.isCenterManager()) {
            return CenterManager.class;
        } else if (type.isRelationshipManager()) {
            return RelationshipManager.class;
        } else if (type.isConsultant()) {
            return Consultant.class;
        } else if (type.isProConsultant()) {
            return ProConsultant.class;
        }
        return Member.class;
    }

    @Transactional
    public void updateTemploraryPassword(Long id) throws NotFoundException {

        Account account = accountRepository.findById(id).orElseThrow(NotFoundException::new);
        String passwd = Hasher.parseMD5(UUID.randomUUID().toString()).substring(0, 10);

        account.setPassword(passwordEncoder.encode(passwd));

        PebbleMailMessage message = new PebbleMailMessage("temporary-password");
        message.addAttribute("userid", account.getUsername());
        message.addAttribute("passwd", passwd);
        message.setTo(account.getEmail());
        message.setSubject("[BANKSCAN] 임시비밀번호 발급안내");

        emailService.sendEmail(message);

    }

    public Map<String, Object> sampleFileDownLoadConsultant(Type type) {

        List<List<String>> body = new ArrayList<>();

        body.add(getConsultantBody(type));

        Map<String, Object> data = Maps.newHashMap();
        data.put(Excel.FILE_NAME.getName(), type.equals(Type.CONSULTANT) ? "상담원 등록 샘플" : "전문 상담원 등록");
        data.put(Excel.HEAD.getName(), this.getConsultantHeaders(type));
        data.put(Excel.BODY.getName(), body);

        return data;
    }


    private List<String> getConsultantHeaders(Type type) {
        if (type.equals(Type.CONSULTANT)) {
            return Arrays.asList(
                    "상담원ID",
                    "비밀번호",
                    "이름",
                    "생년월일",
                    "휴대폰번호",
                    "이메일",
                    "주소",
                    "상세주소",
                    "우편번호",
                    "센터명",
                    "센터코드",
                    "근무형태",
                    "교육수료등급",
                    "상담원레벨",
                    "전문분야",
                    "상담단가"
            );
        }

        if (type.equals(Type.PRO_CONSULTANT)) {
            return Arrays.asList(
                    "상담원ID",
                    "비밀번호",
                    "이름",
                    "생년월일",
                    "휴대폰번호",
                    "이메일",
                    "주소",
                    "상세주소",
                    "우편번호",
                    "기관명",
                    "기관코드",
                    "근무형태",
                    "교육수료등급",
                    "전문분야"
            );
        }
        return null;
    }


    private List<String> getConsultantHeadersInData(Type type, String type2) {
        if (type.equals(Type.CONSULTANT) || type2.equals("consultant")) {
            return Arrays.asList(
                    "상담원ID",
                    "이름",
                    "생년월일",
                    "휴대폰번호",
                    "이메일",
                    "주소",
                    "상세주소",
                    "우편번호",
                    "센터명",
                    "센터코드",
                    "근무형태",
                    "교육수료등급",
                    "상담원레벨",
                    "전문분야",
                    "상담단가"
            );
        }

        if (type.equals(Type.PRO_CONSULTANT) || type2.equals("proConsultant")) {
            return Arrays.asList(
                    "상담원ID",
                    "이름",
                    "생년월일",
                    "휴대폰번호",
                    "이메일",
                    "주소",
                    "상세주소",
                    "우편번호",
                    "기관명",
                    "기관코드",
                    "근무형태",
                    "교육수료등급",
                    "전문분야"
            );
        }


        return Arrays.asList(
                "상담원ID",
                "이름",
                "생년월일",
                "휴대폰번호",
                "이메일",
                "주소",
                "상세주소",
                "우편번호",
                "센터/기관명",
                "센터/기관코드",
                "근무형태",
                "교육수료등급",
                "전문분야"
        );
    }


    private List<String> getConsultantBody(Type type) {
        if (type.equals(Type.CONSULTANT)) {
            return Arrays.asList(
                    "test",
                    "test1234",
                    "홍길동",
                    "1999-11-11",
                    "010-3333-4444",
                    "test@test.com",
                    "서울시 노원구 한글비석로 6",
                    "305동 206호",
                    "19030",
                    "금융건강지원센터4",
                    "CEN004",
                    "정규직",
                    "기본수료",
                    "A레벨",
                    "금융상품",
                    "100000"
            );
        }

        if (type.equals(Type.PRO_CONSULTANT)) {
            return Arrays.asList(
                    "test",
                    "test1234",
                    "홍길동",
                    "1999-11-11",
                    "010-3333-4444",
                    "test@test.com",
                    "서울시 노원구 한글비석로 6",
                    "305동 206호",
                    "19030",
                    "금융건강지원센터4",
                    "CEN004",
                    "정규직",
                    "기본수료",
                    "금융상품"
            );
        }
        return null;
    }


}
