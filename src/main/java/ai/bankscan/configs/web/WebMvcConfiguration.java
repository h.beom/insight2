package ai.bankscan.configs.web;

import ai.bankscan.common.infra.web.excel.components.ExcelReader;
import ai.bankscan.common.infra.web.interceptors.WebLogInterceptor;
import ai.bankscan.common.infra.web.notice.properties.FirebaseProperties;
import ai.bankscan.common.infra.web.notice.service.DefaultFirebaseMessageServiceImpl;
import ai.bankscan.common.infra.web.notice.service.FirebaseMessageService;
import ai.bankscan.common.infra.web.pebble.PebbleViewExtension;
import ai.bankscan.common.infra.web.storage.properties.StorageProperties;
import ai.bankscan.common.infra.web.storage.service.FileSystemStorageService;
import ai.bankscan.common.infra.web.storage.service.StorageService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

/**
 * The type Web auto configuration.
 *
 * @author eomjeongjae
 * @since 2019 -07-22
 */
@Slf4j
@RequiredArgsConstructor
@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {

  private final ObjectMapper objectMapper;
  private final StorageProperties storageProperties;
  private final FirebaseProperties firebaseProperties;
  private final RestTemplateBuilder restTemplateBuilder;
  private final MessageSource messageSource;

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(new WebLogInterceptor())
        .excludePathPatterns(
            "/css/**",
            "/js/**",
            "/fonts/**",
            "/img/**",
            "/images/**",
            "/webfonts/**",
            "/i18n/**",
            "/favicon.ico",
            "/assets/**",
            "/static-bundle/**"
        );
    registry.addInterceptor(localeChangeInterceptor());
  }

  /**
   * Locale change interceptor locale change interceptor.
   *
   * @return the locale change interceptor
   */
  @Bean
  @ConditionalOnMissingBean
  public LocaleChangeInterceptor localeChangeInterceptor() {
    LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
    localeChangeInterceptor.setParamName("lang");
    return localeChangeInterceptor;
  }

  /**
   * Locale resolver locale resolver.
   *
   * @return the locale resolver
   */
  @Bean
  @ConditionalOnMissingBean
  public LocaleResolver localeResolver() {
    CookieLocaleResolver localeResolver = new CookieLocaleResolver();
    localeResolver.setCookieName("i18n");
    localeResolver.setCookieMaxAge(-1);
    localeResolver.setCookiePath("/");
    return localeResolver;
  }

  /**
   * Message source accessor message source accessor.
   *
   * @return the message source accessor
   */
  @Bean
  @ConditionalOnMissingBean
  public MessageSourceAccessor messageSourceAccessor() {
    return new MessageSourceAccessor(messageSource);
  }

  /**
   * Pebble view extension pebble view extension.
   *
   * @return the pebble view extension
   */
  @Bean
  public PebbleViewExtension pebbleViewExtension() {
    return new PebbleViewExtension(messageSourceAccessor(), objectMapper);
  }

  /**
   * Storage service storage service.
   *
   * @return the storage service
   */
  @Bean
  @ConditionalOnProperty(prefix = "storage", name = "location")
  public StorageService storageService() {
    return new FileSystemStorageService(storageProperties);
  }

  /**
   * Firebase message service firebase message service.
   *
   * @return the firebase message service
   */
  @Bean
  public FirebaseMessageService firebaseMessageService() {
    return new DefaultFirebaseMessageServiceImpl(firebaseProperties);
  }

  /**
   * Rest template rest template.
   *
   * @return the rest template
   */
  @Bean
  @ConditionalOnMissingBean
  public RestTemplate restTemplate() {
    return restTemplateBuilder.build();
  }

  /**
   * Excel reader excel reader.
   *
   * @return the excel reader
   */
  @Bean
  @ConditionalOnMissingBean
  public ExcelReader excelReader() {
    return new ExcelReader();
  }

}
