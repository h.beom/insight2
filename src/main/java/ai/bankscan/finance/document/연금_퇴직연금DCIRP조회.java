package ai.bankscan.finance.document;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class 연금_퇴직연금DCIRP조회 extends AuditableJoinEntity<Account> {

  private String 가입회사;
  private String 상품유형;
  private String 상품명;
  private String 가입일;
  private String 연금개시예정일;
  private String 적립금;
  private String 조회기준일;
  private String 계좌번호;
  private String 총납입액;
  private String 중도인출금액;
  private String 납입종료일;
  private String 연납입액;
  private String 연약정납입액;
  private String 예상연금적립액;
  private String 적립금합계금액;

}
