package ai.bankscan.finance.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class 카드_카드청구내역_Dto {

  @Data
  public static class Create {

    private String card;
    private String month;
    private Integer idx;

    private String 카드번호;
    private String 카드종류;
    private String 결제일;
    private String 출금예정일;
    private String 이용일자;
    private String 가맹점명;
    private String 할부개월;
    private String 입금회차;
    private Long 이용대금;
    private Long 청구금액;
    private Long 수수료;
    private Long 결제후잔액;
    private String 가맹점사업자번호;
    private String 가맹점업종;
    private String 가맹점주소;
    private String 가맹점전화번호;
    private String 가맹점대표자명;
    private String 회원사;
    private String 청구내역포인트;
    private String 구분;

    private String 결제계좌은행;
    private String 결제계좌번호;
  }

  @Data
  @NoArgsConstructor
  public static class GroupBy {
    public GroupBy(Long 원금, Long 이자) {
      this.원금 = 원금;
      this.이자 = 이자;
    }

    private Long 원금;
    private Long 이자;
  }
}
