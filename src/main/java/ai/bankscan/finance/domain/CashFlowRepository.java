package ai.bankscan.finance.domain;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CashFlowRepository extends JpaRepository<CashFlow, Long>,
    JpaSpecificationExecutor<CashFlow> {

  Optional<CashFlow> findByCreatedByIdAndYear(Long createdById, String year);

  @Query("SELECT MAX(year) FROM CashFlow WHERE createdBy.id = :createdById")
  String getLastYear(@Param("createdById") Long createdById);

  Long countByCreatedById(Long createdById);
}
