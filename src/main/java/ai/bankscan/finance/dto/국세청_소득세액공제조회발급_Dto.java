package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 국세청_소득세액공제조회발급_Dto {

  private String 귀속연도;
  private String 종류;
  private String 가입자;
  private Long 고지금액합계;
  private Long 납부금액합계;
  private Long 총합계;

  private String extra;
}
