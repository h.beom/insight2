package ai.bankscan.organizations.controller;

import ai.bankscan.common.infra.annotations.ApiPageable;
import ai.bankscan.common.infra.security.utils.SecurityUtils;
import ai.bankscan.common.infra.web.excel.ExcelXlsxView;
import ai.bankscan.organizations.domain.Organization;
import ai.bankscan.organizations.domain.Organization.Type;
import ai.bankscan.organizations.domain.OrganizationRepository;
import ai.bankscan.organizations.dto.CenterDto;
import ai.bankscan.organizations.dto.CenterDto.Response;
import ai.bankscan.organizations.dto.FacilitiesDto;
import ai.bankscan.organizations.dto.FacilitiesDto.Search;
import ai.bankscan.organizations.service.CenterService;
import ai.bankscan.organizations.service.FacilitiesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.validation.Valid;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Api(tags = {"센터 관리 API"})
@RequiredArgsConstructor
@RestController
@RequestMapping(path = CenterRestController.REQUEST_BASE_PATH, produces = MediaTypes.HAL_JSON_VALUE)
public class CenterRestController {

    static final String REQUEST_BASE_PATH = "/api/centers";

    private final CenterService centerService;
    private final FacilitiesService facilitiesService;
    private final OrganizationRepository organizationRepository;
    private final CenterResourceAssembler centerResourceAssembler;
    private final FacilitiesResourceAssembler facilitiesResourceAssembler;

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "센터 코드 중복 검사", notes = "센터 코드가 중복되지 않으면 성공(200)")
    @GetMapping("/{code}/check")
    public ResponseEntity<?> checkCenterCode(
            @ApiParam(required = true, example = "code") @PathVariable final String code
    ) {
        Optional<Organization> optionalOrganization = organizationRepository
                .findByTypeAndCode(Type.CENTER, code);

        if (optionalOrganization.isPresent()) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok().build();
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "센터 등록")
    @PostMapping
    public ResponseEntity<Resource<Response>> createCenter(
            @RequestBody @Valid CenterDto.Create dto
    ) throws URISyntaxException {
        Response savedCenter = centerService.createOrganization(dto);
        Resource<Response> resource = toResource(savedCenter);

        return ResponseEntity
                .created(new URI(resource.getLink(Link.REL_SELF).getHref()))
                .body(resource);
    }

    @ApiPageable
    @ApiOperation(value = "센터 목록 조회")
    @GetMapping
    public Resources<Resource<Response>> getCenters(
            CenterDto.Search search,
            @PageableDefault(sort = "createdAt", direction = Direction.DESC) Pageable pageable,
            PagedResourcesAssembler<Response> pagedResourcesAssembler
    ) {
        search.setDisplay(true);
        Page<Response> page = centerService.getOrganizations(search, pageable);
        return pagedResourcesAssembler.toResource(page, centerResourceAssembler);
    }

    @ApiOperation(value = "센터 조회")
    @GetMapping("/{id}")
    public ResponseEntity<Resource<Response>> getCenter(
            @ApiParam(required = true, example = "1") @PathVariable final Long id
    ) {
        Resource<Response> resource = toResource(centerService.getOrganization(id));
        if (SecurityUtils.anyGranted("ROLE_MEMBER") && !resource.getContent().getDisplay()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok(resource);
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "센터 수정")
    @PutMapping("/{id}")
    public ResponseEntity<Resource<Response>> updateCenter(
            @ApiParam(required = true, example = "1") @PathVariable final Long id,
            @RequestBody @Valid CenterDto.Update dto,
            @AuthenticationPrincipal final String username
    ) throws URISyntaxException {
        Response savedTeacher = centerService
                .updateOrganization(id, dto, username);
        Resource<Response> resource = toResource(savedTeacher);

        return ResponseEntity
                .created(new URI(resource.getLink(Link.REL_SELF).getHref()))
                .body(resource);
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "센터 삭제")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCenter(
            @ApiParam(required = true, example = "1") @PathVariable final Long id,
            @AuthenticationPrincipal final String username
    ) {
        centerService.deleteOrganization(id);
        return ResponseEntity
                .noContent()
                .build();
    }

    @Secured({"ROLE_ADMIN", "ROLE_CENTER_MANAGER"})
    @ApiOperation(value = "센터 시설 정보 조회")
    @GetMapping("/{id}/facilities/{year}")
    public Resources<Resource<FacilitiesDto.Response>> getFacilitiesList(
            @ApiParam(required = true, example = "1") @PathVariable final Long id,
            @ApiParam(required = true, example = "2020") @PathVariable final Integer year
    ) {
        Search search = Search.builder()
                .centerId(id)
                .year(year)
                .build();

        List<Resource<FacilitiesDto.Response>> resources = facilitiesService.getFacilities(search)
                .stream()
                .map(facilities -> facilitiesResourceAssembler.toResource(facilities))
                .collect(Collectors.toList());
        return new Resources<>(resources);
    }

    @Secured({"ROLE_ADMIN", "ROLE_CENTER_MANAGER"})
    @ApiOperation(value = "센터 시설 정보 엑셀 다운로드")
    @GetMapping("/{id}/facilities/{year}/excel")
    public ModelAndView getFacilitiesListToExcel(
            @ApiParam(required = true, example = "1") @PathVariable final Long id,
            @ApiParam(required = true, example = "2020") @PathVariable final Integer year
    ) {
        Map<String, Object> facilitiesToExcelData = facilitiesService.getFacilitiesToExcel(id, year);
        return new ModelAndView(new ExcelXlsxView(), facilitiesToExcelData);
    }

    private Resource<Response> toResource(Response savedCenter) {
        return centerResourceAssembler.toResource(savedCenter);
    }

}
