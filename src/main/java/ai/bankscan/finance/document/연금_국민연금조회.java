package ai.bankscan.finance.document;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class 연금_국민연금조회 extends AuditableJoinEntity<Account> {

  private String 가입자구분;
  private String 연금종류;
  private String 연금개시년월;
  private String 예상연금수령액;
  private String 납부월수;
  private String 납부총액;
  private String 예상총납부월수;
  private String 예상납부보험료;
  private String 조회기준일;

}
