package ai.bankscan.configs.security.device;

import ai.bankscan.accounts.domain.AccountDevice;
import ai.bankscan.accounts.service.AccountDeviceAdapter;
import ai.bankscan.accounts.service.AccountDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class DeviceAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private AccountDeviceService accountDeviceService;

    @Override
    public Authentication authenticate(Authentication auth) throws AuthenticationException {
        final DeviceAuthenticationToken tokenContainer = (DeviceAuthenticationToken) auth;
        final String token = tokenContainer.getToken();

        AccountDevice node = accountDeviceService.findByAccessToken(token);
        if (node == null) throw new UsernameNotFoundException("Not found Device");

        return new DeviceAuthenticationToken(token, new AccountDeviceAdapter(node.getAccount(), node));
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return DeviceAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
