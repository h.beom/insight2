package ai.bankscan.survey.domain;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import ai.bankscan.survey.dto.금융건강진단표AnswersDto;
import ai.bankscan.survey.dto.금융건강진단표AnswersDto.Result;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class 금융건강진단표 extends AuditableJoinEntity<Account> {

  public enum Status {
    READY, ACTIVE;
  }

  @Default
  private String surveyType = "금융건강진단표";

  @Lob
  private List<금융건강진단표AnswersDto.Response> answerList;

  private Result firstType;

  private Result lastType;

  @Lob
  @NotNull
  private String result;

  @Lob
  private String 현명한금융거래;

  @Lob
  private String 행복한노후준비;

  @Lob
  private String 철저한신용관리;

  @Lob
  private String 내살집마련;

  @Lob
  private String 내복지찾기;

  @Lob
  private String financeResult2;

  @Default
  @Enumerated(EnumType.STRING)
  private Status status = Status.READY;

}
