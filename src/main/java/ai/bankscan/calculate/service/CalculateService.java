package ai.bankscan.calculate.service;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.Account.Type;
import ai.bankscan.calculate.domain.Calculate;
import ai.bankscan.calculate.domain.CalculateRepository;
import ai.bankscan.calculate.dto.CalculateDto;
import ai.bankscan.calculate.dto.CalculateDto.Search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service
public class CalculateService {
    private final CalculateRepository calculateRepository;

    private final EntityManager entityManager;

    public List<Map<String, Object>> getCalculateList(Search search, Account account) {
        String where = "";
        if (account.getType().equals(Type.CONSULTANT)) {
            search.setSearchType("consultant.id");
            where += "AND c.consultant_id = " + account.getId() + " ";
            search.setKeyword(account.getId().toString());
        }

        if (account.getType().equals(Type.CENTER_MANAGER)) {
            search.setSearchType("consultation.center");
            where += "AND a.organization_id = " + account.getOrganization().getId() + " ";
            search.setKeyword(account.getOrganization().getId().toString());
        }
        String[] headers = {
                "id",
                "username",
                "name",
                "consultId",
                "consultation_price",
                "organizationName",
                "code",
                "allCount",
                "allResult"
        };

        String sql = "select result.id, " +
                " result.username, " +
                " result.name, " +
                " result.consultId, " +
                " result.consultation_price, " +
                " result.organizationName, " +
                " result.code, " +
                " result.allCount," +
                " result.allCount * result.consultation_price allResult " +
                "FROM (" +
                "select c.id, " +
                " a.username, " +
                " a.id consultId, " +
                " a.name, " +
                " a.consultation_price, " +
                " o.name as organizationName, " +
                " o.code, " +
                " count(c.id) as allCount " +
                "from calculate c, " +
                "account a, " +
                "organization o " +
                "WHERE c.consultant_id = a.id " +
                where +
                "and o.id = a.organization_id " +
                "GROUP BY c.consultant_id " +
                ") result";

        Query query = entityManager.createNativeQuery(sql);

        List<Object[]> dataList = query.getResultList();

        List<Map<String, Object>> data = new ArrayList<>();
        for (Object[] objects : dataList) {
            Map<String, Object> b = new HashMap<>();
            b.put(headers[0], objects[0]);
            b.put(headers[1], objects[1]);
            b.put(headers[2], objects[2]);
            b.put(headers[3], objects[3]);
            b.put(headers[4], objects[4]);
            b.put(headers[5], objects[5]);
            b.put(headers[6], objects[6]);
            b.put(headers[7], objects[7]);
            b.put(headers[8], objects[8]);
            data.add(b);
        }
        return data;
    }

    public Map<String, Object> getCalculateListByUser(long id) {

        String[] headers1 = {
                "username",
                "name",
                "mobilePhone",
                "email",
                "centerName",
                "centerCode"
        };
        String sql1 = "SELECT a.username, " +
                "a.name, " +
                "a.email, " +
                "a.mobile_phone, " +
                "o.name centerName, " +
                "o.code centerCode " +
                "FROM account a, " +
                "calculate c, " +
                "organization o " +
                "WHERE a.organization_id = o.id " +
                "AND c.consultant_id = a.id " +
                "AND c.consultant_id = " + id;


        Query query = entityManager.createNativeQuery(sql1);
        List<Object[]> dataList = query.getResultList();

        //basic information
        Map<String, Object> basicInformation = setMapData(headers1, dataList);


        String[] headers2 = {
                "allCount",
                "allPrice"
        };

        String sql2 = "select " +
                "count(c.id) allCount, " +
                "count(c.id) * a.consultation_price as allPrice " +
                "FROM calculate c, " +
                "account a " +
                "WHERE " +
                "c.consultant_id = a.id " +
                "and c.consultant_id = " + id;
        Query query2 = entityManager.createNativeQuery(sql2);
        List<Object[]> dataList2 = query2.getResultList();

        //all Counts
        Map<String, Object> allCounts = setMapData(headers2, dataList2);

        String[] headers3 = {
                "id",
                "code",
                "username",
                "name",
                "mobilePhone",
                "email",
                "consultWantDate",
                "consultWantTime",
                "createdAt",
                "updatedOn",
                "isCalculate"
        };

        String sql3 = "select  " +
                "c.id, " +
                "con.code, " +
                "a.username, " +
                "a.name, " +
                "a.mobile_phone, " +
                "a.email, " +
                "con.consult_want_date, " +
                "con.consult_want_time, " +
                "con.created_at, " +
                "con.updated_on, " +
                "c.is_calculate " +
                "FROM calculate c, " +
                "consultation con, " +
                "account a " +
                "WHERE  " +
                "a.id = con.created_by_id " +
                "and c.consultation_id = con.id " +
                "and c.consultant_id = " + id;


        Query query3 = entityManager.createNativeQuery(sql3);

        List<Object[]> dataList3 = query3.getResultList();

        List<Map<String, Object>> datas = setMapDataList(headers3, dataList3);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("basic", basicInformation);
        resultMap.put("allCounts", allCounts);
        resultMap.put("lists", datas);
        return resultMap;
    }

    private Map<String, Object> setMapData(String[] headers, List<Object[]> lists) {
        Map<String, Object> dataMap = new HashMap<>();
        for (Object[] objects : lists) {
            for (int i = 0; i < headers.length; i++) {
                dataMap.put(headers[i], objects[i]);
            }
        }
        return dataMap;
    }

    private List<Map<String, Object>> setMapDataList(String[] headers, List<Object[]> lists) {
        List<Map<String, Object>> result = new ArrayList<>();
        for (Object[] objects : lists) {
            Map<String, Object> dataMap = new HashMap<>();
            for (int i = 0; i < headers.length; i++) {
                dataMap.put(headers[i], objects[i]);
            }
            result.add(dataMap);
        }
        return result;
    }

    @Transactional
    public Map<String, Object> updateCalculates(long id, List<CalculateDto.Simple> changeList) {
        for (int i = 0; i < changeList.size(); i++) {
            Calculate calculate = calculateRepository.findById(changeList.get(i).getId()).orElse(null);
            if (calculate != null) {
                if(!calculate.isCalculate()) {
                    calculate.setCalculate(true);
                    calculateRepository.save(calculate);
                }
            }
        }
        return getCalculateListByUser(id);
    }
}
