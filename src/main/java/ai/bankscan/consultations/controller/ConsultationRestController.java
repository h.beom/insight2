package ai.bankscan.consultations.controller;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.common.infra.annotations.ApiPageable;
import ai.bankscan.common.infra.security.common.CurrentUser;
import ai.bankscan.common.infra.web.excel.ExcelXlsView;
import ai.bankscan.common.infra.web.excel.ExcelXlsxView;
import ai.bankscan.common.infra.web.excel.components.ExcelWriter;
import ai.bankscan.consultations.domain.consulting.Consultation.Type;
import ai.bankscan.consultations.dto.ConsultContentDto;
import ai.bankscan.consultations.dto.ConsultationDto;
import ai.bankscan.consultations.dto.ConsultationDto.Response;
import ai.bankscan.consultations.dto.ConsultationHistoryDto;
import ai.bankscan.consultations.dto.ConsultationLogDto;
import ai.bankscan.consultations.service.ConsultContentService;
import ai.bankscan.consultations.service.ConsultationHistoryService;
import ai.bankscan.consultations.service.ConsultationLogService;
import ai.bankscan.consultations.service.ConsultationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.dom4j.rule.Mode;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Api(tags = {"상담 관리 API"})
@RequiredArgsConstructor
@RestController
@RequestMapping(path = ConsultationRestController.REQUEST_BASE_PATH, produces = MediaTypes.HAL_JSON_VALUE)
public class ConsultationRestController {

  static final String REQUEST_BASE_PATH = "/api/consultations";

  private final ConsultContentService consultContentService;
  private final ConsultationService consultationService;
  private final ConsultationLogService consultationLogService;
  private final ConsultationHistoryService consultationHistoryService;


  @Secured({
      "ROLE_ADMIN",
      "ROLE_CENTER_MANAGER",
      "ROLE_RELATIONSHIP_MANAGER",
      "ROLE_CONSULTANT",
      "ROLE_PRO_CONSULTANT"
  })
  @ApiPageable
  @ApiOperation(value = "상담 관리 목록", notes = "상담 관리 목록")
  @GetMapping
  public ResponseEntity<Page<Response>> getConsultations(
      ConsultationDto.Search search,
      @PageableDefault(page = 0, size = 10)
      @SortDefault.SortDefaults({
          @SortDefault(sort = "createdAt", direction = Sort.Direction.DESC),
          @SortDefault(sort = "updatedOn", direction = Sort.Direction.DESC)
      }) Pageable pageable,
      @CurrentUser Account currentUser
  ) {
    Page<ConsultationDto.Response> page;
      page = consultationService.getConsultationList(currentUser, pageable, search);
    return ResponseEntity.ok(page);
  }

  @Secured({
          "ROLE_ADMIN",
          "ROLE_CENTER_MANAGER",
          "ROLE_RELATIONSHIP_MANAGER",
          "ROLE_CONSULTANT",
          "ROLE_PRO_CONSULTANT"
  })
  @ApiPageable
  @ApiOperation(value = "상담 관리 목록 통계", notes = "상담 관리 목록 통계")
  @GetMapping("/statics")
  public ResponseEntity<Map<String, Object>> getConsultationsAllCountFromStatus(
          ConsultationDto.Search search,
          @CurrentUser Account currentUser
  ) {
    Map<String, Object> map = consultationService.getConsultationAllCountFromStatus(currentUser, search);
    return ResponseEntity.ok(map);
  }


  @ApiOperation(value = "내 상담 목록", notes = "내 상담 목록")
  @Secured({"ROLE_MEMBER"})
  @PreAuthorize("isAuthenticated()")
  @GetMapping("/information/my-list")
  public ResponseEntity<List<ConsultationDto.Response>> getMemberSecured(
      @CurrentUser Member currentUser
  ) {
    return ResponseEntity.ok(consultationService.getMyConsultationListRoleUser(currentUser));
  }

  @ApiOperation(value = "상담 필요 기본 정보", notes = "단건 조회")
  @GetMapping("/{id}")
  @Secured({
      "ROLE_ADMIN",
      "ROLE_CENTER_MANAGER",
      "ROLE_RELATIONSHIP_MANAGER",
      "ROLE_CONSULTANT",
      "ROLE_PRO_CONSULTANT",
      "ROLE_MEMBER"
  })
  public ResponseEntity<ConsultationDto.Response> getConsultation(
      @ApiParam(required = true, example = "1") @PathVariable final Long id,
      @CurrentUser Account currentUser
  ) {
    return ResponseEntity.ok(consultationService.getConsultation(currentUser, id));
  }

  @ApiOperation(value = "상담시 기본 정보", notes = "단건 조회")
  @GetMapping("/{id}/basic")
  @Secured({
      "ROLE_ADMIN",
      "ROLE_CENTER_MANAGER",
      "ROLE_RELATIONSHIP_MANAGER",
      "ROLE_CONSULTANT",
      "ROLE_PRO_CONSULTANT",
      "ROLE_MEMBER"
  })
  public ResponseEntity<ConsultationDto.BasicSimple> getConsultationBasic(
      @ApiParam(required = true, example = "1") @PathVariable final Long id,
      @CurrentUser Account currentUser
  ) {
    return ResponseEntity.ok(consultationService.getConsultationBasic(currentUser, id));
  }

  @ApiOperation(value = "상담시 기본 정보", notes = "단건 조회")
  @GetMapping("/{id}/pro")
  @Secured({
      "ROLE_ADMIN",
      "ROLE_CENTER_MANAGER",
      "ROLE_RELATIONSHIP_MANAGER",
      "ROLE_CONSULTANT",
      "ROLE_PRO_CONSULTANT",
      "ROLE_MEMBER"
  })
  public ResponseEntity<ConsultationDto.ProSimple> getConsultationPro(
      @ApiParam(required = true, example = "1") @PathVariable final Long id,
      @CurrentUser Account currentUser
  ) {
    return ResponseEntity.ok(consultationService.getConsultationPro(currentUser, id));
  }


  @ApiOperation(value = "사용자가 상담을 신청", notes = "사용자 상담 신청은 무조건 기본상담 신청임.")
  @PostMapping("/add")
  @Secured("ROLE_MEMBER")
  @PreAuthorize("isAuthenticated()")
  public ResponseEntity<ConsultationDto.Response> createBasicConsultation(
      @RequestBody @Valid ConsultationDto.Create dto,
      @CurrentUser Member member
  ) throws URISyntaxException {

    ConsultationDto.Response saveBasicConsultation = consultationService
        .createBasicConsultation(dto, member);

    return ResponseEntity
        .created(new URI(Link.REL_SELF))
        .body(saveBasicConsultation);
  }

  @ApiOperation(value = "상담 상태, 상담자 변경")
  @PutMapping("/{id}")
  @Secured({
      "ROLE_ADMIN",
      "ROLE_CENTER_MANAGER",
      "ROLE_RELATIONSHIP_MANAGER",
      "ROLE_CONSULTANT",
      "ROLE_PRO_CONSULTANT"
  })
  public ResponseEntity<ConsultationDto.Response> updateBasicConsultation(
      @ApiParam(required = true, example = "1") @PathVariable final Long id,
      @RequestBody @Valid ConsultationDto.Update dto
  ) throws URISyntaxException {

    ConsultationDto.Response saved = consultationService
        .updateConsultation(dto, id);

    return ResponseEntity
        .created(new URI(Link.REL_SELF))
        .body(saved);
  }

  @ApiOperation(value = "상담 상태, 상담자 변경")
  @PostMapping("/update-list")
  @Secured({
          "ROLE_CENTER_MANAGER",
          "ROLE_RELATIONSHIP_MANAGER"
  })
  public ResponseEntity<String> updateBasicConsultationLists(
          @RequestBody List<ConsultationDto.Update2> dtoList
  ) {
    try {
      for(int i =0; i < dtoList.size(); i ++) {
        consultationService.updateConsultation2(dtoList.get(i), dtoList.get(i).getId());
      }
      return ResponseEntity.ok("SUCCESS");
    } catch (Exception e) {
      e.printStackTrace();
    }
    return ResponseEntity.status(400).body(null);
  }
  @ApiOperation(value = "상담 내역 추가")
  @PostMapping("/{id}/contents")
  @Secured({
      "ROLE_PRO_CONSULTANT",
      "ROLE_CONSULTANT"
  })
  public ResponseEntity<ConsultContentDto.Response> createConsultContent(
      @PathVariable final long id,
      @RequestBody @Valid ConsultContentDto.Create dto
  ) throws URISyntaxException {
    ConsultContentDto.Response savedContent = consultContentService.createConsultContent(id, dto);

    return ResponseEntity.created(new URI(Link.REL_SELF))
        .body(savedContent);
  }

  @ApiOperation(value = "상담 내역 리스트 정보")
  @GetMapping("/{id}/contents/{type}")
  @Secured({"ROLE_ADMIN",
      "ROLE_CENTER_MANAGER",
      "ROLE_CONSULTANT",
      "ROLE_RELATIONSHIP_MANAGER",
      "ROLE_PRO_CONSULTANT"
  })
  public ResponseEntity<List<ConsultContentDto.Response>> getConsultContents(
      @PathVariable final Long id,
      @PathVariable final Type type
  ) {
    List<ConsultContentDto.Response> consultContents = null;
    consultContentService
        .getConsultContentList(id, type);
    if(consultContents != null) {
      return ResponseEntity.ok(consultContents);
    } else {
      return ResponseEntity.status(400).body(null);
    }
  }

  @ApiOperation(value = "상담 로그 조회", notes = "상담 status 가 변경 될됨 때마다 작용 ")
  @GetMapping("/information/{id}/logs")
  @Secured({
      "ROLE_ADMIN",
      "ROLE_CENTER_MANAGER",
      "ROLE_RELATIONSHIP_MANAGER",
      "ROLE_CONSULTANT",
      "ROLE_PRO_CONSULTANT"
  })
  public ResponseEntity<List<ConsultationLogDto.Response>> getConsultationLogs(
      @ApiParam(required = true, example = "1") @PathVariable final Long id) {
    return ResponseEntity.ok(consultationLogService.getConsultationLog(id));
  }

  @ApiOperation(value = "상담 이력 조회", notes = "모든 조회 수정 등록 등등 모든 action 에서 적용 됨.")
  @GetMapping("/information/{id}/history")
  @Secured({
      "ROLE_ADMIN",
      "ROLE_CENTER_MANAGER",
      "ROLE_RELATIONSHIP_MANAGER",
      "ROLE_CONSULTANT",
      "ROLE_PRO_CONSULTANT"
  })
  public ResponseEntity<List<ConsultationHistoryDto.Response>> getConsultationHistory(
      @ApiParam(required = true, example = "1") @PathVariable final Long id) {
    return ResponseEntity.ok(consultationHistoryService.getConsultationLHistory(id));
  }

  @ApiOperation(value = "상담 엑셀 다운로드", notes = "엑셀 다운로드")
  @GetMapping("/file-download")
  @Secured({
          "ROLE_ADMIN",
          "ROLE_CENTER_MANAGER",
          "ROLE_RELATIONSHIP_MANAGER",
          "ROLE_CONSULTANT",
          "ROLE_PRO_CONSULTANT"
  })
  public ModelAndView downloadExcel(ConsultationDto.Search search, @CurrentUser Account currentUser, HttpServletRequest req, HttpServletResponse res) {
    Map<String, Object> resource = consultationService.getConsultationListToExcelDownload(currentUser, search);
    return new ModelAndView(new ExcelXlsxView(), resource);
  }

}