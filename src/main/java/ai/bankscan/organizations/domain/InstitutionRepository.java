package ai.bankscan.organizations.domain;

public interface InstitutionRepository extends OrganizationBaseRepository<Institution> {
}
