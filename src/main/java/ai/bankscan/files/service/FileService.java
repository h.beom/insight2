package ai.bankscan.files.service;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.Account.ConsultantLevel;
import ai.bankscan.accounts.domain.Account.ProfessionalField;
import ai.bankscan.accounts.domain.Account.Type;
import ai.bankscan.accounts.domain.Account.WorkerType;
import ai.bankscan.accounts.domain.AccountRepository;
import ai.bankscan.accounts.domain.admin.Admin;
import ai.bankscan.accounts.domain.cm.CenterManager;
import ai.bankscan.accounts.domain.consultants.Consultant;
import ai.bankscan.accounts.domain.consultants.ProConsultant;
import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.accounts.domain.rm.RelationshipManager;
import ai.bankscan.accounts.dto.AccountDto;
import ai.bankscan.common.address.dto.AddressDto;
import ai.bankscan.common.domain.Gender;
import ai.bankscan.common.infra.web.excel.ExcelXlsView;
import ai.bankscan.common.infra.web.excel.components.ExcelReader;
import ai.bankscan.common.infra.web.storage.exceptions.StorageFileNotFoundException;
import ai.bankscan.common.infra.web.storage.service.StorageService;
import ai.bankscan.common.money.domain.Money;
import ai.bankscan.files.domain.AttachFile;
import ai.bankscan.files.domain.AttachFileRepository;
import ai.bankscan.files.dto.ExcelDataDto;
import ai.bankscan.files.dto.FileDto;
import ai.bankscan.files.properties.FileProperties;
import ai.bankscan.organizations.domain.*;
import com.google.common.collect.Lists;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import javax.imageio.ImageIO;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.Resource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author eomjeongjae
 * @since 2019/10/15
 */
@Slf4j
@RequiredArgsConstructor
@Service
@Transactional(readOnly = true)
public class FileService {

    private final AttachFileRepository attachFileRepository;
    private final StorageService storageService;
    private final FileProperties fileProperties;
    private final AccountRepository accountRepository;
    private final PasswordEncoder passwordEncoder;
    private final CenterRepository centerRepository;
    private final InstitutionRepository institutionRepository;
    private final ModelMapper modelMapper;

    @Cacheable("attachFile")
    public FileDto.Response getFile(final Long id) {
        return modelMapper
                .map(attachFileRepository.findById(id).orElseThrow(IllegalArgumentException::new),
                        FileDto.Response.class);
    }

    public List<FileDto.Response> getFiles(final List<Long> ids) {
        List<AttachFile> files = attachFileRepository.findAllById(ids);
        return files.stream().map(v -> modelMapper.map(v, FileDto.Response.class)).collect(Collectors.toList());
    }

    @Transactional
    public List<FileDto.Response> storeFile(List<MultipartFile> files) {
        if (CollectionUtils.isEmpty(files)) {
            return Lists.newArrayList();
        }
        List<FileDto.Response> results = Lists.newArrayList();
        for (MultipartFile file : files) {
            results.add(storeFile(file));
        }
        return results;
    }

    private Iterator<Row> checkExcelFormatter(MultipartFile file) throws IOException {
        if(file.getOriginalFilename().contains(".xls")) {
            HSSFWorkbook hb = new HSSFWorkbook(file.getInputStream());
            HSSFSheet sheet = hb.getSheetAt(0);
            return sheet.iterator();
        } else {
            XSSFWorkbook xb = new XSSFWorkbook(file.getInputStream());
            XSSFSheet sheet = xb.getSheetAt(0);
            return sheet.iterator();
        }
    }

    @Transactional
    public String saveConsultant(MultipartFile file, String type) throws IOException {

        Iterator<Row> rowIterator = checkExcelFormatter(file);
        List<Map<String, Cell>> output = new ArrayList<>();
        List<String> header = new ArrayList<>();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            // For each row, iterate through all the columns01
            Iterator<Cell> cellIterator = row.cellIterator();
            Map<String, Cell> data = new HashMap<>();

            Cell cell = null;
            if (row.getRowNum() == 0) {
                while (cellIterator.hasNext()) {
                    cell = cellIterator.next();
                    header.add(cell.toString());
                }
            }

            while (cellIterator.hasNext()) {
                cell = cellIterator.next();
                data.put(header.get(cell.getColumnIndex()), cell);
            }
            if (data.size() > 0) {
                output.add(data);
            }
        }
        System.out.println("output..." + output);

        for (int i = 0; i < output.size(); i++) {
            this.saveConsultants(output.get(i), type);
        }

        return "success";
    }


    private Account saveConsultants(Map<String, Cell> data, String type) {

        Optional<Organization> organization = null;
        if(type.equals("consultant")) {
            organization = centerRepository.findByCodeAndName(data.get("센터코드").toString(), data.get("센터명").toString());
        } else {
            organization = institutionRepository.findByCodeAndName(data.get("기관코드").toString(), data.get("기관명").toString());
        }



        AddressDto.Save address = AddressDto.Save.builder()
                .address((String) this.cellTypeCheck(data.get("주소")))
                .detailedAddress((String) this.cellTypeCheck(data.get("상세주소")))
                .zipCode(Integer.parseInt(this.cellTypeCheck(data.get("우편번호")).toString()) + "")
                .build();

        AccountDto.Create dto = AccountDto.Create.builder()
                .type(type.equals("consultant") ? Type.CONSULTANT : Type.PRO_CONSULTANT)
                .workerType(WorkerType.valueOf(this.getWorkerType(this.cellTypeCheck(data.get("근무형태")).toString())))

                .educationCompletionLevel((this.cellTypeCheck(data.get("교육수료등급")).equals(""))
                        ? null
                        : Account.EducationCompletionLevel.valueOf(this.getEducationTypes(this.cellTypeCheck(data.get("교육수료등급")).toString())))
                .professionalField(this.cellTypeCheck(data.get("전문분야")).equals("")
                        ? null
                        : ProfessionalField.valueOf(this.getProfessionalField(this.cellTypeCheck(data.get("전문분야")).toString())))
                .birthday(LocalDate.parse((String) this.cellTypeCheck(data.get("생년월일"))))
                .username((String) this.cellTypeCheck(data.get("상담원ID")))
                .password((String) this.cellTypeCheck(data.get("비밀번호")))
                .name((String) this.cellTypeCheck(data.get("이름")))
                .email((String) this.cellTypeCheck(data.get("이메일")))
                .mobilePhone((String) this.cellTypeCheck(data.get("휴대폰번호")))
                .address(address)
                .ci(null)
                .di(null)
                .build();

        if(type.equals("consultant")) {
            long money = 0;
            if (this.cellTypeCheck(data.get("상담단가")).equals("")) {
                money = (long) this.cellTypeCheck(data.get("상담단가"));
            }
            dto.setConsultationPrice(money);
            dto.setConsultantLevel(ConsultantLevel.valueOf(getConsultantLevel(this.cellTypeCheck(data.get("상담원레벨")).toString())));
        }

        Account response = this.createAccount(dto, this.getEntityClass(dto.getType()));

        response.setOrganization(organization.get());

        return accountRepository.save(response);
    }


    private Object cellTypeCheck(Cell cell) {
        switch (cell.getCellTypeEnum()) {
            case BLANK:
                return cell.getDateCellValue();
            case STRING:
                return cell.getStringCellValue();
            case BOOLEAN:
                return cell.getBooleanCellValue();
            case FORMULA:
                return cell.getCellFormula();
            case NUMERIC:
                return (int) cell.getNumericCellValue();
            case ERROR:
                return cell.getErrorCellValue();
            default:
                return "";
        }
    }

    @Transactional
    public FileDto.Response storeFile(MultipartFile file) {
        LocalDateTime now = LocalDateTime.now();
        AttachFile attachFile = AttachFile.builder()
                .filename(file.getOriginalFilename())
                .contentType(file.getContentType())
                .size(file.getSize())
                .serverPath(now.format(DateTimeFormatter.ofPattern("yyyy/MM")))
                .build();

        AttachFile savedAttachFile = attachFileRepository.save(attachFile);
        storageService.store(file, Paths.get(savedAttachFile.getServerFilePath()));
        return modelMapper.map(savedAttachFile, FileDto.Response.class);
    }

    public FileDto.Download loadFile(final Long id) {
        AttachFile attachFile = attachFileRepository.findById(id)
                .orElseThrow(IllegalArgumentException::new);

        Resource resource = storageService.loadAsResource(attachFile.getServerFilePath());
        return FileDto.Download.builder()
                .attachFile(modelMapper.map(attachFile, FileDto.Response.class))
                .resource(resource)
                .build();
    }

    public FileDto.Download loadFile(final Long id, Long width, Long height, final String type) {
        AttachFile attachFile = attachFileRepository.findById(id)
                .orElseThrow(IllegalArgumentException::new);

        String convertedFileName = width + "x" + height + "_" + type + ".jpg";
        Path originPath = Paths.get(attachFile.getServerFilePath());
        Path convertedPath = originPath.getParent().resolve(convertedFileName);

        Path absoluteOriginPath = storageService.load(originPath.toString());
        Path absoluteConvertedPath = absoluteOriginPath.getParent().resolve(convertedFileName);

        try {
            try {
                Resource resource = storageService.loadAsResource(convertedPath.toString());
                if (resource.exists() && resource.isFile()) {
                    return FileDto.Download.builder()
                            .attachFile(modelMapper.map(attachFile, FileDto.Response.class))
                            .resource(resource)
                            .build();
                }
            } catch (StorageFileNotFoundException e) {
                // nothing
                log.error(e.getMessage());
            }

            if (!type.equals("crop")) {
                BufferedImage bufferedImage = ImageIO.read(originPath.toFile());
                float w = (float) bufferedImage.getWidth() / (float) width;
                float h = (float) bufferedImage.getHeight() / (float) height;
                if (h > w) {
                    width = 0L;
                } else {
                    height = 0L;
                }
            }

            Map<String, String> args = new LinkedHashMap<>();
            args.put("-resize", width + "x" + height + "^");
            args.put("-quality", "90");
            args.put("-gravity", "center");
            args.put("-auto-orient", "");
            args.put("-crop", width + "x" + height + "+0+0");
            args.put("+repage", "");

            String commandArgs = mapToCommand(args);
            List<String> commandList = Lists.newLinkedList();
            if (System.getProperty("os.name").contains("windows")) {
                commandList.add("\"" + fileProperties.getExtensionConvert() + "\"");
                commandList.add(
                        absoluteOriginPath.toString() + ((attachFile.isGif() || attachFile.isVideo()) ? "[0]"
                                : ""));
                commandList.add(commandArgs);
                commandList.add(absoluteConvertedPath.toString());
            } else {
                commandList.add(fileProperties.getExtensionConvert());
                commandList.add(absoluteOriginPath.toString() + (
                        (attachFile.isGif() || attachFile.isVideo()) ? "[0]" : ""));
                commandList.add(commandArgs);
                commandList.add(absoluteConvertedPath.toString());
            }

            String command = String.join(" ", commandList);
            log.info("command: {}", command);

            try {
                Process process = Runtime.getRuntime().exec(command);
                process.waitFor();
            } catch (Exception e) {
                // nothing
                log.error("Error occurrence", e);
            }

            return FileDto.Download.builder()
                    .attachFile(modelMapper.map(attachFile, FileDto.Response.class))
                    .resource(storageService.loadAsResource(convertedPath.toString()))
                    .build();
        } catch (IOException e) {
            throw new RuntimeException("Failed to load file " + convertedPath, e);
        }
    }

    private String mapToCommand(final Map<String, String> map) {
        return map.keySet().stream()
                .map(key -> key + " " + map.get(key))
                .collect(Collectors.joining(" "));
    }

    private <T extends Account> T createAccount(
            AccountDto.Create dto,
            Class<T> entityType
    ) {
        T account = modelMapper.map(dto, entityType);
        if (dto.getOrganizationId() != null && dto.getOrganizationId() > 0) {
            Optional<Center> center = null;
            Optional<Institution> institution = null;
            if (dto.getType().equals(Type.CONSULTANT)) {
                center = centerRepository.findById(dto.getOrganizationId());
                account.setOrganization(center.get());
            } else {
                institution = institutionRepository.findById(dto.getOrganizationId());
                account.setOrganization(institution.get());
            }
        }

        account.create(account.getType().getAuthorities(), passwordEncoder);

        return account;
    }

    private Path createDirectory(Path path) {
        try {
            Files.createDirectories(path);
        } catch (FileAlreadyExistsException e) {
            log.error("File already exists: {}", e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException("Failed to create directory", e);
        }
        return path;
    }


    private Class<? extends Account> getEntityClass(Type type) {
        if (type.isAdmin()) {
            return Admin.class;
        } else if (type.isCenterManager()) {
            return CenterManager.class;
        } else if (type.isRelationshipManager()) {
            return RelationshipManager.class;
        } else if (type.isConsultant()) {
            return Consultant.class;
        } else if (type.isProConsultant()) {
            return ProConsultant.class;
        }
        return Member.class;
    }


    private String getWorkerType(String findData) {
        for(WorkerType a: WorkerType.values()) {
            if(a.getTitle().equals(findData)) {
                return a.name();
            }
        }
        return null;
    }

    private String getEducationTypes(String findData) {
        for(Account.EducationCompletionLevel a: Account.EducationCompletionLevel.values()) {
            if(a.getTitle().equals(findData)) {
                return a.name();
            }
        }
        return null;
    }

    private String getConsultantLevel(String findData) {
        for(ConsultantLevel a: ConsultantLevel.values()) {
            if(a.getTitle().equals(findData)) {
                return a.name();
            }
        }
        return null;

    }
    private String getProfessionalField(String findData) {
        for(ProfessionalField a: ProfessionalField.values()) {
            if(a.getTitle().equals(findData)) {
                return a.name();
            }
        }
        return null;

    }
}
