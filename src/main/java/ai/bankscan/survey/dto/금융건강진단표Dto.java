package ai.bankscan.survey.dto;

import ai.bankscan.accounts.dto.MemberDto;
import ai.bankscan.survey.dto.금융건강진단표AnswersDto.Result;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import net.sf.json.JSONObject;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class 금융건강진단표Dto {

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "금융건강진단표Dto.Create")
  public static class Create {

    @ApiModelProperty(value = "답안 리스트",
        example = "[ "
            + "{         "
            + "'key': 0, "
            + "'question': '현재 금융관련 고민을 선택하여주세요. (3가지 선택 가능)', "
            + "'answers' : [ "
            + "{'key': 0, 'answer': '돈 관리 어려움', 'result': '지출', 'result2': null }, "
            + "{'key': 3, 'answer': '신용카드 사용관리', 'result': '지출', 'result2': '신용' }, "
            + "{'key': 8, 'answer': '신용도 하락', 'result': '부채', 'result2': '신용' }"
            + "] "
            + "}, "
            + "{ "
            + "'key': 1, "
            + "'question': '현재 생활에 닥친 어려움을 선택해주세요 (제한 없음)', "
            + "'answers' : [ "
            + "{'key': 8, 'answer': '하우스 푸어', 'result': '부채', 'result2': null }, "
            + "{'key': 9, 'answer': '장기 연체', 'result': '지출', 'result2': null }, "
            + "{'key': 10, 'answer': '고리대출', 'result': '부채', 'result2': null }"
            + "] "
            + "}, "
            + "{ "
            + "'key': 2, "
            + "'question': '현재 필요한 금융 활동을 선택하여 주세요. (3가지 선택 가능)', "
            + "'answers' : [ "
            + "{'key': 0, 'answer': '부채탈출', 'result': '부채', 'result2': null }, "
            + "{'key': 1, 'answer': '금융비용절감', 'result': '지출', 'result2': null }, "
            + "{'key': 2, 'answer': '신용등급관리', 'result': '부채', 'result2': '신용' }"
            + "] "
            + "}"
            + "]",
        position = 1)
    private List<금융건강진단표AnswersDto.Response> answerList;

    @ApiModelProperty(value = "결과", example = "소비지출형", position = 2)
    private String result;

    @ApiModelProperty(value = "첫번쨰 타입", example = "지출", position = 3)
    private Result firstType;

    @ApiModelProperty(value = "두번쨰 타입", example = "소비", position = 4)
    private Result lastType;

    @ApiModelProperty(value = "user", example = "user", position = 5)
    private MemberDto.Response member;

    @ApiModelProperty(value = "등급", example = "2등급", position = 6)
    private String 현명한금융거래;

    @ApiModelProperty(value = "등급", example = "2등급", position = 7)
    private String 행복한노후준비;

    @ApiModelProperty(value = "등급", example = "2등급", position = 8)
    private String 철저한신용관리;

    @ApiModelProperty(value = "등급", example = "2등급", position = 9)
    private String 내살집마련;

    @ApiModelProperty(value = "등급", example = "2등급", position = 10)
    private String 내복지찾기;

    @ApiModelProperty(value = "건강도 종합 ", example = "{}", position = 11)
    private String financeResult;


  }


  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ApiModel(value = "금융건강진단표Dto.Response")
  public static class Response {

    @ApiModelProperty(value = "결과", example = "소비지출형", position = 2)
    private Long id;

    @ApiModelProperty(value = "답안 리스트", example = "[]", position = 1)
    private List<금융건강진단표AnswersDto.Response> answerList;

    @ApiModelProperty(value = "결과", example = "소비지출형", position = 2)
    private String result;

    @ApiModelProperty(value = "첫번쨰 타입", example = "지출", position = 3)
    private Result firstType;

    @ApiModelProperty(value = "두번쨰 타입", example = "소비", position = 4)
    private Result lastType;

    @ApiModelProperty(value = "생성일시", position = 6)
    private LocalDateTime createdAt;

    private JSONObject financeResult;

    @ApiModelProperty(value = "건강도 종합 ", example = "{}", position = 11)
    private JSONObject financeResult2;
  }

}
