package ai.bankscan.accounts.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface AccountEvaluationRepository extends JpaRepository<AccountEvaluation, Long>,
        JpaSpecificationExecutor<AccountEvaluation> {
}
