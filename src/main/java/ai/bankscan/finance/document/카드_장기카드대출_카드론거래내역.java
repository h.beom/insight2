package ai.bankscan.finance.document;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Table(indexes = {@Index(columnList = "card"), @Index(columnList = "거래번호"),
    @Index(columnList = "거래회차")})
public class 카드_장기카드대출_카드론거래내역 extends AuditableJoinEntity<Account> {

  private String card;

  private String 거래번호;
  private String 거래회차;
  private String 거래일자;
  private String 거래금액;
  private String 대출잔액;
  private String 대출금리;
  private String 이자계산시작일;
  private String 이자계산종료일;
  private String 이자계산기간;
  private Long 거래원금;
  private Long 이자금액;
  private Long 이자상세1;
  private Long 이자상세2;
  private Long 이자상세3;
  private String 최종이수일자;


}
