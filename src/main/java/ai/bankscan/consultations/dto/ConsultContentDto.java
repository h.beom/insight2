package ai.bankscan.consultations.dto;

import ai.bankscan.accounts.domain.Account.ProfessionalField;
import ai.bankscan.accounts.domain.Account.ProfessionalField2;
import ai.bankscan.accounts.dto.AccountDto;
import ai.bankscan.consultations.domain.ConsultContent.ConsultType;
import ai.bankscan.consultations.domain.consulting.Consultation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import java.time.LocalTime;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConsultContentDto {

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "ConsultContentDto.Create")
  public static class Create {

    @NotNull
    @ApiModelProperty(value = "상담내용", example = "홍길동님 상담내용은..", position = 1, required = true)
    private String content;

    @NotNull
    @ApiModelProperty(value = "상담 id", example = "1", position = 2, required = true)
    private Long consultationId;

    @NotNull
    @ApiModelProperty(value = "상담사 id", example = "1", position = 3, required = true)
    private Long consultantId;

    @NotNull
    @ApiModelProperty(value = "상담 카테고리1", example = "ProfessionalField", position = 4, required = true)
    private ProfessionalField category;

    @NotNull
    @ApiModelProperty(value = "상담 카테고리2", example = "ProfessionalField2", position = 5, required = true)
    private ProfessionalField2 subCategory;

    @NotNull
    @ApiModelProperty(value = "상담 유형", example = "ConsultType", position = 6, required = true)
    private ConsultType consultType;

    @NotNull
    @ApiModelProperty(value = "기본 상담, 전문 상담", example = "BASIC_CONSULTATION", position = 7, required = true)
    private Consultation.Type type;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "ConsultContentDto.Response")
  public static class Response {

    @ApiModelProperty(value = "상담내역 id", example = "1", position = 1, required = true)
    private Long id;

    @ApiModelProperty(value = "상담사", example = "consultant", position = 3, required = true)
    private AccountDto.Response consultant;

    @ApiModelProperty(value = "상담 내역", example = "홍길동님은...", position = 4, required = true)
    private String content;

    @ApiModelProperty(value = "상담 일자", example = "2020-01-01", position = 5, required = true)
    private LocalDate consultDate;

    @ApiModelProperty(value = "상담 등록 시각", example = "12:00:33", position = 6, required = true)
    private LocalTime consultTime;

    @ApiModelProperty(value = "상담 카테고리", example = "ProfessionalField", position = 7, required = true)
    private ProfessionalField category;

    @ApiModelProperty(value = "상담 카테고리 상세", example = "ProfessionalField2", position = 8, required = true)
    private ProfessionalField2 subCategory;

    @ApiModelProperty(value = "상담 유형", example = "ConsultType", position = 9, required = true)
    private ConsultType consultType;
  }

}
