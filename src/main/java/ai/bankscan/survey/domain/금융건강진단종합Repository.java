package ai.bankscan.survey.domain;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface 금융건강진단종합Repository extends JpaRepository<금융건강진단종합, Long>,
    JpaSpecificationExecutor<금융건강진단종합> {
  List<금융건강진단종합> findAllBy금융건강진단표(금융건강진단표 금융건강진단표);


}
