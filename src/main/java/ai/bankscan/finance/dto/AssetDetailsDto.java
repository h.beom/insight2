package ai.bankscan.finance.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AssetDetailsDto {

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "AssetsDetailsDto.Response")
  public static class Response {

    @ApiModelProperty(value = "생성일자", example = "2020-01-01 22:33:22", position = 1)
    private String createdBy;

    @ApiModelProperty(value = "소득관리", example = " ", position = 2)
    private String 소득관리;

    @ApiModelProperty(value = "소비관리", example = " ", position = 3)
    private String 소비관리;

    @ApiModelProperty(value = "부채관리", example = " ", position = 4)
    private String 부채관리;

    @ApiModelProperty(value = "저축관리", example = " ", position = 5)
    private String 저축관리;

    @ApiModelProperty(value = "투자관리", example = " ", position = 6)
    private String 투자관리;

    @ApiModelProperty(value = "보험관리", example = " ", position = 7)
    private String 보험관리;

    @ApiModelProperty(value = "자산관리", example = " ", position = 8)
    private String 자산관리;

    @ApiModelProperty(value = "금융거래관리", example = " ", position = 9)
    private String 금융거래관리;

    @ApiModelProperty(value = "노후관리", example = " ", position = 10)
    private String 노후관리;

    @ApiModelProperty(value = "신용관리", example = " ", position = 11)
    private String 신용관리;

    @ApiModelProperty(value = "주택관리", example = " ", position = 12)
    private String 주택관리;

    @ApiModelProperty(value = "복지관리", example = " ", position = 13)
    private String 복지관리;

    @ApiModelProperty(value = "현금흐름표", example = " ", position = 14)
    private String 현금흐름표;

  }

}
