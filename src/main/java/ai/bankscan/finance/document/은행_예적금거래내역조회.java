package ai.bankscan.finance.document;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Table(indexes = {@Index(columnList = "bank"), @Index(columnList = "계좌번호"),
    @Index(columnList = "거래회차")})
public class 은행_예적금거래내역조회 extends AuditableJoinEntity<Account> {

  private String bank;
  private String 계좌번호;
  private String 거래일자;
  private String 거래시각;
  private String 거래회차;
  private String 거래월분;
  private String 입금액;
  private String 출금액;
  private String 잔액;
  private String 적요;
  private String 취급점;

}
