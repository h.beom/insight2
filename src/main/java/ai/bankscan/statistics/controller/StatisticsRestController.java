package ai.bankscan.statistics.controller;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.Account.Type;
import ai.bankscan.common.infra.annotations.ApiPageable;
import ai.bankscan.common.infra.security.common.CurrentUser;
import ai.bankscan.common.infra.web.excel.ExcelXlsView;
import ai.bankscan.statistics.dto.StatisticsDto;
import ai.bankscan.statistics.service.StatisticsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Api(tags = {"통계 관리 API"})
@RequiredArgsConstructor
@RestController
@RequestMapping(path = StatisticsRestController.REQUEST_BASE_PATH, produces = MediaTypes.HAL_JSON_VALUE)
public class StatisticsRestController {

  static final String REQUEST_BASE_PATH = "/api/statistics";

  private final StatisticsService statisticsService;
  private final StatisticsResourceAssembler statisticsResourceAssembler;

  @Secured({"ROLE_ADMIN", "ROLE_CENTER_MANAGER"})
  @ApiPageable
  @ApiOperation(value = "통계 관리 목록", notes = "통계 관리 목록")
  @GetMapping
  public Resources<Resource<StatisticsDto.Response>> getStatisticsFromOrganization(
      StatisticsDto.Search search,
      @CurrentUser Account currentUser,
      @PageableDefault(page = 0, size = 10) Pageable pageable,
      PagedResourcesAssembler<StatisticsDto.Response> pagedResourcesAssembler
  ) {

    Page<StatisticsDto.Response> page = statisticsService.getStatisticsFromConsultationCount(pageable, search);

    return pagedResourcesAssembler.toResource(page, statisticsResourceAssembler);
  }

  @Secured({"ROLE_ADMIN", "ROLE_CENTER_MANAGER"})
  @ApiPageable
  @ApiOperation(value = "통계 관리 엑셀", notes = "통계 관리 엑")
  @GetMapping("/excel-download")
  public ModelAndView getStatisticsFromOrganizationForExcel(
          StatisticsDto.Search search,
          @CurrentUser Account currentUser
  ) {

    Map<String, Object> resource = statisticsService.getStatisticsFromConsultationCountToExcel(search);

    return new ModelAndView(new ExcelXlsView(), resource);
  }

  @Secured({
      "ROLE_ADMIN",
      "ROLE_CENTER_MANAGER",
      "ROLE_RELATIONSHIP_MANAGER",
      "ROLE_CONSULTANT",
      "ROLE_PRO_CONSULTANT"
  })
  @ApiPageable
  @ApiOperation(value = "웹페이지 메인 통계 화면", notes = "웹페이지  메인 통계 화면")
  @GetMapping("/main")
  public ResponseEntity<Map<String, Object>> getMainStatistics(@CurrentUser Account account) {
    Map<String , Object> dataList = null;

    if(account.isAdminRole()) {
      dataList = statisticsService.getMainAdmin(account);
    }
    if(account.isCenterManageRole()) {
      dataList = statisticsService.getMainCenterManager(account);
    }

    if(account.isRelationshipManagerRole()) {

    }

    if(account.isProConsultantRole()) {

    }

    return ResponseEntity.ok(dataList);
  }

  @Secured({
      "ROLE_ADMIN",
      "ROLE_CENTER_MANAGER",
      "ROLE_RELATIONSHIP_MANAGER"
  })
  @ApiOperation(value = "role 통계", notes = "role별 통계 ")
  @GetMapping("/user/{type}")
  public ResponseEntity<Map<String, Integer>> getConsultantOfCount(
      @PathVariable Type type,
      @CurrentUser Account account
  ) {
      Map<String, Integer> responseData = statisticsService.getConsultantOfCount(type, account);

      return ResponseEntity.ok(responseData);
  }


  @Secured({
      "ROLE_ADMIN",
      "ROLE_CENTER_MANAGER",
      "ROLE_RELATIONSHIP_MANAGER"
  })
  @ApiOperation(value = "상담 상태 별 통계", notes = "상담 상태 별 통계 ")
  @GetMapping("/consultation/{type}")
  public ResponseEntity<Map<String, Integer>> getConsultationOfCount(
      @PathVariable  Type type,
      @CurrentUser Account account
  ) {
    Map<String, Integer> responseData = statisticsService.getConsultationOfCount(type, account);

    return ResponseEntity.ok(responseData);

}



  @Secured({
      "ROLE_ADMIN",
      "ROLE_CENTER_MANAGER",
      "ROLE_RELATIONSHIP_MANAGER"
  })
  @ApiOperation(value = "상담 상태 별 통계", notes = "멤버 통계 ")
  @GetMapping("/member")
  public ResponseEntity<Map<String, Integer>> getMemberOfCount(
      @CurrentUser Account account
  ) {
    Map<String, Integer> responseData = statisticsService.getMemberAllCount(account.getType());

    return ResponseEntity.ok(responseData);
  }



  @Secured({
      "ROLE_ADMIN",
      "ROLE_CENTER_MANAGER",
      "ROLE_RELATIONSHIP_MANAGER"
  })
  @ApiOperation(value = "상담 상태 별 통계", notes = "기관 별 통계 ")
  @GetMapping("/organization/{type}")
  public ResponseEntity<Map<String, Integer>> getOrganizationOfCount(
      @PathVariable  Type type,
      @CurrentUser Account account
  ) {
    Map<String, Integer> responseData = statisticsService.getOrganizationOfCount(type, account);

    return ResponseEntity.ok(responseData);
  }
}
