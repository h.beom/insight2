package ai.bankscan.organizations.domain;

import ai.bankscan.organizations.domain.Organization.Type;
import java.util.Optional;

public interface OrganizationRepository extends OrganizationBaseRepository<Organization> {

  Optional<Organization> findByTypeAndCode(Type type, String code);


}
