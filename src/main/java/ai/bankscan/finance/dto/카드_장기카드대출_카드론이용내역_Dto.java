package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 카드_장기카드대출_카드론이용내역_Dto {

  private String card;

  private String 대출번호;
  private String 대출종류;
  private String 신규일자;
  private String 만기일자;
  private String 완납일자;
  private Long 대출금액;
  private Long 대출잔액;
  private String 대출금리;
  private String 대출개월;

}
