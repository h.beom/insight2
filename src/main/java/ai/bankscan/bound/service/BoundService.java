package ai.bankscan.bound.service;

import ai.bankscan.bound.domain.Bound;
import ai.bankscan.bound.domain.BoundRepository;
import ai.bankscan.bound.dto.BoundDto;
import ai.bankscan.bound.dto.BoundDto.Search;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service
public class BoundService {
  private final BoundRepository boundRepository;
  private final ModelMapper modelMapper;

  public Page<BoundDto.Response> getBoundList(Search search, Pageable pageable) {
    Page<Bound> boundPage = boundRepository.findAll(search.toSpecification(), pageable);

    List<BoundDto.Response> content = boundPage.getContent().stream()
        .map(this::toResponse)
        .collect(Collectors.toList());

  return new PageImpl<>(content, pageable, boundPage.getTotalElements());
  }

  private BoundDto.Response toResponse(Bound bound) {
    return modelMapper.map(bound, BoundDto.Response.class);
  }

}
