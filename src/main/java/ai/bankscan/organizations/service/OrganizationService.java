package ai.bankscan.organizations.service;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.AccountRepository;
import ai.bankscan.common.infra.web.common.HttpStatusMessageException;
import ai.bankscan.organizations.domain.Organization;
import ai.bankscan.organizations.domain.OrganizationBaseRepository;
import ai.bankscan.organizations.domain.OrganizationRepository;
import ai.bankscan.organizations.dto.OrganizationDto;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

@Slf4j
@Transactional(readOnly = true)
public abstract class OrganizationService<T extends Organization, R extends OrganizationDto.Response> {

  private final OrganizationRepository organizationRepository;
  private final OrganizationBaseRepository organizationBaseRepository;
  private final AccountRepository accountRepository;
  private final ModelMapper modelMapper;
  private final Class<T> entityType;
  private final Class<R> responseType;

  public OrganizationService(
      OrganizationRepository organizationRepository,
      OrganizationBaseRepository organizationBaseRepository,
      AccountRepository accountRepository, ModelMapper modelMapper,
      Class<T> entityType,
      Class<R> responseType
  ) {
    this.organizationRepository = organizationRepository;
    this.organizationBaseRepository = organizationBaseRepository;
    this.accountRepository = accountRepository;
    this.modelMapper = modelMapper;
    this.entityType = entityType;
    this.responseType = responseType;
  }

  @Transactional
  public R createOrganization(
      OrganizationDto.Create dto
  ) {
    T organization = modelMapper.map(dto, entityType);
    Optional<Organization> optionalOrganization = organizationRepository
        .findByTypeAndCode(organization.getType(), organization.getCode());

    if (optionalOrganization.isPresent()) {
      throw new HttpStatusMessageException(HttpStatus.BAD_REQUEST, "organization.code.duplicated",
          organization.getType().getTitle());
    }

    organization.create();

    return toResponse(
        organizationRepository.save(organization),
        o -> modelMapper.map(o, responseType)
    );
  }

  public Page<R> getOrganizations(
      OrganizationDto.Search search,
      Pageable pageable
  ) {
    Page<T> page = organizationBaseRepository
        .findAll(search.toSpecification(entityType), pageable);
    List<R> content = page.getContent().stream()
        .map(organization -> this.toResponse(organization, o -> modelMapper.map(o, responseType)))
        .collect(Collectors.toList());
    return new PageImpl<>(content, pageable, page.getTotalElements());
  }

  public R getOrganization(
      final Long id
  ) {
    Organization organization = organizationRepository.findById(id)
        .orElseThrow(IllegalArgumentException::new);
    return toResponse(organization, o -> modelMapper.map(o, responseType));
  }

  @Transactional
  public R updateOrganization(
      final Long id,
      OrganizationDto.Update dto,
      final String username
  ) {
    Organization existingOrganization = organizationRepository.findById(id)
        .orElseThrow(IllegalArgumentException::new);

    modelMapper.map(dto, existingOrganization);

    existingOrganization.update();

    return toResponse(organizationRepository.save(existingOrganization),
        o -> modelMapper.map(o, responseType));
  }

  @Transactional
  public void deleteOrganization(final Long id) {
    Organization existingOrganization = organizationRepository.findById(id)
        .orElseThrow(IllegalArgumentException::new);

    List<Account> accounts = accountRepository.findAllByOrganization(existingOrganization);
    if (!CollectionUtils.isEmpty(accounts)) {
      throw new HttpStatusMessageException(HttpStatus.PRECONDITION_FAILED,
          "organization.delete.failed.hasAccounts", existingOrganization.getType().getTitle());
    }
  }

  private R toResponse(
      Organization organization,
      Function<Organization, R> responseMapper
  ) {
    return responseMapper.apply(organization);
  }

}
