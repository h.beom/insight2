package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 카드_장기카드대출_카드론거래내역_Dto {

  private String card;
  private String 거래번호;
  private String 거래회차;
  private String 거래일자;
  private String 거래금액;
  private String 대출잔액;
  private String 대출금리;
  private String 이자계산시작일;
  private String 이자계산종료일;
  private String 이자계산기간;
  private Long 거래원금;
  private Long 이자금액;
  private Long 이자상세1;
  private Long 이자상세2;
  private Long 이자상세3;
  private String 최종이수일자;

}
