package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 금결원_계좌정보조회_Dto {

  private String bank;
  private String 은행명;
  private String 계좌번호;
  private String 지점명;
  private String 상품명;
  private String 개설일;
  private String 최종입출금일;
  private Long 잔고;
  private String 만기일;
  private String 회차;
  private String 부기명;
  private String 비고;
  private boolean 활동성;

  public void set은행명(String 은행명) {
    this.은행명 = 은행명;

    if (은행명.contains("기업") || 은행명.contains("ibk")) {
      bank = "ibk";
    } else if (은행명.contains("산업")) {
      bank = "kdb";
    } else if (은행명.contains("국민")) {
      bank = "kbstar";
    } else if (은행명.contains("하나") || 은행명.contains("KEB") || 은행명.contains("외환")) {
      bank = "hanabank";
    } else if (은행명.contains("수협")) {
      bank = "suhyupbank";
    } else if (은행명.contains("농협") || 은행명.contains("농·축협")) {
      bank = "nonghyup";
    } else if (은행명.contains("제주")) {
      bank = "jejubank";
    } else if (은행명.contains("전북")) {
      bank = "jbbank";
    } else if (은행명.contains("경남")) {
      bank = "knbank";
    } else if (은행명.contains("신한")) {
      bank = "shinhan";
    } else if (은행명.contains("우리")) {
      bank = "wooribank";
    } else if (은행명.contains("SC") || 은행명.contains("스탠다드")) {
      bank = "standardchartered";
    } else if (은행명.contains("씨티")) {
      bank = "citibank";
    } else if (은행명.contains("대구")) {
      bank = "dgb";
    } else if (은행명.contains("부산")) {
      bank = "busanbank";
    } else if (은행명.contains("광주")) {
      bank = "kjbank";
    } else if (은행명.contains("새마을")) {
      bank = "kfcc";
    } else if (은행명.contains("신협")) {
      bank = "cu";
    } else if (은행명.contains("우체국") || 은행명.contains("우정")) {
      bank = "epostbank";
    } else if (은행명.contains("K뱅크") || 은행명.contains("kbank") || 은행명.contains("케이뱅크")) {
      bank = "kbank";
    } else if (은행명.contains("카카오뱅크")) {
      bank = "@kakaobank";
    }
  }

}
