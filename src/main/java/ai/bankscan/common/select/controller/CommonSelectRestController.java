package ai.bankscan.common.select.controller;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.Account.ConsultantLevel;
import ai.bankscan.accounts.domain.Account.EducationCompletionLevel;
import ai.bankscan.accounts.domain.Account.LeaveReasonType;
import ai.bankscan.accounts.domain.Account.LeaveType;
import ai.bankscan.accounts.domain.Account.ProfessionalField;
import ai.bankscan.accounts.domain.Account.ProfessionalField2;
import ai.bankscan.accounts.domain.Account.WorkerType;
import ai.bankscan.accounts.domain.member.Member.MemberLevel;
import ai.bankscan.common.infra.security.common.CurrentUser;
import ai.bankscan.common.select.service.CommonSelectService;
import ai.bankscan.consultations.domain.consulting.Consultation.Status;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = {"조회성 select box API"})
@RequiredArgsConstructor
@RestController
@RequestMapping(path = CommonSelectRestController.REQUEST_BASE_PATH, produces = MediaTypes.HAL_JSON_VALUE)
public class CommonSelectRestController {


  static final String REQUEST_BASE_PATH = "/api/account-select";

  private final CommonSelectService commonSelectService;

  @ApiOperation(value = "select-box-list-common", notes = "공통 select box "
      + "type: leaveType(탈퇴타입:관리자 or 회원), professionalField(상담 카테고리1), educationCompletion(교육수료등급), "
      + "leaveReasonType(탈퇴사유), consultantLevel(상담원레벨), workerType(근무형태), memberLevel(사용자 레벨), "
      + "consultationStatus(상담 진행 상태")
  @GetMapping("/type-list/{type}")
  public ResponseEntity<Map<? extends Object, String >> typeList(@PathVariable String type) {

    switch (type) {
      case "leaveType" :
        Map<LeaveType, String> leaveTypes = this.commonSelectService.getLeaveTypes();
        return ResponseEntity.ok(leaveTypes);
      case "professionalField" :
        Map<ProfessionalField, String> professionalTypes = this.commonSelectService.getProfessionalTypes();
        return ResponseEntity.ok(professionalTypes);
      case "educationCompletion" :
        Map<EducationCompletionLevel, String> educationCompletionLevel = this.commonSelectService.getEducationCompletionLevel();
        return ResponseEntity.ok(educationCompletionLevel);
      case "leaveReasonType" :
        Map<LeaveReasonType, String> leaveReasonType = this.commonSelectService.getLeaveReasonType();
        return ResponseEntity.ok(leaveReasonType);
      case "consultantLevel" :
        Map<ConsultantLevel, String> consultantLevel = this.commonSelectService.getConsultantLevel();
        return ResponseEntity.ok(consultantLevel);
      case "workerType" :
        Map<WorkerType, String> workerType = this.commonSelectService.getWorkerType();
        return ResponseEntity.ok(workerType);
      case "memberLevel" :
        Map<MemberLevel, String> memberLevel = this.commonSelectService.getMemberLevel();
        return ResponseEntity.ok(memberLevel);
      case "consultationStatus" :
        Map<Status, String> consultationStatus = this.commonSelectService.getConsultationStatus();
        return ResponseEntity.ok(consultationStatus);
      default :
        return null;
    }

  }


  @ApiOperation(value = "현재 상담 진행 update 가능 list ", notes = "각 상담상태와 행위자에 따른 적용 가능 status 값")
  @GetMapping("/status-list/{status}")
  public ResponseEntity<List<Status>> statusList(
      @PathVariable Status status,
      @CurrentUser Account user) {
    return ResponseEntity.ok(Status.getStatuses(user.getAuthorities(), status));
  }


  @ApiOperation(value = "전문분야 상세 ", notes = "각 전문 분야 sub 카테고리 ProfessionalField key code sub type.")
  @GetMapping("/type-list/professional-field-sub/{key}")
  public ResponseEntity<Map<?, String >> typeListSub(@PathVariable ProfessionalField key) {
        Map<ProfessionalField2, String> professionalField2 = commonSelectService.getProfessionalTypesSub(key);
        return ResponseEntity.ok(professionalField2);
  }

}
