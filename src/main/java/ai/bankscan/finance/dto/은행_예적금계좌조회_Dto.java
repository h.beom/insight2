package ai.bankscan.finance.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class 은행_예적금계좌조회_Dto {

  @Data
  public static class Create {

    private String bank;

    private String 계좌번호;
    private String 계좌번호확장;
    private String 만기일자;
    private String 신규일자;
    private String 예금종류;
    private String 이자율;
    private String 현재잔액;
    private String 월부금;
  }

  @Data
  @NoArgsConstructor
  public static class GroupBy {

    private String bank;
    private String 계좌번호;
    private String date;
    public GroupBy(String bank, String 계좌번호, String date) {
      this.bank = bank;
      this.계좌번호 = 계좌번호;
      this.date = date;
    }
  }
}
