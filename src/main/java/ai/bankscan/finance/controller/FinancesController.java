package ai.bankscan.finance.controller;

import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.common.infra.security.common.CurrentUser;
import ai.bankscan.finance.domain.AssetTable;
import ai.bankscan.finance.domain.CashFlow;
import ai.bankscan.finance.domain.House;
import ai.bankscan.finance.dto.AssetTableDto;
import ai.bankscan.finance.dto.HouseDto;
import ai.bankscan.finance.service.CooconService;
import ai.bankscan.finance.service.FinanceService;
import ai.bankscan.survey.domain.금융건강진단표;
import ai.bankscan.survey.dto.재무달성계획Dto;
import ai.bankscan.survey.service.금융건강진단표Service;
import ai.bankscan.survey.service.재무달성계획Service;
import io.swagger.annotations.Api;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import net.sf.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Secured("ROLE_MEMBER")
@PreAuthorize("isAuthenticated()")
@Api(tags = {"쿠콘 데이터 입력 API"})
@RestController
@RequiredArgsConstructor
@RequestMapping(path = FinancesController.REQUEST_BASE_PATH, produces = MediaTypes.HAL_JSON_VALUE)
public class FinancesController {

  static final String REQUEST_BASE_PATH = "/api/finances";

  private final CooconService cooconService;
  private final FinanceService financeService;
  private final 재무달성계획Service _재무달성계획Service;
  private final 금융건강진단표Service _금융건강진단표Service;
  private final ModelMapper mapper;

  @GetMapping("")
  public ResponseEntity get(@CurrentUser Member member) {
    JSONObject finance = new JSONObject();
    금융건강진단표 data1 = _금융건강진단표Service.getLast(member);
    재무달성계획Dto.Response response = _재무달성계획Service.getLast(member);
    finance.put("금융건강진단", data1 != null);
    finance.put("재무달성표", response != null);
    finance.put("현금흐름표", financeService.hasCashflow(member.getId()));
    finance.put("자산현황표", financeService.hasAssetTable(member.getId()));
    return ResponseEntity.ok(finance);
  }

  @GetMapping("/cashflow")
  public ResponseEntity<JSONObject> cashflow(@CurrentUser Member member) {
    CashFlow cashFlow = financeService.findLastCashFlow(member.getId());
    if (cashFlow == null) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new JSONObject());
    } else {
      JSONObject cashFlowResult = new JSONObject();
      cashFlowResult
          .put("createdAt", cashFlow.getCreatedAt().format(DateTimeFormatter.ISO_DATE_TIME));
      cashFlowResult.put("data", JSONObject.fromObject(cashFlow.getJson()));
      cashFlowResult.put("year", cashFlow.getYear());
      return ResponseEntity.ok(cashFlowResult);
    }
  }

  @GetMapping("/assetTable")
  public ResponseEntity assetTable(@CurrentUser Member member) {
    AssetTableDto.Output assetTable = financeService.findLastAssetTable(member.getId());
    if (assetTable == null) {
      return ResponseEntity.status(404).build();
    } else {
      return ResponseEntity.ok(assetTable);
    }
  }

  @GetMapping("/pension")
  public ResponseEntity yearPension(@CurrentUser Member member) {
    Long yearMoney = financeService.getPension(member.getId());
    return ResponseEntity.ok(yearMoney);
  }

  @GetMapping("/house")
  public ResponseEntity houseGet(@CurrentUser Member member) {
    List<House> houses = cooconService.getHouses(member);
    List<HouseDto> dtos = houses.stream().map(v -> mapper.map(v, HouseDto.class))
        .collect(Collectors.toList());
    return ResponseEntity.ok(dtos);
  }

  @PostMapping("/house")
  public ResponseEntity housePost(@CurrentUser Member member, @RequestBody List<HouseDto> items) {
    cooconService.mergeHouse(items, member);
    return ResponseEntity.ok().build();
  }

  @GetMapping("/range")
  public ResponseEntity range(@CurrentUser Member member) {
    return ResponseEntity.ok(cooconService.getDateRange(member));
  }

  @PostMapping
  public ResponseEntity post(@CurrentUser Member member, @RequestBody JSONObject o)
      throws Exception {
    cooconService.mergeObject(o, member);
    return ResponseEntity.ok().build();
  }

  @PostMapping("/calc")
  public ResponseEntity calc(@CurrentUser Member member) {
    financeService.calcData(member.getId());
    financeService.calcReport(member.getId());
    return ResponseEntity.ok().build();
  }

  @GetMapping("/calc_user/{id}")
  public ResponseEntity calc(@PathVariable Long id) {


    financeService.calcData(id);
    financeService.calcReport(id);
    return ResponseEntity.ok().build();
//    AssetTable table = financeService.calcData(id);
    // CashFlow flow = financeService.createCashFlow(id, "2019");
//    JSONObject o = new JSONObject();
//    o.put("a", JSONObject.fromObject(table.getAssetsJson()));
//    o.put("l", JSONObject.fromObject(table.getLiabilitiesJson()));
//    return ResponseEntity.ok(o.toString());

  }


}
