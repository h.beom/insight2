package ai.bankscan.consultations.service;

import ai.bankscan.common.infra.web.excel.constants.Excel;
import ai.bankscan.consultations.domain.consulting.Consultation;
import ai.bankscan.consultations.domain.consulting.ConsultationRepository;
import ai.bankscan.consultations.dto.ConsultationDto;
import com.google.common.collect.Maps;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service
public class ConsultUtilService {

  private final ConsultationRepository consultationRepository;


  public Map<String, Object> getConsultationListToExcel(ConsultationDto.Search search) {
    List<Consultation> basicConsultationInformationList = consultationRepository
        .findAll(search.toSpecification(),
            Sort.by(Direction.DESC, "createAt"));

//    List<List<String>> body = consultationList.stream()
//        .map(consultation -> {
//          return Arrays.asList(
//
//          )
//
//        }).collect(Collectors.toList());

    Map<String, Object> data = Maps.newHashMap();
    data.put(Excel.FILE_NAME.getName(), "상담사정보");
    data.put(Excel.HEAD.getName(),
        Arrays.asList(
            "센터ID",
            "날짜",
            "센터명",
            "사업자등록번호",
            "센터장이름",
            "임대료",
            "관리비",
            "전기세",
            "수도세",
            "도시가스비",
            "유선전화비",
            "인터넷비",
            "사무용품비",
            "기타",
            "총합계"
        ));
    data.put(Excel.BODY.getName(), basicConsultationInformationList);
    return data;
  }
}
