package ai.bankscan.configs.security;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.AccountRepository;
import ai.bankscan.accounts.service.AccountAdapter;
import ai.bankscan.accounts.service.AccountDeviceService;
import ai.bankscan.common.infra.security.properties.WebSecurityProperties;
import ai.bankscan.common.infra.security.support.RestAuthenticationEntryPoint;
import ai.bankscan.common.infra.security.support.RestAuthenticationFailureHandler;
import ai.bankscan.common.infra.security.support.RestAuthenticationSuccessHandler;
import ai.bankscan.common.infra.security.support.RestLogoutSuccessHandler;
import ai.bankscan.configs.security.device.DeviceAuthFilter;
import ai.bankscan.configs.security.device.DeviceAuthenticationProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AndRequestMatcher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The type Web security config.
 */
@RequiredArgsConstructor
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final WebSecurityProperties webSecurityProperties;
    private final UserDetailsService userDetailsService;
    private final AccountDeviceService accountDeviceService;
    private final DeviceAuthenticationProvider deviceAuthenticationProvider;

    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    @ConditionalOnMissingBean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Bean
    @ConditionalOnMissingBean
    public RestAuthenticationEntryPoint restAuthenticationEntryPoint() {
        return new RestAuthenticationEntryPoint();
    }

    @Bean
    @ConditionalOnMissingBean
    public RestAuthenticationFailureHandler restAuthenticationFailureHandler() {
        return new RestAuthenticationFailureHandler();
    }

    @Bean
    @ConditionalOnMissingBean
    public RestAuthenticationSuccessHandler restAuthenticationSuccessHandler() {
        return new RestAuthenticationSuccessHandler();
    }

    @Bean
    @ConditionalOnMissingBean
    public RestLogoutSuccessHandler restLogoutSuccessHandler() {
        return new RestLogoutSuccessHandler();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .authenticationProvider(deviceAuthenticationProvider)
                .userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Override
    public void configure(WebSecurity web) {
        web
                .ignoring()
                .mvcMatchers(
                        "/docs/index.html",
                        "/raonnx/**",
                        "/swagger-ui.html",
                        "/swagger-resources/**",
                        "/v2/api-docs",
                        "/fonts/**",
                        "/pfm",
                        "/intro",
                        "/bank/**",
                        "/css/**",
                        "/js/**",
                        "/api/files/**",
                        "/pdf/css/**",
                        "/pdf/fonts/**",
                        "/pdf/img/**",
                        "/csrf",
                        "/error",
                        "/privacy",
                        "/tos"
                )
                .requestMatchers(PathRequest.toStaticResources().atCommonLocations());
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {

        http.sessionManagement().maximumSessions(1).expiredUrl("/auth/expired");

        http
                .addFilterBefore(createCustomFilter(), UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers("/api/auth/**", "/oauth2/callback", "/h2-console/*", "/certification/**", "/pdf/**", "/property-prices/**", "/files/**").permitAll()
                .antMatchers(HttpMethod.POST, "/property-prices/**").permitAll()
                .antMatchers(HttpMethod.GET, "/actuator/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/notices/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/centers/**").permitAll()
                .antMatchers(HttpMethod.GET, "/").permitAll()
                .antMatchers(HttpMethod.POST, "/api/accounts/find").permitAll()
                .antMatchers(HttpMethod.POST, "/api/members").permitAll()
                .antMatchers(HttpMethod.POST, "/api/members/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .logoutUrl("/logout")
                .and()
                .csrf()
                .disable();

        if (webSecurityProperties.getRest().isEnabled()) {
            http
                    .exceptionHandling()
                    .authenticationEntryPoint(restAuthenticationEntryPoint())
                    .and()
                    .formLogin()
                    .successHandler(restAuthenticationSuccessHandler())
                    .failureHandler(restAuthenticationFailureHandler())
                    .and()
                    .logout()
                    .logoutSuccessHandler(restLogoutSuccessHandler());
        }
    }


    protected AbstractAuthenticationProcessingFilter createCustomFilter() throws Exception {
        DeviceAuthFilter filter = new DeviceAuthFilter(new NegatedRequestMatcher(
                new AndRequestMatcher(
                        new AntPathRequestMatcher("/oauth/**")
                )
        ));
        filter.setAccountDeviceService(accountDeviceService);
        filter.setAuthenticationManager(authenticationManagerBean());
        return filter;
    }

    @RequiredArgsConstructor
    @Transactional(readOnly = true)
    @Service
    public static class UserDetailsServiceImpl implements UserDetailsService {

        private final AccountRepository accountRepository;

        @Override
        public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
            Account account = accountRepository.findByUsername(username)
                    .orElseThrow(() -> new UsernameNotFoundException(username));
            return new AccountAdapter(account);
        }

    }
}
