package ai.bankscan.common.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Gender {
  MALE("남성"), FEMALE("여성");

  private final String title;

  @Override
  public String toString() {
    return this.getTitle();
  }
}
