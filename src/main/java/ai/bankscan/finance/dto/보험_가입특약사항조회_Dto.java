package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 보험_가입특약사항조회_Dto {

  private String insure;

  private Integer idx;
  private String 증권번호;

  private String 주보험및특약;
  private String 가입금액;
  private String 보험료;
  private String 보험기간;
  private String 납입기간;

}
