package ai.bankscan.messages.controller;

import ai.bankscan.common.infra.annotations.ApiPageable;
import ai.bankscan.messages.dto.NotificationDto;
import ai.bankscan.messages.dto.NotificationDto.Response;
import ai.bankscan.messages.service.NotificationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.net.URI;
import java.net.URISyntaxException;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Api(tags = {"PUSH 관리 API"})
@RequiredArgsConstructor
@RestController
@RequestMapping(path = NotificationRestController.REQUEST_BASE_PATH, produces = MediaTypes.HAL_JSON_VALUE)
public class NotificationRestController {

  static final String REQUEST_BASE_PATH = "/api/notifications";

  private final NotificationService notificationService;
  private final NotificationResourceAssembler notificationResourceAssembler;

  @ApiOperation(value = "PUSH 발송")
  @Secured("ROLE_ADMIN")
  @PostMapping
  ResponseEntity<Resource<Response>> createNotification(
      @RequestBody @Valid NotificationDto.Create dto
  ) throws URISyntaxException {
    Response savedPost = notificationService.createNotification(dto);
    Resource<Response> resource = notificationResourceAssembler.toResource(savedPost);

    return ResponseEntity
        .created(new URI(resource.getLink(Link.REL_SELF).getHref()))
        .body(resource);
  }

  @ApiPageable
  @ApiOperation(value = "PUSH 발송 목록 조회")
  @GetMapping
  Resources<Resource<Response>> getNotifications(
      @Valid NotificationDto.Search search,
      @PageableDefault(sort = "createdAt", direction = Direction.DESC) Pageable pageable,
      PagedResourcesAssembler<Response> pagedResourcesAssembler
  ) {
    Page<Response> page = notificationService.getNotification(search, pageable);
    return pagedResourcesAssembler.toResource(page, notificationResourceAssembler);
  }

  @ApiOperation(value = "PUSH 발송 조회", notes = "TYPE: NOTICE(공지사항)")
  @GetMapping("/{id}")
  Resource<Response> getNotification(
      @ApiParam(required = true, example = "1") @PathVariable final Long id
  ) {
    return notificationResourceAssembler.toResource(notificationService.getNotification(id));
  }

}
