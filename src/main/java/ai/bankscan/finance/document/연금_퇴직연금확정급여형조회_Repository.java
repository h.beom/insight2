package ai.bankscan.finance.document;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface 연금_퇴직연금확정급여형조회_Repository extends JpaRepository<연금_퇴직연금확정급여형조회, Long>,
    JpaSpecificationExecutor<연금_퇴직연금확정급여형조회> {

}
