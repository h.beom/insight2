package ai.bankscan.finance.document;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface 카드_카드청구내역_Repository extends JpaRepository<카드_카드청구내역, Long>,
    JpaSpecificationExecutor<카드_카드청구내역> {

  @Query("SELECT MAX(month) FROM 카드_카드청구내역 WHERE createdBy.id = :createdById")
  String getMaxDate(@Param("createdById") Long createdById);
}
