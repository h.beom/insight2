package ai.bankscan.finance.document;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface 연금_예상연금액조회_Repository extends JpaRepository<연금_예상연금액조회, Long>,
    JpaSpecificationExecutor<연금_예상연금액조회> {

}
