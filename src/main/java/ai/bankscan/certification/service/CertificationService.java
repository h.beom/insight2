package ai.bankscan.certification.service;

import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.accounts.domain.member.MemberRepository;
import ai.bankscan.common.infra.exceptions.AccountNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Transactional
@Service
public class CertificationService {

  private final MemberRepository memberRepository;

  public Member userCiDiValidation(final String ci, final String di) {
    return memberRepository.findByCiAndDi(ci, di);
  }

  private Member userValidation(final String username) {
    return memberRepository.findByUsername(username)
        .orElseThrow(() -> new AccountNotFoundException(username));
  }


}
