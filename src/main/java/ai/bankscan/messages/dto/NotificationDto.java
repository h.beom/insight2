package ai.bankscan.messages.dto;

import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.common.infra.jpa.support.SearchDto;
import ai.bankscan.messages.domain.Notification;
import ai.bankscan.messages.domain.Notification.Status;
import ai.bankscan.messages.domain.NotificationMessage;
import ai.bankscan.messages.domain.NotificationMessage.Type;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.validation.constraints.NotEmpty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.validator.constraints.Length;
import org.springframework.util.ObjectUtils;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class NotificationDto {

  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  @ToString
  @ApiModel(value = "NotificationDto.Create")
  public static class Create {

    @Default
    @ApiModelProperty(value = "푸쉬 타입", example = "MANUAL", required = true, position = 1)
    private NotificationMessage.Type type = Type.MANUAL;

    @NotEmpty
    @ApiModelProperty(value = "제목", example = "푸쉬 제목", required = true, position = 2)
    private String title;

    @NotEmpty
    @Length(max = 280)
    @ApiModelProperty(value = "내용", example = "푸쉬 내용", required = true, position = 3)
    private String body;

    @Length(max = 500)
    @ApiModelProperty(value = "링크", example = "푸쉬 링크", position = 4)
    private String link;


  }

  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  @ToString
  @ApiModel(value = "NotificationDto.Update")
  public static class Update {

    @NotEmpty
    @ApiModelProperty(value = "제목", example = "푸쉬 제목", required = true, position = 1)
    private String title;

    @NotEmpty
    @Length(max = 280)
    @ApiModelProperty(value = "내용", example = "푸쉬 내용", required = true, position = 2)
    private String body;

    @NotEmpty
    @Length(max = 500)
    @ApiModelProperty(value = "링크", example = "푸쉬 링크", position = 3)
    private String link;

  }

  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  @ToString
  @ApiModel(value = "NotificationDto.Response")
  public static class Response {

    @ApiModelProperty(value = "ID", example = "1")
    private Long id;

    @ApiModelProperty(value = "제목", example = "푸쉬 제목", required = true, position = 1)
    private String title;

    @ApiModelProperty(value = "내용", example = "푸쉬 내용", required = true, position = 2)
    private String body;

    @ApiModelProperty(value = "링크", example = "푸쉬 링크", position = 3)
    private String link;

    @ApiModelProperty(value = "생성일시", position = 4)
    private LocalDateTime createdAt;

    @ApiModelProperty(value = "성공 수", position = 4)
    private int successCount;

    @ApiModelProperty(value = "실패 수", position = 4)
    private int failureCount;

    @ApiModelProperty(value = "상태", position = 4)
    private Status status;
  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString(callSuper = true)
  @ApiModel(value = "NotificationDto.Search")
  public static class Search extends SearchDto<Notification> {

    @ApiModelProperty(value = "발송결과", example = "FAILURE", position = 1)
    private Status status;

    @Override
    protected Restrictions generateRestrictions() {
      final Restrictions restrictions = new Restrictions();
      if (!ObjectUtils.isEmpty(this.status)) {
        restrictions.eq("status", this.status);
      }

      return restrictions;
    }

  }

}
