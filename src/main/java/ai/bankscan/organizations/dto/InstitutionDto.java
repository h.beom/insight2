package ai.bankscan.organizations.dto;

import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.organizations.domain.Institution;
import ai.bankscan.organizations.domain.Organization.Type;
import io.swagger.annotations.ApiModel;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class InstitutionDto {

  @Setter
  @Getter
  @NoArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "InstitutionDto.Create")
  public static class Create extends OrganizationDto.Create {


  }

  @Setter
  @Getter
  @NoArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "InstitutionDto.Update")
  public static class Update extends OrganizationDto.Update {


  }

  @Setter
  @Getter
  @NoArgsConstructor
  @SuperBuilder
  @ToString(callSuper = true)
  @ApiModel(value = "InstitutionDto.Response")
  public static class Response extends OrganizationDto.Response {


  }


  @Setter
  @Getter
  @NoArgsConstructor
  @SuperBuilder
  @ToString(callSuper = true)
  @ApiModel(value = "InstitutionDto.Simple")
  public static class Simple extends OrganizationDto.Simple {


  }

  @Getter
  @Setter
  @NoArgsConstructor
  @SuperBuilder
  @ToString(callSuper = true)
  @ApiModel(value = "InstitutionDto.Search")
  public static class Search extends OrganizationDto.Search<Institution> {

    @Override
    protected Restrictions generateRestrictions() {
      final Restrictions restrictions = super.generateRestrictions();
      restrictions.eq("type", Type.INSTITUTION);
      return restrictions;
    }

  }

}
