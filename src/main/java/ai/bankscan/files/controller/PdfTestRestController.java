package ai.bankscan.files.controller;

import ai.bankscan.finance.domain.CashFlow;
import ai.bankscan.finance.service.FinanceService;
import ai.bankscan.survey.dto.금융건강진단표Dto;
import ai.bankscan.survey.dto.재무달성계획Dto;
import ai.bankscan.survey.service.금융건강진단표Service;
import ai.bankscan.survey.service.재무달성계획Service;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import net.sf.json.JSONObject;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Api (tags = {"pdf 생성 api"})
@RequiredArgsConstructor
@RestController
@RequestMapping(path = PdfTestRestController.REQUEST_BASE_PATH, produces = MediaTypes.HAL_JSON_VALUE)
public class PdfTestRestController {
  static final String REQUEST_BASE_PATH = "/api/pdf";
  private final 금융건강진단표Service 금융건강진단표Service;
  private final FinanceService financeService;
  private final 재무달성계획Service 재무달성계획Service;

  @GetMapping("/create/{id}")
  public ResponseEntity<Map<String, Object>> createPDF(@PathVariable final long id) {
    금융건강진단표Dto.Response dto = 금융건강진단표Service.responseResult(id);
    CashFlow cashFlow = financeService.findLastCashFlow(id);
    Map<String, Object> data = new HashMap<>();
    JSONObject cashFlowResult = new JSONObject();
    if (cashFlow != null) {
      cashFlowResult
              .put("createdAt", cashFlow.getCreatedAt().format(DateTimeFormatter.ISO_DATE_TIME));
      cashFlowResult.put("data", JSONObject.fromObject(cashFlow.getJson()));
      cashFlowResult.put("year", cashFlow.getYear());
    }

    재무달성계획Dto.Response dto1 = 재무달성계획Service.getLatestOne(id);

    data.put("id", dto.getId());
    data.put("answerList", dto.getAnswerList());
    data.put("createdAt", dto.getCreatedAt());
    data.put("financeResult", dto.getFinanceResult2());
    data.put("firstType", dto.getFirstType());
    data.put("lastType", dto.getLastType());
    data.put("result", dto.getResult());
    data.put("allData", dto.getFinanceResult());
    data.put("cashFlow", cashFlowResult);
    data.put("lifeCycle", dto1);
    return ResponseEntity.ok(data);
  }

}
