package ai.bankscan.accounts.controller;

import static java.util.stream.Collectors.toMap;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.Account.ProfessionalField;
import ai.bankscan.accounts.dto.AccountDto;
import ai.bankscan.accounts.dto.AccountDto.Response;
import ai.bankscan.accounts.service.AccountDeviceService;
import ai.bankscan.accounts.service.AccountService;
import ai.bankscan.common.email.service.EmailService;
import ai.bankscan.common.infra.annotations.ApiPageable;
import ai.bankscan.common.infra.mail.messages.PebbleMailMessage;
import ai.bankscan.common.infra.security.common.CurrentUser;
import ai.bankscan.common.infra.security.utils.SecurityUtils;
import ai.bankscan.common.infra.web.excel.ExcelXlsView;
import ai.bankscan.configs.security.Hasher;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.function.Function;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Api(tags = {"상담원 API"})
@RequiredArgsConstructor
@RestController
@RequestMapping(path = AccountRestController.REQUEST_BASE_PATH, produces = MediaTypes.HAL_JSON_VALUE)
public class AccountRestController {

  static final String REQUEST_BASE_PATH = "/api/accounts";

  private final AccountService accountService;
  private final AccountResourceAssembler accountResourceAssembler;
  private final ModelMapper modelMapper;
  private final AccountDeviceService accountDeviceService;

  @ApiOperation(value = "상담원 등록", notes = "센터장, RM, 상담원 등을 등록")
  @Secured({"ROLE_ADMIN", "ROLE_CENTER_MANAGER", "ROLE_RELATIONSHIP_MANAGER"})
  @PostMapping
  public ResponseEntity<Resource<Response>> createAccount(
      @RequestBody @Valid AccountDto.Create dto
  ) throws URISyntaxException {
    Response savedAccount = accountService.createAccount(dto);
    Resource<Response> resource = accountResourceAssembler.toResource(savedAccount);

    return ResponseEntity
        .created(new URI(resource.getLink(Link.REL_SELF).getHref()))
        .body(resource);
  }

  @ApiOperation(value = "상담원 단건 조회", notes = "상담원 단건 조회")
  @GetMapping("/{id}")
  public ResponseEntity<Resource<Response>> getAccount(
      @ApiParam(required = true, example = "1")
      @PathVariable final Long id) {
    Resource<Response> resource = accountResourceAssembler
        .toResource(accountService.getAccount(id));
    return ResponseEntity.ok(resource);
  }

  @ApiPageable
  @ApiOperation(value = "상담원 리스트 조회", notes = "상담원 리스트 ")
  @Secured({"ROLE_ADMIN", "ROLE_CENTER_MANAGER", "ROLE_RELATIONSHIP_MANAGER", "ROLE_CONSULTANT", "ROLE_PRO_CONSULTANT"})
  @GetMapping
  public Resources<Resource<Response>> getAccounts(
      AccountDto.Search search,
      @PageableDefault(sort = "createdAt", direction = Direction.DESC) Pageable pageable,
      PagedResourcesAssembler<Response> pagedResourcesAssembler,
      @CurrentUser Account account
  ) {

    if(account.getType().equals(Account.Type.CENTER_MANAGER)) {
        search.setCenter(account.getOrganization().getId());
    }

    if(account.getType().equals(Account.Type.RELATIONSHIP_MANAGER)) {
      search.setInstitution(account.getOrganization().getId());
    }

    Page<Response> page = accountService.getAccounts(search, pageable);
    return pagedResourcesAssembler.toResource(page, accountResourceAssembler);
  }


  @ApiOperation(value = "상담원 정보 수정", notes = "상담원 정보 수정")
  @PutMapping("/{id}")
  public ResponseEntity<Resource<Response>> updateAccount(
      @ApiParam(required = true, example = "1") @PathVariable final Long id,
      @RequestBody @Valid AccountDto.Update dto,
      @CurrentUser Account account
  ) throws URISyntaxException {
    AccountDto.Response savedAccount = accountService.updateAccount(id, dto, account.getUsername());
    Resource<AccountDto.Response> resource = accountResourceAssembler.toResource(savedAccount);

    return ResponseEntity
        .created(new URI(resource.getLink(Link.REL_SELF).getHref()))
        .body(resource);
  }


  @ApiOperation(value ="push 인원 수", notes = "push 인원 수")
  @GetMapping("/push-count")
  public ResponseEntity<Map<String, Integer>> getPushTokens() {
    Map<String, Integer> data = new HashMap<>();
    long datas = accountDeviceService.countAllByPushTokens();
    data.put("counts", Integer.parseInt(String.valueOf(datas)));
    return ResponseEntity.ok(data);
  }

  @GetMapping("/professional-field")
  public Map<ProfessionalField, String> getProfessionalField() {
    return Arrays.stream(ProfessionalField.values())
        .collect(toMap(Function.identity(), ProfessionalField::getTitle, (e1, e2) -> e2,
            LinkedHashMap::new));
  }

  @ApiOperation(value = "인증된 사용자 정보 조회", notes = "인증이 되어있지 않을 경우 Unauthorized(401)")
  @GetMapping("/authentication")
  public ResponseEntity<AccountDto.Response> getAuthentication(
      @CurrentUser Account currentUser
  ) {
    AccountDto.Response response = accountService.getAccount(currentUser.getId());
    response.setNow(System.currentTimeMillis());
    return ResponseEntity.ok(response);
  }

  @ApiOperation(value = "id, pw 찾기", notes = "id pw를 잃어버렸을 때")
  @PostMapping("/find")
  public ResponseEntity<AccountDto.Response> findByUserInformation(
      @RequestBody AccountDto.Find find
  ) {

    AccountDto.Response response = accountService.findByUsername(find.getUsername());
    try {
      if (response == null) throw new Exception();
      if (!response.getEmail().equalsIgnoreCase(find.getEmail())) throw new Exception();

      accountService.updateTemploraryPassword(response.getId());
      return ResponseEntity.ok(response);
    }
    catch(Exception ex) {
      return ResponseEntity.status(400).body(null);
    }

  }



  @ApiOperation(value = "상담원 리스트 엑셀 다운로드", notes = "상담원 리스트드 엑셀 다운로드 ")
  @Secured({"ROLE_ADMIN", "ROLE_CENTER_MANAGER", "ROLE_RELATIONSHIP_MANAGER"})
  @GetMapping("excel/{type2}")
  public ModelAndView downloadExcelList(
          AccountDto.Search search,
          @PathVariable String type2,
          @CurrentUser Account account
  ) {

    Map<String, Object> resource = accountService.getAccountsListToExcel(search, Account.Type.ADMIN, type2);;
    if(account.getType().equals(Account.Type.CENTER_MANAGER) || type2.equals("consultant")) {
      search.setCenter(account.getOrganization().getId());
      resource = accountService.getAccountsListToExcel(search, Account.Type.CONSULTANT, type2);
    }

    if(account.getType().equals(Account.Type.RELATIONSHIP_MANAGER) || type2.equals("proConsultant")) {
      search.setInstitution(account.getOrganization().getId());
      resource = accountService.getAccountsListToExcel(search, Account.Type.PRO_CONSULTANT, type2);

    }

    return new ModelAndView(new ExcelXlsView(), resource);
  }

  @ApiOperation(value = "sample 상담원 등록 excel file download", notes = "상담원 파일 받기 type: consultant, proConsultant")
  @GetMapping("sample-excel/{type}")
  @Secured({
          "ROLE_ADMIN",
          "ROLE_CENTER_MANAGER",
          "ROLE_RELATIONSHIP_MANAGER"
  })
  public ModelAndView downloadExcel(@PathVariable String type,  @CurrentUser Account account) {

    Map<String, Object> resource = null;

    if(account.getType().equals(Account.Type.CENTER_MANAGER) || type.equals("consultant") ) {
      resource = accountService.sampleFileDownLoadConsultant(Account.Type.CONSULTANT);
    }

    if(account.getType().equals(Account.Type.RELATIONSHIP_MANAGER) || type.equals("proConsultant")) {
      resource = accountService.sampleFileDownLoadConsultant(Account.Type.PRO_CONSULTANT);
    }
    return new ModelAndView(new ExcelXlsView(), resource);
  }

}
