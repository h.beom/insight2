package ai.bankscan.consultations.service;

import ai.bankscan.consultations.domain.consulting.Consultation;
import ai.bankscan.consultations.domain.consulting.ConsultationRepository;
import ai.bankscan.consultations.domain.logs.ConsultationLog;
import ai.bankscan.consultations.domain.logs.ConsultationLogRepository;
import ai.bankscan.consultations.dto.ConsultationLogDto;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service
public class ConsultationLogService {

  private final ConsultationRepository consultationRepository;
  private final ConsultationLogRepository consultationLogRepository;
  private final ModelMapper modelMapper;

  @Transactional
  public void createConsultationLog(
      ConsultationLogDto.Create dto
  ) {
    try {

      ConsultationLog consultationLog = modelMapper.map(dto, ConsultationLog.class);
      consultationLogRepository.save(consultationLog);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public List<ConsultationLogDto.Response> getConsultationLog(
      final Long consultationId
  ) {
    Consultation consultation = consultationRepository
        .findById(consultationId)
        .orElseThrow(IllegalAccessError::new);

    List<ConsultationLog> consultationLogs = consultationLogRepository
        .findByConsultation(consultation);

    return consultationLogs.stream()
        .map(this::toResponse)
        .collect(Collectors.toList());
  }

  private ConsultationLogDto.Response toResponse(ConsultationLog consultationLog) {
    return modelMapper.map(consultationLog, ConsultationLogDto.Response.class);
  }

}
