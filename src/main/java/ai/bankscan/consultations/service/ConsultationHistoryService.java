package ai.bankscan.consultations.service;

import ai.bankscan.consultations.domain.consulting.Consultation;
import ai.bankscan.consultations.domain.consulting.Consultation.Status;
import ai.bankscan.consultations.domain.consulting.ConsultationRepository;
import ai.bankscan.consultations.domain.logs.ConsultationHistory;
import ai.bankscan.consultations.domain.logs.ConsultationHistoryRepository;
import ai.bankscan.consultations.dto.ConsultationHistoryDto;
import com.google.api.client.util.Lists;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Transactional
@Service
public class ConsultationHistoryService {

  private final ConsultationRepository consultationRepository;
  private final ConsultationHistoryRepository consultationHistoryRepository;
  private final ModelMapper modelMapper;

  public void createHistory(ConsultationHistoryDto.Create dto, Status status, String key) {

    ConsultationHistory consultationHistory = modelMapper.map(dto, ConsultationHistory.class);
      List<ConsultationHistory> arrays = Lists.newArrayList();
    if(status.equals(Status.PRO_APPLY) && key.equals("update")) {
      ConsultationHistoryDto.Create dto1 = ConsultationHistoryDto.Create.builder()
          .consultantId(null)
          .consultationId(consultationHistory.getConsultation().getId())
          .status(Status.COMPLETE)
          .logComment(Status.COMPLETE.getTitle())
          .logDateTime(LocalDateTime.now())
          .build();
      ConsultationHistory consultationHistory1 = modelMapper.map(dto1, ConsultationHistory.class);
      arrays.add(consultationHistory1);
    }
    arrays.add(consultationHistory);
    for(int i = 0; i < arrays.size(); i++) {
      this.createConsultationHistory(arrays.get(i));
    }

  }

  public void createConsultationHistory(
      ConsultationHistory consultationHistory
  ) {
    try {
      consultationHistoryRepository.save(consultationHistory);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public List<ConsultationHistoryDto.Response> getConsultationLHistory(
      final Long consultationId
  ) {
    Consultation consultation = consultationRepository
        .findById(consultationId)
        .orElseThrow(IllegalAccessError::new);

    List<ConsultationHistory> consultationHistories = consultationHistoryRepository
        .findByConsultation(consultation);

    return consultationHistories.stream()
        .map(this::toResponse)
        .collect(Collectors.toList());
  }

  private ConsultationHistoryDto.Response toResponse(ConsultationHistory consultationHistory) {
    return modelMapper.map(consultationHistory, ConsultationHistoryDto.Response.class);
  }

}
