package ai.bankscan.calculate.dto;

import ai.bankscan.accounts.dto.AccountDto;
import ai.bankscan.calculate.domain.Calculate;
import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.common.infra.jpa.support.SearchDto;
import ai.bankscan.consultations.dto.ConsultationDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Arrays;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CalculateDto {

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "CalculateDto.Create")
  public static class Create {
    private long consultationId;

    private long consultant;

  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "CalculateDto.Update")
  public static class Update {
    private long id;
    private boolean isCalculate;
  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "CalculateDto.Response")
  public static class Response {
    private long id;
    private ConsultationDto.Response consultation;
    private AccountDto.Response consultant;
    private boolean isCalculate;

  }


  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "CalculateDto.Simple")
  public static class Simple {
    private long id;

  }


  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString(callSuper = true)
  @ApiModel(value = "CalculateDto.Search", description = "periodType: 접수일(createdAt), 완료일(consultDueDate)")
  public static class Search extends SearchDto<Calculate> {

    @ApiModelProperty(value = "검색구분", position = 1)
    private String searchType;

    @ApiModelProperty(value = "검색어", position = 2)
    private String keyword;

    @Override
    protected Restrictions generateRestrictions() {
      final Restrictions restrictions = new Restrictions();

      if (StringUtils.isNotEmpty(this.keyword)) {
        if (StringUtils.isEmpty(this.searchType)) {
          final Restrictions restrictionsByKeyword = new Restrictions(Restrictions.Conn.OR);
          List<String> keywords = Arrays.asList(this.keyword.trim().split("\\s+"));
          keywords.stream()
              .forEach(keyword -> {
                restrictionsByKeyword.like("center.name", "%" + keyword + "%");
                restrictionsByKeyword.like("center.code", "%" + keyword + "%");
                restrictionsByKeyword.like("consultant.username", "%" + keyword + "%");
                restrictionsByKeyword.like("consultant.name", "%" + keyword + "%");
              });
          restrictions.addChild(restrictionsByKeyword);
        }if(this.searchType.equals("consultant.id")) {
          restrictions.eq(searchType, keyword);
        } else {
          restrictions.like(searchType, "%" + keyword + "%");
        }
      }
      return restrictions;
    }
    protected void addSearchKeyword(final Restrictions restrictions, final String keyword) {}
  }

}
