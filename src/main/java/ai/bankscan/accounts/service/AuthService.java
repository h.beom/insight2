package ai.bankscan.accounts.service;

import ai.bankscan.accounts.domain.AccountDevice;
import ai.bankscan.accounts.domain.AccountDeviceRepository;
import ai.bankscan.accounts.dto.MemberDto;
import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.configs.security.Hasher;
import ai.bankscan.configs.security.device.DeviceAuthenticationToken;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service
public class AuthService {

    private final AccountDeviceRepository accountDeviceRepository;
    private final UserDetailsService userDetailsService;
    private final PasswordEncoder passwordEncoder;

    @Transactional
    public AccountDevice login(MemberDto.Login dto, HttpServletRequest request) throws Exception {

        AccountAdapter details = (AccountAdapter) userDetailsService.loadUserByUsername(dto.getUsername());
        if (!passwordEncoder.matches(dto.getPassword(), details.getPassword())) {
            throw new Exception();
        }

//        accountDeviceRepository.deleteByAccountId(details.getAccount().getId());

        String nowStr = DateTimeFormatter.ISO_DATE.format(LocalDate.now());

        StringBuilder secret = new StringBuilder();
        secret.append(dto.getDeviceName() + "@");
        secret.append(dto.getSerialNumber() + "@");
        secret.append(nowStr);

        String secretToken = Hasher.parseMD5(secret.toString());
        String accessToken = this.getUniqueAccessToken(secretToken);

        AccountDevice device = AccountDevice.builder()
                .accessToken(accessToken)
                .secretToken(secretToken)
                .account(details.getAccount())
                .deviceName(dto.getDeviceName())
                .platform(dto.getPlatform())
                .pushToken(dto.getPushToken())
                .serialNumber(dto.getSerialNumber())
                .build();

        if (dto.getPlatform() == AccountDevice.Platform.WEB) {
            Authentication authentication = new DeviceAuthenticationToken(accessToken, details);

            SecurityContext securityContext = SecurityContextHolder.getContext();
            securityContext.setAuthentication(authentication);
            HttpSession session = request.getSession(true);
            session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
        }

        return accountDeviceRepository.save(device);
    }

    private String getUniqueAccessToken(String secretKey) {
        String accessToken = Hasher.parseMD5(UUID.randomUUID().toString()).substring(0, 22);
        while (true) {
            Restrictions r = new Restrictions();
            r.like("accessToken", accessToken + "%");
            long rowSize = accountDeviceRepository.count(r.output());

            if (rowSize == 0) {
                accessToken = accessToken + secretKey.substring(27) + secretKey.substring(0, 5);
                break;
            } else {
                accessToken = Hasher.parseMD5(UUID.randomUUID().toString()).substring(0, 22);
            }
        }
        return accessToken;
    }

}
