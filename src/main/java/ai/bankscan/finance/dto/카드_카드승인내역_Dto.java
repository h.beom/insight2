package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 카드_카드승인내역_Dto {

  private String card;

  private String 가맹점명;
  private String 가맹점사업자번호;
  private String 가맹점업종;
  private String 가맹점전화번호;
  private String 가맹점주소;
  private String 가맹점코드;
  private String 결제예정일;
  private String 국내외구분;
  private String 매출종류;
  private String 승인금액;
  private String 승인번호;
  private String 승인시간;
  private String 승인일자;
  private String 취소년월일;
  private String 카드번호;
  private String 카드종류;
  private String 통화코드;
  private String 할부기간;

}
