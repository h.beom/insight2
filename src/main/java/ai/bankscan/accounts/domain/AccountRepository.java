package ai.bankscan.accounts.domain;


import java.util.Map;

public interface AccountRepository extends AccountBaseRepository<Account> {
  Account save(Map<String, Object> data);

}