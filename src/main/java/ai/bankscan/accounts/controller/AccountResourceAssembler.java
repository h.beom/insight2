package ai.bankscan.accounts.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import ai.bankscan.accounts.dto.AccountDto;
import ai.bankscan.accounts.dto.AccountDto.Response;
import org.springframework.data.domain.PageRequest;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class AccountResourceAssembler implements ResourceAssembler<Response, Resource<Response>> {

  @Override
  public Resource<AccountDto.Response> toResource(AccountDto.Response entity) {
    return new Resource<>(
        entity,
        linkTo(methodOn(AccountRestController.class).getAccount(entity.getId())).withSelfRel(),
        linkTo(methodOn(AccountRestController.class)
            .getAccounts(
                AccountDto.Search.builder().build(),
                PageRequest.of(0, 10),
                null,
                null
            )).withRel("member"));

  }

}
