package ai.bankscan.accounts.service.member;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.Account.Status;
import ai.bankscan.accounts.domain.AccountDevice;
import ai.bankscan.accounts.domain.AccountDeviceRepository;
import ai.bankscan.accounts.domain.AccountRepository;
import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.accounts.domain.member.MemberRepository;
import ai.bankscan.accounts.dto.AccountDeviceDto;
import ai.bankscan.accounts.dto.MemberDto;
import ai.bankscan.accounts.dto.MemberDto.Response;
import ai.bankscan.common.infra.exceptions.AccountNotFoundException;
import ai.bankscan.common.infra.exceptions.UserDuplicatedException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import ai.bankscan.common.infra.web.excel.constants.Excel;
import com.google.common.collect.Maps;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service
public class MemberService {

  private final AccountRepository accountRepository;
  private final MemberRepository memberRepository;
  private final AccountDeviceRepository accountDeviceRepository;
  private final ModelMapper modelMapper;
  private final PasswordEncoder passwordEncoder;


  public Page<Response> getMembers(MemberDto.Search search, Pageable pageable) {
    Page<Member> page = memberRepository.findAll(search.toSpecification(), pageable);
    List<Response> content = page.getContent().stream()
        .map(this::toResponse)
        .collect(Collectors.toList());
    return new PageImpl<>(content, pageable, page.getTotalElements());
  }

  public Map<String, Object> getMembersToExcel(MemberDto.Search search, String listType) {
    List<Member> memberList = memberRepository
        .findAll(search.toSpecification(), Sort.by(Direction.DESC, "createdAt"));

    List<Member> liveMemberList = new ArrayList<>();
    List<Member> leavedMember = new ArrayList<>();

    for (Member member : memberList) {
      if (!member.getIsLeaved()) {
        liveMemberList.add(member);
      } else {
        leavedMember.add(member);
      }
    }

    if (listType.equals("live")) {
      return getLiveMembers(liveMemberList);
    } else {
      return  getLeaveMembers(leavedMember);
    }

  }

  private Map<String, Object> getLiveMembers(List<Member> memberList) {
    List<List<String>> body = memberList.stream()
        .map(member -> Arrays.asList(
            member.getUsername(),
            member.getName(),
            member.getBirthday().format(DateTimeFormatter.ISO_DATE),
            member.getMobilePhone(),
            member.getEmail(),
            member.getAddress() != null ? member.getAddress().getAddress() : "주소 없음",
            member.getIsReceiveMail() ? "Y" : "N",
            member.getIsReceivePhone() ? "Y" : "N",
            member.getNotificationConsent() ? "Y" : "N",
            member.getType().isMember() ? "일반" : "",
            member.getCreatedAt().format(DateTimeFormatter.ISO_DATE),
            member.getEffectiveDate() != null ? member.getEffectiveDate()
                .format(DateTimeFormatter.ISO_DATE) : "접속 기록 없음"
        )).collect(Collectors.toList());

    Map<String, Object> data = Maps.newHashMap();
    data.put(Excel.FILE_NAME.getName(), "회원목록");
    data.put(Excel.HEAD.getName(),
            Arrays.asList(
                    "고객ID",
                    "이름",
                    "생년월일",
                    "전화번호",
                    "이메일",
                    "주소1",
                    "이메일 수신 여부",
                    "SMS 수신 여부",
                    "PUSH 수신 여부",
                    "회원등급",
                    "가입일",
                    "최근접속일"
            ));
    data.put(Excel.BODY.getName(), body);
    return data;
  }

  private Map<String, Object> getLeaveMembers(List<Member> memberList) {
    List<List<String>> body = memberList.stream()
        .map(member -> Arrays.asList(
            member.getUsername(),
            member.getLeaveReasonType().getTitle(),
            member.getLeaveReason(),
            member.getLeaveType().getTitle(),
            member.getCreatedAt().format(DateTimeFormatter.ISO_DATE),
            member.getLeaveDate().format(DateTimeFormatter.ISO_DATE)
        )).collect(Collectors.toList());

    Map<String, Object> data = Maps.newHashMap();
    data.put(Excel.FILE_NAME.getName(), "탈퇴_회원목록");
    data.put(Excel.HEAD.getName(),
            Arrays.asList(
                    "고객ID",
                    "탈퇴사유",
                    "상세내용",
                    "처리유형",
                    "가입일",
                    "탈퇴일"
            ));
    data.put(Excel.BODY.getName(), body);
    return data;

  }

  public MemberDto.Response getMember(final Long id, final String username) {
    Member member = this.validate(id, username);
    return toResponse(member);
  }


  public MemberDto.Response findMember(final String ci,
      final String di) {

    Member member = memberRepository.findByCiAndDi(ci, di);
    if(member != null) {
      return modelMapper.map(member, MemberDto.Response.class);
    } else {
      return null;
    }

  }


  @Transactional
  public MemberDto.Response createMember(MemberDto.Create dto) {

    Member member = modelMapper.map(dto, Member.class);

    final String username = dto.getUsername();
    Optional<Member> optionalMember = memberRepository.findByUsername(username);
    if (optionalMember.isPresent()) {
      throw new UserDuplicatedException(username);
    }


    Member checkMember = this.validateCiDi(member.getCi(), member.getDi());

    if(checkMember != null) {
      throw new UserDuplicatedException(checkMember.getUsername());
    }
    member.create(member.getType().getAuthorities(), passwordEncoder);
    return toResponse(memberRepository.save(member));
  }

  @Transactional
  public MemberDto.Response updateMember(final Long id, MemberDto.Update dto,
      final String username) {

    Member existingAccount = this.validate(id, username);

    if (dto.getIsLeaved()) {
      dto.setStatus(Account.Status.DEACTIVATION);
      dto.setIsReceiveMail(false);
      dto.setIsReceivePhone(false);
      dto.setIsReceivePush(false);
      dto.setNotificationConsent(false);
      dto.setAddress(null);
      dto.setMobilePhone(null);
      dto.setEmail(null);
    }

    //관리자가 탈퇴시킨 경우
    if(dto.getLeaveReasonType() != null && dto.getLeaveType().equals(Account.LeaveType.THROW_OUT)) {
      if(!dto.getLeaveReasonType().equals(Account.LeaveReasonType.REQ_USER)) {
        dto.setStatus(Account.Status.BLOCK);
      } else {
        dto.setStatus(Account.Status.DEACTIVATION);
      }
      dto.setAddress(null);
      dto.setEmail(null);
      dto.setMobilePhone(null);
      dto.setNotificationConsent(false);
      dto.setMobilePhoneCorp(null);
      dto.setLeaveDate(LocalDateTime.now());
    }

    if(existingAccount.getStatus().equals(Status.READY)) {
      dto.setStatus(Status.ACTIVATION);
    }

    modelMapper.map(dto, existingAccount);

    if (dto.getPassword() != null) {
      existingAccount.update(passwordEncoder);
    }

    return toResponse(memberRepository.save(existingAccount));

  }

  public AccountDeviceDto.Response myList(String token) {
   Optional<AccountDevice> accountDevice = accountDeviceRepository.findByAccessToken(token);

    return modelMapper.map(accountDevice.get(), AccountDeviceDto.Response.class);
  }


  private Member validate(final Long id, final String username) {
    Account authUser = accountRepository.findByUsername(username)
        .orElseThrow(() -> new AccountNotFoundException(username));

    Member member = memberRepository.findById(id)
        .orElseThrow(() -> new AccountNotFoundException(id));

    if (!authUser.isAdminRole() && !member.getUsername().equals(username)) {
      throw new AccessDeniedException("Access is denied.");
    }

    return member;
  }


  public AccountDeviceDto.Response myPushListUpdate(String token, AccountDeviceDto.Update update) {

    Optional<AccountDevice> accountDevice = accountDeviceRepository.findByAccessToken(token);
    if(accountDevice.get() != null) {
      accountDevice.get().setPushConsulting(update.isPushConsulting());
      accountDevice.get().setPushDiagnosisEnd(update.isPushDiagnosisEnd());
      accountDevice.get().setPushNotice(update.isPushNotice());
      accountDeviceRepository.save(accountDevice.get());

      return modelMapper.map(accountDevice.get(), AccountDeviceDto.Response.class);
    }
    return null;
  }

  private MemberDto.Response toResponse(Member member) {
    return modelMapper.map(member, Response.class);
  }


  private Member validateCiDi(String ci, String di) {

    Member member = memberRepository.findByCiAndDi(ci, di);

    if (member != null) {
      return member;
    }
    return null;
  }

}
