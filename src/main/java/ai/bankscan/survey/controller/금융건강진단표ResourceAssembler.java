package ai.bankscan.survey.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import ai.bankscan.survey.dto.금융건강진단표Dto;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class 금융건강진단표ResourceAssembler implements
    ResourceAssembler<금융건강진단표Dto.Response, Resource<금융건강진단표Dto.Response>> {

  @Override
  public Resource<금융건강진단표Dto.Response> toResource(금융건강진단표Dto.Response entity) {
    return new Resource<> (
        entity,
        linkTo(methodOn(SurveyRestController.class)
            .getSurveyAnswer(entity.getAnswerList())).withSelfRel());
  }

}
