package ai.bankscan.survey.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import net.sf.json.JSONObject;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class 희망노후생활비Dto {
  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "희망노후생활비Dto.Create")
  public static class Create {
    @ApiModelProperty(value = "string",example = "{}", position = 1)
    private JSONObject answer;

    @ApiModelProperty(value ="string", example = "{}", position = 2)
    private JSONObject result;
  }
  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "희망노후생활비Dto.Response")
  public static class Response {
    private Long id;

    @ApiModelProperty(value = "string",example = "{}", position = 1)
    private JSONObject answer;

    @ApiModelProperty(value ="string", example = "{}", position = 2)
    private JSONObject result;
  }

}
