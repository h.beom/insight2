package ai.bankscan.messages.service;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.AccountDevice;
import ai.bankscan.accounts.domain.AccountDeviceRepository;
import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.accounts.domain.member.MemberRepository;
import ai.bankscan.common.infra.exceptions.AccountNotFoundException;
import ai.bankscan.common.infra.web.notice.service.FirebaseMessageService;
import ai.bankscan.messages.domain.NotificationMessage;
import ai.bankscan.messages.domain.NotificationMessage.Status;
import ai.bankscan.messages.domain.NotificationMessageRepository;
import ai.bankscan.messages.dto.NotificationMessageDto;
import com.google.common.collect.Lists;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author eomjeongjae
 * @since 2019/12/16
 */
@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@Service
public class NotificationMessageService {

    private final MemberRepository memberRepository;
    private final AccountDeviceRepository accountDeviceRepository;
    private final NotificationMessageRepository notificationMessageRepository;
    private final FirebaseMessageService firebaseMessageService;
    private final ModelMapper modelMapper;

    @Transactional
    public CompletableFuture<BatchResponse> sendMessageBySchedule() {
        final LocalDateTime now = LocalDateTime.now();
        List<NotificationMessage> notificationMessages = notificationMessageRepository
                .findAllByStatusAndScheduledDateTimeNotNull(Status.IN_PREPARATION);

        notificationMessages.stream()
                .filter(notificationMessage -> now.isAfter(notificationMessage.getScheduledDateTime()))
                .forEach(notificationMessage -> notificationMessage.completeToSend());

        List<NotificationMessage> savedNotificationMessages = notificationMessageRepository
                .saveAll(notificationMessages);

        return firebaseMessageService.sendMessage(generateMessage(savedNotificationMessages));
    }

    @Async
    @Transactional
    public CompletableFuture<BatchResponse> sendMessage(NotificationMessageDto.Send sendMessage) {
        Member member = memberRepository.findById(sendMessage.getReceiverId()).orElseThrow();
        AccountDevice receiver = accountDeviceRepository.findByAccount(member);
        NotificationMessage newNotificationMessage = modelMapper
                .map(sendMessage, NotificationMessage.class);
        newNotificationMessage.create(receiver);

        NotificationMessage savedNotificationMessage = notificationMessageRepository
                .save(newNotificationMessage);

        CompletableFuture<BatchResponse> messageResponse = firebaseMessageService
                .sendMessage(generateMessage(savedNotificationMessage));

        messageResponse.thenAccept((batchResponse) -> {
            savedNotificationMessage
                    .completeToSend(batchResponse.getSuccessCount(), batchResponse.getFailureCount());
            notificationMessageRepository.save(savedNotificationMessage);
        }).exceptionally(throwable -> {
            log.error("Send message error:", throwable);
            savedNotificationMessage.failToSend(throwable.getMessage());
            notificationMessageRepository.save(savedNotificationMessage);
            return null;
        });

        return messageResponse;
    }

    @Async
    @Transactional
    public CompletableFuture<BatchResponse> sendToAll(NotificationMessageDto.SendToAll dto) {
        List<AccountDevice> members = accountDeviceRepository
                .findAllByPushTokenNotNull();

        List<NotificationMessage> notificationMessages = members.stream()
                .map(member -> {
                    NotificationMessage newNotificationMessage = modelMapper
                            .map(dto, NotificationMessage.class);
                    newNotificationMessage.create(member);
                    return newNotificationMessage;
                })
                .collect(Collectors.toList());

        List<NotificationMessage> savedNotificationMessages = notificationMessageRepository
                .saveAll(notificationMessages);

        return firebaseMessageService
                .sendMessage(generateMessage(savedNotificationMessages));
    }

    private List<Message> generateMessage(NotificationMessage notificationMessage) {
        return this.generateMessage(Lists.newArrayList(notificationMessage));
    }

    private List<Message> generateMessage(List<NotificationMessage> savedNotificationMessages) {
        List<Message> messages = Lists.newArrayList();
        savedNotificationMessages.stream()
                .filter(notificationMessage -> notificationMessage.getSendType().isDirect())
                .forEach(notificationMessage -> {
                            if (notificationMessage.getReceiver().getPushToken() != null) {
                                Message message = Message.builder()
                                        .setNotification(new Notification(
                                                notificationMessage.getTitle(),
                                                notificationMessage.getBody()))
                                        .setToken(notificationMessage.getReceiver().getPushToken())
                                        .putData("link", notificationMessage.getLink())
                                        .build();
                                messages.add(message);
                            }
                        }
                );
        return messages;
    }

}