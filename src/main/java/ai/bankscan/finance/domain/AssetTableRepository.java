package ai.bankscan.finance.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AssetTableRepository extends JpaRepository<AssetTable, Long>,
    JpaSpecificationExecutor<AssetTable> {

  Long countByCreatedById(Long createdById);
}
