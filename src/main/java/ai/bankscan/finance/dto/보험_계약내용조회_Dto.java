package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 보험_계약내용조회_Dto {

  private String insure;

  private String 계약금액;
  private String 계약상태;
  private String 계약일자;
  private String 계약자;
  private String 계약자연락처;
  private String 계약자주민번호;
  private String 계좌번호;
  private Long 기대출금액;
  private String 납입기간;
  private String 납입보험료;
  private String 납입주기;
  private String 납입횟수;
  private String 담당컨설턴트;
  private String 만기일자;
  private String 보장기간시작일;
  private String 보장기간종료일;
  private String 보험계약대출가능금액;
  private String 보험료;
  private String 상품명;
  private String 수익자;
  private String 예금주;
  private String 은행명;
  private String 이체일자;
  private String 자동차보험여부;
  private String 잔여횟수;
  private String 정상대출이율;
  private String 종피보험자;
  private String 증권번호;
  private String 지점명;
  private String 최종납입월;
  private String 피보험자;
  private String 피보험자연락처;
  private String 피보험자주민번호;
  private Long 해지환급금;
  private String 환급율;
  private String 휴대폰번호;

}
