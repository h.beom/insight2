package ai.bankscan.finance.domain;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Entity;
import javax.persistence.Lob;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import net.sf.json.JSONObject;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class CooconLog extends AuditableJoinEntity<Account> {

  private String errorCode;
  private String errorMessage;
  @Lob
  private String json;

  public CooconLog(JSONObject o) {
    super();
    JSONObject output = o.getJSONObject("Output");

    this.errorCode = String.valueOf(output.getString("ErrorCode"));
    this.errorMessage = String.valueOf(output.getString("ErrorMessage"));
    this.json = o.toString();
  }

}
