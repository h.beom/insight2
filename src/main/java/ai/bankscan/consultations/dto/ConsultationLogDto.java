package ai.bankscan.consultations.dto;

import ai.bankscan.accounts.dto.AccountDto;
import ai.bankscan.consultations.domain.consulting.Consultation.Status;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConsultationLogDto {
  @Setter
  @Getter
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "ConsultationLogDto.Create")
  public static class Create{
    @ApiModelProperty(value = "상담 정보", example = "1", position = 1)
    private Long consultationId;

    @Default
    @ApiModelProperty(value = "상담 상태", example = "1", position = 1)
    private Status status = Status.APPLY;

    @Default
    @ApiModelProperty(value = "상태 일자", example = "2020-01-01", position = 3)
    private LocalDate logDate = LocalDate.now();

  }



  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "ConsultationLogDto.Response")
  public static class Response{
    private Long id;

    @ApiModelProperty(value = "상담 정보", example = "1", position = 1)
    private ConsultationDto.Response consultationInformation;

    @ApiModelProperty(value = "상담 상태", example = "1", position = 1)
    private Status status;

    @ApiModelProperty(value = "상담사", example = "consultant", position = 2)
    private AccountDto.Response createdBy;

    @ApiModelProperty(value = "상태 일자", example = "2020-01-01", position = 3)
    private LocalDate logDate;

  }


}
