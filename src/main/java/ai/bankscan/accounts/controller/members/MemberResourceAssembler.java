package ai.bankscan.accounts.controller.members;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import ai.bankscan.accounts.dto.MemberDto;
import ai.bankscan.accounts.dto.MemberDto.Response;
import org.springframework.data.domain.PageRequest;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
class MemberResourceAssembler implements ResourceAssembler<Response, Resource<Response>> {

  @Override
  public Resource<Response> toResource(Response entity) {
    return new Resource<>(
        entity,
        linkTo(methodOn(MemberRestController.class).getMember(entity.getId(), null)).withSelfRel(),
        linkTo(methodOn(MemberRestController.class)
            .getMembers(
                MemberDto.Search.builder().build(),
                PageRequest.of(0, 10),
                null
            )).withRel("member"));

  }
}
