package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 연금_국민연금조회_Dto {

  private String 가입자구분;
  private String 연금종류;
  private String 연금개시년월;
  private String 예상연금수령액;
  private String 납부월수;
  private String 납부총액;
  private String 예상총납부월수;
  private String 예상납부보험료;
  private String 조회기준일;

}
