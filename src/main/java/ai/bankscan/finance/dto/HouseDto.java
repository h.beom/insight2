package ai.bankscan.finance.dto;

import ai.bankscan.finance.domain.House;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Data
public class HouseDto {

  private House.HouseMode mode;

  private String category;

  private String type;

  private String name;

  private String address;

  private Long price;

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "HouseDto.Response")
  public static class Response {
    private House.HouseMode mode;

    private String category;

    private String type;

    private String name;

    private String address;

    private Long price;
  }


}
