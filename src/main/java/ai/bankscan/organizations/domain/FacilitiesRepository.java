package ai.bankscan.organizations.domain;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface FacilitiesRepository extends JpaRepository<Facilities, Long>,
    JpaSpecificationExecutor<Facilities> {

  Optional<Facilities> findByDaysAndCenter(String days, Center center);

  @Query("SELECT "
      + "sum(f.rent) as rentSum, "
      + "sum(f.maintenanceFee) as maintenanceFeeSum, "
      + "sum(f.electricityFee) as electricitySum, "
      + "sum(f.waterFee) as waterFeeSum, "
      + "sum(f.cityGasFee) as cityGasFeeSum, "
      + "sum(f.phoneFee) as phoneFeeSum, "
      + "sum(f.internetFee) as internetFeeSum, "
      + "sum(f.officeSuppliesCost) as officeSuppliesCostSum, "
      + "sum(f.otherCost) as otherCostSum, "
      + "sum("
      + " f.rent + f.maintenanceFee + f.electricityFee"
      + " + f.waterFee + f.cityGasFee + f.phoneFee "
      + " + f.internetFee + f.officeSuppliesCost + f.otherCost"
      + ") as totalSum "
      + "FROM Facilities f "
      + "WHERE f.year = :year"
  )
  Map<String, BigDecimal> findSumByYear(@Param("year") Integer year);



}
