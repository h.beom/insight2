package ai.bankscan.configs.security.device;

import ai.bankscan.accounts.service.AccountDeviceAdapter;
import ai.bankscan.accounts.service.AccountDeviceService;
import java.io.IOException;
import java.util.Collections;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

public class DeviceAuthFilter extends AbstractAuthenticationProcessingFilter {

  public static final String TOKEN_HEADER = "x-bankscan-token";

  @Setter
  private AccountDeviceService accountDeviceService;

  public DeviceAuthFilter(RequestMatcher requestMatcher) {
    super(requestMatcher);
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, final FilterChain chain)
      throws IOException, ServletException {
    final String token = getTokenValue((HttpServletRequest) request);
    if (StringUtils.isEmpty(token)) {
      chain.doFilter(request, response);
      return;
    }

    this.setAuthenticationSuccessHandler(
        (request1, response1, authentication) -> {
          try {
            SecurityContext securityContext = SecurityContextHolder.getContext();
            securityContext.setAuthentication(authentication);
            HttpSession session = request1.getSession(true);
            session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
          } catch (Exception e) {
            e.printStackTrace();
          }
          chain.doFilter(request1, response1);
        }
    );

    super.doFilter(request, response, chain);

  }

  @Override
  public Authentication attemptAuthentication(HttpServletRequest request,
      HttpServletResponse response)
      throws AuthenticationException {

    String accessToken = getTokenValue(request);
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

    if (authentication != null && authentication.isAuthenticated()
        && accountDeviceService != null) {
      AccountDeviceAdapter adapter = (AccountDeviceAdapter) authentication.getPrincipal();
      String userAccessToken = adapter.getAccountDevice().getAccessToken();
      System.out.println(accessToken + " / " + userAccessToken);

      try {
        if (!userAccessToken.equals(accessToken)
            || accountDeviceService.findByAccessToken(accessToken) == null) {
          throw new Exception();
        }
      } catch (Exception ex) {
        authentication.setAuthenticated(false);
        SecurityContextHolder.clearContext();
        return null;
      }

    } else if (!StringUtils.isEmpty(accessToken)) {
      DeviceAuthenticationToken token = new DeviceAuthenticationToken(accessToken);
      token.setDetails(authenticationDetailsSource.buildDetails(request));
      authentication = this.getAuthenticationManager().authenticate(token);
    }

    return authentication;
  }

  private String getTokenValue(HttpServletRequest req) {
    return Collections.list(req.getHeaderNames()).stream()
        .filter(header -> {
          return header.equalsIgnoreCase(TOKEN_HEADER) || header.equalsIgnoreCase("authorization");
        })
        .map(header -> {
          if (header.equalsIgnoreCase(TOKEN_HEADER)) {
            return req.getHeader(header);
          } else if (req.getHeader(header).startsWith("bs_token")) {
            return req.getHeader(header).substring("bs_token".length() + 1).trim();
          } else {
            return null;
          }
        })
        .filter(v -> v != null)
        .findFirst()
        .orElse(null);
  }
}
