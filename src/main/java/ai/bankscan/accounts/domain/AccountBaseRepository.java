package ai.bankscan.accounts.domain;

import ai.bankscan.accounts.domain.Account.Status;
import ai.bankscan.organizations.domain.Organization;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface AccountBaseRepository<T extends Account> extends JpaRepository<T, Long>,
    JpaSpecificationExecutor<T> {

  Optional<T> findByUsername(String username);

  List<T> findAllByOrganization(Organization organization);

  List<T> findAllByStatusAndNotificationConsent(Status status, Boolean notificationConsent);

}