package ai.bankscan.accounts.dto;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.accounts.domain.Account.LeaveReasonType;
import ai.bankscan.accounts.domain.Account.LeaveType;
import ai.bankscan.accounts.domain.Account.Status;
import ai.bankscan.accounts.domain.Account.Type;
import ai.bankscan.common.address.domain.Address;
import ai.bankscan.common.address.dto.AddressDto;
import ai.bankscan.common.domain.Gender;
import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.common.infra.jpa.support.SearchDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.util.CollectionUtils;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AccountBaseDto {

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "AccountBaseDto.Create")
  public abstract static class Create {

    @NotEmpty
    @ApiModelProperty(value = "아이디", example = "abcd1234", position = 1, required = true)
    private String username;

    @Length(min = 8, max = 20)
    @ApiModelProperty(value = "비밀번호", example = "a123456A", position = 2, required = true)
    private String password;

    @NotEmpty
    @ApiModelProperty(value = "이름", example = "홍길동", position = 3, required = true)
    private String name;

    @NotEmpty
    @ApiModelProperty(value = "이메일주소", example = "abcd1234@naver.com", position = 4, required = true)
    private String email;

    @NotEmpty
    @ApiModelProperty(value = "휴대폰", example = "01012345678", position = 5, required = true)
    private String mobilePhone;

    @ApiModelProperty(value = "통신사", example = "SKT", position = 5, required = true)
    private String mobilePhoneCorp;

    @ApiModelProperty(value = "성별", example = "FEMALE", position = 6, required = true)
    private Gender gender;

    @ApiModelProperty(value = "거주지 주소", example = "서울시 노원구 월계", position = 7)
    private AddressDto.Save address;

    @NotNull
    @ApiModelProperty(value = "생년월일", example = "1983-01-14", position = 8, required = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "Asia/Seoul")
    private LocalDate birthday;

    @Default
    @ApiModelProperty(value = "푸시알림 여부", example = "false", position = 9, required = true)
    private Boolean notificationConsent = Boolean.FALSE;


    @ApiModelProperty(value = "Connecting Information", example = "ci", position = 10)
    private String ci;

    @ApiModelProperty(value = "Duplication information", example = "di", position = 11)
    private String di;

    @ApiModelProperty(value = "createdAt", example = "2020-01-01 22:22", position = 12)
    private LocalDateTime createdAt = LocalDateTime.now();

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "AccountBaseDto.Update")
  public static class Update {

    @ApiModelProperty(value = "이메일주소", example = "abcd1234@naver.com", position = 1, required = true)
    private String email;

    @ApiModelProperty(value = "휴대폰", example = "01012345678", position = 2, required = true)
    private String mobilePhone;

    @ApiModelProperty(value = "통신사", example = "SKT", position = 5, required = true)
    private String mobilePhoneCorp;

    @ApiModelProperty(value = "푸시알림 여부", example = "false", position = 3, required = true)
    private Boolean notificationConsent;

    @ApiModelProperty(value = "거주지 주소", example = "서울시 노원구 월계", position = 4)
    private Address address;

    @Default
    @ApiModelProperty(value = "탈퇴 여부", example = "false", position = 5)
    private Boolean isLeaved = Boolean.FALSE;

    @ApiModelProperty(value = "탈퇴 사유", example = "탈퇴 합니다.", position = 6)
    private String leaveReason;

    @ApiModelProperty(value = "탈퇴 이유 타입", example = "NO_0(고객 요청)", position = 7)
    private LeaveReasonType leaveReasonType;

    @ApiModelProperty(value = "탈퇴 유형", example = "THROW_OUT(관리자탈퇴)", position = 8)
    private LeaveType leaveType;

    @Length(min = 8, max = 20)
    @ApiModelProperty(value = "비밀번호", example = "a123456A", position = 9)
    private String password;

    @ApiModelProperty(value = "status", example = "ready, ativaction", position = 10)
    private Status status;

    @ApiModelProperty(value = "leaveDate", example = "2020-03-03 22:00", position = 11)
    private LocalDateTime leaveDate;

    @ApiModelProperty(value ="authority", example = "CENTER_MANAGER, CONSULTANT", position = 12)
    private Set<Account.Authority> authorities;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "AccountBaseDto.Simple")
  public static class Simple {

    @ApiModelProperty(value = "ID", example = "1")
    private Long id;

    @ApiModelProperty(value = "타입", example = "CENTER_MANAGER", position = 2, required = true)
    private Account.Type type;

    @ApiModelProperty(value = "아이디", example = "abcd1234", position = 3, required = true)
    private String username;

    @ApiModelProperty(value = "이름", example = "홍길동", position = 4, required = true)
    private String name;

    @ApiModelProperty(value = "이메일주소", example = "abcd1234@naver.com", position = 5, required = true)
    private String email;

    @ApiModelProperty(value = "휴대폰", example = "01012345678", position = 6, required = true)
    private String mobilePhone;

    @ApiModelProperty(value = "통신사", example = "SKT", position = 5, required = true)
    private String mobilePhoneCorp;

    @ApiModelProperty(value = "성별", example = "FEMALE", position = 7, required = true)
    private Gender gender;

    @ApiModelProperty(value = "생년월일", example = "1983-01-14", position = 8, required = true)
    private LocalDate birthday;

    @DateTimeFormat(iso = ISO.DATE_TIME)
    @ApiModelProperty(value = "마지막 접속시간 ", example = "2020-03-22-22:11", position = 9)
    private LocalDateTime lastAccessTime;

    @ApiModelProperty(value = "createdAt", example = "2020-11-11", position = 12)
    private LocalDateTime createdAt;

    @ApiModelProperty(value = "createdAt", example = "2020-11-11", position = 12)
    private LocalDateTime updateOn;

    @ApiModelProperty(value = "status", example = "status", position = 13)
    private Status status;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "AccountBaseDto.Response")
  public static class Response {

    @ApiModelProperty(value = "ID", example = "1")
    private Long id;

    @ApiModelProperty(value = "타입", example = "CENTER_MANAGER", position = 2, required = true)
    private Account.Type type;

    @ApiModelProperty(value = "아이디", example = "abcd1234", position = 3, required = true)
    private String username;

    @ApiModelProperty(value = "이름", example = "홍길동", position = 4, required = true)
    private String name;

    @ApiModelProperty(value = "이메일주소", example = "abcd1234@naver.com", position = 5, required = true)
    private String email;

    @ApiModelProperty(value = "휴대폰", example = "01012345678", position = 6, required = true)
    private String mobilePhone;

    @ApiModelProperty(value = "통신사", example = "SKT", position = 5, required = true)
    private String mobilePhoneCorp;

    @ApiModelProperty(value = "성별", example = "FEMALE", position = 7, required = true)
    private Gender gender;

    @ApiModelProperty(value = "생년월일", example = "1983-01-14", position = 8, required = true)
    private LocalDate birthday;

    @DateTimeFormat(iso = ISO.DATE_TIME)
    @ApiModelProperty(value = "마지막 접속시간 ", example = "2020-03-22-22:11", position = 9)
    private LocalDateTime lastAccessTime;

    @ApiModelProperty(value = "권한", position = 10)
    private List<Account.Authority> authorities;

    @ApiModelProperty(value = "거주지 주소", example = "서울시 노원구 월계", position = 11)
    private AddressDto.Response address;

    @ApiModelProperty(value = "createdAt", example = "2020-11-11", position = 12)
    private LocalDateTime createdAt;

    @ApiModelProperty(value = "createdAt", example = "2020-11-11", position = 12)
    private LocalDateTime updateOn;

    @ApiModelProperty(value = "status", example = "status", position = 13)
    private Status status;

  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString(callSuper = true)
  public abstract static class Search<T extends Account> extends SearchDto<T> {

    @ApiModelProperty(value = "검색구분")
    private String searchType;

    @ApiModelProperty(value = "검색어")
    private String keyword;

    private Boolean isLeaved;

    @Default
    @ApiModelProperty(value = "계정유형", position = 100)
    private List<Type> types = Lists.newArrayList();

    @ApiModelProperty(value = "조직명(센터 or 기관)", position = 2)
    private String organizationName;

    @ApiModelProperty(value = "조직 아이디", position = 2)
    private Long organizationId;

    @Override
    protected Restrictions generateRestrictions() {
      final Restrictions restrictions = new Restrictions();

      if (!CollectionUtils.isEmpty(this.types)) {
        restrictions.in("type", this.types);
      }

      if (StringUtils.isNotEmpty(organizationName)) {
        restrictions.eq("organization.name", this.organizationName);
      }

      if (organizationId != null && organizationId > 0) {
        restrictions.eq("organization.id", organizationId);
      }

      if (isLeaved != null) {
        restrictions.eq("isLeaved", isLeaved);
      }

      if (StringUtils.isNotEmpty(this.keyword)) {
        if (StringUtils.isEmpty(this.searchType)) {
          final Restrictions restrictionsByKeyword = new Restrictions(Restrictions.Conn.OR);
          List<String> keywords = Arrays.asList(this.keyword.trim().split("\\s+"));
          keywords.stream()
              .forEach(keyword -> {
                restrictionsByKeyword.like("username", "%" + keyword + "%");
                restrictionsByKeyword.like("name", "%" + keyword + "%");
                restrictionsByKeyword.like("mobilePhone", "%" + keyword + "%");
                restrictionsByKeyword.like("email", "%" + keyword + "%");
              });
          restrictions.addChild(restrictionsByKeyword);
        } else {
          restrictions.like(searchType, "%" + keyword + "%");
        }
      }

      return restrictions;
    }

  }

}
