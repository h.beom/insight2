package ai.bankscan.common.email.service;

import ai.bankscan.common.infra.mail.messages.MailMessage;
import ai.bankscan.common.infra.mail.messages.PebbleMailMessage;
import ai.bankscan.configs.properties.AppProperties;
import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.template.PebbleTemplate;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

/**
 * @author eomjeongjae
 * @since 2019-06-26
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class EmailService {

  private final JavaMailSender mailSender;
  private final PebbleEngine pebbleEngine;
  private final AppProperties appProperties;

  @Value("${app.mail-sender}")
  private String from;

  public void sendEmail(MailMessage mailMessage) {
    try {
      if (PebbleMailMessage.class.isAssignableFrom(mailMessage.getClass())) {
        PebbleMailMessage pebbleMailMessage = (PebbleMailMessage) mailMessage;
        pebbleMailMessage.addAttribute("host", appProperties.getUrl());
        PebbleTemplate compiledTemplate = pebbleEngine
            .getTemplate("mails/" + pebbleMailMessage.getTemplateName());

        Writer writer = new StringWriter();
        compiledTemplate.evaluate(writer, pebbleMailMessage.getAttributes());
        String output = writer.toString();

        String contents = generateLink(output);
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        helper.setFrom(from);
        helper.setTo(mailMessage.getTo());
        helper.setSubject(mailMessage.getSubject());
        helper.setText(contents, true);
        mailSender.send(message);
      }
    } catch (Exception e) {
      log.error("Error send mail", e);
    }
  }

  private String generateLink(String output) {
    Map<String, String> map = new HashMap<String, String>();
    Pattern nonValidPattern = Pattern.compile("(src|href)=[\"']?([^>\"']+)[\"']?[^>]*>");
    Matcher matcher = nonValidPattern.matcher(output);
    while (matcher.find()) {
      String finder = matcher.group(0);
      StringBuilder path = new StringBuilder();
      path.append(finder.substring(0, finder.indexOf("/")));
      String host = appProperties.getUrl();

      path.append(host);
      path.append(finder.substring(finder.indexOf("/")));
      map.put(finder, path.toString());
    }

    for (String key : map.keySet()) {
      output = output.replaceAll(key, map.get(key));
    }
    return output;
  }
}
