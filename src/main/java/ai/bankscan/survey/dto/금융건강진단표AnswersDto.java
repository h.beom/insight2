package ai.bankscan.survey.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class 금융건강진단표AnswersDto {

  @Getter
  @RequiredArgsConstructor
  public enum Result {
    부채("부채"),
    소득("소득"),
    저축("저축"),
    지출("지출"),
    기타("기타"),
    모름("모름");

    private final String tile;
  }


//  @Getter
//  @RequiredArgsConstructor
//  public enum ConsultationResultType {
//    소득지출형("소득지출형", Result.소득, Result.지출),
//    소득부채형("소득부채형", Result.소득, Result.부채),
//    소득저축형("소득저축형", Result.소득, Result.저축),
//    부채지출형("부채지출형", Result.부채, Result.지출),
//    부채저축형("부채저축형", Result.부채, Result.저축),
//    지출저축형("지출저축형", Result.지출, Result.저축);
//
//    private final String title;
//    private final Result firstType;
//    private final Result lastType;
//
//    public static String findByType(Result type, Result type2) {
//
//      String result = null;
//   HashMap<ConsultationResultType, String> findType = Stream.of(ConsultationResultType.values())
//          .filter( data -> data.getFirstType().equals(type) && data.getLastType().equals(type2))
//          .collect(Collectors.toMap(Function.identity(), ConsultationResultType::getTitle, (e1, e2) -> e2, LinkedHashMap::new));
//
//      List keys = new ArrayList(findType.keySet());
//      for(int i = 0; i < keys.size(); i++) {
//        result = ((ConsultationResultType) keys.get(i)).title;;
//      }
//
//     return result;
//    }
//  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ApiModel(value = "SurveyAnswersDto.Response")
  public static class AnswerResponse {

    @ApiModelProperty(value = "key", example = "1", position = 1)
    private Long key;

    @ApiModelProperty(value = "답변", example = "저축마", position = 2)
    private String answer;

    @ApiModelProperty(value = "결과", example = "신용", position = 3)
    private String result;
    @ApiModelProperty(value = "2차결과", example = "저축", position = 4)
    private String result2;

    @ApiModelProperty(value = "기타 답변", example = "돈이 없어요", position = 5)
    private String etc;

  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ApiModel(value = "DefaultSurveyDto.Response")
  public static class Response {

    @ApiModelProperty(value = "질문", example = "~~~~다.", position = 1)
    private String question;


    @ApiModelProperty(value = "답변 리스트", example = "1., 2., 3.", position = 2)
    private List<AnswerResponse> answers;
  }

}
