package ai.bankscan.consultations.dto;

import ai.bankscan.accounts.dto.AccountDto;
import ai.bankscan.accounts.dto.MemberDto;
import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.common.infra.jpa.support.Restrictions.Conn;
import ai.bankscan.common.infra.jpa.support.SearchDto;
import ai.bankscan.consultations.domain.consulting.Consultation;
import ai.bankscan.consultations.domain.consulting.Consultation.ConsultWantTime;
import ai.bankscan.consultations.domain.consulting.Consultation.Status;
import ai.bankscan.consultations.domain.consulting.Consultation.Type;
import ai.bankscan.finance.dto.HouseDto;
import ai.bankscan.organizations.domain.Organization;
import ai.bankscan.organizations.dto.CenterDto;
import ai.bankscan.organizations.dto.InstitutionDto;
import ai.bankscan.survey.dto.금융건강진단표Dto;
import ai.bankscan.survey.dto.재무달성계획Dto;
import ai.bankscan.survey.dto.희망노후생활비Dto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConsultationDto {

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "ConsultationDto.Create")
  public static class Create {

    @Default
    @ApiModelProperty(value = "APPLY", notes = "Status")
    private Status status = Status.APPLY;

    @Default
    private Type type = Type.BASIC_CONSULTATION;

    private LocalDate consultWantDate;

    private ConsultWantTime consultWantTime;

    @Default
    private Long surveyId = null;

    @Default
    private Long consultantId = null;

    private Long centerId;

    @Default
    private Long proConsultantId = null;

    @Default
    private Long institutionId = null;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "ConsultationDto.Update")
  public static class Update {
    private Status status;

    private Type type;

    private Long consultantId;

    private Long proConsultantId;

    private Long institutionId;
  }


  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "ConsultationDto.Update2")
  public static class Update2 {
    private Long id;

    private Status status;

    private Type type;

    private Long consultantId;

    private Long proConsultantId;

    private Long institutionId;
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "ConsultationDto.Response")
  public static class Response {

    private Long id;

    @NotNull
    private String code;

    private MemberDto.Simple createdBy;

    private Status status;

    private Type type;

    private LocalDate consultWantDate;

    private ConsultWantTime consultWantTime;

    private AccountDto.Simple consultant;

    private CenterDto.Simple center;

    private List<HouseDto.Response> house;

    private AccountDto.Simple proConsultant;

    private InstitutionDto.Simple institution;

    private 금융건강진단표Dto.Response 금융건강진단표;

    private 재무달성계획Dto.Response 재무달성계획;

    private 희망노후생활비Dto.Response 희망노후생활비;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "ConsultationDto.Simple")
  public static class Simple {

    private Long id;

    @NotNull
    private String code;

    private MemberDto.Simple createdBy;

    private Status status;

    private Type type;

    private LocalDate consultWantDate;

    private ConsultWantTime consultWantTime;

    private CenterDto.Response center;

    private InstitutionDto.Response institution;

    private AccountDto.Simple consultant;

    private AccountDto.Simple proConsultant;

    private 금융건강진단표Dto.Response 금융건강진단표;

    private 재무달성계획Dto.Response 재무달성계획;

    private 희망노후생활비Dto.Response 희망노후생활비;

    private List<HouseDto.Response> house;

  }


  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "ConsultationDto.BasicSimple")
  public static class BasicSimple {

    private Long id;

    @NotNull
    private String code;

    private MemberDto.Simple createdBy;

    private Status status;

    private Type type;

    private LocalDate consultWantDate;

    private ConsultWantTime consultWantTime;

    private CenterDto.Response center;

    private InstitutionDto.Response institution;

    private AccountDto.Simple consultant;

    private List<ConsultContentDto.Response> consultContent;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "ConsultationDto.ProSimple")
  public static class ProSimple {

    private Long id;

    @NotNull
    private String code;

    private MemberDto.Simple createdBy;

    private Status status;

    private Type type;

    private LocalDate consultWantDate;

    private ConsultWantTime consultWantTime;

    private CenterDto.Response center;

    private InstitutionDto.Response institution;

    private AccountDto.Simple proConsultant;

    private List<ConsultContentDto.Response> consultContent;

  }
  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString(callSuper = true)
  @ApiModel(value = "ConsultationDto.Search", description = "periodType: 접수일(createdAt), 완료일(consultDueDate)")
  public static class Search extends SearchDto<Consultation> {

    @ApiModelProperty(value = "검색어")
    private String keyword;

    @ApiModelProperty(value = "검색 구분",
        example = "고객명: memberName, "
            + "상담원명: accountName, "
            + "센터명/기관명: organizationName, "
            + "센터코드/기관코드: organizationCode")
    private String searchType;

    @ApiModelProperty(value = "상담 유형")
    private Type type;

    @ApiModelProperty(value = "상담 진행 상태")
    private Status status;

    @ApiModelProperty(value = "organization Type", hidden = true)
    private Organization.Type organizationType;

    @JsonIgnore
    @ApiModelProperty(value = "조직 ID(센터 or 기관)", hidden = true)
    private Long organizationId;

    @JsonIgnore
    @ApiModelProperty(value = "상담원 ID", hidden = true)
    private Long consultantId;

    @JsonIgnore
    @ApiModelProperty(value = "전문상담원 ID", hidden = true)
    private Long proConsultantId;

    @Override
    protected Restrictions generateRestrictions() {
      final Restrictions restrictions = new Restrictions();
      if(status != null) {
        restrictions.eq("status", this.status);
      }
      if(StringUtils.isNotEmpty(this.keyword)) {
        if(StringUtils.isEmpty(this.searchType)) {
          final Restrictions restrictionsByKeyword = new Restrictions(Restrictions.Conn.OR);
          List<String> keywords = Arrays.asList(this.keyword.trim().split("\\s+"));
//          keywords.stream()
//              .forEach(keyword -> {
//                restrictionsByKeyword.like("consultationInformation.status", "%" + keyword + "%");
//              });
          restrictions.addChild(restrictionsByKeyword);
        } else {
          restrictions.like(searchType, "%" + keyword + "%");
        }
      }

      if (consultantId != null && consultantId > 0) {
        restrictions.eq("consultant.id", consultantId);
      }

      if (proConsultantId != null && proConsultantId > 0) {
        restrictions.eq("proConsultant.id", consultantId);
      }

      if (organizationId != null && organizationId > 0) {
        Restrictions r = new Restrictions(Conn.OR);
        r.eq("institution.id", organizationId);
        r.eq("center.id", organizationId);
        restrictions.addChild(r);
      }

      if(organizationType != null) {
       if(organizationType.equals(Organization.Type.CENTER) ) {
         restrictions.eq("center.type", Organization.Type.CENTER);
       } else {
         restrictions.eq("institution.type", Organization.Type.INSTITUTION);
       }
      }
      return restrictions;
    }
    protected void addSearchKeyword(final Restrictions restrictions, final String keyword) {}

  }

}
