package ai.bankscan.finance.dto;

import com.google.api.client.util.Lists;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Lob;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.sf.json.JSONObject;
import org.springframework.format.annotation.DateTimeFormat;


public class AssetTableDto {

  @Data
  public static class Response {

    private Long total;

    private Long assetsTotal;

    @Lob
    private JSONObject assetsJson;

    private Long liabilitiesTotal;

    @Lob
    private JSONObject liabilitiesJson;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDateTime createdAt;

  }

  @Data
  public static class Output {

    Response data;
    List<Simple> olders = Lists.newArrayList();
  }


  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  public static class Simple {

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDateTime createdAt;
    private Long price;
  }

}
