package ai.bankscan.statistics.controller;


import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import ai.bankscan.statistics.dto.StatisticsDto;
import org.springframework.data.domain.PageRequest;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class StatisticsResourceAssembler implements
    ResourceAssembler<StatisticsDto.Response, Resource<StatisticsDto.Response>> {

  @Override
  public Resource<StatisticsDto.Response> toResource(StatisticsDto.Response entity) {
    return new Resource<>(
        entity,
        linkTo(methodOn(StatisticsRestController.class)
            .getStatisticsFromOrganization(
                StatisticsDto.Search.builder().build(),
                null,
                PageRequest.of(0, 10),
                null))
            .withSelfRel());
  }

}
