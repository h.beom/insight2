package ai.bankscan.consultations.dto;

import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.accounts.dto.MemberDto;
import ai.bankscan.consultations.domain.consulting.Consultation;
import net.sf.json.JSONObject;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ConsultationMapper {

  ConsultationMapper INSTANCE = Mappers.getMapper(ConsultationMapper.class);

  @Mapping(target = "id")
  ConsultationDto.Response consultationToDto(Consultation consultation);

  default String toString(JSONObject obj) {
    return obj == null ? null : obj.toString();
  }

  default JSONObject toJSONObject(String data) {
    return data == null ? null : JSONObject.fromObject(data);
  }

}
