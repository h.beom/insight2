package ai.bankscan.bound.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface BoundRepository extends JpaRepository<Bound, Long>, JpaSpecificationExecutor<Bound> {

}
