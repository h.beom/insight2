package ai.bankscan.home.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("")
public class HomeController {

    @RequestMapping("")
    public String index(ModelMap model) {
        return "index";
    }

    @RequestMapping("/pfm")
    public String pfm(ModelMap model) {
        return "pfm";
    }

    @RequestMapping("/tos")
    public String tos(ModelMap model) {
        return "tos";
    }

    @RequestMapping("/privacy")
    public String privacy(ModelMap model) {
        return "privacy";
    }

    @RequestMapping("/intro")
    public String intro(ModelMap model) { return "intro"; }

}
