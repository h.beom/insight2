package ai.bankscan.finance.document;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Table(indexes = {@Index(columnList = "bank"), @Index(columnList = "계좌번호")})
public class 은행_외화계좌조회 extends AuditableJoinEntity<Account> {

  private String bank;

  private String 계좌번호;
  private String 만기일자;
  private String 신규일자;
  private Double 예금잔액;
  private String 예금종류;
  private String 통화;

}
