package ai.bankscan.finance.document;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface 보험_가입특약사항조회_Repository extends JpaRepository<보험_가입특약사항조회, Long>,
    JpaSpecificationExecutor<보험_가입특약사항조회> {

}
