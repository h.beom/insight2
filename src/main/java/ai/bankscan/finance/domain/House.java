package ai.bankscan.finance.domain;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class House extends AuditableJoinEntity<Account> {

  @Getter
  @RequiredArgsConstructor
  public static enum HouseMode {
    TRADING("매매"),
    LEASE("임차"),
    HIRE("임대");
    private final String title;
  }

  @Enumerated(EnumType.STRING)
  private HouseMode mode;

  private String category;

  private String type;

  private String name;

  private String address;

  private Long price;


}
