package ai.bankscan.organizations.dto;

import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.common.infra.jpa.support.SearchDto;
import ai.bankscan.organizations.domain.Facilities;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FacilitiesDto {

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "FacilitiesDto.Create")
  public static class Create {

    @NotNull
    @ApiModelProperty(value = "센터 ID", example = "1", position = 1, required = true)
    private Long centerId;

    @NotNull
    @ApiModelProperty(value = "년월 ", example = "2020-09", position = 2, required = true)
    private String days;

    @Default
    @ApiModelProperty(value = "임차료", example = "100000", position = 3, required = true)
    private BigDecimal rent = BigDecimal.ZERO;

    @Default
    @ApiModelProperty(value = "관리비", example = "200000", position = 4)
    private BigDecimal maintenanceFee = BigDecimal.ZERO;

    @Default
    @ApiModelProperty(value = "전기세", example = "300000", position = 5)
    private BigDecimal electricityFee = BigDecimal.ZERO;

    @Default
    @ApiModelProperty(value = "수도세", example = "400000", position = 6)
    private BigDecimal waterFee = BigDecimal.ZERO;

    @Default
    @ApiModelProperty(value = "도시가스비", example = "500000", position = 7)
    private BigDecimal cityGasFee = BigDecimal.ZERO;

    @Default
    @ApiModelProperty(value = "유선전화비", example = "600000", position = 8)
    private BigDecimal phoneFee = BigDecimal.ZERO;

    @Default
    @ApiModelProperty(value = "인터넷비", example = "700000", position = 9)
    private BigDecimal internetFee = BigDecimal.ZERO;

    @Default
    @ApiModelProperty(value = "사무용품비", example = "800000", position = 10)
    private BigDecimal officeSuppliesCost = BigDecimal.ZERO;

    @Default
    @ApiModelProperty(value = "기타", example = "900000", position = 11)
    private BigDecimal otherCost = BigDecimal.ZERO;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "FacilitiesDto.Update")
  public static class Update {

    @ApiModelProperty(value = "임차료", example = "100000", position = 4, required = true)
    private BigDecimal rent;

    @ApiModelProperty(value = "관리비", example = "200000", position = 5)
    private BigDecimal maintenanceFee;

    @ApiModelProperty(value = "전기세", example = "300000", position = 6)
    private BigDecimal electricityFee;

    @ApiModelProperty(value = "수도세", example = "400000", position = 7)
    private BigDecimal waterFee;

    @ApiModelProperty(value = "도시가스비", example = "500000", position = 8)
    private BigDecimal cityGasFee;

    @ApiModelProperty(value = "유선전화비", example = "600000", position = 9)
    private BigDecimal phoneFee;

    @ApiModelProperty(value = "인터넷비", example = "700000", position = 10)
    private BigDecimal internetFee;

    @ApiModelProperty(value = "사무용품비", example = "800000", position = 11)
    private BigDecimal officeSuppliesCost;

    @ApiModelProperty(value = "기타", example = "900000", position = 12)
    private BigDecimal otherCost;

  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString
  @ApiModel(value = "FacilitiesDto.Response")
  public static class Response {

    @ApiModelProperty(value = "ID", example = "1")
    private Long id;

    @ApiModelProperty(value = "년도", example = "2020", position = 2, required = true)
    private Integer year;

    @ApiModelProperty(value = "월", example = "3", position = 3, required = true)
    private Integer month;

    @NotNull
    @ApiModelProperty(value = "년월 ", example = "2020-09", position = 4, required = true)
    private LocalDate days;

    @ApiModelProperty(value = "임차료", example = "100000", position = 5, required = true)
    private BigDecimal rent;

    @ApiModelProperty(value = "관리비", example = "200000", position = 6)
    private BigDecimal maintenanceFee;

    @ApiModelProperty(value = "전기세", example = "300000", position = 7)
    private BigDecimal electricityFee;

    @ApiModelProperty(value = "수도세", example = "400000", position = 8)
    private BigDecimal waterFee;

    @ApiModelProperty(value = "도시가스비", example = "500000", position = 9)
    private BigDecimal cityGasFee;

    @ApiModelProperty(value = "유선전화비", example = "600000", position = 10)
    private BigDecimal phoneFee;

    @ApiModelProperty(value = "인터넷비", example = "700000", position = 11)
    private BigDecimal internetFee;

    @ApiModelProperty(value = "사무용품비", example = "800000", position = 12)
    private BigDecimal officeSuppliesCost;

    @ApiModelProperty(value = "기타", example = "900000", position = 13)
    private BigDecimal otherCost;

    @ApiModelProperty(value = "센터", position = 14)
    private CenterDto.Response center;

  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @SuperBuilder
  @ToString(callSuper = true)
  @ApiModel(value = "FacilitiesDto.Search")
  public static class Search extends SearchDto<Facilities> {

    @ApiModelProperty(value = "검색구분", position = 1)
    private String searchType;

    @ApiModelProperty(value = "검색어", position = 2)
    private String keyword;

    @ApiModelProperty(value = "년도", position = 3)
    private Integer year;

    @ApiModelProperty(value = "월", position = 4)
    private Integer month;

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Long centerId;

    @Override
    protected Restrictions generateRestrictions() {
      final Restrictions restrictions = new Restrictions();

      if (!ObjectUtils.isEmpty(this.year)) {
        restrictions.eq("year", this.year);
      }

      if (!ObjectUtils.isEmpty(this.month)) {
        restrictions.eq("month", this.month);
      }

      if (!ObjectUtils.isEmpty(this.centerId)) {
        restrictions.eq("center.id", this.centerId);
      }

      if (StringUtils.isNotEmpty(this.keyword)) {
        if (StringUtils.isEmpty(this.searchType)) {
          final Restrictions restrictionsByKeyword = new Restrictions(Restrictions.Conn.OR);
          List<String> keywords = Arrays.asList(this.keyword.trim().split("\\s+"));
          keywords.stream()
              .forEach(keyword -> {
                restrictionsByKeyword.like("center.name", "%" + keyword + "%");
                restrictionsByKeyword.like("center.code", "%" + keyword + "%");
              });
          restrictions.addChild(restrictionsByKeyword);
        } else {
          restrictions.like(searchType, "%" + keyword + "%");
        }
      }

      return restrictions;
    }
  }

}
