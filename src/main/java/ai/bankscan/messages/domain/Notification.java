package ai.bankscan.messages.domain;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import ai.bankscan.messages.domain.NotificationMessage.Type;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class Notification extends AuditableJoinEntity<Account> {

  @RequiredArgsConstructor
  @Getter
  public enum Status {
    IN_PREPARATION, SUCCESS, FAILURE
  }

  @Default
  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private Type type = Type.MANUAL;

  @Column(nullable = false)
  private String title;

  @Column(length = 280, nullable = false)
  private String body;

  @Column(length = 500)
  private String link;

  @Default
  @Enumerated(EnumType.STRING)
  private Status status = Status.IN_PREPARATION;

  @Setter
  private int successCount;

  @Setter
  private int failureCount;

  private LocalDateTime sentDateTime;

  private LocalDateTime completionDateTime;

  @Lob
  private String errorMsg;

  public void completeToSend(
      int successCount,
      int failureCount
  ) {
    this.successCount = successCount;
    this.failureCount = failureCount;
    this.status = Status.SUCCESS;
    this.sentDateTime = LocalDateTime.now();
  }

  public void failToSend(String errorMsg) {
    this.status = Status.FAILURE;
    this.errorMsg = errorMsg;
    this.sentDateTime = LocalDateTime.now();
  }

}
