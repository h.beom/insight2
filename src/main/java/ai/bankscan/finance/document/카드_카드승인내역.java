package ai.bankscan.finance.document;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Table(indexes = {@Index(columnList = "card"), @Index(columnList = "카드번호"),
    @Index(columnList = "승인번호")})
public class 카드_카드승인내역 extends AuditableJoinEntity<Account> {

  private String card;

  private String 가맹점명;
  private String 가맹점사업자번호;
  private String 가맹점업종;
  private String 가맹점전화번호;
  private String 가맹점주소;
  private String 가맹점코드;
  private String 결제예정일;
  private String 국내외구분;
  private String 매출종류;
  private String 승인금액;
  private String 승인번호;
  private String 승인시간;
  private String 승인일자;
  private String 취소년월일;
  private String 카드번호;
  private String 카드종류;
  private String 통화코드;
  private String 할부기간;
}
