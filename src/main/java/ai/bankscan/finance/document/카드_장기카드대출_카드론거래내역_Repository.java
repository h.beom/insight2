package ai.bankscan.finance.document;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface 카드_장기카드대출_카드론거래내역_Repository extends JpaRepository<카드_장기카드대출_카드론거래내역, Long>,
    JpaSpecificationExecutor<카드_장기카드대출_카드론거래내역> {

  @Query("SELECT COALESCE(SUM(거래원금), 0) FROM 카드_장기카드대출_카드론거래내역 WHERE createdBy.id = :createdById")
  Long get빌린돈_카드대출(@Param("createdById") Long createdById);

}
