package ai.bankscan.files.dto;

import com.google.api.client.util.Lists;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;

/**
 * @author eomjeongjae
 * @since 2019/10/15
 */
public class FileDto {

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @ToString
  @Builder
  public static class Info {

    @Builder.Default
    List<Long> ids = Lists.newArrayList();

  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @ToString
  @Builder
  public static class Download {

    private Response attachFile;

    private Resource resource;

  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @ToString
  @Builder
  @ApiModel("FileDto.Response")
  public static class Response {

    private Long id;

    private String serverPath;

    private String filename;

    private String extension;

    private String contentType;

    private Long size;

    @ApiModelProperty(hidden = true)
    private MediaType mediaType;

    private boolean isImage;

    private String serverFilePath;

    private LocalDateTime createdAt;

  }
}
