package ai.bankscan.finance.dto;

import lombok.Data;

@Data
public class 카드_포인트내역_Dto {

  private String card;

  private String 포인트구분;
  private String 보유포인트;

}
