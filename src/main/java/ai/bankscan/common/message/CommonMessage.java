package ai.bankscan.common.message;

import java.util.Locale;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

@Component
public class CommonMessage {

  @Autowired
  private MessageSource messageSource;

  private MessageSourceAccessor accessor;

  @PostConstruct
  private  void init() {
    accessor = new MessageSourceAccessor(messageSource, Locale.KOREAN);
  }


  public String getMessage(String key) {
    return accessor.getMessage(key, Locale.getDefault());
  }

  public String getMessage(String key, Object[] objects) {
    return accessor.getMessage(key, objects, Locale.getDefault());
  }

}
