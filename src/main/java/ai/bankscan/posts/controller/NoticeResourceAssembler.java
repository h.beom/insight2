package ai.bankscan.posts.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import ai.bankscan.posts.dto.NoticeDto.Response;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
class NoticeResourceAssembler implements ResourceAssembler<Response, Resource<Response>> {

  @Override
  public Resource<Response> toResource(Response entity) {
    return new Resource<>(
        entity,
        linkTo(methodOn(NoticeRestController.class).getPost(entity.getId())).withSelfRel()
    );
  }
}