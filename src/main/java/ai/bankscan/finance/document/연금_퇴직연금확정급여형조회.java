package ai.bankscan.finance.document;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class 연금_퇴직연금확정급여형조회 extends AuditableJoinEntity<Account> {

  private String 상품유형;
  private String 상품명;
  private String 입사연도;
  private String 퇴직예정연도;
  private String 예시연금액;
  private String 가입회사;

}
