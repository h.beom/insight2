package ai.bankscan.finance.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class 은행_펀드계좌조회_Dto {

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class Create {

    private String bank;

    private String 계좌번호;
    private String 만기일자;
    private String 수익률;
    private String 신규일자;
    private String 예금종류;
    private String 투자원금;
    private String 평가금액;
  }


  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class GroupBy {

    private String bank;
    private String date;
    private String 계좌번호;
  }

}
