package ai.bankscan.finance.document;

import ai.bankscan.accounts.domain.Account;
import ai.bankscan.common.infra.jpa.domain.AuditableJoinEntity;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
@Table(indexes = {@Index(columnList = "bank"), @Index(columnList = "계좌번호"),
    @Index(columnList = "거래일자"), @Index(columnList = "대출잔액")})
public class 은행_대출거래내역조회 extends AuditableJoinEntity<Account> {

  private String bank;
  private String 계좌번호;
  private String 거래일자;
  private String 거래구분;
  private String 거래금액;
  private String 거래원금;
  private String 대출잔액;
  private String 대출이율;
  private String 이자계산시작일;
  private String 이자계산종료일;
  private String 이자계산기간;
  private String 이자금액;
  private String 이자상세1;
  private String 이자상세2;
  private String 이자상세3;
  private String 이자상세4;
  private String 이자상세5;
  private String 비고1;
  private String 비고2;
  private String 계좌번호확장1;


}
