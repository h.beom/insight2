package ai.bankscan.survey.service;

import ai.bankscan.accounts.domain.member.Member;
import ai.bankscan.certification.service.CertificationService;
import ai.bankscan.common.infra.jpa.support.Restrictions;
import ai.bankscan.survey.domain.금융건강진단종합;
import ai.bankscan.survey.domain.금융건강진단종합.Type;
import ai.bankscan.survey.domain.금융건강진단종합Repository;
import ai.bankscan.survey.domain.금융건강진단표;
import ai.bankscan.survey.domain.금융건강진단표.Status;
import ai.bankscan.survey.domain.금융건강진단표Repository;
import ai.bankscan.survey.dto.금융건강진단종합Dto;
import ai.bankscan.survey.dto.금융건강진단표AnswersDto;
import ai.bankscan.survey.dto.금융건강진단표AnswersDto.Result;
import ai.bankscan.survey.dto.금융건강진단표Dto;
import ai.bankscan.survey.dto.금융건강진단표Dto.Response;
import com.google.api.client.util.Lists;

import java.util.*;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Transactional
@Service
public class 금융건강진단표Service {

  private final ModelMapper modelMapper;

  private final 금융건강진단표Repository 금융건강진단표Repository;

  private final 금융건강진단종합Repository 금융건강진단종합Repository;

  private final CertificationService certificationService;

  private final EntityManager entityManager;

  public List<Response> getMyResult(Member member, Long limit) {

    if (limit != null && limit > 0) {
      PageRequest pageRequest = PageRequest.of(0, limit.intValue(), Sort.by(Direction.DESC, "id"));
      Page<금융건강진단표> result = 금융건강진단표Repository.findAllByCreatedBy(member, pageRequest);

      return result.getContent().stream().map(
          this::toResponse
      ).collect(Collectors.toList());
    } else {
      List<금융건강진단표> result = 금융건강진단표Repository.findAllByCreatedBy(member);
      return result.stream().map(
          this::toResponse
      ).collect(Collectors.toList());
    }
  }


  public 금융건강진단표Dto.Response responseResult(Long id) {

    Optional<금융건강진단표> oitem = 금융건강진단표Repository.findFirstByCreatedByIdAndStatusOrderByCreatedAtDesc(id, Status.ACTIVE);
    금융건강진단표 item = oitem.orElse(null);
    if (item != null) {
      List<금융건강진단종합> dataList2 = 금융건강진단종합Repository.findAllBy금융건강진단표(item);

      JSONObject jsonObject = new JSONObject();
      JSONObject jsonObject1 = new JSONObject();
      JSONObject jsonObject2 = new JSONObject();
      JSONObject jsonObject3 = new JSONObject();
      JSONObject jsonObject4 = new JSONObject();
      JSONObject jsonObject5 = new JSONObject();

      jsonObject1.put("등급", item.get내복지찾기());
      jsonObject2.put("등급", item.get내살집마련());
      jsonObject3.put("등급", item.get철저한신용관리());
      jsonObject4.put("등급", item.get행복한노후준비());
      jsonObject5.put("등급", item.get현명한금융거래());

      for (int i = 0; i < dataList2.size(); i++) {
        if (Type.내복지찾기.equals(dataList2.get(i).getType())) {
          jsonObject1.put(dataList2.get(i).getTitle(), dataList2.get(i).getResult());
        }
        if (Type.내살집마련.equals(dataList2.get(i).getType())) {
          jsonObject2.put(dataList2.get(i).getTitle(), dataList2.get(i).getResult());
        }
        if (Type.철저한신용관리.equals(dataList2.get(i).getType())) {
          jsonObject3.put(dataList2.get(i).getTitle(), dataList2.get(i).getResult());
        }
        if (Type.행복한노후준비.equals(dataList2.get(i).getType())) {
          jsonObject4.put(dataList2.get(i).getTitle(), dataList2.get(i).getResult());
        }
        if (Type.현명한금융거래.equals(dataList2.get(i).getType())) {
          jsonObject5.put(dataList2.get(i).getTitle(), dataList2.get(i).getResult());
        }
      }
      jsonObject.put(Type.현명한금융거래, jsonObject5);
      jsonObject.put(Type.행복한노후준비, jsonObject4);
      jsonObject.put(Type.철저한신용관리, jsonObject3);
      jsonObject.put(Type.내살집마련, jsonObject2);
      jsonObject.put(Type.내복지찾기, jsonObject1);

      금융건강진단표Dto.Response dto = modelMapper.map(item, 금융건강진단표Dto.Response.class);
      dto.setFinanceResult2(JSONObject.fromObject(item.getFinanceResult2()));

      System.out.println("-----");
      System.out.println("-----");
      System.out.println("-----");
      System.out.println(item.getFinanceResult2());
      System.out.println(dto.getFinanceResult2());
      System.out.println("-----");
      System.out.println("-----");
      System.out.println("-----");

      dto.setFinanceResult(jsonObject);

      return dto;
    }
    return null;

  }


  public List<금융건강진단표Dto.Response> responseHistory(Member member) {
    List<금융건강진단표> dataList = 금융건강진단표Repository.findAllByCreatedByAndStatus(member, 금융건강진단표.Status.ACTIVE);
    List<금융건강진단표Dto.Response> result = null;
    if (dataList.size() > 0) {
    result = dataList.stream().map(data -> {
        List<금융건강진단종합> dataList2 = 금융건강진단종합Repository.findAllBy금융건강진단표(data);
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObject1 = new JSONObject();
        JSONObject jsonObject2 = new JSONObject();
        JSONObject jsonObject3 = new JSONObject();
        JSONObject jsonObject4 = new JSONObject();
        JSONObject jsonObject5 = new JSONObject();

        jsonObject1.put("등급", dataList.get(0).get내복지찾기());
        jsonObject2.put("등급", dataList.get(0).get내살집마련());
        jsonObject3.put("등급", dataList.get(0).get철저한신용관리());
        jsonObject4.put("등급", dataList.get(0).get행복한노후준비());
        jsonObject5.put("등급", dataList.get(0).get현명한금융거래());
        for(int i = 0; i< dataList2.size(); i++) {
          if (Type.내복지찾기.equals(dataList2.get(i).getType())) {
            jsonObject1.put(dataList2.get(i).getTitle(), dataList2.get(i).getResult());
          }
          if (Type.내살집마련.equals(dataList2.get(i).getType())) {
            jsonObject2.put(dataList2.get(i).getTitle(), dataList2.get(i).getResult());
          }
          if (Type.철저한신용관리.equals(dataList2.get(i).getType())) {
            jsonObject3.put(dataList2.get(i).getTitle(), dataList2.get(i).getResult());
          }
          if (Type.행복한노후준비.equals(dataList2.get(i).getType())) {
            jsonObject4.put(dataList2.get(i).getTitle(), dataList2.get(i).getResult());
          }
          if (Type.현명한금융거래.equals(dataList2.get(i).getType())) {
            jsonObject5.put(dataList2.get(i).getTitle(), dataList2.get(i).getResult());
          }
        }
        jsonObject.put(Type.현명한금융거래, jsonObject5);
        jsonObject.put(Type.행복한노후준비, jsonObject4);
        jsonObject.put(Type.철저한신용관리, jsonObject3);
        jsonObject.put(Type.내살집마련, jsonObject2);
        jsonObject.put(Type.내복지찾기, jsonObject1);
        금융건강진단표Dto.Response dto = modelMapper.map(data, 금융건강진단표Dto.Response.class);
        dto.setFinanceResult(jsonObject);
        return dto;
      }).collect(Collectors.toList());
    }
    return result;
  }

  private 금융건강진단종합Dto.Response toResponse2(금융건강진단종합 test) {
    return modelMapper.map(test, 금융건강진단종합Dto.Response.class);
  }


  public Response toResponseResult(List<금융건강진단표AnswersDto.Response> result) {

    List<String> responseAnswersTypeList = Lists.newArrayList();

    int 지출 = 0;
    int 부채 = 0;
    int 저축 = 0;
    int 소득 = 0;
    int 기타 = 0;
    int 모름 = 0;

    for (int i = 0; i < result.size(); i++) {
      for (int j = 0; j < result.get(i).getAnswers().size(); j++) {
        responseAnswersTypeList.add(result.get(i).getAnswers().get(j).getResult());
      }
    }

    for (int i = 0; i < responseAnswersTypeList.size(); i++) {
      switch (responseAnswersTypeList.get(i)) {
        case "부채":
          부채 += 1;
          break;
        case "소득":
          소득 += 1;
          break;
        case "저축":
          저축 += 1;
          break;
        case "지출":
          지출 += 1;
          break;
        case "기타":
          기타 += 1;
        default:
          모름 += 1;
          break;
      }
    }

    Map<Result, Integer> resultData = new HashMap<>();
    resultData.put(Result.부채, 부채);
    resultData.put(Result.소득, 소득);
    resultData.put(Result.저축, 저축);
    resultData.put(Result.지출, 지출);
    resultData.put(Result.기타, 기타);
    resultData.put(Result.모름, 모름);

    Object[] a = resultData.entrySet().toArray();

    Arrays.sort(a,
        new Comparator<Object>() {
          @Override
          public int compare(Object o1, Object o2) {
            return ((Map.Entry<Result, Integer>) o2).getValue()
                .compareTo(((Map.Entry<Result, Integer>) o1).getValue());
          }
        });

    String resultType = null;

    resultType =
        ((Map.Entry<Result, Integer>) a[0]).getKey() + "원인 " + ((Map.Entry<Result, Integer>) a[1])
            .getKey() + "결과 형";

    if (((Map.Entry<Result, Integer>) a[1]).getKey().equals(Result.기타) ||
        ((Map.Entry<Result, Integer>) a[1]).getKey().equals(Result.모름)) {
      // 첫번째 값이 기타 또는 모름 일경우 1번 문항의 답을 가져옴
      resultType = ((Map.Entry<Result, Integer>) a[0]).getKey() + "문제 형";
    }
    if (((Map.Entry<Result, Integer>) a[0]).getKey().equals(Result.기타) ||
        ((Map.Entry<Result, Integer>) a[0]).getKey().equals(Result.모름)) {
      resultType = ((Map.Entry<Result, Integer>) a[1]).getKey() + "문제 형";
    }
    if (((Map.Entry<Result, Integer>) a[0]).getKey().equals(Result.기타) &&
        ((Map.Entry<Result, Integer>) a[1]).getValue() == 0) {
      // 기타 그리고 1번의 값이 0 일 경우 기타 문제형.
      resultType = "기타 문제 형";
    }
    if (((Map.Entry<Result, Integer>) a[0]).getKey().equals(Result.모름) &&
        ((Map.Entry<Result, Integer>) a[1]).getValue() == 0) {
      resultType = "금융 회피 형";
    }

    HashMap<String, Object> returnData = new HashMap<>();

    returnData.put("answerList", result);
    returnData.put("result", resultType);
    returnData.put("firstType", ((Map.Entry<Result, Integer>) a[0]).getKey());
    returnData.put("lastType", ((Map.Entry<Result, Integer>) a[1]).getKey());

    금융건강진단표Dto.Create defaultSurveyDto = 금융건강진단표Dto.Create
        .builder()
        .answerList(result)
        .result(resultType == null ? "금융 회피 형" : resultType)
        .firstType(((Map.Entry<Result, Integer>) a[0]).getKey())
        .lastType(((Map.Entry<Result, Integer>) a[1]).getKey())
        .build();

    금융건강진단표 금융건강진단표 = 금융건강진단표Repository
        .save(modelMapper.map(defaultSurveyDto, 금융건강진단표.class));

    return toResponse(금융건강진단표);
  }

  private 금융건강진단표Dto.Response toResponse(금융건강진단표 금융건강진단표) {
    return modelMapper.map(금융건강진단표, 금융건강진단표Dto.Response.class);
  }


  public 금융건강진단표 getLast(Member member) {

    Restrictions r = new Restrictions();
    r.eq("createdBy", member);

    Page<금융건강진단표> pages = 금융건강진단표Repository
        .findAll(r.output(), PageRequest.of(0, 1, Sort.by(Direction.DESC, "id")));
    if (pages.getTotalElements() > 0) {
      return pages.getContent().get(0);
    } else {
      return null;
    }

  }
}
